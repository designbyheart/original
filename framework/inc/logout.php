<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/14/12
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
require_once("../lib/setup.php");
$session->logout();
$message = "Uspešno ste izlogovani.";
$session->message($message);
// Unset all of the session variables.
$_SESSION = array();
// If it's desired to kill the session, also delete the session cookie.
// Note: This will destroy the session, and not just the session data!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finally, destroy the session.
session_destroy();
redirect_to(SITE_ROOT);
