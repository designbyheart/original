<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/10/12
 * Time: 3:40 AM
 * To change this template use File | Settings | File Templates.
 */

//cleaning function fro POST and GET content
// cleaning function
// of course. the script must be connected to a database.
function clean($data)
{
    if (is_string($data)) {
        return strip_tags(mysql_real_escape_string(htmlentities($data)));
    }
}

// start filtering $_POST, $_GET and secure any data within
$data = array_merge($_POST, $_GET);
foreach ($data AS $key => $val) {
    $data[$key] = clean($val);
}

if (!isset($_GET['page'])) {
    $page = 'home';
} else {
    $page = $_GET['page'];
    if ($page == '') {
        $page = 'home';
    }
}
$session->page = $page;

//locking pages for not logged in users

//if(!isset($_SESSION['clientID']) || $_SESSION['clientID']==0){
//  redirect_to(SITE_ROOT."login");
//}


//setting user variable for loged user
if (!isset($_SESSION['clientID'])) {
    $_SESSION['clientID'] = "0";
}

if (isset($session->client_id) && $session->client_id != '' || isset($_SESSION['clientID'])) {
    $client = Customer::find_by_id($_SESSION['clientID']);
} else {
    $client = NULL;
    unset($_SESSION['clientID']);
    $session->client_id = 0;
}

switch ($page) {
    case "cart":
    case "myAccount":
    case "sendOrder":
        if (!$client && !isset($session->client_id)) {
            redirect_to(SITE_ROOT . 'login');
        }
        break;
}
//language settings //init
if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = "sr";
} else {
    if (isset($_GET['lang'])) {
        switch ($_GET['lang']) {
            case "english":
                $_SESSION['lang'] = 'en';
                redirect_to(str_replace('/english/', '', SITE_ROOT . $_SERVER['REQUEST_URI']));
                return;
                break;
            case 'srpski':
                $_SESSION['lang'] = "sr";
                redirect_to(str_replace('/srpski/', '', SITE_ROOT . $_SERVER['REQUEST_URI']));
                return;
                break;
        }
    }
}

$key = '';
$desc = '';
$title = 'Original';
// $cache = new CacheBlocks(ROOT_DIR.'cache/', 60*60*24);
// $cacheID =str_replace('/', '_', $_SERVER['REQUEST_URI']);

if (VisitLog::isUnique($page)) {
    $visit = new VisitLog();
    $visit->ipAddress = $_SERVER['REMOTE_ADDR'];
    if ($client) {
        $visit->user = $client->id;
    }
    $visit->time = time();
    if ($page == 'product') {
        $pr = explode('/', $_GET['id']);
        $visit->productID = $pr[0];
    }
    $visit->page = $page;
    $visit->save();
}

switch ($page) {

    case 'sr':
        $_SESSION['lang'] = 'sr';
        ;
        if ($_GET['destPage'] == 'home' || $_GET['destPage'] == '/home' || $_GET['destPage'] == '/') {
            redirect_to(SITE_ROOT);
        }
        redirect_to(SITE_ROOT . $_GET['destPage']);
        break;
    case 'en':
        $_SESSION['lang'] = 'en';
        if ($_GET['destPage'] == 'home' || $_GET['destPage'] == '/home' || $_GET['destPage'] == '/') {
            redirect_to(SITE_ROOT);
        }
        redirect_to(SITE_ROOT . $_GET['destPage']);
        break;
    case $page:
        // if(!$cache->Start($cacheID)){
//        $pR = GeneralPage::find_by_name($page);
//
//        if(!$pR && 'product'){
//            $pR = Product::find_by_id($_GET['id']);
//            print_r($pR);
//        }
        if ($page != 'products' && $page != 'product') {
            $gPage = GeneralPage::find_by_name($page);
            if ($gPage) {
                if (trans($gPage->seo_title_sr, $gPage->seo_title_en)) {
                    $title = trans($gPage->seo_title_sr, $gPage->seo_title_en);
                }
                if (trans($gPage->seo_keywords_sr, $gPage->seo_keywords_en) != '') {
                    $key = trans($gPage->seo_keywords_sr, $gPage->seo_keywords_en);
                }
                if (trans($gPage->seo_desc_sr, $gPage->seo_desc_en) != '') {
                    $desc = trans($gPage->seo_desc_sr, $gPage->seo_desc_en);
                }
            }
        }
        if ($page != 'sr' && $page != 'en') {
            require_once(INC . "header.php");
        }
        require_once MOD . $page . ".php";
        require_once VIEW . $page . ".php";
        // }
        // echo $cache->Stop();
        break;
}

require_once(INC . "footer.php");
