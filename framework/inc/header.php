<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/10/12
 * Time: 3:23 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>

    <!-- Basic Page Needs
   ================================================== -->
    <meta charset="utf-8">
    <title><?=$title?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
   ================================================== -->
    <link href="<?=CSS?>main.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href="<?=CSS?>print.css" media="print" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?=CSS?>jquery.coolfieldset.css"/>
    <link rel="stylesheet" type="text/css" href="<?=CSS?>jquery.coolautosuggest.css"/>
    <!--[if IE]>
    <link href="<?=CSS?>ie.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
     ================================================== -->
    <link rel="shortcut icon" href="<?=IMG?>images/favicon.ico">
    <link rel="apple-touch-icon" href="<?=IMG?>images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=IMG?>images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=IMG?>images/apple-touch-icon-114x114.png">
</head>
<body>
<section class="container">
    <section class="sixteen columns header ">
        <a href="<?=SITE_ROOT?>" class="eleven columns logo">
            <img src="<?=IMG?>original-logo.png">
        </a>
        <nav class="langMenu">
            <?php if ($client) { ?>
            <p><span class="welcome"><?= trans('Dobrodošli', 'Welcome') ?>,</span> <a class="brdRight"
                                                                                      href="<?=SITE_ROOT?>my-account"><?=$client->full_name();?>
                <?php //trans('Vaš nalog', 'Your account')?></a>
            <a href="<?=SITE_ROOT?>cart"
               class="basketInfo brdRight"><?php require_once(MOD . 'addProductToBasket.php')?></a>
            <a href="<?=SITE_ROOT?>logout" class="brdRight">Logout</a>
            <?php } else { ?>

            <a href="<?=SITE_ROOT?>login" class='brdRight'>Login</a>
            <a href="<?=SITE_ROOT?>registration" class="brdRight"><?=trans('Registracija', 'Register')?></a>
            <?php
        }
            if ($_SESSION['lang'] != 'sr') {
                ?>
                <a href="<?=SITE_ROOT?>sr<?=str_replace('~origb2b/', '', $_SERVER['REQUEST_URI'])?>">
                    <img src="<?=IMG?>srp.png" alt="Srpski">
                </a>
                <?php
            }
            if ($_SESSION['lang'] != 'en') {
                ?>
                <a href="<?=SITE_ROOT?>en<?=str_replace('~origb2b/', '', $_SERVER['REQUEST_URI'])?>">
                    <img src="<?=IMG?>eng.png" alt="Engleski">
                </a>
                <?php } ?>

        </p>
        </nav>
        <nav class="subMenu">
            <a href="<?=SITE_ROOT?>company"><?=trans('Kompanija', 'Company')?></a>
            <a href="<?=SITE_ROOT?>partners"><?=trans('Partneri', 'Partners')?></a>
            <a href="<?=SITE_ROOT?>napredna-pretraga"><?=trans('Napredna pretraga', 'Advanced search')?></a>

            <form action="<?=SITE_ROOT?>search" method="post">
                <input type="text" name="searchField" id="searchField" autocomplete="off">
                <input type="submit" class="srchBttn" name="startSearch">
            </form>
        </nav>
        <nav class="sixteen columns mainMenu">
            <?php
            $mainMenu = MainMenu::find_by_type('main');
//            print_r($mainMenu);
            if (count($mainMenu) > 1) {
                foreach ($mainMenu as $m) {
                    if ($m->url == '' && (strpos($m->name_sr, "naslovna")) == false) {
                        ?>
                  <a href="<?=SITE_ROOT?>"><img src="<?=IMG?>homeIconLight.png"
                                                alt="<?=trans($m->name_sr, $m->name_en)?>">
                        <?php
                    } else {
                        $link = str_replace('//', '', str_replace('http://', '', SITE_ROOT));
                        ?>
                        <a href="<?=SITE_ROOT . str_replace($link, '', $m->url)?>"><?=trans($m->name_sr, $m->name_en)?></a>
                        <?php
                    }
                }
            }?>
        </a>
            <?php if (IS_SERVER == 'yes') {
            $sp = '&';
        } else {
            $sp = "/";
        }?>
        </nav>
    </section>