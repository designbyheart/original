<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/29/12
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../lib/setup.php');
$mail = new Mail();
$mail->title = "Nova poruka - Original.com";
if (isset($_POST['orderID'])) {
    $mail->title = '[Porudžbina:' . $_POST["orderID"] . ']' . ' Nova poruka';
}

if (isset($_POST['email'])) {
    $mail->email = trim($_POST['email']);
}

if (isset($_POST['phone'])) {
    $mail->phone = trim($_POST['phone']);
}
$mail->receiver = 'desingbyheart@gmail.com';
$mail->messageContent = '';
if (isset($_POST['ticketType'])) {
    $mail->messageContent = "Tip tiketa: " . TicketsCategory::getTicketType($_POST['ticketType']) . '<br>';
}
$mail->messageContent .= trim(strip_tags($_POST['messageText']));

if (isset($_POST['name'])) {
    $mail->name = trim($_POST['name']);
}
if (isset($_POST['orderID'])) {
    $mail->orderID = $_POST['orderID'];
}

$mail->status = 0;
$mail->customer = isCLientLogin();

$mail->type = $_POST['messageType'];

/*
 * 1. order to support
 * 2. profile to support
 * 3. message from contact page
 */

$prevPage = 'kontakt';
if (isset($_POST['messageType'])) {
    switch ($_POST['messageType']) {
        case 'contactSupport':
            $prevPage = 'contact-support';
            break;
        case 'orderTicket':
            $prevPage = 'my-account';
            break;
        case 'contactSupportProfile':
            $prevPage = 'contact-support';
            break;
    }

}

$unsubscribe = '';
$reason = '';
$mail->date = date("Y-m-d", time());

if ($mail->save()) {
    $mail->sendingMessage(SITE_ROOT . $prevPage . '/' . trans('poruka-poslata', 'message-sent'), SITE_ROOT . $prevPage . '/' . trans('poruka-nije-poslata', 'message-not-sent'));
}
