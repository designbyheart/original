<link href="../../assets/js/counter/jquery.counter.css" media="screen" rel="stylesheet" type="text/css"/>
<script src="../../assets/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="../../assets/js/counter/jquery.counter.js" type="text/javascript"></script>
<?php
$dataStop = 0;
$dayInterval = 1000;
$direction = 'down';

if (isset($endDate)) {
    $dataStop = $endDate - time();
    if (time() > $endDate) {
        $direction = 'up';
        $dataStop = $endDate + time();
    }
} ?>

<p style="width:15em;line-height: 4em;padding-left:3em;">
    <span class="counter" data-direction="<?=$direction?>" data-format="<?=date('d-m-y  h-i-s', $endDate); ?>"
          data-stop="<?=$dataStop?>"
          data-interval="<?=$dayInterval?>"><?=date('d-m-y  h-m-s', $endDate); ?></span>
</p>

<script>
    $('.counter').counter();
</script>
