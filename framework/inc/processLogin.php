<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/5/12
 * Time: 1:28 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../lib/setup.php');
if(!isset($_POST['email']) &&  !isset($_POST['password'])){
    $session->message(trans("Morate uneti e-mail adresu i lozinku. Pokušajte ponovo.", "You need to enter email adress and password. Please try again.")) ;
    redirect_to(SITE_ROOT."login");
}
$username = mysql_real_escape_string($_POST['email']);
$pass = md5(trim($_POST['email'].'--'.$_POST['password']));

$client=Customer::authenticate($username, $pass);
if($client){
    $session->client_id = $client->id;
    $_SESSION['clientID'] = $client->id;
    redirect_to(SITE_ROOT);
}else{
    $session->message(trans("E-mail adresa ili lozinka su netačni. Pokušajte ponovo.", "Email adress or password are incorect. Please try again.")) ;
    redirect_to(SITE_ROOT."login");
}