<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/5/12
 * Time: 1:28 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../lib/setup.php');
$customer  =new Customer();
if(!isset($_POST['email']) && count(trim($_POST['email']))>0 &&  !isset($_POST['password'])){
    $session->message(trans("Morate uneti e-mail adresu i lozinku. Pokušajte ponovo.", "You need to enter email adress and password. Please try again.")) ;
    redirect_to(SITE_ROOT."registration");
}
if(!Customer::isEmailFree($_POST['email'])){
    $session->message(trans("Ova email adresa je već registrovana. Unesite drugu email adresu ili se ulogujte <a href='".SITE_ROOT."login' style='color:#222e5c; font-weight:700'>ovde</a>.", "This email address is already registered. Please enter another email address or login <a href='".SITE_ROOT."registration' style='color:#222e5c; font-weight:700'>here</a>.")) ;
    redirect_to(SITE_ROOT."registration");
}
if(!checkEmail($_POST['email'])){
    $session->message(trans("Email adresa nije ispravna. Pokušajte ponovo.", "Email address is not correct. Please try again.")) ;
    redirect_to(SITE_ROOT."registration");
}else{
$username = mysql_real_escape_string($_POST['email']);
$pass = md5(trim(mysql_real_escape_string($_POST['email'].'--'.$_POST['password'])));


    $customer->name = $_POST['name'];
    $customer->company = $_POST['company'];
    $customer->username = $_POST['email'];
    $customer->password = $_POST['password'];
    $customer->type = 1;
    $customer->registered_date = date('d.m.Y. H.i.s',time());
    $customer->auth_code = md5(uniqid(rand()));
    $customer->address = $_POST['address'];
    $customer->phones = $_POST['phone'];
    $customer->email = $_POST['email'];
    $customer->company_PIB = $_POST['pib'];
    $customer->city = $_POST['city'];
    $customer->state = $_POST['country'];
    $customer->ip_address = $_SERVER['REMOTE_ADDR'];

    if($customer->save()){
        //send email
        $mail= new Mail();
        $mail->receiver = $customer->email;
        $mail->date = date("Y-m-d H:i:s", time());
        $mail->phone = $customer->phones;
        $mail->name  = "Registracija novog korisnika";
        $mail->title = trans('Original.doo - Uspešna registracija', 'Original - Registration successfull');
            $mail->messageContent = trans('Poštovani '.$customer->name.',<br>
            Uspešno ste započeli proces registracije e-maila. Da biste završili registraciju, kliknite na link:<br>
            <a href="'.SITE_ROOT.'activateEmail/'.$customer->auth_code.'">'.SITE_ROOT.'activateEmail/'.$customer->auth_code.'</a><br><br>Hvala na poverenju, <br>Original tim', 'Dear '.$customer->name.',<br>
            You\'ve successfully started the registration process. To complete the registration, click on the link:<br>
            <a href="'.SITE_ROOT.'activateEmail/'.$customer->auth_code.'">'.SITE_ROOT.'activateEmail/'.$customer->auth_code.'</a>Thank you for your trust, <br>Original team');

        $mail->sendNoSave(SITE_ROOT.'info/registrationDone', SITE_ROOT.'info/registrationError');
    }
}