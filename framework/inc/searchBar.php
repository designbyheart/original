<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 9:12 AM
 * To change this template use File | Settings | File Templates.
 */
$categories = Category::find_main();
?>

<section id="advancedSearch">
    <form action="<?=SITE_ROOT?>napredna-pretraga" class="adSearchForm" method="post">
        <h4><?=trans('Napredna pretraga', 'Advanced search')?></h4>
        <article>
            <h5><a href="#" id="categories"><?=trans('Kategorija', 'Category')?></a></h5>
            <ul class="categories">
                <?php foreach ($categories as $c): if (trans($c->name_sr, $c->name_en) != '') { ?>
                <li><input type="checkbox" name="category[<?=$c->id?>]"
                           value="<?=$c->id?>"><?=trans($c->name_sr, $c->name_en)?></li>
                <?php } endforeach; ?>
            </ul>
        </article>
        <article>
            <h5><a href="#" id="manufacturesList"><?=trans('Proizvođači', 'Manufacturers')?></a></h5>
            <ul class="manufacturesList">
                <?php $partners = Manufacturer::find_active();
                foreach ($partners as $pR):?>
                    <li><input type="checkbox" name="partner[<?=$pR->id?>]"
                               value="<?=$pR->id?>"><span><?=trans($pR->name_sr, $pR->name_en)?></span></li>
                    <?php endforeach;
                ?>
            </ul>
        </article>
        <?php if($client){ ?>
        <article>
            <h5>
                <a href="#" id="priceList"><?=trans('Cena', 'Price')?></a>
            </h5>
            <ul class="priceList">
                <li style="margin-top:.4em"><?=trans('Po ceni od:', 'With price from:')?> <input type="text" name="from" id="from"></li>
                <li><?=trans('Do:', 'To:')?> <input type="text" name="to" id="to"></li>
            </ul>
        </article>
            <?php } ?>
        <article>
          <?php /*; ?>  <h5>
                <a href="#" id="characteristicsList"><?=trans('Karakteristike', 'Characteristics')?></a>
            </h5>
            <?php
            $chs = new Characteristics();
            ?>
            <ul class="characteristicsList">
                <?php foreach ($chs as $key => $value):
                if (translateField($key) != '') {
                    ?>
                    <li><input type="checkbox" name="characteristic[]" value="<?=$key?>"><span><?=translateField($key)?> </span></li>
                    <?php } endforeach; ?>

            </ul> */ ?>
        </article>
        <!--        <article>-->
        <!--            <h5>-->
        <!--                <a href="#" id="characteristicsList">Cena</a>-->
        <!--            </h5>-->
        <!--            --><?php
//            $chs = new Characteristics();
//            ?>
        <!--            <ul class="characteristicsList">-->
        <!--                --><?php //foreach($chs as $key=>$value):
//                if(translateField($key)!=''){
//                    ?>
        <!--                    <li><input type="checkbox" name="--><?//=$key?><!--">-->
        <?//=translateField($key)?><!-- </li>-->
        <!--                    --><?php //} endforeach; ?>
        <!---->
        <!--            </ul>-->
        <!--        </article>-->

        <button class="bttn searchBttn" type="submit"><?=trans('Pretraži', "Search")?></button>
    </form>
</section>