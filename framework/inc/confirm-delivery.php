<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 10:30 PM
 * To change this template use File | Settings | File Templates.
 */

require_once "../lib/setup.php";
$order = Order::find_by_orderID(alphaID((int)$_GET['orderID']));

if($order){
    $order->status = 8;
    $order->modified =time();
    $order->save();
    $session->message('<p style="font-size:1.2em;position:relative; top:-20px;">Porudžbina br.<strong>'.alphaID((int)$_GET['orderID']).'</strong> je isporučena</p>', '<p style="position:relative; font-size:1.2em; top:-20px;">Order id:<strong>'.alphaID((int)$_GET['orderID']).'</strong> is delivered</p>');
    redirect_to(SITE_ROOT.'my-account');
}else{
   redirect_to(SITE_ROOT.'order-info/'.$_GET['orderID']);
}
