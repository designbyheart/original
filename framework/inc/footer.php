<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/10/12
 * Time: 3:23 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns">
    <input class="siteURL" type="hidden" value="<?=SITE_ROOT?>">
    <section class="footer">
        <section class="four columns">
            <h3>Info</h3>
            <?php $links = MainMenu::find_by_type('footer');
            if ($links && count($links) > 0) {
                foreach ($links as $link) {
                    if ($link->columnT == 0) {
                        ?>
                        <a href="<?=SITE_ROOT . $link->url?>"><?=trans($link->name_sr, $link->name_en)?></a>
                        <?php
                    }
                }
            } ?>
        </section>
        <section class="four columns">
            <h3><?=trans('Partneri', 'Partners')?></h3>
            <?php
            if ($links && count($links) > 0) {
                foreach ($links as $link) {
                    if ($link->columnT == 1) {
                        ?>
                        <a href="<?=SITE_ROOT . $link->url?>"><?=trans($link->name_sr, $link->name_en)?></a>
                        <?php
                    }
                }
            } ?>
        </section>
        <section class="four columns">
            <img src="<?=IMG?>original-blue.png" alt="Original">

            <p class="copy">2012 Original d.o.o. <?=trans('Sva prava zadržana.', 'All rights reserved.')?></p>
        </section>
        <section class="four columns">
            <h3><?=trans('Kontakt', 'Contact')?></h3>

            <p>Antifašističke borbe 21ž <br>
                11070 Novi Beograd <br>
                tel./fax: +381 (0)11 314 84 34<br>
                tel./fax: 14 84 35, 314 84 36<br>
                e-mail: office@originalgrupa.com</p>
        </section>
    </section>
</section>
</section>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='<?=SITE_ROOT?>assets/js/jquery-1.7.1.min.js'>\x3C/script>")</script>

<?php if ($page == 'home') { ?>
<script src="<?=ASSETS?>js/basic-jquery-slider.min.js"></script>
<script>
    $(document).ready(function () {

        $("#slider").bjqs({
            'height':310,
            'width':880,
            'animation':'slide',
            'animationDuration':200,
            'centerMarkers':false,
            'centerControls':true,
            'useCaptions':false,
            'keyboardNav':true,
            'showControls':true,
            'showMarkers':false

        });

    });
</script>
<?php } ?>
<script src="<?=JS?>custom.js"></script>

<?php /*if($page=='kontakt'){ ?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyDyLWwuLULyRTtpDunsRIzGUdDrLDw3nvk" type="text/javascript"></script>
<script type="text/javascript" src='<?=JS?>gmap.js'></script>
<script>
    $(".map").gMap({ markers: [
        { address: "Antifašističke borbe 21ž,Novi Beograd,Srbija",
            html: "<h4 style='padding-left:0; margin-bottom:.5em'>Original d.o.o. </h4><span>Antifašističke borbe 21ž, Novi Beograd, Srbija<br>+381 (0)11 314 84 34,<br>+381 (0)11 314 84 35,<br>+381 (0)11 314 84 36</span>" ,popup:true}

    ],
        address: "Gavrila Principa 75,Beograd,Srbija",
        zoom: 16 });
</script>
<?php } */
?>
<script language="javascript" type="text/javascript" src="<?=JS?>jquery.coolautosuggest.js"></script>
<script language="javascript" type="text/javascript" src="<?=JS?>jquery.coolfieldset.js"></script>
<script language="javascript" type="text/javascript">
    $("#searchField").coolautosuggest({
        url:"<?=SITE_ROOT?>framework/models/processSearch.php?chars=",
        submitOnSelect:true,
        minChars:2,
        onSelected:function (result) {
            // $("#searchField").val(result);
        }

        // Check if the result is not null
//            if(result!=null){
//                $("#text1_id").val(result.id); // Get the ID field
//                $("#text1_description").val(result.description); // Get the description
//            }
//            else{
//                $("#text1_id").val(""); // Empty the ID field
//                $("#text1_description").val(""); // Empty the description
//            }
    });
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set.
                    _.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute('charset', 'utf-8');
        $.src = '//cdn.zopim.com/?tIqHU1PQD13NBFqHMWVCzE7Ak8HoL9p6';
        z.t = +new Date;
        $.
                type = 'text/javascript';
        e.parentNode.insertBefore($, e)
    })(document, 'script');
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>