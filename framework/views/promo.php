<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="columns product">

    <div class="separatorDown"></div>
    <?php /*
  *     <nav class="submenu">
        <a href="#" class="tab1"><?=trans('Opis proizvoda', 'Description')?></a>
        <a href="#" class="tab2"><?=trans('Uputstva', 'Instructions')?></a>
        <a href="#" class="tab3"><?=trans('Drajveri', 'Drivers')?></a>
        <a href="#" class="tab4"><?=trans('Dodatna oprema', 'Additional Equipment')?></a>
    </nav>

    <section class="sixteen columns" id="grayNav">
        <nav>
            <li>
                <a href="<?=SITE_ROOT . trans('proizvodi', 'products')?>" <?php if (isset($_GET['cat']) && $_GET['cat'] == trans('Svi proizvodi', 'All products')) {
                    echo 'class="selected"';
                }?>><?=trans('Svi proizvodi', 'All products')?></a>
            </li>
            <? /* this is category list/
            foreach ($catList as $c): ?>
                <li>
                    <a href="<?=SITE_ROOT . trans('proizvodi', 'products')?>/<?=$c->id?>/<?=urlSafe(trans($c->name_sr, $c->name_en))?>" <?php if (isset($_GET['cat']) && $_GET['cat'] == trans($c->name_sr, $c->name_en)) {
                        echo 'class="selected"';
                    }?>><?=trans($c->name_sr, $c->name_en)?></a>
                </li>
                <?
            endforeach; ?>
        </nav>
    </section>
    */ ?>
    <section class="one-third column productDetailGallery" style="height:<?=$height * 80 + 230?>px">

        <div class="mainImg">
            <?php foreach ($gallery as $gI): ?>
            <img src="<?=GALL . $gI->file?>" class="<?=$gI->id?>" alt="">
            <?php endforeach; ?>
        </div>
        <div class="thumbs galleryListing">
            <?php
            $i = 0;
            foreach ($gallery as $g): ?>
                <img src="<?=Th_S . $g->file?>" alt="" id="<?=$g->id?>" class="<?php if ($i == 0) {
                    echo 'active';
                }?>">
                <?php
                $i++;
            endforeach; ?>
        </div>
    </section>
    <section class="half column productDetails">
        <div class="breadCrumb">
            <a href="<?=SITE_ROOT?>"><img src="<?=IMG?>home_icon.png" alt=""></a>
            <a href="<?=SITE_ROOT?>promocije">
                <?=trans('Promocije', 'Promotions')?>
            </a>

            <span><?=trans($promo->name_sr,$promo->name_sr)?></span>
            <?php /*<span><strong><?=trans('Cena proizvoda od', 'Promo price')?></strong><?=$promo->price?></span>*/;  ?>
        </div>
        <h1><?=trans($promo->name_sr, $promo->name_en)?>
            <span><?php
//                if(count(array_unique($manufactures))>1){
//                foreach($manufactures as $m){
//                       echo trans($m->name_sr, $m->name_en).',  ';
//                    }
//
//
//            }?></span>
        </h1>

        <div class="tabs">
            <div id="tab1" class="tab active">
                <h2><?=trans('Opis proizvoda', 'Product Description')?></h2>

                <div class="promoDetails">
                    <p style="text-transform: none;">
                        <strong><?=trans('Akcija traje do: ', 'Until: ').'</strong>'.timeStampToDate($promo->dateEnd)?>  <br>
                    </p>

                </div>

                <div class="productDescription">
                    <?=strip_tags(trans($promo->desc_sr, $promo->desc_en), '<br><strong><li><ul><ol><h2><h1><h4><h3><h5>')?>
                </div>
            </div>
            <div id="tab2" class="tab">
                <h2><?=trans('Uputstva', 'Instructions')?></h2>
                <ul class="driversList">
                    <?php foreach ($instructions as $in): ?>
                    <li>
                        <h4 data-icon="~">
                            <a href="<?=SITE_ROOT?>instructionDownload/<?=$in->file?>"><?=trans($in->name_sr, $in->name_en)?></a>
                        </h4>

                        <p>
                            <?=trans('Format: ', 'Format: ') . $in->version.' '.$in->size?>
                            &nbsp;&nbsp;|&nbsp;&nbsp;<?=trans('Datum: ', 'Date: ') . formatDate($in->releaseDate)?>
                        </p>
                        <?php if ($in->desc_sr != '') { ?>
                        <p><?=trans($in->desc_sr, $in->desc_en)?></p>
                        <?php } ?>
                        <a class="bttn" href="<?=SITE_ROOT?>driversDownload/<?=$in->file?>">Download</a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div id="tab3" class="tab">
                <h2><?=trans('Drajveri', 'Drivers')?></h2>
                <ul class="driversList">
                    <?php foreach ($drivers as $d): ?>
                    <li>
                        <h4 data-icon="~">
                            <a href="<?=SITE_ROOT?>driversDownload/<?=$d->file?>"><?=trans($d->name_sr, $d->name_en)?></a>
                        </h4>

                        <p>
                            <?=trans('Verzija: ', 'Version: ') . $d->version?>
                            &nbsp;&nbsp;|&nbsp;&nbsp;<?=trans('Datum: ', 'Date: ') . formatDate($d->releaseDate). " | ".$d->size?>
                        </p>
                        <?php if ($d->desc_sr != '') { ?>
                        <p><?=trans($d->desc_sr, $d->desc_en)?></p>
                        <?php } ?>
                        <a class="bttn" href="<?=SITE_ROOT?>driversDownload/<?=$d->file?>">Download</a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div id="tab4" class="tab productListing" >
                <h2><?=trans('Dodatna oprema', 'Additional Equipments')?></h2>
                <?php foreach($optional as $opt): ?>
                <a href="<?=SITE_ROOT?>proizvod/<?=$opt->id?>/<?=urlSafe(trans($opt->name_sr,$opt->name_en))?>">
                    <span class="title"><?=trans($opt->name_sr,$opt->name_en)?></span>
                    <span>
                        <img src="<?=Th_S?><?=getGallery($opt->id, 'product')?>" alt="<?=trans($opt->name_sr,$opt->name_en)?>">
                    </span>
                </a>

                <?php endforeach; ?>
            </div>
        </div>
        <p>

        </p>
    </section>
    <?php /*
  *  <section class="one-third column drivers">
        <? for ($i = 0; $i < 5; $i++) { ?>
        <a href="<?=SITE_ROOT?>">
            <img src="<?=GALL?>drivers1.png" alt="">
            <span>Preuzmi drajver i softver
za kopir/print sisteme</span>
        </a>
        <?php } ?>
    </section>
  * */; ?>
    <section class="tabsGallery productListing">
        <h4 class="promoProducts"><?=trans('Proizvodi na promociji', 'Products on promotion')?></h4>
        <section class="productGallery productListing">
                <?php
                foreach ($products as $p):
                    //  $p = Product::find_by_id($pID);
                    if (isset($p) && $p && findImageForProduct($p->id)) {
                        $pImg = findImageForProduct($p->id); ?>
                        <a href="<?=SITE_ROOT . trans('proizvod', 'product')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>" class="productItem">
                            <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                            <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                        </a>
                        <?php
                    }
                endforeach;
            ?>

        </section>

        <article class="products">
<?php
//            foreach($ as $sPr):
//                ?>
<!--                <a href="--><?//=SITE_ROOT?><!--proizvod/--><?//=$sPr->id?><!--/--><?//=urlSafe(trans($sPr->name_sr,$sPr->name_en))?><!--">-->
<!---->
<!--                    <img src="--><?//=Th_M?><!----><?//=getGallery($sPr->id, 'product')?><!--" alt="--><?//=trans($sPr->name_sr, $sPr->name_en)?><!--">-->
<!--                    <div class="separatorRight"></div>-->
<!--                </a>-->
<!--                --><?php
//            endforeach;
//            ?>
        </article>
    </section>
    <section class="half column productInfo">
        <h2>showroom</h2>

        <p>Posetite naš showroom gde ćemo Vas upoznati sa mogućnostima uređaja koje nudimo. Prezentaciju možete zakazati
            tako što ćete popuniti formular nakon čega će Vas kontaktirati naši konsultanti.</p>
        <a href=""><img src="<?=IMG?>showroom.jpg" alt=""></a>

        <div class="separatorRight"></div>
    </section>
    <section class="half column productInfo">
        <h2>total care</h2>

        <p>Za potpuni komfor u radu! Naka Vaši uređaji uvek budu u besprekornom stanju.</p>
        <a href=""><img src="<?=IMG?>totalcare.jpg" alt="" style="top:1.395em"></a>
    </section>
</section>