<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 4/21/12
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */
?>

<section class="sixteen columns partner general_page">
    <?php /*
  *  <nav>
        <a href="#">Uređaji B&W</a>
        <a href="#">Uređaji Kolor</a>
        <a href="#">Tabela paketa</a>
        <a href="#">Napravite svoj paket</a>
        <a href="#">Partneri</a>
    </nav>
 */ ?>
    <section class="partnerInfo pageInfo">
        <h1><?=trans($gP->name_sr,$gP->name_en)?></h1>

        <?php
        if ($session->message() != '') {
            echo '<p class="errorMessage">' . $session->message() . '</p>';
        }
        ?>
        <form action="<?= SITE_ROOT ?>registerIn" class="loginForm" method="post" style="float:left;">
            <label><?=trans('Ime i prezime', 'Name')?></label>
            <input type="text" name="name">
            <label><?=trans('Ime firme', 'Company')?></label>
            <input type="text" name="company">
            <label><?=trans('PIB', 'VAT')?></label>
            <input type="text" name="pib">
            <label><?=trans('Telefon', 'Phone')?></label>
            <input type="text" name="phone">
            <label><?=trans('Adresa', 'Address')?></label>
            <input type="text" name="address">
            <label><?=trans('Grad', 'City')?></label>
            <input type="text" name="city">
            <label><?=trans('Država', 'Country')?></label>
            <input type="text" name="country">

            <br><br>
            <label><?=trans('E-mail adresa', 'E-mail address')?></label>
            <input type="text" name="email">
            <label><?=trans('Lozinka', 'Password')?></label>
            <input type="password" name="password">
            <label>
                <input type="submit" class="bttn" value="<?= trans('Registruj me', 'Register') ?>">
            </label>
        </form>

        <section style="float:right; width:40%;">
            <p><?=trans($gP->desc_sr, $gP->desc_en)?></p>
            <?php if($gP->img!=''){ ?>
            <img src="<?=GALL.$gP->img?>" style="z-index:0" alt="">
            <?php } ?>
        </section>

    </section>
</section>