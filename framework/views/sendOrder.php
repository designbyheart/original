<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 8:43 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="columns product">

    <h1 style="margin-left:1em; margin-top:1em;">
        <?=trans('Izaberite adresu', 'Select your address')?>
    </h1>

    <div class="separatorDown" style="width: 98%;"></div>


    <form action="<?=SITE_ROOT?>finalizeOrder" method="post" class="cartForm" style="clear:both">
        <input type="hidden" name="orderID" value="<?=$_SESSION['orderID']?>">

        <?php
        $display = 'style="display:none"';
        if (!$address) {
            $display = '';
            ?>
            <p><?=trans('Unesite detalje o adresi za dostavu', "Please enter details for delivery address")?>:</p>
            <?php
        } else {
            foreach ($address as $a) {
                ?>
                <label for="address">
                    <p>
                        <label>
                            <input type="checkbox" name="address" selected value="<?=$a->id?>"/>
                            <?php
                            echo $a->address1 . " " . $a->city . $a->state . $a->country;
                            ?>
                        </label>
                    </p>
                </label>
                <?php
            }
        } ?>
        <fieldset class="newAddress" <? //$display?>>
            <div class="addressType">
                <label><input type="radio" name="addressType" value="1"> Kućna adresa</label>
                <label><input type="radio" name="addressType" value="2"> Poslovna adresa</label>
            </div>

            <label for="">Address 1
                <input type="text" name="address1">
            </label>
            <label for=""> Address 2
                <input type="text" name="address2"></label>
            <label for=""> Postal
                <input type="text" name="postal">
            </label>
            <label for=""> Zip
                <input type="text" name="zip">
            </label>
            <label for=""> City
                <input type="text" name="city">
            </label>
            <label for=""> State
                <input type="text" name="state">
            </label>
            <label for=""> Country
                <input type="text" name="country">
            </label>
        </fieldset>
        <button class="orderBttn continueBttn   " style="padding:.5em 1em"><?=trans("Nastavi", "Continue")?></button>
        <button class="cancelBttn continueCancelBttn"><?=trans('Otkaži kupovinu', "Cancel this order")?></button>
    </form>
    <?php /*
  *  <section class="one-third column drivers">
        <? for ($i = 0; $i < 5; $i++) { ?>
        <a href="<?=SITE_ROOT?>">
            <img src="<?=GALL?>drivers1.png" alt="">
            <span>Preuzmi drajver i softver
za kopir/print sisteme</span>
        </a>
        <?php } ?>
    </section>
  * */; ?>
</section>