<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/26/12
 * Time: 10:37 AM
 * To change this template use File | Settings | File Templates.
 */
$customer = Customer::find_by_id($_SESSION['clientID']);
?>
<section class="sixteen columns partner general_page">

    <nav>
        <a href="<?=SITE_ROOT?>my-account" ><?=trans('Povratak na profil', 'Return to profile')?></a>
    </nav>

    <section class="partnerInfo pageInfo profileInfo" style="min-height: 450px;" >
        <h1><?=trans('Izmena profila', 'Edit profile')?></h1>

        <article>
            <form action="<?=SITE_ROOT?>saveProfile">
                <form action="<?= SITE_ROOT ?>registerIn" class="loginForm" method="post" style="float:left;">
                    <input type="hidden" name="id" value="<?=$customer->id?>">
                    <label><?=trans('Ime i prezime', 'Name')?></label>
                    <input type="text" name="name" value="<?=$customer->name?>">
                    <label><?=trans('Ime firme', 'Company')?></label>
                    <input type="text" name="company" value="<?=$customer->company?>">
                    <label><?=trans('PIB', 'VAT')?></label>
                    <input type="text" name="pib" value="<?=$customer->company_PIB?>">
                    <label><?=trans('Telefon', 'Phone')?></label>
                    <input type="text" name="phone" value="<?=$customer->phones?>">
                    <label><?=trans('Adresa', 'Address')?></label>
                    <input type="text" name="address" value="<?=$customer->address?>">
                    <label><?=trans('Grad', 'City')?></label>
                    <input type="text" name="city" value="<?=$customer->city?>">
                    <label><?=trans('Država', 'Country')?></label>
                    <input type="text" name="country" value="<?=$customer->state?>">

                    <br><br>
                    <label><?=trans('E-mail adresa', 'E-mail address')?></label>
                    <input type="text" name="email" value="<?=$customer->email?>">
                    <label><?=trans('Valuta', 'Currency')?></label>
                    <label><input type="checkbox" name="currency" value="1"> EUR</label>
                    <label><input type="checkbox" name="currency" value="2"> RSD</label>

                    <label style="margin-top:1em"><?=trans('Lozinka', 'Password')?></label>
                    <input type="password" name="password" value="<?=$customer->password?>" >
                    <label>
                        <input type="submit" class="bttn" value="<?= trans('Sačuvaj izmene', 'Save') ?>">
                    </label>

            </form>
        </article>
    </section>
</section>