<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 6/16/12
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */
                              ?>
<div class="path">
    <ul class="breadcrumb">
        <div id="crumb">
            <a href="<?=SITE_ROOT?>">
                <img src="<?=SITE_ROOT?>assets/img/home-icon.png" class="homeLink" alt="">
            </a>
            <a class="active" href="#"><?=trans("Registracija", 'Registration')?></a>
        </div>
    </ul>
</div>
<div id="outer1">
    <div id="outer">
        <!-- Wapper Sec -->
        <div id="wrapper_sec">
            <!-- Content Seciton -->
            <div id="content_section">
                <div class="col1">
                    <?php //check is page exists or is active
                    if(!$gP){
                        require_once(PAGE_404);
                    }
                    else{?>
                        <div class="quick_overview product_info">
                            <h2 class="heading colr" ><?=trans($gP->name_sr, $gP->name_en);?></h2>
                            <p><?=trans($gP->desc_sr, $gP->desc_en)?></p>

                        <?php }
                    if($session->message()!=''){
                        ?>
                        <p class="errorMessage"><?=$session->message()?></p>
                        <?php
                    }
                    ?>
                    <form action="<?=SITE_ROOT?>registration" class="registerForm" style="padding-top:20px;float:left;" method="post">
                        <label for="">
                            <?=trans("Ime i prezime", "Your name")?>
                            <input type="text" name="name">
                        </label>
                        <label for="">
                            E-mail
                            <input type="text" name="email">
                        </label>
                        <label for="">
                            <?=trans('Ime firme', 'Company name')?>
                            <input type="text" name="company">
                        </label>
                        <label for="">
                            <?=trans("Telefon", 'Phone')?>
                            <input type="text" name="phone">
                        </label>
                        <label for="" style="margin-top:40px;">
                            <button class="btn" name="register" type="submit"><?=trans('Registruj se','Register')?></button>
                    </form>
                </div>
                    <div class="clear"></div>
                </div>
                <div class="col2">
                    <?php require_once(INC.'sidebar.php'); ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>