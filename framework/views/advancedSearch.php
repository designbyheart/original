<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 1:02 PM
 * To change this template use File | Settings | File Templates.
 */         ?>
<section class="sixteen columns search">
<h1 class="remove-bottom"
    style="margin-top: 40px; margin-bottom: 7em;"><?=trans('Napredna pretraga', 'Advanced search')?></h1>


<?php require_once(INC.'searchBar.php'); ?>

<section class="productGallery productListing" id="searchResults" style="padding-top:30px;">
        <?php
        $error = 0;
        if (isset($products) && count($products) > 0) {
            $error = 1;
            foreach ($products as $pID):
                $p = Product::find_by_id($pID->id);
                //  $p = Product::find_by_id($pID);

                if (isset($p) && $p) {
                    $pImg = findImageForProduct($p->id);
                    if (!$pImg) {
                        $pImg = DEF_IMG;
                    }
                    ?>
                    <a href="<?=SITE_ROOT . trans('proizvod', 'product')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>"
                       class="productItem">
                        <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                        <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                    </a>
                    <?php
                }
            endforeach;
        } ?>
        <?php
//        if (isset($partners) && count($partners) > 0) {
//            $error = 1;
//            foreach ($partners as $pID):
//                $p = Manufacturer::find_by_id($pID->id);
//                //  $p = Product::find_by_id($pID);
//
//                if (isset($p) && $p) {
//                    $pImg = findImageForProduct($p->id);
//                    if (!$pImg) {
//                        $pImg = DEF_IMG;
//                    }
                    /* ?>
<!--                    <a href="--><?//=SITE_ROOT?><!--partner/--><?//=$p->id?><!--/--><?//=urlSafe(trans($p->name_sr, $p->name_en))?><!--"-->
<!--                       class="productItem">-->
<!--                        <span class="title">--><?//=trans($p->name_sr, $p->name_en)?><!--</span>-->
<!--                        <span class="img"><img src="--><?//=Th_M . $pImg?><!--" alt=""></span>-->
<!--                    </a>-->
<!--                    --><?php
/                }
//            endforeach;
        }
 */      ?>
    <section class='searchResuls'></section>

    <?php if ($error == 0) {   ?>
    <div class="noSearchInfo">
        <p><?=trans('Koristite meni za pretragu radi preciznih rezultata', 'Please use search menu for more acurate results')?></p>
    </div>
    <? } ?>

</section>

</section>