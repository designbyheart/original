<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */

?>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 8:43 AM
 * To change this template use File | Settings | File Templates.
 */
$ch = Characteristics::find_by_product($leftProductID->id, 1);
?>
<section class="columns product" style="width:90%">

    <h1 style="margin-left:1em; margin-top:1em;">
        <?=trans('Poređenje proizvoda', 'Product comparison')?>
    </h1>

    <div class="separatorDown" style="width: 98%;"></div>

    <section class="orderDetails">

        <table style="width:90%;margin:auto; margin-left:4em">

            <tr>
                <th width="30%"><h4><?=trans($leftProductID->name_sr, $leftProductID->name_en)?></h4></th>
                <th align="center"><strong><?=trans('Karakteristika', 'Characteristic')?></strong></th>
                <th width="30%"><h4><?=trans($rightProductID->name_sr, $rightProductID->name_en)?></h4></th>
            </tr>

            <?php foreach ($ch as $key=>$value) {
                if($key != 'lang' && $key !='id' && $value !='' && $key!='productID' && $value> 0){
            ?>
            <tr>
                <td align="center"><?=$value?></td>
                <td align="center"><?=translateField($key)?></td>
                <td align="center"><?=Characteristics::getCharacteristic($rightProductID->id, $key)?></td>
            </tr>
            <?php

                    }
                } ?>

        </table>
    </section>

    <?php /*
  *  <section class="one-third column drivers">
        <? for ($i = 0; $i < 5; $i++) { ?>
        <a href="<?=SITE_ROOT?>">
            <img src="<?=GALL?>drivers1.png" alt="">
            <span>Preuzmi drajver i softver
za kopir/print sisteme</span>
        </a>
        <?php } ?>
    </section>
  * */; ?>
</section>
