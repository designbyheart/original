<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 4:18 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner">
    <nav>
        <?php
        if($subMenu){
        foreach($subMenu as $sM): ?>
        <a href="<?=SITE_ROOT.$sM->linkSr?>"><?=trans($sM->name_sr, $sM->name_en)?></a>
        <?php endforeach; } ?>
    </nav>
    <section class="partnerInfo">
        <h1><?=trans($partner->name_sr,$partner->name_en)?></h1>

        <p><?=trans($partner->desc_sr,$partner->desc_en)?></p>
        <img src="<?=GALL?>iplus.jpg" alt="">
    </section>
    <section class="partnerInfo noPadding tab1">
        <article class="one-third column">
            <h2><?=trans('Benefiti', 'Benefits')?></h2>

            <p>
                Povoljna nabavna cena - korisnik odmah    postaje vlasnik uređaja uz izuzetno povoljne finansijske uslove.
                Povoljna cena otiska - transparentno izražena i bez ikakvih skrivenih troškova.
                Isporuka uređaja, instalacija i obuka operatera na lokaciji korisnika.
                Tehnička Podrška za vreme i nakon prestanka trajanja paketa.
                Zamenski uređaj u ekstremnim uslovima za koje su potrebni servisni zahvati dužeg vremenskog intervala.
            </p>
            <img src="<?=GALL?>hp.jpg" class="alignLeft" alt="">
            <div class="separatorRight"></div>
        </article>
        <article class="one-third column">
            <h2><?=trans('Paketi', 'Packages')?></h2>

            <p>Paketi se biraju prema potrebama korisnika, a ukoliko među ponuđenim ne pronađete odgovarajući ili ukoliko imate eventualnih nejasnoća oko odabira, molimo Vas da popunite upitnik ili da nas direktno kontaktirate na mail: office@originalgrupa.com

                Konsultantski tim Originala će isti pažljivo razmotriti i u dogovoru sa Vama preporučiti postojeći ili napraviti novi paket samo za Vas.
            </p>
            <img src="<?=GALL?>hp2.jpg" class="alignCenter" alt="">

            <div class="separatorRight"></div>
        </article>
        <article class="one-third column">
            <h2><?=trans('Akcija', 'Action')?></h2>

            <p>Paketi se biraju prema potrebama korisnika, a ukoliko među ponuđenim ne pronađete odgovarajući ili ukoliko imate eventualnih nejasnoća oko odabira, molimo Vas da popunite upitnik ili da nas direktno kontaktirate na mail: office@originalgrupa.com
            </p>
            <img src="<?=GALL?>hp3.jpg" class="alignRight" alt="">

        </article>
    </section>
    <section class="partnerInfo">
        <h1><?=trans($partner->name_sr,$partner->name_en)?></h1>

        <p><?=trans($partner->desc_sr,$partner->desc_en)?></p>
        <img src="<?=GALL?>iplus.jpg" alt="">
    </section>
    <section class="partnerInfo noPadding tab2">
            <h2><?=trans('Proizvodi', 'Products')?></h2>
    </section>
    <img src="<?=IMG?>printerFooter.png" alt="" class="homeMain" >

    <section class="half column productInfo">
        <h2>showroom</h2>

        <p>Posetite naš showroom gde ćemo Vas upoznati sa mogućnostima uređaja koje nudimo. Prezentaciju možete zakazati
            tako što ćete popuniti formular nakon čega će Vas kontaktirati naši konsultanti.</p>
        <a href=""><img src="<?=IMG?>showroom.jpg" alt=""></a>

        <div class="separatorRight"></div>
    </section>
    <section class="half column productInfo">
        <h2>total care</h2>

        <p>Za potpuni komfor u radu! Naka Vaši uređaji uvek budu u besprekornom stanju.</p>
        <a href=""><img src="<?=IMG?>totalcare.jpg" alt="" style="top:1.395em"></a>
    </section>
</section>