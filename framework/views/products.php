<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
/*
 *        <?php
        if (!isset($_GET['cat'])) {
            echo trans($gP->desc_sr, $gP->desc_en);
        }?>
*/
?>
<section class="columns productsPage">

    <section class="productGallery categoryPage">
        <ul style="margin-left: <?=$margL?>">
            <?php foreach($subCats as $cList): ?>
            <li><a href="<?=SITE_ROOT . trans('proizvodi', 'products')?>/<?=$cList->id?>/<?=urlSafe(trans($cList->name_sr, $cList->name_en))?>">
            <?php echo galleryImg($cList->id, 'cat-menu', TRUE, 1)?>
                <span><?=trans($cList->name_sr, $cList->name_en)?></span></a>
            </li>
            <?php endforeach; ?>
        </ul>

    </section>
    <section class="sixteen columns" id="grayNav">
        <nav>
            <? /* this is category list*/
            foreach ($mainCats as $c): ?>
                <li>
                    <a href="<?=SITE_ROOT . trans('proizvodi', 'products')?>/<?=$c->id?>/<?=urlSafe(trans($c->name_sr, $c->name_en))?>" <?php if ((isset($id) && $id==$c->id) || (isset($parentCat) && $parentCat==$c->id)) {
                        echo 'class="selected"';
                    }?>><?=trans($c->name_sr, $c->name_en)?></a>
                </li>
                <?
            endforeach; ?>
        </nav>
    </section>

    <h1 class="padding10">
        <?php
        if (!isset($_GET['cat']) || $_GET['cat'] == '') {
            echo trans('Svi proizvodi', 'All products');
        } else {
            echo trans($cat->name_sr, $cat->name_en);
        }?>
    </h1>
    <section class="productGallery productListing">
        <?php
        if (!$products) {
            ?>
            <div class="page_desc">
                <p>Za izabranu kategoriju trenutno nema proizvoda...</p>
            </div>
            <?php
        } else {
            foreach ($products as $p):
                //  $p = Product::find_by_id($pID);
                if(isset($_GET['cat'])){
                if (isset($p) && $p) {
                    $pImg = findImageForProduct($p->id);
                    if(!$pImg) {$pImg = DEF_IMG; }
                    ?>
                    <a href="<?=SITE_ROOT . trans('proizvod', 'product')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>" class="productItem">
                        <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                        <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                    </a>
                    <?php
                }      }
            endforeach;
        } ?>
    </section>


    <div class="separatorDown"></div>
    <section>
        <?php
        $i = 0;
        foreach ($news as $n) {
            ?>
            <section class="half column productInfo" style="min-height: 17em;">
                <h2><?=trans($n->title, $n->title_en)?></h2>
                <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
                <a href="<?=SITE_ROOT?>aktuelnosti/<?=$n->id?>/<?=urlSafe($n->name_sr)?>"><img src="<?=Th_S.$galleryImages[$i]?>" alt=""></a>
                <div class="separatorRight"></div>
            </section>
            <?php
            $i++;
        } ?>
    </section>
</section>