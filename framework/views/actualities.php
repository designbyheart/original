<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/8/12
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="columns productsPage" style="width:100%;">


    <h1 class="padding10"><?=trans('Aktuelnosti', 'Actualities')?></h1>
    <section style="float:left;margin-top:2em;width:100%;">

        <?php
        if ($news && count($news) > 0) {
            foreach ($news as $p):
                $g = Gallery::find_by_reference('news', $p->id);
                if ($g) {
                    $img = $g->file;
                } ?>
                <div style="float:left;width:25%;clear:left">
                    <a href="<?=SITE_ROOT . trans('aktuelnost', 'aktuelnost')?>/<?=$p->id?>/<?=urlSafe(trans($p->title, $p->title_en))?>"
                       class="productItem" style="margin:1em;float:left;clear:left; position:relative;left:2em;width:17%;">
                                <span style="color:#666;float:left;width:10em;clear:left; text-align: center;">
                                    <img src="<?=Th_S . $img?>" alt="" style="display:block;margin:auto;margin-top:-.5em; margin-bottom:.3em">
                                    <?=formatDate($p->date)?>
                                </span>
                    </a>
                </div>

                <div style=" width:65%;float: left;">
                    <h4 style="font-size:2.2em;float:left; margin:0 0 .4em 0; width:100%;"><a href="<?=SITE_ROOT . trans('aktuelnost', 'aktuelnost')?>/<?=$p->id?>/<?=urlSafe(trans($p->title, $p->title_en))?>"><?=trans($p->title, $p->title_en)?></a></h4>
                    <p style="float:left;clear:left; display:block;width:100%:"><?=trans($p->content, $p->content_en)?></p>
                </div>

                <?php

            endforeach;
        } ?>
    </section>

    <div class="separatorDown"></div>
    <section>
        <?php
        $i = 0;
        foreach ($news as $n) {
            if ($i < 2) {
                ?>
                <section class="half column productInfo" style="min-height: 17em;">
                    <h2><?=trans($n->title, $n->title_en)?></h2>

                    <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
                    <a href="<?=SITE_ROOT?>aktuelnosti/<?=$n->id?>/<?=urlSafe($n->title)?>"><img
                            src="<?=Th_S . $galleryImages[$i]?>" alt=""></a>

                    <div class="separatorRight"></div>
                </section>
                <?php
                $i++;
            }
        } ?>
    </section>
</section>