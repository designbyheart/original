<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">

    <nav>
        <a href="<?=SITE_ROOT?>my-account" ><?=trans('Povratak na profil', 'Return to profile')?></a>
    </nav>

    <section class="partnerInfo pageInfo profileInfo" style="min-height: 450px;" >
        <h1><?=trans('Istorija porudžbina', 'Orders history')?></h1>

            <article>
            <table id="ordersHistory">
                <tr>
                    <th width="15%"><?=trans('Datum', 'Date')?></th>
                    <th width="15%"><?=trans('Broj', 'Number')?></th>
                    <th><?=trans('Proizvodi', 'Products')?></th>
                    <th width="20%"><?=trans('Cena porudžbine', 'Order price')?></th>
                </tr>
                <?php foreach ($prevOrders as $pO):
                    $producsNames = array();
                    $i = 0;
                    foreach(array_filter(explode(',', $pO->products)) as $pID){
                        if($i<5){
                        $p = Product::find_by_id($pID);
                        $producsNames[] = trans($p->name_sr, $p->name_en);
                        $i++;
                        }
                    }
                ?>
                <tr>
                    <td><?=date('d.m.y', $pO->orderDate)?></td>
                    <td><a href="<?=SITE_ROOT?>order-info/<?=alphaID(intval($pO->orderID))?>"><?=trans('Br. ', 'No. ') . alphaID(intval($pO->orderID))?></a></td>
                    <td><?=implode(',', $producsNames)?> <?=count(explode(',', $pO->products))?> <?=trans('proizvoda', ' products')?></td>
                    <td><?=$pO->totalPrice?></td>
                </tr>
                <?php endforeach; ?>
            </table>
            </article>
        </section>
</section>