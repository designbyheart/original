<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/23/12
 * Time: 10:22 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">

    <section class="partnerInfo pageInfo">
        <h1><?=$h1?></h1>


        <div class="content_text" style="color:#555!important;width:100%">
            <p><?=$desc?></p>
            <?php if ($result == 'error') { ?>
            <form action="<?=SITE_ROOT?>resendAuthentication" method="post">
                <p><?=trans('Vaš verifikacioni kod nije ispravan. Unesite email adresu koju ste registrovali da bi vam poslali email sa novim verifikacionim brojem', 'Your verification code is not valid. Enter the email address you have registered to send you an email with a new verification code')?>
                </p>
                <label>
                    <input type="text" placeholder="<?=trans('moja-adresa@adresa.com', 'my-email@email.com')?>" name="email" >
                </label>
                <input type="submit" value="<?=trans('Pošalji', "Send")?>">
            </form>
            <?php } ?>
        </div>

    </section>

</section>