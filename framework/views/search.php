<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 5:49 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns search">
    <h1 class="remove-bottom"
        style="margin-top: 40px; margin-bottom: 7em;"><?=trans('Rezultati pretrage', 'Search results')?></h1>

    <h2 class="advSearch"><a href="<?=SITE_ROOT?>napredna-pretraga">Napredna pretraga</a></h2>

    <section class="productGallery productListing" id="searchResults" style="padding-top:30px;">
        <?php
        $error = 0;
        if (isset($products) && count($products) > 0) {
            $error = 1;
            foreach ($products as $pID):
                $p = Product::find_by_id($pID->id);
                //  $p = Product::find_by_id($pID);

                if (isset($p) && $p) {
                    $pImg = findImageForProduct($p->id);
                    if (!$pImg) {
                        $pImg = DEF_IMG;
                    }
                    ?>
                    <a href="<?=SITE_ROOT . trans('proizvod', 'product')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>"
                       class="productItem">
                        <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                        <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                    </a>
                    <?php
                }
            endforeach;
        } ?>
        <?php
        if (isset($partners) && count($partners) > 0) {
            $error = 1;
            foreach ($partners as $pID):
                $p = Manufacturer::find_by_id($pID->id);
                //  $p = Product::find_by_id($pID);

                if (isset($p) && $p) {
                    $pImg = findImageForProduct($p->id);
                    if (!$pImg) {
                        $pImg = DEF_IMG;
                    }
                    ?>
                    <a href="<?=SITE_ROOT?>partner/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>"
                       class="productItem">
                        <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                        <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                    </a>
                    <?php
                }
            endforeach;
        }
        if ($error == 0) {
            ?>
            <div style="margin-left: 3em;margin-top: -1em;">
                <p><?=trans('Unesite pojam za pretragu', 'Please enter term for start searching products')?></p>
            </div>
            <? } ?>

    </section>

</section>