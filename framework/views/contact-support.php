<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">
    <nav>
        <a href="<?=SITE_ROOT?>order-info/<?=alphaID($order->orderID)?>"><?=trans('Povratak na detalje narudžbine', 'Back to order')?></a>
    </nav>
    <section class="partnerInfo pageInfo profileInfo">
        <h1><?=trans('Kontaktirajte podršrku', 'Contact support')?></h1>
        <article class="orderInfo">

            <form action="<?=SITE_ROOT?>sendMessage" class="contactMessage" method="post">
                <fieldset>
                    <input type="hidden" name="messageType" value="orderTicket">
                    <input type="hidden" name="orderID" value="<?=alphaID($order->orderID)?>">

                    <p><strong><?=trans("Narudžbina: ", "Order: ") . alphaID($order->orderID)?></strong></p>
                    <?php if (count($categories) > 0) { ?>
                    <label for="category"><?=trans('Izaberite kategoriju tiketa', 'Select ticket category')?></label>
                    <select name="ticketCategory" id="category" style="margin-top:5px">
                        <option value=""><?=trans('Izaberite..', 'Choose')?></option>
                        <?php foreach ($categories as $c): ?>
                        <option value="<?=$c->id?>"><?=trans($c->name_sr, $c->name_en)?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php } ?>
                    <label><?=trans('Unesite vaše pitanje', 'Enter you question')?><br>
                        <textarea name="messageText"></textarea>
                    </label>
                    <button type="submit"><?=trans('Pošalji poruku', 'Send message')?></button>
                </fieldset>
                <fieldset></fieldset>

            </form>
        </article>
    </section>
</section>