<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/10/12
 * Time: 3:42 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns home">
    <div class="separatorDown"></div>
    <section class="slider" id="slider">
        <ul class="bjqs">
            <?php foreach ($promo as $pro):
                $img = array_shift(Gallery::find_by_sql("select * from gallery where refID = '{$pro->id}' and type = 'promotion' LIMIT 1"));
            if($img){
                $imgL =$img->file;
            }else{
                $imgL = 'printer1.jpg';
            }
            ?>
            <li>
                <article class="slide">
                    <a href="<?=SITE_ROOT?>promo/<?=$pro->id?>" class="slideImg"><img src="<?=GALL.$imgL?>" alt=""></a>

                    <h1 class="remove-bottom"><?=trans($pro->subTitle_sr,$pro->subTitle_en)?><strong><?=trans($pro->name_sr, $pro->name_en)?></strong></h1>

                    <p>
                        <?=trunc(strip_tags(trans($pro->desc_sr, $pro->desc_en), '<br><b>'), 250)?>
                    </p>

                    <p class="price"><?=trans('već od', 'from')?> <strong><?=formatMoney($pro->price)?> RSD</strong>
                    </p>

                    <a class="bttn" href="<?=SITE_ROOT?>promo/<?=$pro->id?>"><?=trans('Pogledajte ponudu', 'Check the offer')?></a>
                </article>
            </li>
            <?php endforeach; ?>
        </ul>
    </section>
    <div class="separatorDown"></div>
    <?php
    $i = 0;
    foreach ($news as $n) {
        ?>
        <section class="one-third column" style="max-height: 20em; overflow:hidden">
            <h2 style="margin-bottom:1.8em"><a href="<?=SITE_ROOT?>aktuelnost/<?=$n->id?>/<?=urlSafe(trans($n->title, $n->title_en))?>"><?=trans($n->title, $n->title_en)?></a></h2>
            <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
            <a href="<?=SITE_ROOT?>aktuelnost/<?=$n->id?>/<?=urlSafe(trans($n->title, $n->title_en))?>"><img src="<?=Th_S.$galleryImages[$i]?>" alt=""></a>
            <?php if ($i < 2) { ?>
            <div class="separatorRight"></div>
            <?php } ?>
        </section>
        <?php
        $i++;
    } ?>
    <div class="separatorDown"></div>
    <?php
//    <img src="<?=IMGprinterFooter.png" alt="" class="homeMain">
    ; ?>
</section>

