<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
/*
 *        <?php
        if (!isset($_GET['cat'])) {
            echo trans($gP->desc_sr, $gP->desc_en);
        }?>

*/

?>
<section class="columns productsPage">

    <h1 class="padding10">
        <?php if ($gP) {
        echo trans($gP->name_sr, $gP->name_en);
    } else {
        echo trans('Partneri', 'Partners');
    }?></h1>
    <section class="productGallery productListing">
        <?php
        if ($partners) {
            foreach ($partners as $p):
                //  $p = Product::find_by_id($pID);
                if (isset($_GET['cat'])) {
                    if (isset($p) && $p) {
                        $pImg = findImageForProduct($p->id);
                        if (!$pImg) {
                            $pImg = DEF_IMG;
                        }
                        ?>
                        <a href="<?=SITE_ROOT . trans('proizvod', 'product')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>"
                           class="productItem">
                            <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                            <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                        </a>
                        <?php
                    }
                }
            endforeach;
        } ?>
    </section>
    <section class="productGallery productListing">
        <?php
        if ($partners && count($partners) > 0) {
            foreach ($partners as $p):?>
                <a href="<?=SITE_ROOT . trans('partner', 'partner')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>"
                   class="productItem">
                    <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                    <span class="img"><img src="<?=Th_M . $p->logo?>" alt=""></span>
                </a>
                <?php

            endforeach;
        } ?>
    </section>

    <div class="separatorDown"></div>
    <section>
        <?php
        $i = 0;
        foreach ($news as $n) {
            ?>
            <section class="half column productInfo" style="min-height: 17em;">
                <h2><?=trans($n->title, $n->title_en)?></h2>

                <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
                <?php if (isset($galleryImages)) { ?>
                <a href="<?=SITE_ROOT?>aktuelnosti/<?=$n->id?>/<?=urlSafe($n->title)?>"><img
                        src="<?=Th_S . $galleryImages[$i]?>" alt=""></a>
                <?php } ?>
                <div class="separatorRight"></div>
            </section>
            <?php
            $i++;
        } ?>
    </section>
</section>