<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */

?>

<div id="compareContent" style="display:none" xmlns="http://www.w3.org/1999/html">
    <h3><?=trans('Uporedi proizvode', 'Compare products')?></h3>
    <a href="#" class="closeCompare" style="position:absolute;top:10px; left:500px;"><img src="<?=IMG?>close.png"
                                                                                          alt=""></a>

    <div class="column compareItem" style="border-right: solid 1px #ddd;width:48%;">
        <h4><?=trans($product->name_sr, $product->name_en)?></h4>
        <?php

        $gCompare = $gallery[0]; ?>
        <div class="imgCompare">
            <img src="<?=Th_M . $gCompare->file?>" class="<?=$gCompare->id?>" alt="">
        </div>
    </div>
    <?php echo $gallery; ?>
    <div class="column compareItem" style="margin-bottom:2em;">
        <h4><?=trans('Izaberi proizvod', 'Select product')?></h4>


        <section class="productSection">
            <input class="selected" type="hidden" name="" value="<?=$product->id?>">
            <select name="selectGroup" id="selectGroup">
                <option value=""><?=trans('Izaberi kategoriju', 'Select category')?></option>
                <?php $groups = Category::find_active();
                foreach ($groups as $g) {
                    ?>
                    <option value="<?=$g->id?>"><?=trans($g->name_sr, $g->name_en)?></option>
                    <? } ?>
            </select>
            <select name="manufacturer" id="selectManufacturer">
                <option value=""><?=trans('Izaberi proizvođača', "Select manufacturer")?></option>
                <?php $mnfS = Manufacturer::find_active();
                foreach ($mnfS as $m) {
                    ?>
                    <option value="<?=$m->id?>"><?=trans($m->name_sr, $m->name_en)?></option>
                    <? } ?>
            </select>
            <select name="productCompare" id="selectedProduct">
                <option value="<?=$m->id?>"><?=trans('Izbor proizvoda', 'Select product')?></option>
            </select>
        </section>
    </div>

    <a class="bttn continue" href="<?=SITE_ROOT?>uporediProizvode/<?=$product->id?>/" id="comparePrBttn"
       style="margin:2em 0 2em 35%;position:relative;top:-.2em"><?=trans('Uporedi proizvode', 'Compare products')?></a>

    <div class="addProductInfo"></div>

</div>

<section class="columns product">
<nav class="submenu">
    <a href="#" class="tab1"><?=trans('Opis proizvoda', 'Description')?></a>
    <?php if ($chacCategories) { ?>
    <a href="#" class="tab5"><?=trans('Karakteristike', 'Characteristics')?></a>
    <?php
}
    if ($instructions) {
        ?>
        <a href="#" class="tab2"><?=trans('Uputstva', 'Instructions')?></a>
        <?php
    }
    if ($drivers) {
        ?>
        <a href="#" class="tab3"><?=trans('Drajveri', 'Drivers')?></a>
        <?php
    }
    if ($optional) {
        ?>
        <a href="#" class="tab4"><?=trans('Dodatna oprema', 'Additional Equipment')?></a>
        <?php
    }
    $subMenu = SubMenu::find_for_page($product->id, 'product');
    if ($subMenu) {
        foreach ($subMenu as $s) {
            ?>
            <a href="#"><?=trans($s->name_sr, $s->name_en)?></a>
            <?
        }
    }
    ?>
</nav>
<div class="separatorDown"></div>
<?php /*
    <section class="sixteen columns" id="grayNav">
        <nav>
            <li>
                <a href="<?=SITE_ROOT . trans('proizvodi', 'products')?>" <?php if (isset($_GET['cat']) && $_GET['cat'] == trans('Svi proizvodi', 'All products')) {
                    echo 'class="selected"';
                }?>><?=trans('Svi proizvodi', 'All products')?></a>
            </li>
            <? /* this is category list/
            foreach ($catList as $c): ?>
                <li>
                    <a href="<?=SITE_ROOT . trans('proizvodi', 'products')?>/<?=$c->id?>/<?=urlSafe(trans($c->name_sr, $c->name_en))?>" <?php if (isset($_GET['cat']) && $_GET['cat'] == trans($c->name_sr, $c->name_en)) {
                        echo 'class="selected"';
                    }?>><?=trans($c->name_sr, $c->name_en)?></a>
                </li>
                <?
            endforeach; ?>
        </nav>
    </section>
    */ ?>
<section class="one-third column productDetailGallery" style="height:<?=$height * 80 + 230?>px">
    <div class="mainImg">
        <?php foreach ($gallery as $gI):
            if(file_exists(GALL_DIR . $gI->file)){
        ?>
            <img src="<?=GALL . $gI->file?>" class="<?=$gI->id?>" alt="">
        <?php
            }
             endforeach; ?>
    </div>
    <div class="thumbs galleryListing">
        <?php
        $i = 0;
        foreach ($gallery as $g):
             if(file_exists(GALL_DIR . $g->file)){?>
                 <span class="imgContainer">
            <img src="<?=Th_S . $g->file?>" alt="" id="<?=$g->id?>" class="<?php if ($i == 0) {
                 }
                echo 'active';
            ?>"> </span>
            <?php
            $i++;
            ?>
        <?php } endforeach; ?>
    </div>
</section>
<section class="half column productDetails">
<div class="breadCrumb">
    <a href="<?=SITE_ROOT?>"><img src="<?=IMG?>home_icon.png" alt=""></a>
    <a href="<?=SITE_ROOT?>proizvodi">
        <?=trans('Proizvodi', 'Products')?>
    </a>
    <?php
    $path = array_reverse($path);
    foreach ($path as $categoryP) {
        ?>
        <a href="<?=SITE_ROOT?>proizvodi/<?=$categoryP->id?>/<?=urlSafe($categoryP->name_sr)?>"><?=trans($categoryP->name_sr, $categoryP->name_en)?></a>

        <?php } ?>

    <span><?=trans($product->name_sr, $product->name_en)?></span>
</div>
<h1><?=trans($product->name_sr, $product->name_en)?>
    <span><?php if ($man) {
        echo trans($man->name_sr, $man->name_en);
    }?></span>
</h1>

<div class="priceBox">
    <?php $price = price($product->id);
    if ($client) {
        if ($price) {
            $price = $price . trans(' rsd', ' eur');
            ?>
            <h2 class="priceHeadline"><?=trans("Cena", "Price") . ': ' . $price?></h2>
            <a href="<?=SITE_ROOT?>" class="bttn orangeBttn toBasket"
               id="<?=$product->id?>"><?=trans("Dodaj u korpu", "Add to basket")?></a>
            <?php
        }
    } else {
        ?>
        <?=checkiPlus($product->id); ?>
        <a href="<?=SITE_ROOT?>login" class="bttn orangeBttn"><?=trans("Ulogujte se", "Login")?></a>
        <? } ?>
    <a href="#" class="compare">Uporedi proizvode</a>

    <div id="resultAdd"></div>
</div>


<div class="tabs">
    <div id="tab1" class="tab active">
        <h2><?=trans('Opis proizvoda', 'Product Description')?></h2>

        <div class="productDescription">
            <p style="text-transform: none!important"><?=trans($product->desc_sr, $product->desc_en);
                //, '<br><strong><li><ul><ol><h2><h1><h4><h3>')?></p>
        </div>
    </div>
    <div id="tab5" class="tab">
        <h2><?=trans('Karakteristike', 'Characteristics')?></h2>

        <div class="productDescription">


            <table class="chTable" style="width:97%;margin:0 1em 1.5em 1em;padding-left:2em">

                <?php
                foreach ($chacCategories as $charC): /*?>

                    <tr>
                        <td colspan="2">
                            <strong style="padding:10px 0 5px 0"><?=trans($charC->category_name_sr, $charC->category_name_en)?></strong>
                        </td>

                    </tr>


                    <?php         */
                    $sql = "select *, (select characteristics_name_" . trans('sr', 'en') . " from characteristics where id = characteristics_id order by ordering ASC) as characteristics_id, (select ordering from characteristics where  id = characteristics_id) as ordering  from characteristics_value where productID = '" . $product->id . "' order by ordering ASC";

//                    select *, (select characteristics_name_sr from characteristics where id = characteristics_id order by ordering ASC ) as characteristics_id, (select ordering from characteristics where  id = characteristics_id) as ordering from characteristics_value where productID = '8' order by ordering
                    $characteristics = CharacteristicsValue::find_by_sql($sql);
                    foreach ($characteristics as $ch) {
                        if (strlen($ch->characteristics_id) > 0 && strlen(trans($ch->value_name_sr, $ch->value_name_en)) > 0) {
                            ?>
                            <tr>
                                <td><?=$ch->characteristics_id?></td>
                                <td><?=trans($ch->value_name_sr, $ch->value_name_en)?></td>
                            </tr>
                            <?
                        }
                    }
                endforeach;  ?>

                <?
//                $i = 0;
//                    if ($ch && count($ch) > 0) {
//                        foreach ($ch as $key => $value):
//                            if ($key != 'id' && $key != 'lang' && $key != 'productID' && $key != 'obligatory' && $key != 'similar') {
//                                if (($value != '0' && $value != '' && $value != 'NA')) {
//                                    echo '<tr><td width="45%" style="padding:0">  <strong data-icon="S" style="position:relative; margin-right:5px;top: -13px!important;"> ' . translateField($key) . '</strong>:</td>
//                                    <td><span style="float:left;"> ' . $value . '<span></td>';
//                                    if ($i == 1) {
//                                        echo '</tr>';
//                                        $i = 0;
//                                    } else {
//                                        $i++;
//                                    }
//                                }
//                            }
//                        endforeach;
//                    }
//                     echo '<pre>';
//                     print_r(CharacteristicsCategory::find_all());
//                     echo '</pre>';
                //   $characteristicsCategory = CharacteristicsCategory::find_all();
                // print_r($characteristicsCategory);
//                $charValuesLeft = array();
//                $charValuesRight = array();
                /* ?>
                            <tr>
                                <?php foreach ($characteristicsCategory as $charCat) {
                                $characteristics = Characteristics::find_by_category($charCat);
                                print_r($characteristics);
                                ?>
                                <td><?= trans($charCat->category_name_sr, $charCat->category_name_en); ?></td>
                                        <td><?
                                $charValuesLeft = CharacteristicsValue::find_by_id($charCat->id);
                                foreach ($charValuesLeft as $charValLeft) {
                                    //ZASTOOOOOOOOOO?!?!?!?!??!?!?!?!
                                    trans($charValLeft->value_name_sr, $charValLeft->value_name_en);
                                    ?>
                                            </td>
                                    </tr>
                                    <?
                                }
                            }
                               */
                /*
                ?>

            </table>
            <table>
                <?
                $charValuesRight = $charValuesLeft;
                if (count($characteristicsCategory) > 0) {
                    foreach ($characteristicsCategory as $charCat) {
                        ?>
                        <td><?  trans($charCat->category_name_sr, $charCat->category_name_en);  ?></td>
                        <td>
                            <?foreach ($charValuesRight as $charValRight) {
                            trans($charValuesRight->value_name_sr, $charValuesRight->value_name_en); ?></td>
                            <?
                        }
                    }
                } */?>
            </table>
            <div class="separatorDown"></div>

        </div>
    </div>
    <div id="tab2" class="tab">
        <h2><?=trans('Brošure', 'Brochures')?></h2>
        <ul class="driversList">
            <?php
            if ($instructions) {
                foreach ($instructions as $in):
                    if ($in->file != '') {
                        $link = SITE_ROOT . 'instructionDownload/' . $in->file;
                    } else {
                        $link = '#';
                    }
                    ?>
                    <li>
                        <h4 data-icon="~">
                            <a href="<?=$link?>" target="_blank"><?=trans($in->name_sr, $in->name_en)?></a>
                        </h4>
                        <?php if ($in->file != '') {
                        ?>
                        <a class="bttn" href="<?=$link?>" target="_blank">Download</a>
                        <?php }  ?>
                        <p>
                            <?=trans('Format: ', 'Format: ') . $in->version . ' ' . $in->size?>
                            &nbsp;&nbsp;<?php if ($in->releaseDate > 0) {
                            echo '|&nbsp;&nbsp;' . trans('Datum: ', 'Date: ') . formatDate($in->releaseDate) . " | " . $in->size;
                        }?>
                        </p>
                        <?php if ($in->desc_sr != '') { ?>
                        <p><?=trans($in->desc_sr, $in->desc_en)?></p>
                        <?php
                    }
                        ?>
                    </li>
                    <?php endforeach;
            }?>
        </ul>
    </div>
    <div id="tab3" class="tab">
        <h2><?=trans('Drajveri', 'Drivers')?></h2>
        <ul class="driversList">
            <?php
            if ($drivers) {
                foreach ($drivers as $d):
                    if ($d->file != '') {
                        $link = SITE_ROOT . 'driversDownload/' . $d->file;
                    } else {
                        $link = '#';
                    }
                    ?>
                    <li>
                        <h4 data-icon="~">
                            <a href="<?=$link?>" target="_blank"><?=trans($d->name_sr, $d->name_en)?></a>
                        </h4>

                        <p>
                            <?=trans('Verzija: ', 'Version: ') . $d->version?>
                            &nbsp;&nbsp;<?php if ($d->releaseDate > 0) {
                            echo '|&nbsp;&nbsp;' . trans('Datum: ', 'Date: ') . formatDate($d->releaseDate) . " | " . $d->size;
                        }?>
                        </p>
                        <?php if ($d->desc_sr != '') { ?>
                        <p><?=trans($d->desc_sr, $d->desc_en)?></p>
                        <?php } ?>
                        <a class="bttn" href="<?=$link?>" target="_blank">Download</a>
                    </li>
                    <?php endforeach;
            }?>
        </ul>
    </div>
    <div id="tab4" class="tab productListing">
        <h2><?=trans('Dodatna oprema', 'Additional Equipments')?></h2>
        <?php foreach ($optional as $opt): ?>
        <a href="<?=SITE_ROOT?>proizvod/<?=$opt->id?>/<?=urlSafe(trans($opt->name_sr, $opt->name_en))?>">
            <span class="title"><?=trans($opt->name_sr, $opt->name_en)?></span>
                    <span>
                        <img src="<?=Th_S?><?=getGallery($opt->id, 'product')?>"
                             alt="<?=trans($opt->name_sr, $opt->name_en)?>">
                    </span>
        </a>

        <?php endforeach; ?>
    </div>
</div>
<p>

</p>
</section>
<?php /*
  *  <section class="one-third column drivers">
        <? for ($i = 0; $i < 5; $i++) { ?>
        <a href="<?=SITE_ROOT?>">
            <img src="<?=GALL?>drivers1.png" alt="">
            <span>Preuzmi drajver i softver
za kopir/print sisteme</span>
        </a>
        <?php } ?>
    </section>
  * */; ?>
<section class="tabsGallery">
    <article>
        <?php
        foreach ($promo as $p):
            $i = 0;
            ?>
            <section class="half column productInfo" style="overflow:hidden; float:left;height:330px;">
                <a href="<?=SITE_ROOT?>promo/<?=$p->id?>/<?=urlSafe($p->name_sr)?>">
                    <h2><?=trans($p->name_sr, $p->name_en)?></h2></a>

                <p><?=trunc(strip_tags(trans($p->desc_sr, $p->desc_en)), 150)?></p>
                <?php
                $img = array_shift(Gallery::find_by_sql("select * from gallery where refID = '{$p->id}' and type = 'promotion' LIMIT 1"));
                if ($img) {
                    $imgL = $img->file;
                } else {
                    $imgL = 'printer1.jpg';
                }
                ?>
                <a href="<?=SITE_ROOT?>promo/<?=$p->id?>/<?=urlSafe($p->name_sr)?>"><img
                        src="<?=GALL . 'thumbsM/' . $imgL?>" alt=""></a>

                <?php if ($i != 1) {
                echo '<div class="separatorRight"></div>';
            } ?>
            </section>
            <?php
            $i++;
        endforeach; ?>
    </article>
    <article class="one-third column">
        <?php /*<img src="<?=GALL?>magcolor.jpg" alt="">*/ ?>
    </article>

    <article class="products">
        <h3 class="similarProducts" style="background:#fff"><?=trans('Slični proizvodi', 'Related Products')?></h3>

        <div class="separatorDown"></div>
        <?php
        foreach ($simProducts as $sPr):
            ?>
            <a href="<?=SITE_ROOT?>proizvod/<?=$sPr->id?>/<?=urlSafe(trans($sPr->name_sr, $sPr->name_en))?>">

                <h4><?=trans($sPr->name_sr, $sPr->name_en)?></h4>
                <img src="<?=Th_M?><?=getGallery($sPr->id, 'product')?>" alt="<?=trans($sPr->name_sr, $sPr->name_en)?>">

                <div class="separatorRight"></div>
            </a>
            <?php
        endforeach;
        ?>
    </article>
</section>
<section>
    <?php
    $i = 0;
    foreach ($news as $n) {
        ?>
        <section class="half column productInfo" style="min-height: 17em;">
            <h2><?=trans($n->title, $n->title_en)?></h2>

            <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
            <a href="<?=SITE_ROOT?>aktuelnosti/<?=$n->id?>/<?=urlSafe($n->name_sr)?>"><img
                    src="<?=Th_S . $galleryImages[$i]?>" alt=""></a>

            <div class="separatorRight"></div>
        </section>
        <?php
        $i++;
    } ?>
</section>

</section>