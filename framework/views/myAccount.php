<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">

    <nav>
        <a href="<?=SITE_ROOT?>profil"><?=trans('Izmeni profil', 'Change profile')?></a>
        <a href="<?=SITE_ROOT?>contact-support"><?=trans("Kontaktiraj podršku", 'Contact support')?></a>
    </nav>

    <section class="partnerInfo pageInfo profileInfo" style="min-height: 450px;" >
        <h1><?=$client->name?></h1>
        <?php if($session->message()){
            echo '<p>'.$session->message().'</p>';
        } ?>
        <section class="info">
            <h2><?=trans('Aktivne promocije', 'Promotions')?></h2>

            <?php foreach ($promos as $promo) {
            $g = getImage($promo->id, 'promotion', 1);
            ?>
            <article>
                <span class="img"><img src="<?=Th_S . $g?>" alt=""/></span>
                <h4>
                    <a href="<?=SITE_ROOT?>promo/<?=$promo->id?>/<?=urlSafe(trans($promo->name_sr, $promo->name_en))?>">

                        <?=trans($promo->name_sr, $promo->name_en)?></a></h4>

                <p><?=trunc(strip_tags(trans($promo->desc_sr, $promo->desc_en)), 100)?>
                    <a href="<?=SITE_ROOT?>promo/<?=$promo->id?>/<?=urlSafe(trans($promo->name_sr, $promo->name_en))?>"
                       class="inline"><?=trans('Detaljnije', 'Details')?></a>
                </p>
            </article>
            <? } ?>

        </section>
        <section class="previousOrder">
            <h3><?=trans('Poslednje narudžbine', 'Recent orders')?></h3>
            <article>
                <?php foreach ($prevOrders as $pO): ?>
                <h4><?=date('d.m.y', $pO->orderDate)?> - <a
                        href="<?=SITE_ROOT?>order-info/<?=alphaID(intval($pO->orderID))?>"><?=trans('Br. ', 'No. ') . alphaID(intval($pO->orderID))?></a></h4>
                <p><?=count(explode(',', $pO->products))?> <?=trans('proizvoda', ' products')?></p>
                <?php endforeach; ?>
            </article>
            <a href="<?=SITE_ROOT?>orders-history" class="bttn" style="position:relative;top:.5em;"><?=trans('Kompletna istorija', 'Full history')?></a>
        </section>

    </section>
    <section class="tabsGallery similarProducts">
        <article class="products" style="box-shadow: 0 0 0 0 #fff;border:none;">
            <h3 class="similarProducts"><?=$productTitle?></h3>

            <div class="separatorDown"></div>

            <?php
            foreach ($simProducts as $sPr):
                ?>
                <a href="<?=SITE_ROOT?>proizvod/<?=$sPr->id?>/<?=urlSafe(trans($sPr->name_sr, $sPr->name_en))?>">

                    <h4><?=trans($sPr->name_sr, $sPr->name_en)?></h4>
                    <img src="<?=Th_M?><?=getGallery($sPr->id, 'product')?>"
                         alt="<?=trans($sPr->name_sr, $sPr->name_en)?>">

                    <div class="separatorRight"></div>
                </a>
                <?php
            endforeach;
            ?>
        </article>
    </section>
    <?php /*; ?>
    <section class="partnerInfo noPadding">
        <article class="one-third column">
            <h2><?=trans('Benefiti', 'Benefits')?></h2>

            <p>
                Povoljna nabavna cena - korisnik odmah    postaje vlasnik uređaja uz izuzetno povoljne finansijske uslove.
                Povoljna cena otiska - transparentno izražena i bez ikakvih skrivenih troškova.
                Isporuka uređaja, instalacija i obuka operatera na lokaciji korisnika.
                Tehnička Podrška za vreme i nakon prestanka trajanja paketa.
                Zamenski uređaj u ekstremnim uslovima za koje su potrebni servisni zahvati dužeg vremenskog intervala.
            </p>
            <img src="<?=GALL?>hp.jpg" class="alignLeft" alt="">
            <div class="separatorRight"></div>
        </article>
        <article class="one-third column">
            <h2><?=trans('Paketi', 'Packages')?></h2>

            <p>Paketi se biraju prema potrebama korisnika, a ukoliko među ponuđenim ne pronađete odgovarajući ili ukoliko imate eventualnih nejasnoća oko odabira, molimo Vas da popunite upitnik ili da nas direktno kontaktirate na mail: office@originalgrupa.com

                Konsultantski tim Originala će isti pažljivo razmotriti i u dogovoru sa Vama preporučiti postojeći ili napraviti novi paket samo za Vas.
            </p>
            <img src="<?=GALL?>hp2.jpg" class="alignCenter" alt="">

            <div class="separatorRight"></div>
        </article>
        <article class="one-third column">
            <h2><?=trans('Akcija', 'Action')?></h2>

            <p>Paketi se biraju prema potrebama korisnika, a ukoliko među ponuđenim ne pronađete odgovarajući ili ukoliko imate eventualnih nejasnoća oko odabira, molimo Vas da popunite upitnik ili da nas direktno kontaktirate na mail: office@originalgrupa.com
            </p>
            <img src="<?=GALL?>hp3.jpg" class="alignRight" alt="">

        </article>
    </section>

    <img src="<?=IMG?>printerFooter.png" alt="" class="homeMain" >

    <section class="half column productInfo">
        <h2>showroom</h2>

        <p>Posetite naš showroom gde ćemo Vas upoznati sa mogućnostima uređaja koje nudimo. Prezentaciju možete zakazati
            tako što ćete popuniti formular nakon čega će Vas kontaktirati naši konsultanti.</p>
        <a href=""><img src="<?=IMG?>showroom.jpg" alt=""></a>

        <div class="separatorRight"></div>
    </section>
    <section class="half column productInfo">
        <h2>total care</h2>

        <p>Za potpuni komfor u radu! Naka Vaši uređaji uvek budu u besprekornom stanju.</p>
        <a href=""><img src="<?=IMG?>totalcare.jpg" alt="" style="top:1.395em"></a>
    </section>
 <?php */
    ; ?>
</section>
<section class="sixteen columns home myAccountNews">
    <h3 class="similarProducts"><?=trans('Aktuelnosti', 'Actualities')?></h3>
    <div class="separatorDown"></div>
    <?php
    $i = 0;
    foreach ($news as $n) {
        ?>
        <section class="one-third column" style="max-height: 20em; overflow:hidden">
            <h2><?=trans($n->title, $n->title_en)?></h2>
            <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
            <a href="<?=SITE_ROOT?>aktuelnost/<?=$n->id?>/<?=urlSafe(trans($n->title, $n->title_en))?>"><img src="<?=Th_S.$galleryImages[$i]?>" alt=""></a>
            <?php if ($i < 2) { ?>
            <div class="separatorRight"></div>
            <?php } ?>
        </section>
        <?php
        $i++;
    } ?>
</section>