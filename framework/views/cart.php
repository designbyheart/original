<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="columns product">

    <h1 style="margin-left:1em; margin-top:1em;">
        <?=trans('Moja korpa', 'My cart')?>
    </h1>

    <div class="separatorDown"></div>

    <?php if(count($selectedProducts)>0) { ?>

    <form action="<?=SITE_ROOT?>saveCart" method="post" class="cartForm" style="clear:both">
        <?php $priceCount = 0; ?>
    <table class="cartTable">
        <thead>
        <tr>
            <th width="15%">
                <?=trans('Serijski broj', "Product ID")?>
            </th>
            <th width="35%"><?=trans('Ime proizvoda', "Product name")?></th>
            <th><?=trans('Cena', "Price")?></th>
            <th width="10%">
                <?=trans('Količina', "Number of items")?>
            </th>
            <th width="5%">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $filteredA = array_unique($selectedProducts);
            foreach ($filteredA as  $pID) {
                $p = Product::find_by_id($pID);
                $i = 0;
                foreach($selectedProducts as $pS){
                    if($pS == $pID){
                        $i++;
                    }
                }
                $price = price($p->id);
                $priceCount+= ($price*$i);
                ?>
        <tr id="cartItem<?=$p->id?>">
            <td width="10%"><?=$p->plD?></td>
            <td class="lefTAl"><?=trans($p->name_sr, $p->name_en)?></td>
            <td><span class="price"><?=$price?></span> </td>
            <td ><input type="text" name="numberOfItems[<?=$p->id?>]" value="<?=$i?>" id="item<?=$p->id?>"></td>

            <td>
                <a href="<?=SITE_ROOT?>" rel="<?=$p->id?>"  class="removeItem">
                    <img src="<?=SITE_ROOT?>assets/images/close.png" alt="<?=trans('Ukloni', "Cancel")?>">
                </a>
            </td>
        </tr>
            <?php  } ?>
        </tbody>
    </table>
        <div class="cartFooter">
            <input type="hidden" name="products" value="<?=Log::usersProducts($_SESSION['clientID'])?>">
            <label><span><?=trans('Ukupno: ',  'Total Amount')?></span><input type="text" name="totalPrice"  value="<?=$priceCount?>" id="totalPrice"></label>
        </div>

        <button class="orderBttn" name="submitOrder"><?=trans("Poruči proizvode", "Order products")?></button>
        <button class="removeCartItem cancelBttn" name="cancelOrder"><?=trans('Otkaži celu porudžbinu', "Cancel this order")?></button>
    </form>
    <?php
    }else{?>
    <section>
        <p style="padding-left: 2.5em;"><?=trans('Trenutno nemate izabranih proizvoda', "There is no selected products")?>
        <br><a href="<?=SITE_ROOT?>proizvodi"><?=trans('Proizvodi', 'Products')?></a></p>

    </section>
    <? } ?>

    <?php
    /*
  *  <section class="one-third column drivers">
        <? for ($i = 0; $i < 5; $i++) { ?>
        <a href="<?=SITE_ROOT?>">
            <img src="<?=GALL?>drivers1.png" alt="">
            <span>Preuzmi drajver i softver
za kopir/print sisteme</span>
        </a>
        <?php } ?>
    </section>
  * */; ?>
    <div class="separatorDown"></div>
    <section class="tabsGallery">
        <article>
            <?php
            foreach ($promo as $p):
                $i = 0;
                ?>
                <section class="half column productInfo"
                         style="overflow:hidden; float:left;height:330px;margin-bottom:2em;">
                    <a href="<?=SITE_ROOT?>promo/<?=$p->id?>/<?=urlSafe($p->name_sr)?>">
                        <h2><?=trans($p->name_sr, $p->name_en)?></h2></a>

                    <p><?=trunc(strip_tags(trans($p->desc_sr, $p->desc_en)), 150)?></p>
                    <?php
                    $img = array_shift(Gallery::find_by_sql("select * from gallery where refID = '{$p->id}' and type = 'promotion' LIMIT 1"));
                    if ($img) {
                        $imgL = $img->file;
                    } else {
                        $imgL = 'printer1.jpg';
                    }
                    ?>
                    <a href="<?=SITE_ROOT?>promo/<?=$p->id?>/<?=urlSafe($p->name_sr)?>"><img
                            src="<?=GALL . 'thumbsM/' . $imgL?>" alt=""></a>

                    <?php if ($i != 1) {
                    echo '<div class="separatorRight" style="right: -.5em;"></div>';
                } ?>
                </section>
                <?php
                $i++;
            endforeach; ?>
        </article>
    </section>
    <div class="separatorDown"></div>
    <section>
        <?php
        $i = 0;
        foreach ($news as $n) {
            ?>
            <section class="half column productInfo" style="min-height: 17em;">
                <h2><?=trans($n->title, $n->title_en)?></h2>

                <p><?=strip_tags(trans($n->content, $n->content_en))    ?></p>
                <a href="<?=SITE_ROOT?>aktuelnost/<?=$n->id?>/<?=urlSafe(trans($n->title, $n->title_en))?>"><img
                        src="<?=Th_S . $galleryImages[$i]?>" alt=""></a>

                <?php if($i<1){ ?>
                <div class="separatorRight" style="right:-1em"></div>
                    <?php } ?>
            </section>
            <?php
            $i++;
        } ?>
    </section>
</section>