<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 4:18 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">
    <nav>
        <?php if ($subMenus) {
        foreach ($subMenus as $sB) {
            ?>

            <a href="<?=$sB->url?>"><?=trans($sB->name_sr, $sB->name_en)?></a>
            <?php
        }
    } ?>
    </nav>
    <section class="partnerInfo pageInfo">
        <h1><?=trans($gP->name_sr, $gP->name_en)?></h1>

        <?php if ($gP->img != '') { ?>
        <img src="<?=GALL . $gP->img?>" style="z-index:0" alt="">
        <?php } ?>

        <?php
        if ($gP->counter > 0) {
            $endDate = $gP->counter;
            require_once(COUNTER);
        }?>

        <div class="content_text" style="color:#555!important;width:100%">
            <?=strip_tags(trans($gP->desc_sr1, $gP->desc_en1), '<p><br><strong><b><li><ol><ul><h2><h3><h5>'); ?>
        </div>

    </section>
    <section class="connectedPages">
        <?php
        $size = 'half';
        if (count($totalPages) > 2) {
            $size = 'one-third';
        } else {
            $size = 'half';
        }
        if (count($totalPages) > 0) {
            $i = 0;
            foreach ($totalPages as $key => $value) {
                $bP = GeneralPage::find_by_counter($value);
                if ($bP) {
                    ?>
                    <section class="<?=$size?> column" style="max-height: 20em; overflow:hidden;">
                        <h2 style="margin-bottom:1.8em"><a
                                href="<?=SITE_ROOT?>info/<?=$bP->pg_name?>/<?=urlSafe(trans($bP->name_sr, $bP->name_en))?>"><?=trans($bP->name_sr, $bP->name_en)?></a>
                        </h2>
                        <?php if ($bP->counter > 0) {
                        $endDate = $bP->counter;
                        require_once(COUNTER);?>
                        <div><?=trunc(trans($bP->desc_sr, $bP->desc_en), 200)?></div>

                        <?php
                    } else {
                        ?>
                        <p><?=trunc(strip_tags(trans($bP->desc_sr, $bP->desc_en)), 200)?></p>
                        <a href="<?=SITE_ROOT?>aktuelnost/<?=$bP->id?>/<?=urlSafe(trans($bP->name_sr, $bP->name_en))?>">
                            <img src="<?=Th_S . $galleryImages[$i]?>" alt=""></a>
                        <?php } ?>

                        <?php if ($i < 2) { ?>
                        <div class="separatorRight"></div>
                        <?php } ?>
                    </section>
                    <?php
                }
            }
        }    ?>
    </section>
    <?

    /*; ?>
    <section class="partnerInfo noPadding">
        <article class="one-third column">
            <h2><?=trans('Benefiti', 'Benefits')?></h2>

            <p>
                Povoljna nabavna cena - korisnik odmah    postaje vlasnik uređaja uz izuzetno povoljne finansijske uslove.
                Povoljna cena otiska - transparentno izražena i bez ikakvih skrivenih troškova.
                Isporuka uređaja, instalacija i obuka operatera na lokaciji korisnika.
                Tehnička Podrška za vreme i nakon prestanka trajanja paketa.
                Zamenski uređaj u ekstremnim uslovima za koje su potrebni servisni zahvati dužeg vremenskog intervala.
            </p>
            <img src="<?=GALL?>hp.jpg" class="alignLeft" alt="">
            <div class="separatorRight"></div>
        </article>
        <article class="one-third column">
            <h2><?=trans('Paketi', 'Packages')?></h2>

            <p>Paketi se biraju prema potrebama korisnika, a ukoliko među ponuđenim ne pronađete odgovarajući ili ukoliko imate eventualnih nejasnoća oko odabira, molimo Vas da popunite upitnik ili da nas direktno kontaktirate na mail: office@originalgrupa.com

                Konsultantski tim Originala će isti pažljivo razmotriti i u dogovoru sa Vama preporučiti postojeći ili napraviti novi paket samo za Vas.
            </p>
            <img src="<?=GALL?>hp2.jpg" class="alignCenter" alt="">

            <div class="separatorRight"></div>
        </article>
        <article class="one-third column">
            <h2><?=trans('Akcija', 'Action')?></h2>

            <p>Paketi se biraju prema potrebama korisnika, a ukoliko među ponuđenim ne pronađete odgovarajući ili ukoliko imate eventualnih nejasnoća oko odabira, molimo Vas da popunite upitnik ili da nas direktno kontaktirate na mail: office@originalgrupa.com
            </p>
            <img src="<?=GALL?>hp3.jpg" class="alignRight" alt="">

        </article>
    </section>

    <img src="<?=IMG?>printerFooter.png" alt="" class="homeMain" >

    <section class="half column productInfo">
        <h2>showroom</h2>

        <p>Posetite naš showroom gde ćemo Vas upoznati sa mogućnostima uređaja koje nudimo. Prezentaciju možete zakazati
            tako što ćete popuniti formular nakon čega će Vas kontaktirati naši konsultanti.</p>
        <a href=""><img src="<?=IMG?>showroom.jpg" alt=""></a>

        <div class="separatorRight"></div>
    </section>
    <section class="half column productInfo">
        <h2>total care</h2>

        <p>Za potpuni komfor u radu! Naka Vaši uređaji uvek budu u besprekornom stanju.</p>
        <a href=""><img src="<?=IMG?>totalcare.jpg" alt="" style="top:1.395em"></a>
    </section>
 <?php */; ?>
</section>