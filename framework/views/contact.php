<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 5:49 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns contact">
    <h1 class="remove-bottom" style="margin-top: 40px">
        <?=trans('Kontakt', 'Contact')?>
    </h1>

    <div class="contactLeft">
        <div class="mapContainer">
            <div class="map">
                <iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                        src="https://maps.google.com/maps/ms?msa=0&amp;msid=206729850258532211693.0004c85bedec0591c9ffb&amp;ie=UTF8&amp;t=m&amp;iwloc=0004c85beded79b3d8312&amp;ll=44.812596,20.425231&amp;spn=0,0&amp;output=embed"></iframe>
                <br/>
                <small>View <a
                        href="https://maps.google.com/maps/ms?msa=0&amp;msid=206729850258532211693.0004c85bedec0591c9ffb&amp;ie=UTF8&amp;t=m&amp;iwloc=0004c85beded79b3d8312&amp;ll=44.812596,20.425231&amp;spn=0,0&amp;source=embed"
                        style="color:#0000FF;text-align:left">Original d.o.o.</a> in a larger map
                </small>
            </div>
        </div>

        <div id="contactForm">
            <?php if (!isset($_GET['message'])) { ?>

            <form action="<?=SITE_ROOT?>sendMessage" class="contact" method="post">
                <label for="name"><?=trans('Ime', 'Name')?>
                    <input name="name" type="text">
                    <span class="required reqname">Obavezno polje</span>
                </label>
                <label for="email">E-mail
                    <input type="text" name="email">
                    <span class="required reqemail">Obavezno polje</span>
                </label>
                <label for="phone"><?=trans('Telefon', 'Phone')?>
                    <input type="text" name="phone">
                </label>
                <label for="message"><?=trans('Text poruke', 'Message')?>
                    <textarea name="message"></textarea>
                    <span class="required reqmessage">Obavezno polje</span>
                </label>
                <input type="submit" name="sendMessage" id="sendMessage" class="bttn"
                       value="<?=trans('Pošalji poruku', 'Send message')?>">
            </form>
            <?php
        } else {
            switch ($_GET['message']) {
                case 'poruka-poslata':
                case 'message-sent':
                    echo '<h2>' . trans('Poruka poslata', 'Message sent') . '</h2>';
                    echo '<p>' . trans('Vaša poruka je uspešno poslata.<br><br>Hvala na poverenju.<br>Omnipromet tim', 'Your message has been successfully sent. <br> Thank you for your trust. <br> Omnipromet team') . '</p>';
                    break;
                case 'poruka-nije-poslata':
                case 'message-not-sent':
                    echo '<h2>' . trans('Poruka nije poslata', 'Message has not sent') . '</h2>';
                    echo '<p>' . trans('Došlo je do greške u slanju vaše poruke.<br>Molimo, pokušajte ponovo', 'There was an error in sending your message. <br> Please try again') . '</p>';
                    break;

            }
        }
            ?>
        </div>
        <div class="sharingContact">
            <?=skype(Settings::find_by_name("skype"))?>
        </div>
</section>