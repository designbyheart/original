<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">
    <nav>
        <a href="<?=SITE_ROOT?>my-account?>"><?=trans('Povratak na profil', 'Back to my account')?></a>
    </nav>
    <section class="partnerInfo pageInfo profileInfo">
        <h1><?=trans('Kontaktirajte podršrku', 'Contact support')?></h1>
        <article class="orderInfo">

            <form action="<?=SITE_ROOT?>sendMessage" class="contactMessage" method="post">
                <fieldset>
                    <input type="hidden" name="messageType" value="contactSupportProfile">

                    <label><?=trans('Unesite vaše pitanje, save, kritiku..', 'Enter you question, suggestion, critique..')?><br>
                        <textarea name="messageText"></textarea>
                    </label>
                    <button type="submit"><?=trans('Pošalji poruku', 'Send message')?></button>
                </fieldset>
                <fieldset></fieldset>

            </form>
        </article>
    </section>
</section>