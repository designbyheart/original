<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 10:36 AM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="columns product" style="width:95%;">

    <h1 style="margin-left:1em; margin-top:1em;">
        <?=trans('Pordžbina je poslata', 'Order is sent')?>
    </h1>

    <div class="separatorDown" style="width: 98%;"></div>


    <section class="orderDetails">
    <h4>Detalji porudžbine</h4>

    <table style="width:95%;">
        <tr>
            <td>Datum</td>
            <td><strong><?=date("d.m.Y", $order->orderDate);?></strong></td>
        </tr>
        <tr>
            <td>Vrednost porudžbine</td>
            <td><strong><?=$order->totalPrice?></strong></td>
        </tr>
        <tr>
            <td>Izabrani proizvodi</td>
            <td width="60%"><strong><?php
                $products = explode(',',$order->products);
                foreach($products as $pID){?>
                    <a href="<?=SITE_ROOT?>product/<?=$pID?>/<?=urlSafe(Product::product_name($pID))?>"><?=urlSafe(Product::product_name($pID))?></a>
                <? }?></strong></td>
        </tr>
    </table>
        <a href="<?=SITE_ROOT?>proizvodi" class="continueShopping">Nastavi kupovinu</a>
    </section>


    <?php /*
  *  <section class="one-third column drivers">
        <? for ($i = 0; $i < 5; $i++) { ?>
        <a href="<?=SITE_ROOT?>">
            <img src="<?=GALL?>drivers1.png" alt="">
            <span>Preuzmi drajver i softver
za kopir/print sisteme</span>
        </a>
        <?php } ?>
    </section>
  * */; ?>
</section>

