<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<section class="sixteen columns partner general_page">

    <nav>
        <a href="<?=SITE_ROOT?>contact-support/<?=alphaID($order->orderID)?>"><?=trans('Kontaktirajte podršrku', 'Contact support')?></a>
        <?php if($order->t); ?>
        <?php if($order && $order->status== 4){ ?>
        <a href="<?=SITE_ROOT?>confirm-delivery/<?=$order->orderID?>"
           class="confirmDelivery"><?=trans('Potvrdite preuzimanje', 'Confirm delivery')?></a>
         <?php } ?>
    </nav>

    <section class="partnerInfo pageInfo profileInfo">
        <h1><?=trans('Narudžbina id: ', 'Order ID: ') . alphaID($order->orderID)?></h1>
        <a href="#" class="changeProfile"><?=trans('Datum: ', "Date: ") . date("d.M.Y.", $order->orderDate)?></a>

        <article class="orderInfo">
            <table>

                <tr>
                    <td>Status</td>
                    <td><span class="statusInfo<?=$order->status?>"><?=$order->status()?></span></td>
                </tr>
                <tr>
                    <td><?=trans('Vrednost porudžbine', 'Order value')?>:</td>
                    <td><?=$order->totalPrice . trans(' rsd', ' &euro;')?></td>
                </tr>
                <tr>
                    <td><?=trans('Adresa', 'Address')?></td>
                    <td>
                        <?=$address->address1 . ' ' . $address->city . ' ' . $address->country?>
                    </td>
                </tr>

                <tr>
                    <td><?=trans('Proizvodi', 'Products')?></td>
                    <td></td>
                </tr>
                <?php
                $prIDs = array_filter(explode(',', $order->products));
                $prID = array_unique($prIDs);
                foreach ($prID as $p):
                    $tmp = array_count_values($prIDs);
                    $cnt = $tmp[$p];
                    $sum = '';
                    if ($cnt > 0) {
                        $sum = $cnt;
                    }

                    $product = Product::find_by_id($p);
                    ?>
                    <tr>
                        <td><?=$sum?> X</td>
                        <td>
                            <a href="<?=SITE_ROOT?>product/<?=$product->id?>"><?=trans($product->name_sr, $product->name_en)?></a>
                        </td>

                    </tr>
                    <?php endforeach; ?>
            </table>
        </article>
    </section>
</section>