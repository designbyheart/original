<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
/*
 *        <?php
        if (!isset($_GET['cat'])) {
            echo trans($gP->desc_sr, $gP->desc_en);
        }?>
*/
?>
<section class="columns productsPage">


    <h1 class="padding10">
        <?php

            echo trans('Aktivne promocije proizvoda', 'Active product promotions');
        ?>
    </h1>

    <p style="float:left; clear:left;padding:.7em 0 2em 2.4em">Ukupno aktivnih promocija: <?=count($promotions)?></p>

    <section class="productGallery productListing">
        <?php
            if(count($promotions)>1){
            foreach ($promotions as $p):
                //  $p = Product::find_by_id($pID);
//                if (isset($p) && $p && findImageForProduct($p->id)) {
                    $pImg = findImageForProduct($p->id); ?>
                    <a href="<?=SITE_ROOT . trans('promo', 'promo')?>/<?=$p->id?>/<?=urlSafe(trans($p->name_sr, $p->name_en))?>" class="productItem">
                        <span class="title"><?=trans($p->name_sr, $p->name_en)?></span>
                        <span class="img"><img src="<?=Th_M . $pImg?>" alt=""></span>
                    </a>
                    <?php
//                }
            endforeach;
            }?>
    </section>


    <div class="separatorDown"></div>
    <section class="half column productInfo">

        <h2>showroom</h2>

        <p>Posetite naš showroom gde ćemo Vas upoznati sa mogućnostima uređaja koje nudimo. Prezentaciju možete zakazati
            tako što ćete popuniti formular nakon čega će Vas kontaktirati naši konsultanti.</p>
        <a href=""><img src="<?=IMG?>showroom.jpg" alt=""></a>

        <div class="separatorRight"></div>
    </section>
    <section class="half column productInfo">
        <h2>total care</h2>

        <p>Za potpuni komfor u radu! Naka Vaši uređaji uvek budu u besprekornom stanju.</p>
        <a href=""><img src="<?=IMG?>totalcare.jpg" alt="" style="top:1.395em"></a>
    </section>
</section>