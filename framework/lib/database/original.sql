#
# Encoding: Unicode (UTF-8)
#


CREATE TABLE `administrator` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `e_mail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tel` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `role` int(2) DEFAULT NULL,
  `permissions` int(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;


CREATE TABLE `attributeSet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `details` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `active` int(11) NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `parent_cat` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


CREATE TABLE `characteristics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(128) DEFAULT '0',
  `speedCopyA4Color` int(5) DEFAULT '0',
  `speedCopyA4Black` int(5) DEFAULT '0',
  `speedCopyA3Color` int(5) DEFAULT '0',
  `speedCopyA3Black` int(5) DEFAULT '0',
  `firstCopyColor` int(5) DEFAULT '0',
  `firstCopyBlack` int(5) DEFAULT '0',
  `heatingTime` int(11) DEFAULT '0',
  `copyResolution` varchar(128) DEFAULT NULL,
  `gradation` varchar(128) DEFAULT NULL,
  `mutliplyCopy` varchar(128) DEFAULT NULL,
  `formatOrig` varchar(128) DEFAULT NULL,
  `zoom` text,
  `copyFunction` text,
  `printSpeedA4Color` int(5) DEFAULT '0',
  `printSpeedA4Black` int(5) DEFAULT '0',
  `printSpeedA3Color` int(5) DEFAULT '0',
  `printSpeedA3Black` int(5) DEFAULT '0',
  `firstCopyPrintColor` int(5) DEFAULT '0',
  `firstCopyPrintBlack` int(5) DEFAULT '0',
  `printRes` varchar(128) DEFAULT NULL,
  `processor` varchar(256) DEFAULT NULL,
  `driver` text,
  `os` text,
  `fonts` text,
  `printFunctions` text,
  `scanSpeedColor` varchar(256) DEFAULT '0',
  `scanSpeedBlack` varchar(256) DEFAULT '0',
  `scanRes` varchar(128) DEFAULT NULL,
  `scanMethods` text,
  `scanFormats` text,
  `scanDestination` varchar(256) DEFAULT NULL,
  `faxStandard` varchar(256) DEFAULT NULL,
  `faxTransmit` varchar(256) DEFAULT NULL,
  `lang` int(1) DEFAULT '0',
  `faxRes` varchar(128) DEFAULT NULL,
  `faxCompression` varchar(128) DEFAULT NULL,
  `faxModem` varchar(128) DEFAULT NULL,
  `faxDestination` varchar(128) DEFAULT NULL,
  `faxFunctions` text,
  `hddMax` int(11) DEFAULT '0',
  `memory` varchar(128) DEFAULT NULL,
  `documentsMax` varchar(128) DEFAULT '0',
  `storageType` varchar(256) DEFAULT NULL,
  `storageSystemType` varchar(256) DEFAULT NULL,
  `hddFunctions` varchar(256) DEFAULT NULL,
  `hdd` varchar(128) DEFAULT NULL,
  `compConnection` varchar(128) DEFAULT NULL,
  `networkProtocols` text,
  `paperFormat` text,
  `paperWeight` varchar(128) DEFAULT NULL,
  `inputCapacity` varchar(128) DEFAULT NULL,
  `outputCapacity` varchar(128) DEFAULT NULL,
  `monthly` varchar(128) DEFAULT NULL,
  `initialToner` varchar(32) DEFAULT NULL,
  `tonerDeclaration` varchar(256) DEFAULT NULL,
  `energyConsumption` varchar(128) DEFAULT NULL,
  `dimensions` varchar(128) DEFAULT NULL,
  `weight` int(32) DEFAULT '0',
  `security` text,
  `software` text,
  `users` text,
  `optional` text,
  `obligatory` text,
  `similar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;


CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(15) DEFAULT '0',
  `country` int(15) DEFAULT '0',
  `price` float DEFAULT '0',
  `percent` int(15) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `countryCode` varchar(5) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;


CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `company` varchar(256) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` text NOT NULL,
  `client_category` int(128) DEFAULT '0',
  `type` int(11) NOT NULL,
  `status` varchar(256) NOT NULL,
  `registered_date` date NOT NULL,
  `auth_code` varchar(256) NOT NULL,
  `updated` datetime NOT NULL,
  `address` varchar(256) NOT NULL,
  `phones` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `company_PIB` int(30) NOT NULL,
  `company_regNumber` int(30) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `notes` text NOT NULL,
  `logo` varchar(256) NOT NULL,
  `website` varchar(256) NOT NULL,
  `contactPerson` varchar(256) NOT NULL,
  `ip_address` varchar(256) NOT NULL,
  `contact_person` varchar(128) DEFAULT NULL,
  `currency` int(1) DEFAULT '0',
  `partner` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;


CREATE TABLE `customer_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_updated` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `id_order` int(128) DEFAULT '0',
  `status_order` int(11) DEFAULT '0',
  `id_product` int(128) DEFAULT '0',
  `id_promo` int(128) DEFAULT '0',
  `quantity` int(10) DEFAULT '0',
  `id_customer` int(128) DEFAULT '0',
  `ip_address` varchar(128) DEFAULT NULL,
  `contact_person` varchar(256) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `logo` varchar(256) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `notes` text,
  `company_regNumber` varchar(128) DEFAULT NULL,
  `company_PIB` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phones` varchar(128) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `registered_date` timestamp NULL DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `customerAddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `address1` text,
  `address2` text,
  `state` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `zip` varchar(128) DEFAULT NULL,
  `post` varchar(128) DEFAULT NULL,
  `note` text,
  `customerID` int(128) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


CREATE TABLE `driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) DEFAULT NULL,
  `name_en` varchar(256) DEFAULT NULL,
  `version` varchar(256) DEFAULT NULL,
  `desc_en` text,
  `desc_sr` text,
  `releaseDate` int(11) DEFAULT '0',
  `size` varchar(128) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `ordering` int(15) DEFAULT '0',
  `file` varchar(256) DEFAULT NULL,
  `products` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


CREATE TABLE `euro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT '0',
  `userID` int(15) DEFAULT '0',
  `amount` float DEFAULT '0',
  `amountEur` float DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;


CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(256) NOT NULL,
  `refID` int(11) NOT NULL,
  `type` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;


CREATE TABLE `general_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `counter` int(128) DEFAULT '0',
  `active` int(11) NOT NULL,
  `rightBoxID` int(15) DEFAULT '0',
  `rightBox` text,
  `centerBoxID` int(15) DEFAULT '0',
  `centerBox` text,
  `leftBoxID` int(15) DEFAULT '0',
  `leftBox` text,
  `counterEndMessage` text,
  `desc_sr1` longtext NOT NULL,
  `desc_sr2` longtext,
  `desc_sr3` longtext,
  `desc_en1` longtext NOT NULL,
  `desc_en2` longtext,
  `desc_en3` longtext,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `pg_name` varchar(128) NOT NULL,
  `homePage` varchar(128) DEFAULT NULL,
  `template` int(3) DEFAULT '0',
  `img` varchar(256) DEFAULT NULL,
  `imagePosition` int(1) DEFAULT '0',
  `column3Menu` int(1) DEFAULT '0',
  `column2Menu` int(1) DEFAULT '0',
  `column1Menu` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;


CREATE TABLE `instruction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) DEFAULT NULL,
  `name_en` varchar(256) DEFAULT NULL,
  `version` varchar(256) DEFAULT NULL,
  `desc_en` text,
  `desc_sr` text,
  `releaseDate` int(11) DEFAULT '0',
  `size` varchar(128) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `ordering` int(15) DEFAULT '0',
  `file` varchar(256) DEFAULT NULL,
  `products` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventTime` int(16) DEFAULT '0',
  `userID` int(15) DEFAULT '0',
  `note` text,
  `refID` int(15) DEFAULT '0',
  `type` varchar(128) DEFAULT NULL,
  `selectedProducts` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;


CREATE TABLE `mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL,
  `reftitle` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `orderID` varchar(128) DEFAULT NULL,
  `phone` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `messageContent` text NOT NULL,
  `refMessageContent` text NOT NULL,
  `receiver` varchar(256) DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `customer` int(15) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


CREATE TABLE `mainmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) DEFAULT NULL,
  `name_en` varchar(256) DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `url` text,
  `type` varchar(128) DEFAULT NULL,
  `css` text,
  `columnT` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `logo` varchar(256) NOT NULL,
  `country_sr` varchar(256) NOT NULL,
  `country_en` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `banner_home` int(1) DEFAULT '0',
  `type` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `banner` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL,
  `active` int(1) NOT NULL,
  `title_en` varchar(256) NOT NULL,
  `content_en` longtext NOT NULL,
  `seo_title_sr` text NOT NULL,
  `seo_title_en` text NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;


CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) DEFAULT '0',
  `textSrb` text,
  `textEn` text,
  `refID` varchar(128) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;


CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupID` int(128) DEFAULT '0',
  `templateID` int(128) DEFAULT '0',
  `updated` int(128) DEFAULT '0',
  `productID` int(128) DEFAULT '0',
  `amount` float DEFAULT '0',
  `changedBy` int(128) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;


CREATE TABLE `price_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateID` int(128) DEFAULT '0',
  `percentage` float DEFAULT '0',
  `name` varchar(128) DEFAULT NULL,
  `isMain` varchar(128) DEFAULT NULL,
  `parent` int(128) DEFAULT '0',
  `productID` int(128) DEFAULT '0',
  `tax` int(128) DEFAULT '0',
  `conversionType` int(3) DEFAULT '0',
  `calcType` int(128) DEFAULT '0',
  `updated` int(128) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;


CREATE TABLE `price_group_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateID` int(128) DEFAULT '0',
  `percentage` float DEFAULT '0',
  `name` varchar(128) DEFAULT NULL,
  `isMain` varchar(128) DEFAULT NULL,
  `parent` int(128) DEFAULT '0',
  `productID` int(128) DEFAULT '0',
  `conversionType` int(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `price_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `countryCode` varchar(128) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `tax` int(128) DEFAULT '0',
  `country` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `attributeSet` int(11) DEFAULT '0',
  `category` int(11) NOT NULL,
  `country` int(5) DEFAULT '0',
  `plD` varchar(128) NOT NULL DEFAULT '0',
  `model` varchar(128) DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `manufacturer` int(11) NOT NULL,
  `manufacturerName` varchar(128) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(1) DEFAULT '0',
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `size_sr` text NOT NULL,
  `size_en` text NOT NULL,
  `power_sr` text NOT NULL,
  `power_en` text NOT NULL,
  `power_type_sr` text NOT NULL,
  `power_type_en` text NOT NULL,
  `characteristics_sr` text NOT NULL,
  `characteristics_en` text NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `promo` int(1) DEFAULT '0',
  `price` float DEFAULT '0',
  `template` int(11) DEFAULT '0',
  `calcType` int(1) DEFAULT '0',
  `secondIDs` text,
  `lang` int(1) DEFAULT '0',
  `obligatory` text,
  `similar` text,
  `optional` text,
  `url` varchar(256) DEFAULT NULL,
  `pdv` float DEFAULT '0',
  `mainCatID` varchar(128) DEFAULT NULL,
  `distribID` varchar(128) DEFAULT NULL,
  `attributes` text,
  `countryTax` float DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plD` (`plD`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


CREATE TABLE `productDetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(128) DEFAULT '0',
  `value_sr` text,
  `value_en` text,
  `detailID` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;


CREATE TABLE `productDetailsSettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(128) DEFAULT NULL,
  `name_en` varchar(128) DEFAULT NULL,
  `parent` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;


CREATE TABLE `productOrder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerID` int(20) DEFAULT '0',
  `products` text,
  `address` int(128) DEFAULT '0',
  `ipAddress` varchar(128) DEFAULT NULL,
  `orderDate` int(128) DEFAULT '0',
  `status` int(2) DEFAULT '0',
  `totalPrice` float DEFAULT '0',
  `orderID` varchar(256) DEFAULT NULL,
  `modified` int(128) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;


CREATE TABLE `promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateStart` int(128) DEFAULT '0',
  `dateEnd` int(128) DEFAULT '0',
  `name_en` varchar(256) DEFAULT NULL,
  `name_sr` varchar(256) DEFAULT NULL,
  `desc_en` text,
  `desc_sr` text,
  `products` text,
  `discount` int(128) DEFAULT '0',
  `price` float DEFAULT '0',
  `active` int(1) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `subTitle_sr` varchar(256) DEFAULT NULL,
  `subTitle_en` varchar(256) DEFAULT NULL,
  `seo_title_sr` varchar(256) DEFAULT NULL,
  `seo_title_en` varchar(256) DEFAULT NULL,
  `seo_desc_en` text,
  `seo_desc_sr` text,
  `seo_keywords_en` text,
  `seo_keywords_sr` text,
  `fullWidth` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `searchTemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query` text,
  `name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


CREATE TABLE `settingsInfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `settingsName` varchar(128) DEFAULT NULL,
  `settingsValue` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `submenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` int(11) DEFAULT '0',
  `name_sr` varchar(256) DEFAULT NULL,
  `name_en` varchar(256) DEFAULT NULL,
  `linkSr` varchar(256) DEFAULT NULL,
  `linkEng` varchar(256) DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `active` int(1) DEFAULT '0',
  `permissions` text,
  `pageType` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


CREATE TABLE `tempProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` varchar(256) NOT NULL,
  `name_en` varchar(256) NOT NULL,
  `attributeSet` int(11) DEFAULT '0',
  `category` int(11) NOT NULL,
  `country` int(5) DEFAULT '0',
  `plD` varchar(128) NOT NULL DEFAULT '0',
  `model` varchar(128) DEFAULT NULL,
  `ordering` int(11) DEFAULT '0',
  `manufacturer` int(11) NOT NULL,
  `manufacturerName` varchar(128) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(1) DEFAULT '0',
  `desc_sr` longtext NOT NULL,
  `desc_en` longtext NOT NULL,
  `size_sr` text NOT NULL,
  `size_en` text NOT NULL,
  `power_sr` text NOT NULL,
  `power_en` text NOT NULL,
  `power_type_sr` text NOT NULL,
  `power_type_en` text NOT NULL,
  `characteristics_sr` text NOT NULL,
  `characteristics_en` text NOT NULL,
  `seo_title_sr` varchar(256) NOT NULL,
  `seo_title_en` varchar(256) NOT NULL,
  `seo_desc_sr` text NOT NULL,
  `seo_desc_en` text NOT NULL,
  `seo_keywords_sr` text NOT NULL,
  `seo_keywords_en` text NOT NULL,
  `updated` datetime NOT NULL,
  `created` datetime NOT NULL,
  `promo` int(1) DEFAULT '0',
  `price` float DEFAULT '0',
  `template` int(11) DEFAULT '0',
  `calcType` int(1) DEFAULT '0',
  `secondIDs` text,
  `lang` int(1) DEFAULT '0',
  `obligatory` text,
  `similar` text,
  `optional` text,
  `url` varchar(256) DEFAULT NULL,
  `pdv` float DEFAULT '0',
  `mainCatID` varchar(128) DEFAULT NULL,
  `distribID` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plD` (`plD`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;


CREATE TABLE `ticketsCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_sr` text,
  `name_en` varchar(128) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `visitLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(128) DEFAULT NULL,
  `user` int(128) DEFAULT '0',
  `time` int(128) DEFAULT '0',
  `ipAddress` varchar(128) DEFAULT NULL,
  `productID` int(128) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;




SET FOREIGN_KEY_CHECKS = 0;


INSERT INTO `administrator` (`id`, `username`, `password`, `first_name`, `last_name`, `e_mail`, `tel`, `role`, `permissions`) VALUES (1, 'pedja', '1858b78054f5328781ab9c1b3d30b6d3', 'Pedja', 'Jevtic', 'designbyheart@gmail.com', '', 2, 4), (12, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test', 'test', '', '', 4, 0);


INSERT INTO `attributeSet` (`id`, `name`, `details`) VALUES (1, 'TExt', '16,14,10,9,6,3,1'), (3, 'test2', '18,17,15,14,13,11,10,7,3,2');


INSERT INTO `category` (`id`, `name_sr`, `name_en`, `active`, `seo_title_sr`, `seo_title_en`, `seo_desc_sr`, `seo_desc_en`, `seo_keywords_sr`, `seo_keywords_en`, `parent_cat`, `ordering`) VALUES (2, 'Scanner', '', 1, '', '', '', '', '', '', 0, 0), (3, 'Malolitrazni&amp;Motori?!@#$$%^&amp;**', '', 1, '', '', '', '', '', '', 0, 0), (4, 'HP printer 2300', '', 1, '', '', '', '', '', '', 2, 0), (5, 'Crno Beli printeri', '', 1, '', '', '', '', '', '', 4, 0), (6, 'Multifunkcionalni uredjaji', '', 1, '', '', '', '', '', '', 0, 0), (7, 'Crno beli', '', 1, '', '', '', '', '', '', 6, 0);


INSERT INTO `characteristics` (`id`, `productID`, `speedCopyA4Color`, `speedCopyA4Black`, `speedCopyA3Color`, `speedCopyA3Black`, `firstCopyColor`, `firstCopyBlack`, `heatingTime`, `copyResolution`, `gradation`, `mutliplyCopy`, `formatOrig`, `zoom`, `copyFunction`, `printSpeedA4Color`, `printSpeedA4Black`, `printSpeedA3Color`, `printSpeedA3Black`, `firstCopyPrintColor`, `firstCopyPrintBlack`, `printRes`, `processor`, `driver`, `os`, `fonts`, `printFunctions`, `scanSpeedColor`, `scanSpeedBlack`, `scanRes`, `scanMethods`, `scanFormats`, `scanDestination`, `faxStandard`, `faxTransmit`, `lang`, `faxRes`, `faxCompression`, `faxModem`, `faxDestination`, `faxFunctions`, `hddMax`, `memory`, `documentsMax`, `storageType`, `storageSystemType`, `hddFunctions`, `hdd`, `compConnection`, `networkProtocols`, `paperFormat`, `paperWeight`, `inputCapacity`, `outputCapacity`, `monthly`, `initialToner`, `tonerDeclaration`, `energyConsumption`, `dimensions`, `weight`, `security`, `software`, `users`, `optional`, `obligatory`, `similar`) VALUES (5, 2, 123123, 0, 0, 0, 0, 0, 0, NULL, NULL, 'testing again', NULL, NULL, 'sfgafasdf', 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (6, 2, 123123, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (7, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (8, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (9, 3, 5, 0, 5, 0, 0, 0, 0, '12312', 'asdfasdf', NULL, 'asdfasf', NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'afdasdfasfd', 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (10, 3, 0, 5, 5, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (11, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (12, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (13, 6, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (14, 6, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (15, 4, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (16, 4, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (17, 5, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL), (18, 5, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);




INSERT INTO `country` (`id`, `name`, `countryCode`, `active`) VALUES (1, 'Afghanistan', 'AF', 1), (2, 'Aland Islands', 'AX', 1), (3, 'Albania', 'AL', 1), (4, 'Algeria', 'DZ', 1), (5, 'American Samoa', 'AS', 1), (6, 'Andorra', 'AD', 1), (7, 'Angola', 'AO', 0), (8, 'Anguilla', 'AI', 0), (9, 'Antarctica', 'AQ', 0), (10, 'Antigua And Barbuda', 'AG', 0), (11, 'Argentina', 'AR', 0), (12, 'Armenia', 'AM', 0), (13, 'Aruba', 'AW', 0), (14, 'Australia', 'AU', 0), (15, 'Austria', 'AT', 0), (16, 'Azerbaijan', 'AZ', 0), (17, 'Bahamas', 'BS', 0), (18, 'Bahrain', 'BH', 0), (19, 'Bangladesh', 'BD', 0), (20, 'Barbados', 'BB', 0), (21, 'Belarus', 'BY', 0), (22, 'Belgium', 'BE', 0), (23, 'Belize', 'BZ', 0), (24, 'Benin', 'BJ', 0), (25, 'Bermuda', 'BM', 0), (26, 'Bhutan', 'BT', 0), (27, 'Bolivia', 'BO', 0), (28, 'Bosnia And Herzegovina', 'BA', 0), (29, 'Botswana', 'BW', 0), (30, 'Bouvet Island', 'BV', 0), (31, 'Brazil', 'BR', 0), (32, 'British Indian Ocean Territory', 'IO', 0), (33, 'Brunei Darussalam', 'BN', 0), (34, 'Bulgaria', 'BG', 0), (35, 'Burkina Faso', 'BF', 0), (36, 'Burundi', 'BI', 0), (37, 'Cambodia', 'KH', 0), (38, 'Cameroon', 'CM', 0), (39, 'Canada', 'CA', 0), (40, 'Cape Verde', 'CV', 0), (41, 'Cayman Islands', 'KY', 0), (42, 'Central African Republic', 'CF', 0), (43, 'Chad', 'TD', 0), (44, 'Chile', 'CL', 0), (45, 'China', 'CN', 0), (46, 'Christmas Island', 'CX', 0), (47, 'Cocos (Keeling) Islands', 'CC', 0), (48, 'Colombia', 'CO', 0), (49, 'Comoros', 'KM', 0), (50, 'Congo', 'CG', 0), (51, 'Congo, The Democratic Republic Of The', 'CD', 0), (52, 'Cook Islands', 'CK', 0), (53, 'Costa Rica', 'CR', 0), (54, 'Cote D\'Ivoire', 'CI', 0), (55, 'Croatia', 'HR', 0), (56, 'Cuba', 'CU', 0), (57, 'Cyprus', 'CY', 0), (58, 'Czech Republic', 'CZ', 0), (59, 'Denmark', 'DK', 0), (60, 'Djibouti', 'DJ', 0), (61, 'Dominica', 'DM', 0), (62, 'Dominican Republic', 'DO', 0), (63, 'Ecuador', 'EC', 0), (64, 'Egypt', 'EG', 0), (65, 'El Salvador', 'SV', 0), (66, 'Equatorial Guinea', 'GQ', 0), (67, 'Eritrea', 'ER', 0), (68, 'Estonia', 'EE', 0), (69, 'Ethiopia', 'ET', 0), (70, 'Falkland Islands (Malvinas)', 'FK', 0), (71, 'Faroe Islands', 'FO', 0), (72, 'Fiji', 'FJ', 0), (73, 'Finland', 'FI', 0), (74, 'France', 'FR', 0), (75, 'French Guiana', 'GF', 0), (76, 'French Polynesia', 'PF', 0), (77, 'French Southern Territories', 'TF', 0), (78, 'Gabon', 'GA', 0), (79, 'Gambia', 'GM', 0), (80, 'Georgia', 'GE', 0), (81, 'Germany', 'DE', 0), (82, 'Ghana', 'GH', 0), (83, 'Gibraltar', 'GI', 0), (84, 'Greece', 'GR', 0), (85, 'Greenland', 'GL', 0), (86, 'Grenada', 'GD', 0), (87, 'Guadeloupe', 'GP', 0), (88, 'Guam', 'GU', 0), (89, 'Guatemala', 'GT', 0), (90, 'Guernsey', ' GG', 0), (91, 'Guinea', 'GN', 0), (92, 'Guinea-Bissau', 'GW', 0), (93, 'Guyana', 'GY', 0), (94, 'Haiti', 'HT', 0), (95, 'Heard Island And Mcdonald Islands', 'HM', 0), (96, 'Holy See (Vatican City State)', 'VA', 0), (97, 'Honduras', 'HN', 0), (98, 'Hong Kong', 'HK', 0), (99, 'Hungary', 'HU', 0), (100, 'Iceland', 'IS', 0), (101, 'India', 'IN', 0), (102, 'Indonesia', 'ID', 0), (103, 'Iran, Islamic Republic Of', 'IR', 0), (104, 'Iraq', 'IQ', 0), (105, 'Ireland', 'IE', 0), (106, 'Isle Of Man', 'IM', 0), (107, 'Israel', 'IL', 0), (108, 'Italy', 'IT', 0), (109, 'Jamaica', 'JM', 0), (110, 'Japan', 'JP', 0), (111, 'Jersey', 'JE', 0), (112, 'Jordan', 'JO', 0), (113, 'Kazakhstan', 'KZ', 0), (114, 'Kenya', 'KE', 0), (115, 'Kiribati', 'KI', 0), (116, 'Korea, Democratic People\'S Republic Of', 'KP', 0), (117, 'Korea, Republic Of', 'KR', 0), (118, 'Kuwait', 'KW', 0), (119, 'Kyrgyzstan', 'KG', 0), (120, 'Lao People\'S Democratic Republic', 'LA', 0), (121, 'Latvia', 'LV', 0), (122, 'Lebanon', 'LB', 0), (123, 'Lesotho', 'LS', 0), (124, 'Liberia', 'LR', 0), (125, 'Libyan Arab Jamahiriya', 'LY', 0), (126, 'Liechtenstein', 'LI', 0), (127, 'Lithuania', 'LT', 0), (128, 'Luxembourg', 'LU', 0), (129, 'Macao', 'MO', 0), (130, 'Macedonia, The Former Yugoslav Republic Of', 'MK', 0), (131, 'Madagascar', 'MG', 0), (132, 'Malawi', 'MW', 0), (133, 'Malaysia', 'MY', 0), (134, 'Maldives', 'MV', 0), (135, 'Mali', 'ML', 0), (136, 'Malta', 'MT', 0), (137, 'Marshall Islands', 'MH', 0), (138, 'Martinique', 'MQ', 0), (139, 'Mauritania', 'MR', 0), (140, 'Mauritius', 'MU', 0), (141, 'Mayotte', 'YT', 0), (142, 'Mexico', 'MX', 0), (143, 'Micronesia, Federated States Of', 'FM', 0), (144, 'Moldova, Republic Of', 'MD', 0), (145, 'Monaco', 'MC', 0), (146, 'Mongolia', 'MN', 0), (147, 'Montserrat', 'MS', 0), (148, 'Morocco', 'MA', 0), (149, 'Mozambique', 'MZ', 0), (150, 'Myanmar', 'MM', 0), (151, 'Namibia', 'NA', 0), (152, 'Nauru', 'NR', 0), (153, 'Nepal', 'NP', 0), (154, 'Netherlands', 'NL', 0), (155, 'Netherlands Antilles', 'AN', 0), (156, 'New Caledonia', 'NC', 0), (157, 'New Zealand', 'NZ', 0), (158, 'Nicaragua', 'NI', 0), (159, 'Niger', 'NE', 0), (160, 'Nigeria', 'NG', 0), (161, 'Niue', 'NU', 0), (162, 'Norfolk Island', 'NF', 0), (163, 'Northern Mariana Islands', 'MP', 0), (164, 'Norway', 'NO', 0), (165, 'Oman', 'OM', 0), (166, 'Pakistan', 'PK', 0), (167, 'Palau', 'PW', 0), (168, 'Palestinian Territory, Occupied', 'PS', 0), (169, 'Panama', 'PA', 0), (170, 'Papua New Guinea', 'PG', 0), (171, 'Paraguay', 'PY', 0), (172, 'Peru', 'PE', 0), (173, 'Philippines', 'PH', 0), (174, 'Pitcairn', 'PN', 0), (175, 'Poland', 'PL', 0), (176, 'Portugal', 'PT', 0), (177, 'Puerto Rico', 'PR', 0), (178, 'Qatar', 'QA', 0), (179, 'Reunion', 'RE', 0), (180, 'Romania', 'RO', 0), (181, 'Russian Federation', 'RU', 0), (182, 'Rwanda', 'RW', 0), (183, 'Saint Helena', 'SH', 0), (184, 'Saint Kitts And Nevis', 'KN', 0), (185, 'Saint Lucia', 'LC', 0), (186, 'Saint Pierre And Miquelon', 'PM', 0), (187, 'Saint Vincent And The Grenadines', 'VC', 0), (188, 'Samoa', 'WS', 0), (189, 'San Marino', 'SM', 0), (190, 'Sao Tome And Principe', 'ST', 0), (191, 'Saudi Arabia', 'SA', 0), (192, 'Senegal', 'SN', 0), (193, 'Serbia And Montenegro', 'CS', 0), (194, 'Seychelles', 'SC', 0), (195, 'Sierra Leone', 'SL', 0), (196, 'Singapore', 'SG', 0), (197, 'Slovakia', 'SK', 0), (198, 'Slovenia', 'SI', 0), (199, 'Solomon Islands', 'SB', 0), (200, 'Somalia', 'SO', 0), (201, 'South Africa', 'ZA', 0), (202, 'South Georgia And The South Sandwich Islands', 'GS', 0), (203, 'Spain', 'ES', 0), (204, 'Sri Lanka', 'LK', 0), (205, 'Sudan', 'SD', 0), (206, 'Suriname', 'SR', 0), (207, 'Svalbard And Jan Mayen', 'SJ', 0), (208, 'Swaziland', 'SZ', 0), (209, 'Sweden', 'SE', 0), (210, 'Switzerland', 'CH', 0), (211, 'Syrian Arab Republic', 'SY', 0), (212, 'Taiwan, Province Of China', 'TW', 0), (213, 'Tajikistan', 'TJ', 0), (214, 'Tanzania, United Republic Of', 'TZ', 0), (215, 'Thailand', 'TH', 0), (216, 'Timor-Leste', 'TL', 0), (217, 'Togo', 'TG', 0), (218, 'Tokelau', 'TK', 0), (219, 'Tonga', 'TO', 0), (220, 'Trinidad And Tobago', 'TT', 0), (221, 'Tunisia', 'TN', 0), (222, 'Turkey', 'TR', 0), (223, 'Turkmenistan', 'TM', 0), (224, 'Turks And Caicos Islands', 'TC', 0), (225, 'Tuvalu', 'TV', 0), (226, 'Uganda', 'UG', 0), (227, 'Ukraine', 'UA', 0), (228, 'United Arab Emirates', 'AE', 0), (229, 'United Kingdom', 'GB', 0), (230, 'United States', 'US', 0), (231, 'United States Minor Outlying Islands', 'UM', 0), (232, 'Uruguay', 'UY', 0), (233, 'Uzbekistan', 'UZ', 0), (234, 'Vanuatu', 'VU', 0), (235, 'Venezuela', 'VE', 0), (236, 'Viet Nam', 'VN', 0), (237, 'Virgin Islands, British', 'VG', 0), (238, 'Virgin Islands, U.S.', 'VI', 0), (239, 'Wallis And Futuna', 'WF', 0), (240, 'Western Sahara', 'EH', 0), (241, 'Yemen', 'YE', 0), (242, 'Zambia', 'ZM', 0), (243, 'Zimbabwe', 'ZW', 0), (244, '(Not Specified)', 'ZZ', 0);


INSERT INTO `customer` (`id`, `name`, `company`, `username`, `password`, `client_category`, `type`, `status`, `registered_date`, `auth_code`, `updated`, `address`, `phones`, `email`, `company_PIB`, `company_regNumber`, `city`, `state`, `notes`, `logo`, `website`, `contactPerson`, `ip_address`, `contact_person`, `currency`, `partner`) VALUES (1, 'Pedjo', '', 'pedja', 'a6e99c6a493e06aef87ff282112a7b48', 28, 0, '', '0000-00-00', '', '0000-00-00 00:00:00', '', '', 'pedja', 0, 0, '', '', '', '', '', '', '', NULL, 0, 1), (2, 'asdfasdfasdf', 'asdfasdf', 'asdfasdfasd fasdf', 'asdfasdfasdf', 0, 2, '', '0000-00-00', 'MgNlG1h', '0000-00-00 00:00:00', 'asdf', 'asdf', 'asdfasdfasd fasdf', 0, 0, 'asdf', 'asdfasdfasdf', '', '', '', '', '127.0.0.1', NULL, 0, 0), (3, 'fasd f', 'saf asd', 'ljlj', 'ljl', 0, 1, '', '0000-00-00', 'GlNQHybc', '0000-00-00 00:00:00', 'kjl', 'kjl', 'ljlj', 0, 0, 'jl', 'jlj', '', '', '', '', '127.0.0.1', NULL, 0, 0), (4, 'sdfg', 'kjhkh', 'hkhkhk@gmail.com', 'b1fa214b60c3dc6741fd1e46f6256921', 0, 1, '', '0000-00-00', '6ac21901f168326bf8654349c95cdac4', '0000-00-00 00:00:00', 'kjhk', 'jkjh', 'hkhkhk@gmail.com', 0, 0, 'hk', 'hk', '', '', '', '', '127.0.0.1', NULL, 0, 0), (5, 'sadfasdf', 'hkj', 'hk@gmail.com', 'hkhk', 0, 1, '', '0000-00-00', '3bd1bf974507b07f34ac6a6754a014b1', '0000-00-00 00:00:00', 'hk', 'hk', 'hk@gmail.com', 0, 0, 'hk', 'hk', '', '', '', '', '127.0.0.1', NULL, 0, 0), (6, 'asdfasd', 'kjh', 'hkjhkh@gmail.com', 'lsakdjfaksdjf', 0, 2, '', '0000-00-00', '6798b1b89f163c65b9369fdaf4b54898', '0000-00-00 00:00:00', 'k', 'kh', 'hkjhkh@gmail.com', 0, 0, 'jhk', 'hjkhkj', '', '', '', '', '127.0.0.1', NULL, 0, 0), (7, 'asdfasdfjkh', 'kjhk', 'khjkhkh@gmail.com', 'saldkfjasldfkj', 0, 2, '', '0000-00-00', 'd4442c01f1979673924cedfa02b74850', '0000-00-00 00:00:00', 'hkhkh', 'khk', 'khjkhkh@gmail.com', 0, 0, 'khkh', 'khkjhkh', '', '', '', '', '127.0.0.1', NULL, 0, 0), (8, 'asdfasdfjkh', 'kjhk', 'khjkhasdfsadfkh@gmail.com', '5b36d1377c7aea9a092d2ab07f1286d7', 0, 4, '', '0000-00-00', '294b17887664d57e223f3521af0a11fa', '0000-00-00 00:00:00', 'hkhkh', 'khk', 'khjkhasdfsadfkh@gmail.com', 0, 0, 'khkh', 'khkjhkh', '', '', '', '', '127.0.0.1', NULL, 0, 0), (9, 'asdfasdf', 'ljl', 'jljljljlj@asdfasdf.com', 'asdfasdfasdf', 0, 1, '', '0000-00-00', '56d532243a1c1dfe1262c59d65eb69b1', '0000-00-00 00:00:00', 'jl', 'ljl', 'jljljljlj@asdfasdf.com', 0, 0, 'jlj', 'ljljljl', '', '', '', '', '127.0.0.1', NULL, 0, 0), (10, 'safsdfs', 'lkjl', 'ljljljljlJ@asdfasd.com', 'asdfasdf', 0, 1, '', '0000-00-00', '93f50f6dcf06d449fb975c83e8c5e454', '0000-00-00 00:00:00', 'jl', 'ljl', 'ljljljljlJ@asdfasd.com', 0, 0, 'jljl', 'jlj', '', '', '', '', '127.0.0.1', NULL, 0, 0), (11, 'asdfasdfl', 'kjl', 'ljjlj@gmail.com', 'ee8ddff8c271c22fb7ad2ff81746b258', 0, 1, '', '0000-00-00', '1470447ca49898e130b1d7f03c73eebc', '0000-00-00 00:00:00', 'lj', 'lj', 'ljjlj@gmail.com', 0, 0, 'ljl', 'jlkj', '', '', '', '', '127.0.0.1', NULL, 0, 0), (12, 'asdfasdfl', 'sdlfjalsdfjlj', 'designbyheart@gmail.com', 'likaasdfasd', 0, 2, '', '0000-00-00', 'ccd3312b9e533235d0289034bdcfbfbc', '0000-00-00 00:00:00', 'jljflasdjlj', 'jlasjdfljl', 'designbyheart@gmail.com', 0, 0, 'ljlsadjflj', 'ljljladsjflsajl', '', '', '', '', '127.0.0.1', NULL, 0, 0), (13, 'asdfasdf', 'asdfasd', 'design.by.heart@hotmail.com', 'alsdfsadkjflsadkjf', 0, 1, '', '0000-00-00', '69fee86437ca73cb0f0ec0a631becdc7', '0000-00-00 00:00:00', 'kljlkjl', 'asdfasd', 'design.by.heart@hotmail.com', 0, 0, 'jljlkjl', 'jljljl', '', '', '', '', '127.0.0.1', NULL, 0, 0), (14, 'asdfasdf', 'asdfasd', 'design1.by.heart@hotmail.com', 'asdfasdf', 0, 2, '', '0000-00-00', '69c22197a19d4e242a042fd986f5fcb6', '0000-00-00 00:00:00', 'kljlkjl', 'asdfasd', 'design1.by.heart@hotmail.com', 0, 0, 'jljlkjl', 'jljljl', '', '', '', '', '127.0.0.1', NULL, 0, 0), (15, 'asdfasdf', 'asdfasd', 'desin1.by.heart@hotmail.com', 'asdfasdfasdf', 0, 2, '', '0000-00-00', 'c5322a4778d988cc78ed46e8005b6101', '0000-00-00 00:00:00', 'kljlkjl', 'asdfasd', 'desin1.by.heart@hotmail.com', 0, 0, 'jljlkjl', 'jljljl', '', '', '', '', '127.0.0.1', NULL, 0, 0), (16, 'asdfasdf', 'asdfasd', 'desin1.by.asdfasdfheart@hotmail.com', 'asdfasdf', 0, 1, '', '0000-00-00', '856b941d387f4e5591aa1df19cf82dd7', '0000-00-00 00:00:00', 'kljlkjl', 'asdfasd', 'desin1.by.asdfasdfheart@hotmail.com', 0, 0, 'jljlkjl', 'jljljl', '', '', '', '', '127.0.0.1', NULL, 0, 0), (17, 'asdfasdfasdf', 'asdfasdf', 'fasdfasdf@asdfasd.com', 'bb222b19e2e39422d3ebc88e0eceed56', 0, 2, '', '0000-00-00', 'cb7c689a56af4d1f8fa574bcd1543e24', '0000-00-00 00:00:00', 'asdfasdfasdf', 'asdfasdf', 'fasdfasdf@asdfasd.com', 0, 0, 'asdfasdfsadf', 'asdfasdf', '', '', '', '', '127.0.0.1', NULL, 0, 0), (18, 'asdfasfd', ';lk', 'design.by.heart@yahoo.com', 'dsafasdf', 0, 1, '', '0000-00-00', 'd7de59e6683f785f72fb3c2ff3539cc9', '0000-00-00 00:00:00', ';k;', ';lk', 'design.by.heart@yahoo.com', 0, 0, 'k;;', 'lk;k', '', '', '', '', '127.0.0.1', NULL, 0, 0);




INSERT INTO `customerAddress` (`id`, `name`, `address1`, `address2`, `state`, `city`, `country`, `zip`, `post`, `note`, `customerID`, `status`) VALUES (1, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (2, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (3, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (4, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (5, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (6, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (7, '1', 'asdfasdf', 'asdfas', 'jljlj', 'ljl', 'jlkl', 'jlkj', NULL, NULL, 1, 0), (8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0), (9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0), (10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0);


INSERT INTO `driver` (`id`, `name_sr`, `name_en`, `version`, `desc_en`, `desc_sr`, `releaseDate`, `size`, `active`, `ordering`, `file`, `products`) VALUES (2, 'test fasdfasdf1111', NULL, '1.2.2.', NULL, NULL, 0, NULL, 0, 0, 'analytics.png', NULL), (3, 'test', 'asdfsdf sdfdf', 'pdf ', NULL, NULL, 0, NULL, 0, 0, 'dashboard.png', NULL), (4, '44test', 'asdfsdf', 'pdf ', NULL, NULL, 0, NULL, 0, 0, 'blog news.png', '1,'), (5, 'test fasdfasdf1111', NULL, '1.2.2.', NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (6, 'test', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (7, 'test', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (8, 'test', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (9, 'test', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (10, 'kjbkb', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (11, 'kjkh', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL), (12, '111111123', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 'screen shot 2012-09-04 at 2.18.10 pm.png', NULL);


INSERT INTO `euro` (`id`, `date`, `userID`, `amount`, `amountEur`) VALUES (1, 1347646046, 2, 220, 220), (2, 1347646491, 1, 0, 0), (3, 1347646505, 1, 0, 0), (4, 1347646519, 1, 0, 0), (5, 1347646529, 1, 114.554, 0), (6, 1347646608, 1, 200, 0), (7, 1347647102, 1, 180, 0), (8, 1347647109, 1, 180.22, 0), (9, 1347647114, 1, 180.223, 0), (10, 1348570414, 1, 195.223, 0), (11, 1348661116, 1, 195.223, 0), (12, 1348661128, 1, 1988.22, 0), (13, 1351938305, 1, 1988.22, 0);


INSERT INTO `gallery` (`id`, `img`, `refID`, `type`, `file`) VALUES (1, '', 3, 'news', 'aktuelnost-partner.jpg'), (2, '', 2, 'general_page', 'general_page-the-moon.jpg'), (3, '', 2, 'general_page', 'general_page-the-moon.jpg'), (4, '', 2, 'general_page', 'general_page-the-moon.jpg'), (5, '', 2, 'general_page', 'general_page-the-moon.jpg'), (6, '', 2, 'general_page', 'general_page-cityscapes_32.jpg'), (7, '', 1, 'general_page', 'general_page-perfect-hue-bluewave.jpg'), (9, '', 2, 'product', 'product-cityscapes_32.jpg'), (11, '', 6, 'product', 'product-cityscapes_32.jpg'), (12, '', 2, 'general_page', 'general_page-img_0002.jpg'), (13, '', 4, 'cat-menu', 'cat-menu-printers_23.png'), (14, '', 6, 'product', 'product-hp2.jpg'), (15, '', 1, 'product', 'product-pedja_jevtic.jpg'), (17, '', 1, 'promo', 'promo-3d-underwater-render.jpg'), (18, '', 1, 'promotion', 'promo-cityscapes_32.jpg'), (19, '', 2, 'cat-menu', 'cat-menu-pedja_jevtic.jpg'), (20, '', 4, 'cat-menu', 'cat-menu-perfect-hue-bluewave.jpg'), (21, '', 2, 'promotion', 'promo-sending feedback.png'), (22, '', 5, 'general_page', 'general_page-icons for ios apps.png'), (23, '', 2, 'news', 'aktuelnost-the-moon.jpg'), (24, '', 2, 'product', 'product-array'), (25, '', 6, 'product', 'product-array'), (26, '', 3, 'product', 'product-array'), (27, '', 4, 'product', 'product-array'), (28, '', 3, 'product', 'product-array'), (29, '', 3, 'product', 'product-array'), (33, '', 2, 'product', 'scan 2.jpeg'), (34, '', 2, 'product', 'perfect-hue-bluewave.jpg'), (35, '', 2, 'product', 'scan 2.jpeg'), (36, '', 2, 'product', 'perfect-hue-bluewave.jpg'), (37, '', 2, 'product', 'scan 2.jpeg'), (38, '', 2, 'product', 'perfect-hue-bluewave.jpg'), (39, '', 2, 'product', 'scan 2.jpeg'), (40, '', 2, 'product', 'perfect-hue-bluewave.jpg'), (41, '', 2, 'product', 'scan 2.jpeg'), (42, '', 2, 'product', 'perfect-hue-bluewave.jpg'), (43, '', 2, 'product', 'scan 2.jpeg'), (44, '', 2, 'product', 'pedja_jevtic.jpg'), (45, '', 2, 'product', 'scan 2.jpeg'), (46, '', 2, 'product', 'pedja_jevtic.jpg'), (47, '', 2, 'product', 'cityscapes_32.jpg'), (48, '', 2, 'product', '3d-underwater-render.jpg'), (50, '', 2, 'product', '3d-underwater-render.jpg'), (51, '', 2, 'product', '3'), (52, '', 2, 'product', 't'), (53, '', 2, 'product', '3'), (54, '', 2, 'product', 't'), (55, '', 2, 'product', '3d-underwater-render.jpg'), (56, '', 2, 'product', 'the-moon.jpg'), (58, '', 2, 'product', 'managewp_app_promo.png'), (59, '', 2, 'product', '3d-underwater-render.jpg'), (60, '', 2, 'product', 'scan.jpeg'), (61, '', 2, 'product', '3d-underwater-render.jpg'), (62, '', 2, 'product', 'perfect-hue-bluewave.jpg'), (63, '', 2, 'product', '3d-underwater-render.jpg'), (64, '', 2, 'product', 'planetary-sunrise-with-awesome-space-view.jpg'), (65, '', 2, 'product', 'pedja_jevtic.jpg'), (66, '', 5, 'product', 'bike3.png');


INSERT INTO `general_page` (`id`, `name_sr`, `name_en`, `counter`, `active`, `rightBoxID`, `rightBox`, `centerBoxID`, `centerBox`, `leftBoxID`, `leftBox`, `counterEndMessage`, `desc_sr1`, `desc_sr2`, `desc_sr3`, `desc_en1`, `desc_en2`, `desc_en3`, `seo_title_sr`, `seo_title_en`, `seo_desc_sr`, `seo_desc_en`, `seo_keywords_sr`, `seo_keywords_en`, `pg_name`, `homePage`, `template`, `img`, `imagePosition`, `column3Menu`, `column2Menu`, `column1Menu`) VALUES (1, 'O nama', '', 1348607231, 1, 5, NULL, 3, NULL, 6, NULL, NULL, 'asdfasdfa<p>                    				                    			</p>', NULL, NULL, '<p>                    				                    			</p>', NULL, NULL, 'Test title', '', '', '', '', '', 'o-nama', NULL, 2, 'general_page-perfect-hue-bluewave.jpg', 0, 0, 0, 0), (3, 'Proizvodi', '', 0, 1, 0, NULL, 0, NULL, 0, NULL, NULL, '<div class="oneone">                                    <font color="#424242" face="Arial"><span style="font-size: 12px;">Prikaz liste svih proizvoda...</span></font><div class="oneone" style="color: rgb(66, 66, 66); font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; "><p>                                                                    </p></div>                                </div>', NULL, NULL, '<div class="oneone">                                    <div class="oneone"><p>                                                                    </p></div>                                </div>', NULL, NULL, '', '', '', '', '', '', 'proizvodi', NULL, 0, NULL, 0, 0, 0, 0), (5, 'Login', '', 1349017437, 1, 8, NULL, 3, NULL, 1, NULL, NULL, 'sadfasdf asdf asdf sa                                    <p>                                                                    </p>', NULL, NULL, '<p>                                                                    </p>', NULL, NULL, '', '', '', '', '', '', 'login', NULL, 0, 'general_page-icons for ios apps.png', 0, 0, 0, 0), (6, 'Registracija', 'Registration', 0, 1, 0, NULL, 0, NULL, 0, NULL, NULL, '<div class="oneone">                                    <div class="oneone"><p>                                                                    </p></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>', NULL, NULL, '<div class="oneone">                                    <div class="oneone"><p>                                                                    </p></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>', NULL, NULL, '', '', '', '', '', '', 'registration', NULL, 0, NULL, 0, 0, 0, 0), (8, 'Partneri', '', 1349030440, 1, 0, NULL, 0, NULL, 0, NULL, NULL, 'asdfadsfa fsad<br>f<br>asd<br>fas<br>d fasdf<br><p>                                                                    </p>', NULL, NULL, '<p>                                                                    </p>', NULL, NULL, '', '', '', '', '', '', 'partners', NULL, 0, NULL, 0, 0, 0, 0), (13, 'UspeÅ¡no ste se registrovali...', 'sfasdf asdf', 1348430028, 1, 0, NULL, 0, NULL, 0, NULL, NULL, 'asdf sdfasdf asd<p>                                    </p>f&nbsp;asdf&nbsp;asdf as', NULL, NULL, '&nbsp;asdfasdfas dfasdf<p>                                    </p>', NULL, NULL, '', '', '', '', '', '', 'registrationDone', NULL, 0, NULL, 0, 0, 0, 0), (14, 'Greska u verifikaciji klijenta', '', 0, 1, 0, NULL, 0, NULL, 0, NULL, NULL, '<p>                                    </p>', NULL, NULL, '<p>                                    </p>', NULL, NULL, '', '', '', '', '', '', 'authentication-error', NULL, 0, NULL, 0, 0, 0, 0), (15, 'iPlus', '', 0, 1, 1, NULL, 8, NULL, 3, NULL, NULL, '<p>                                    </p>', '<p>                                    </p>', '<p>                                    </p>', '<p>                                    </p>', '<p>                                    </p>', '<p>                                    </p>', '', '', '', '', '', '', 'iplus', NULL, 1, NULL, 2, 0, 0, 0);


INSERT INTO `instruction` (`id`, `name_sr`, `name_en`, `version`, `desc_en`, `desc_sr`, `releaseDate`, `size`, `active`, `ordering`, `file`, `products`) VALUES (1, 'test\';,\';\'\';', 'asdfsdf', 'pdf ', NULL, NULL, 123123123, '2.2mb', 1, 0, NULL, '2,1,3,5,'), (2, 'sdfsdafasdfasdf', NULL, 'safasdf', NULL, NULL, 0, NULL, 1, 0, 'analytics.png', '5,6,2,'), (3, 'sdfsdafasdfasdf', NULL, 'sad fsdf asdf', NULL, NULL, 0, NULL, 1, 0, NULL, '2,5,4,');


INSERT INTO `log` (`id`, `eventTime`, `userID`, `note`, `refID`, `type`, `selectedProducts`) VALUES (1, 2012, 0, 'Klijent dodao proizvod u korpu', 0, NULL, '1'), (2, 1347672768, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '1'), (3, 1347673240, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2'), (4, 1347673249, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2'), (5, 1347673340, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2'), (6, 1347673360, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
'), (7, 1347674042, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2'), (8, 1347674066, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2'), (9, 1347674178, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2'), (10, 1347674180, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2'), (11, 1347704876, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1'), (12, 1347705043, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1,1'), (13, 1347705055, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1,1,1'), (14, 1347705086, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1,1,1,1'), (15, 1347705117, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1,1,1,1,1'), (16, 1347719057, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1,1,1,1,1,1'), (17, 1347719189, 1, 'Klijent dodao proizvod u korpu', 0, NULL, '2,3,4,1
,2,2,2,2,1,1,1,1,1,1,1'), (18, 1347734554, 1, 'NarudÅ¾bina je otkazana', 0, NULL, NULL), (19, 1347734654, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (20, 1347734668, 1, 'NarudÅ¾bina je otkazana', 0, NULL, NULL), (21, 1347734788, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (22, 1347734910, 1, 'NarudÅ¾bina je otkazana', 0, NULL, NULL), (23, 1347738926, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',2'), (24, 1348014627, 1, 'Klijent uklonio proizvod2 iz porudÅ¾bine', 0, NULL, NULL), (25, 1348016998, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (26, 1348016998, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1'), (27, 1348017121, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1'), (28, 1348273267, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, ',1,1'), (29, 1348273270, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, ',1'), (30, 1348273287, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, NULL), (31, 1348273346, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (32, 1348273353, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1'), (33, 1348273377, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, ',1'), (34, 1348273511, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, NULL), (35, 1348273585, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (36, 1348273899, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1'), (37, 1348274709, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, ',1'), (38, 1348274906, 1, 'Klijent uklonio proizvod1 iz porudÅ¾bine', 0, NULL, NULL), (39, 1348276598, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',2'), (40, 1348276603, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',2,2'), (41, 1348276706, 1, 'Klijent uklonio proizvod2 iz porudÅ¾bine', 0, NULL, ',2'), (42, 1348276741, 1, 'Klijent uklonio proizvod2 iz porudÅ¾bine', 0, NULL, ',2'), (43, 1348277352, 1, 'NarudÅ¾bina je otkazana', 0, NULL, NULL), (44, 1348277530, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (45, 1348277532, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1'), (46, 1348277882, 1, 'NarudÅ¾bina je otkazana', 0, NULL, NULL), (47, 1348277904, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (48, 1348277907, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1'), (49, 1348277908, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1'), (50, 1348277908, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1,1'), (51, 1348277908, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1,1,1'), (52, 1348278033, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1,1,1,1'), (53, 1348278034, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1,1,1,1,1'), (54, 1348278035, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1,1,1,1,1,1,1'), (55, 1348544023, 1, 'Finalized order', 34, NULL, NULL), (56, 1348544064, 1, 'Finalized order', 34, NULL, NULL), (57, 1348544118, 1, 'Finalized order', 34, NULL, NULL), (58, 1348663027, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1'), (59, 1348663029, 1, 'Klijent dodao proizvod u korpu', 0, NULL, ',1,1'), (60, 1349032092, 1, 'Finalized order', 35, NULL, NULL);


INSERT INTO `mail` (`id`, `title`, `reftitle`, `email`, `name`, `date`, `orderID`, `phone`, `status`, `messageContent`, `refMessageContent`, `receiver`, `type`, `customer`) VALUES (1, 'Nova poruka - Original.com', 'RE:[Nova poruka - Original.com]', '', '', '2012-09-24 00:00:00', NULL, '', 1, '<div class="oneone"><p>                    				test                    			</p></div>', '<div class="oneone"><p>                    				</p></div>', 'desingbyheart@gmail.com', 'contactSupportProfile', 1), (2, '[PorudÅ¾bina:J20Xx7] Nova poruka', '', '', '', '2012-09-30 00:00:00', 'J20Xx7', '', 0, 'asdfasdfasdf
a
sdf
as
dfasd', '', 'desingbyheart@gmail.com', 'orderTicket', 1), (3, '[PorudÅ¾bina:J20Xx7] Nova poruka', '', '', '', '2012-09-30 00:00:00', 'J20Xx7', '', 0, 'asdf sdfdf', '', 'desingbyheart@gmail.com', 'orderTicket', 1), (4, '[PorudÅ¾bina:Ls06y7] Nova poruka', '', '', '', '2012-10-31 00:00:00', 'Ls06y7', '', 0, 'asdfsadfsd', '', 'desingbyheart@gmail.com', 'orderTicket', 1);


INSERT INTO `mainmenu` (`id`, `name_sr`, `name_en`, `ordering`, `url`, `type`, `css`, `columnT`) VALUES (1, 'sadfasdf', NULL, 0, 'asdf', 'main', NULL, 0), (2, 'asdfasdf', NULL, 0, 'asdf', 'main', NULL, 0), (3, 'asdf', NULL, 232, 'asdf', 'main', NULL, 0), (4, 'asdfasdf', NULL, 0, 'asdf', 'main', NULL, 0), (5, 'sadfas', NULL, 0, 'asdf', 'main', NULL, 0), (6, 'asdfasdf', NULL, 0, 'asdf', 'main', NULL, 0), (7, 'asdfasdfs', NULL, 0, 'asdf', 'main', NULL, 0), (8, 'asasdf', NULL, 22131, 'asdfasdf', 'footer', NULL, 0), (9, 'asdfasdf', NULL, 0, 'asdfasdf', 'footer', NULL, 1), (10, 'asdfasdfasdf', NULL, 0, 'asdfasdfsa', 'footer', NULL, 0), (11, 'sadfsad', 'asdf', 0, 'asdfasdf', 'footer', NULL, 0);


INSERT INTO `manufacturer` (`id`, `name_sr`, `name_en`, `logo`, `country_sr`, `country_en`, `url`, `banner_home`, `type`, `active`, `desc_sr`, `desc_en`, `seo_title_sr`, `seo_title_en`, `seo_desc_sr`, `seo_desc_en`, `seo_keywords_sr`, `seo_keywords_en`, `banner`) VALUES (1, 'Konica', '', 'proizvodjac-logo-mwpappicon.png', '', '', '', 0, 0, 1, '<div class="oneone">                        asdfds fdsaf asdf asdf sadf sad<div class="oneone">                        <div class="oneone"><p>                    				                    			</p></div>                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div><div>asd</div><div>f asd</div><div>f&nbsp;</div><div>asd</div><div>fa</div><div>sdf asd</div>                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>', '<div class="oneone">                        <div class="oneone">                        <div class="oneone"><p>                    				                    			</p></div>                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>', '', '', '', '', '', '', 0);


INSERT INTO `news` (`id`, `title`, `content`, `date`, `active`, `title_en`, `content_en`, `seo_title_sr`, `seo_title_en`, `seo_desc_sr`, `seo_desc_en`, `seo_keywords_en`, `ordering`, `seo_keywords_sr`) VALUES (1, 'a sdf sda asdf', 'sdfasdfsad<div class="oneone"><p>                    				                    			</p></div>', '2012-08-15 00:00:00', 1, '', '<div class="oneone"><p>                    				                    			</p></div>', '', '', '', '', '', 0, ''), (2, 'a sdf sda asdf', '<div class="oneone">                    				sdfasdfsad<div class="oneone"><p>                    				                    			</p></div>                    			</div>', '2012-08-15 00:00:00', 1, '', '<div class="oneone">                    				<div class="oneone"><p>                    				                    			</p></div>                    			</div>', '', '', '', '', '', 0, ''), (3, 'aloha', '<div class="oneone">                    				<div class="oneone">d fasdf asdfsadfasdf sad<div class="oneone"><p>                    				                    			</p></div></div><div class="oneone"><br></div>                    			</div>', '2012-08-15 00:00:00', 1, '', '<div class="oneone">                    				<div class="oneone"><div class="oneone"><p>                    				                    			</p></div></div>                    			</div>', '', '', '', '', '', 0, ''), (4, 'alloha', '<div class="oneone">                    				<div class="oneone"><p>                    				                    			</p></div>                    			</div>', '1970-01-01 00:00:00', 0, '', '<div class="oneone">                    				<div class="oneone"><p>                    				                    			</p></div>                    			</div>', '', '', '', '', '', 0, ''), (5, 'kjhkjhk', '<div class="oneone"><p>                    				                    			</p></div>', '0000-00-00 00:00:00', 0, '', '<div class="oneone"><p>                    				                    			</p></div>', '', '', '', '', '', 0, ''), (6, 'kjhkjhk', 'asdf asdf 32 rasdf asdf<div class="oneone">                    				<div class="oneone"><p>                    				                    			</p></div>                    			</div>', '1970-01-01 00:00:00', 0, '', '<div class="oneone">                    				<div class="oneone"><p>                    				                    			</p></div>                    			</div>', '', '', '', '', '', 0, '');


INSERT INTO `notes` (`id`, `productID`, `textSrb`, `textEn`, `refID`) VALUES (1, 3, NULL, 'asdfasdfasdf', 'admin'), (2, 3, NULL, NULL, 'comercial'), (3, 3, NULL, NULL, 'Maloprodaja'), (4, 3, NULL, NULL, 'Veleprodaja'), (5, 6, NULL, NULL, 'admin'), (6, 6, NULL, NULL, 'comercial'), (7, 6, NULL, NULL, 'Maloprodaja'), (8, 6, NULL, NULL, 'Veleprodaja'), (9, 2, NULL, NULL, 'admin'), (10, 2, NULL, NULL, 'comercial'), (11, 2, NULL, NULL, 'Maloprodaja'), (12, 2, NULL, NULL, 'Veleprodaja'), (13, 1, NULL, NULL, 'admin'), (14, 1, NULL, NULL, 'comercial'), (15, 1, NULL, NULL, 'Maloprodaja'), (16, 1, NULL, NULL, 'Veleprodaja'), (17, 1, NULL, NULL, 'Nova grupa'), (18, 1, NULL, NULL, 'D1'), (19, 1, NULL, NULL, 'X GRUPA'), (20, 5, NULL, NULL, 'admin'), (21, 5, NULL, NULL, 'comercial'), (22, 5, NULL, NULL, 'Maloprodaja'), (23, 5, NULL, NULL, 'Veleprodaja'), (24, 7, NULL, NULL, 'admin'), (25, 7, NULL, NULL, 'comercial'), (26, 7, NULL, NULL, 'Maloprodaja'), (27, 7, NULL, NULL, 'Veleprodaja');


INSERT INTO `price` (`id`, `groupID`, `templateID`, `updated`, `productID`, `amount`, `changedBy`) VALUES (46, 99, 0, 0, 1, 111, 1), (47, 100, 0, 0, 1, 22, 1), (48, 101, 0, 0, 1, 22, 1), (49, 102, 0, 0, 1, 21, 1), (50, 103, 0, 0, 1, 21, 1), (51, 104, 0, 0, 1, 6, 1), (52, 105, 0, 0, 1, 105, 1), (53, 106, 0, 0, 1, 170, 1), (54, 107, 0, 0, 1, 105, 1), (55, 108, 0, 0, 1, 170, 1), (56, 109, 0, 0, 1, 105, 1), (57, 110, 0, 0, 1, 170, 1), (58, 111, 0, 0, 1, 21, 1), (59, 112, 0, 0, 1, 19, 1), (60, 113, 0, 0, 1, 111, 1), (61, 114, 0, 0, 1, 21, 1), (62, 115, 0, 0, 1, 111, 1), (63, 116, 0, 0, 1, 21, 1), (64, 117, 0, 0, 1, 19, 1), (65, 118, 0, 0, 1, 111, 1), (66, 119, 0, 0, 1, 19, 1), (67, 120, 0, 0, 1, 111, 1), (68, 121, 0, 0, 1, 19, 1), (69, 122, 0, 0, 1, 111, 1), (70, 123, 0, 0, 1, 19, 1), (71, 124, 0, 0, 1, 111, 1), (72, 125, 0, 0, 1, 111, 1), (73, 126, 0, 0, 1, 111, 1), (74, 127, 0, 0, 1, 111, 1), (75, 128, 0, 0, 1, 99, 1), (76, 129, 0, 0, 1, 99, 1), (77, 130, 0, 0, 1, 111, 1), (78, 143, 0, 0, 1, 99, 1), (79, 144, 0, 0, 1, 99, 1), (80, 145, 0, 0, 1, 99, 1), (81, 146, 0, 0, 1, 99, 1), (82, 147, 0, 0, 1, 99, 1), (83, 148, 0, 0, 1, 99, 1), (84, 149, 0, 0, 1, 99, 1), (85, 150, 0, 0, 1, 99, 1), (86, 153, 0, 0, 1, 99, 1), (87, 154, 0, 0, 1, 99, 1), (88, 155, 0, 0, 1, 99, 1), (89, 156, 0, 0, 1, 99, 1), (90, 157, 0, 0, 1, 99, 1), (91, 158, 0, 0, 1, 99, 1), (92, 159, 0, 0, 1, 99, 1), (93, 160, 0, 0, 1, 99, 1);


INSERT INTO `price_group` (`id`, `templateID`, `percentage`, `name`, `isMain`, `parent`, `productID`, `tax`, `conversionType`, `calcType`, `updated`) VALUES (28, 0, 0, 'Maloprodaja', '1', 0, 0, 0, 0, 0, 0), (29, 0, 0, 'Veleprodaja', '1', 0, 0, 0, 0, 0, 0), (99, 0, 11, NULL, NULL, 0, 1, 0, 0, 2, 0), (100, 0, 3, NULL, NULL, 28, 1, 0, 0, 0, 0), (101, 0, 3, NULL, NULL, 0, 1, 0, 0, 0, 0), (102, 0, 5, NULL, NULL, 28, 1, 0, 0, 1, 0), (103, 0, 5, NULL, NULL, 0, 1, 0, 0, 1, 0), (104, 0, 70, NULL, NULL, 0, 1, 0, 0, 1, 0), (105, 0, 5, NULL, NULL, 0, 1, 0, 0, 2, 0), (106, 0, 70, NULL, NULL, 0, 1, 0, 0, 2, 0), (107, 0, 5, NULL, NULL, 0, 1, 0, 0, 2, 0), (108, 0, 70, NULL, NULL, 0, 1, 0, 0, 2, 0), (109, 0, 5, NULL, NULL, 0, 1, 0, 0, 2, 0), (110, 0, 70, NULL, NULL, 0, 1, 0, 0, 2, 0), (111, 0, 5, NULL, NULL, 29, 1, 0, 0, 1, 0), (112, 0, 11, NULL, NULL, 28, 1, 0, 0, 1, 0), (113, 0, 11, NULL, NULL, 0, 1, 0, 0, 2, 0), (114, 0, 5, NULL, NULL, 0, 1, 0, 0, 1, 0), (115, 0, 11, NULL, NULL, 29, 1, 0, 0, 2, 0), (116, 0, 5, NULL, NULL, 0, 1, 0, 0, 1, 0), (117, 0, 11, NULL, NULL, 0, 1, 0, 0, 1, 0), (118, 0, 112, 'Maloprodaja', NULL, 28, 1, 0, 0, 2, 0), (119, 0, 11, NULL, NULL, 0, 1, 0, 0, 1, 0), (120, 0, 112, 'Maloprodaja', NULL, 28, 1, 0, 0, 2, 0), (121, 0, 11, NULL, NULL, 0, 1, 0, 0, 1, 0), (122, 0, 112, 'Maloprodaja', NULL, 28, 1, 0, 0, 2, 0), (123, 0, 11, NULL, NULL, 0, 1, 0, 0, 1, 0), (124, 0, 112, 'Maloprodaja', NULL, 28, 1, 0, 0, 2, 0), (125, 0, 112, NULL, NULL, 0, 1, 0, 0, 2, 0), (126, 0, 33, 'Maloprodaja', NULL, 28, 1, 0, 0, 2, 0), (127, 0, 33, NULL, NULL, 0, 1, 0, 0, 2, 0), (128, 0, 11, 'Maloprodaja', NULL, 28, 1, 0, 0, 1, 0), (129, 0, 11, 'Veleprodaja', NULL, 29, 1, 0, 0, 1, 0), (130, 0, 11, NULL, NULL, 0, 1, 0, 0, 2, 0), (131, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (132, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (133, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (134, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (135, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (136, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (137, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (138, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (139, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (140, 1, 0, 'Proizvod', NULL, 0, 0, 0, 0, 0, 0), (141, 2, 0, 'Proizvod', NULL, 0, 0, 0, 0, 2, 0), (142, 2, 0, 'Proizvod', NULL, 0, 0, 0, 0, 2, 0), (143, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (144, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (145, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (146, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (147, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (148, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (149, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (150, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (151, 0, 0, 'Proizvod', NULL, 0, 3, 0, 0, 0, 0), (152, 0, 0, 'Proizvod', NULL, 0, 3, 0, 0, 0, 0), (153, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (154, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (155, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (156, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (157, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (158, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (159, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (160, 0, 11, 'Proizvod', NULL, 0, 1, 0, 0, 1, 0), (161, 0, 0, 'Proizvod', NULL, 0, 5, 0, 0, 0, 0), (162, 0, 0, 'Proizvod', NULL, 0, 5, 0, 0, 0, 0), (163, 0, 0, 'Proizvod', NULL, 0, 7, 0, 0, 0, 0), (164, 0, 0, 'Proizvod', NULL, 0, 7, 0, 0, 0, 0), (165, 0, 0, 'Proizvod', NULL, 0, 5, 0, 0, 0, 0), (166, 0, 0, 'Proizvod', NULL, 0, 5, 0, 0, 0, 0), (167, 0, 0, 'Proizvod', NULL, 0, 5, 0, 0, 0, 0), (168, 0, 0, 'Proizvod', NULL, 0, 5, 0, 0, 0, 0);




INSERT INTO `price_template` (`id`, `name`, `type`, `countryCode`, `active`, `tax`, `country`) VALUES (1, 'test', '0', NULL, 0, 10, 5), (2, 'new template', '2', NULL, 0, 22, 3), (3, 'oki', '0', NULL, 0, 0, 0), (4, 'KM', '0', NULL, 0, 0, 0);


INSERT INTO `product` (`id`, `name_sr`, `name_en`, `attributeSet`, `category`, `country`, `plD`, `model`, `ordering`, `manufacturer`, `manufacturerName`, `active`, `status`, `type`, `desc_sr`, `desc_en`, `size_sr`, `size_en`, `power_sr`, `power_en`, `power_type_sr`, `power_type_en`, `characteristics_sr`, `characteristics_en`, `seo_title_sr`, `seo_title_en`, `seo_desc_sr`, `seo_desc_en`, `seo_keywords_sr`, `seo_keywords_en`, `updated`, `created`, `promo`, `price`, `template`, `calcType`, `secondIDs`, `lang`, `obligatory`, `similar`, `optional`, `url`, `pdv`, `mainCatID`, `distribID`, `attributes`, `countryTax`) VALUES (1, 'as23423430', '', 0, 5, 0, 'as23423431', NULL, 0, 1, 'Konica', 0, 0, 0, '', '<div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone">                    <div class="oneone"><p>                                    </p></div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>                </div>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 100, 0, 0, 'as23423433', 0, NULL, NULL, NULL, NULL, 0, 'as23423432', NULL, '38,36,10,16', 0), (2, 'as23423432', '', 0, 2, 0, 'as23423433', NULL, 0, 1, 'Konica', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423435', 0, NULL, NULL, NULL, NULL, 0, 'as23423434', NULL, NULL, 0), (3, '', '', 3, 0, 0, '5MM63b', NULL, 0, 0, NULL, 0, 0, 0, '', '<div class="oneone"><p>                                    </p></div>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2012-11-09 12:11:00', 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0), (4, 'as23423452', '', 0, 2, 0, 'as23423453', NULL, 0, 1, 'Konica', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423455', 0, NULL, NULL, NULL, NULL, 0, 'as23423454', NULL, NULL, 0), (5, 'BizHub 165', '', 0, 7, 0, '123123123', NULL, 0, 1, 'Konica', 1, 0, 0, '', '<div class="oneone">                    <div class="oneone">                    <div class="oneone"><p>                                    </p></div>                </div>                </div>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2012-11-15 04:11:00', 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '38', 0), (7, 'BizHub 165', '', 0, 7, 0, '111111111', NULL, 0, 1, NULL, 1, 0, 0, '', '<div class="oneone"><p>                                    </p></div>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2012-11-15 04:11:00', 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0);


INSERT INTO `productDetails` (`id`, `productID`, `value_sr`, `value_en`, `detailID`) VALUES (1, 2, 'asdfasdf', 'sdfasdf', '1'), (2, 2, 'asdfasdfasdf', 'asdf', '3'), (3, 2, 'asdfasdf', 'asdfasdf', '2'), (4, 4, 'sdafsdfasd', 'adfasdfasdf', '1'), (5, 4, 'dasdfasdf', 'asdfasdfasdf', '3'), (6, 4, NULL, NULL, '2'), (7, 0, 'asdfasdf', 'asdfasdf', '1'), (8, 0, 'asdfasd', 'asdfasd', '6'), (9, 0, 'asdfasdf', 'asdfasdf', '9'), (10, 0, 'asdfasdf', 'asdfasdf', '10'), (11, 0, 'asdfasdf', 'asdfasdf', '14'), (12, 0, 'asdfasd', 'asfdafsdf', '16'), (13, 3, 'asdfasdf', 'asdfasdf', '1'), (14, 3, 'asdfasd', 'asdfasd', '6'), (15, 3, 'asdfasdf', 'asdfasdf', '9'), (16, 3, 'asdfasdf', 'asdfasdf', '10'), (17, 3, 'asdfasdf', 'asdfasdf', '14'), (18, 3, 'asdfasd', 'asfdafsdf', '16'), (19, 4, 'pedja', 'oedja', '3'), (25, 3, 'tets', 'sadfsdf', '13'), (32, 0, 'KOJI?', 'WHO?', '17'), (33, 0, 'A4', 'A4', '22'), (34, 0, 'A3', 'A3', '22'), (35, 0, 'Banner', 'Banner', '22'), (36, 0, 'Printer', 'Printer', '23'), (37, 0, 'MFP', 'MFP', '23'), (38, 0, '3 u 1', '3 in 1', '24'), (39, 0, '4 u 1', '4 in 1', '24');


INSERT INTO `productDetailsSettings` (`id`, `name_sr`, `name_en`, `parent`) VALUES (1, 'asdfasdf', 'asdfasdf', '0'), (2, 'sadfasdf', 'asdfasdf', '0'), (3, 'Aloha', 'test', '0'), (4, 'alloah', NULL, '0'), (5, 'TESET#', 'TEST#', '0'), (6, 'TEST1', 'TEST1', '0'), (7, 'alloah', 'hgfhgf', '0'), (8, 'test', 'hgfhgf', '0'), (9, 'alloah', 'hgfhgf', '0'), (10, 'alloah', 'hgfhgf', '0'), (11, 'alloah', 'hgfhgf', '0'), (12, 'alloah', 'asdfasdf', '1'), (13, 'test4', 'test4', '1'), (14, 'test3', 'test3', '13'), (15, 'TEst2', 'test2', '0'), (16, 'Boja otiska', 'Print colour', '14'), (17, '1234567890', '1234567890', '16'), (18, 'Boja otiska2', 'Boja otiska2', '0'), (19, 'test', 'lsakf;asldf', '17'), (20, 'sadfasdf', 'asdfasdf', '17'), (21, 'Kancelarjiski UreÄ‘aji', 'Office devices', '0'), (22, 'Format papira', 'Papir format', '21'), (23, 'Tip ureÄ‘aja', 'Device type', '21'), (24, 'Functionalnost', 'Functionality', '21'), (27, 'mutliParentTest', NULL, '24'), (28, 'Novi multiTest', NULL, '21,24,27,23,22');


INSERT INTO `productOrder` (`id`, `customerID`, `products`, `address`, `ipAddress`, `orderDate`, `status`, `totalPrice`, `orderID`, `modified`) VALUES (1, 1, NULL, 0, NULL, 0, 1, 0, '999999999999', 0), (2, 1, '2,3,4,1
,2,2,2,2', 0, NULL, 0, 1, 0, '999999999999', 0), (3, 1, '2,3,4,1
,2,2,2,2', 0, NULL, 0, 1, 0, '999999999999', 0), (4, 1, '2,3,4,1
,2,2,2,2', 0, NULL, 0, 1, 0, NULL, 0), (5, 1, '2,3,4,1
,2,2,2,2', 0, NULL, 0, 1, 0, NULL, 0), (6, 1, '2,3,4,1
,2,2,2,2', 0, NULL, 0, 1, 0, NULL, 0), (7, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (8, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (9, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (10, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (11, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (12, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (13, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (14, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (15, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (16, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (17, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (18, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (19, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (20, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (21, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, '1347828399210', 0), (22, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (23, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, '1347828414230', 0), (24, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (25, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (26, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, NULL, 0), (27, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 1, 0, '134770391413', 0), (28, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 7, 0, '134770391413', 0), (29, 1, '2,3,4,1
,2,2,2,2', 0, '127.0.0.1', 0, 7, 0, '134770391413
', 0), (30, 1, '2,3,4,1
,2,2,2,2,1,1,1,1,1', 0, '127.0.0.1', 1347715880, 7, 0, '13477158801', 0), (31, 1, '2,3,4,1
,2,2,2,2,1,1,1,1,1', 0, '127.0.0.1', 1347716351, 7, 0, '13477163511', 0), (32, 1, ',1,1,1,1,1,1,1,1', 0, '127.0.0.1', 1348543390, 7, 1873870000, '13485433901', 0), (33, 1, NULL, 0, '127.0.0.1', 1348543390, 7, 0, '13485433901', 0), (34, 1, ',1,1,1,1,1,1,1,1', 9, '127.0.0.1', 1348543991, 8, 1873870000, '13485439911', 1349032884), (35, 1, ',1,1', 10, '127.0.0.1', 1349032081, 8, 468468000, '13490320811', 1349032793);


INSERT INTO `promo` (`id`, `dateStart`, `dateEnd`, `name_en`, `name_sr`, `desc_en`, `desc_sr`, `products`, `discount`, `price`, `active`, `ordering`, `subTitle_sr`, `subTitle_en`, `seo_title_sr`, `seo_title_en`, `seo_desc_en`, `seo_desc_sr`, `seo_keywords_en`, `seo_keywords_sr`, `fullWidth`) VALUES (1, 2147468400, -2147483648, 'testEmg', 'testsadfsdf', '<div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone">                                        <div class="oneone"><p>                                        fasdfas d;fasdf;as                                    </p></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>                                    </div>', '<div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><div class="oneone"><p>sadfasdfasfasdfasdf</p><p>as</p><p>df</p><p>dsafasdf asdf</p><p>as</p><p>df</p></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div></div>', '2,4,3,', 20, 63000, 1, 0, 'Konika Minolta
', 'Konika Minolta', NULL, NULL, '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ', 0), (2, 1347055200, -3600, NULL, 'aloha....', '<div class="oneone">                                        <div class="oneone">                                        <div class="oneone"><p>                                                                            </p></div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>                                    </div><style id="_clearly_component__css" type="text/css">#next_pages_container { width: 5px; hight: 5px; position: absolute; top: -100px; left: -100px; z-index: 2147483647 !important; } </style><div id="_clearly_component__next_pages_container"></div>', '<strong style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify; ">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans; font-size: 11px; line-height: 14px; text-align: justify; ">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span>', NULL, 0, 0, 1, 0, NULL, NULL, NULL, NULL, '                                                                                                ', '                                                                                                ', '                                                                                                ', '                                                                                                ', 0);


INSERT INTO `searchTemplates` (`id`, `query`, `name`) VALUES (1, 'a:93:{s:12:"manufacturer";s:1:"0";s:8:"category";s:1:"0";s:7:"country";s:1:"3";s:3:"plD";s:0:"";s:9:"mainCatID";s:0:"";s:9:"secondIDs";s:0:"";s:9:"distribID";s:0:"";s:3:"pdv";s:0:"";s:8:"lowPrice";s:0:"";s:9:"highPrice";s:0:"";s:14:"percentFrom_28";s:0:"";s:12:"percentTo_28";s:0:"";s:12:"priceFrom_28";s:0:"";s:10:"priceTo_28";s:0:"";s:14:"percentFrom_29";s:0:"";s:12:"percentTo_29";s:0:"";s:12:"priceFrom_29";s:0:"";s:10:"priceTo_29";s:0:"";s:16:"speedCopyA4Color";s:0:"";s:16:"speedCopyA4Black";s:0:"";s:16:"speedCopyA3Color";s:0:"";s:16:"speedCopyA3Black";s:0:"";s:14:"firstCopyColor";s:0:"";s:14:"firstCopyBlack";s:0:"";s:11:"heatingTime";s:0:"";s:14:"copyResolution";s:0:"";s:9:"gradation";s:0:"";s:12:"mutliplyCopy";s:0:"";s:10:"formatOrig";s:0:"";s:4:"zoom";s:0:"";s:12:"copyFunction";s:0:"";s:17:"printSpeedA4Color";s:0:"";s:17:"printSpeedA4Black";s:0:"";s:17:"printSpeedA3Color";s:0:"";s:17:"printSpeedA3Black";s:0:"";s:19:"firstCopyPrintColor";s:0:"";s:19:"firstCopyPrintBlack";s:0:"";s:8:"printRes";s:0:"";s:9:"processor";s:0:"";s:6:"driver";s:0:"";s:2:"os";s:0:"";s:5:"fonts";s:0:"";s:14:"printFunctions";s:0:"";s:14:"scanSpeedColor";s:0:"";s:14:"scanSpeedBlack";s:0:"";s:7:"scanRes";s:0:"";s:11:"scanMethods";s:0:"";s:11:"scanFormats";s:0:"";s:15:"scanDestination";s:0:"";s:11:"faxStandard";s:0:"";s:11:"faxTransmit";s:0:"";s:6:"faxRes";s:0:"";s:14:"faxCompression";s:0:"";s:8:"faxModem";s:0:"";s:14:"faxDestination";s:0:"";s:12:"faxFunctions";s:0:"";s:6:"hddMax";s:0:"";s:6:"memory";s:0:"";s:12:"documentsMax";s:0:"";s:11:"storageType";s:0:"";s:17:"storageSystemType";s:0:"";s:12:"hddFunctions";s:0:"";s:3:"hdd";s:0:"";s:14:"compConnection";s:0:"";s:16:"networkProtocols";s:0:"";s:11:"paperFormat";s:0:"";s:11:"paperWeight";s:0:"";s:13:"inputCapacity";s:0:"";s:14:"outputCapacity";s:0:"";s:7:"monthly";s:0:"";s:12:"initialToner";s:0:"";s:16:"tonerDeclaration";s:0:"";s:17:"energyConsumption";s:0:"";s:10:"dimensions";s:0:"";s:6:"weight";s:0:"";s:8:"security";s:0:"";s:8:"software";s:0:"";s:5:"users";s:0:"";s:12:"attributeSet";s:1:"0";s:11:"3__sadfasdf";s:0:"";s:8:"3__Aloha";s:0:"";s:9:"3__alloah";s:0:"";s:8:"3__test4";s:0:"";s:8:"3__test3";s:0:"";s:8:"3__TEst2";s:0:"";s:13:"3__1234567890";s:0:"";s:15:"3__Boja_otiska2";s:0:"";s:11:"1__asdfasdf";s:0:"";s:8:"1__Aloha";s:0:"";s:8:"1__TEST1";s:0:"";s:9:"1__alloah";s:0:"";s:8:"1__test3";s:0:"";s:14:"1__Boja_otiska";s:0:"";}', NULL), (2, 'a:0:{}', 'test'), (3, 'a:93:{s:12:"manufacturer";s:1:"0";s:8:"category";s:1:"0";s:7:"country";s:1:"0";s:3:"plD";s:0:"";s:9:"mainCatID";s:0:"";s:9:"secondIDs";s:0:"";s:9:"distribID";s:0:"";s:3:"pdv";s:0:"";s:8:"lowPrice";s:0:"";s:9:"highPrice";s:0:"";s:14:"percentFrom_28";s:0:"";s:12:"percentTo_28";s:0:"";s:12:"priceFrom_28";s:0:"";s:10:"priceTo_28";s:0:"";s:14:"percentFrom_29";s:0:"";s:12:"percentTo_29";s:0:"";s:12:"priceFrom_29";s:0:"";s:10:"priceTo_29";s:0:"";s:16:"speedCopyA4Color";s:0:"";s:16:"speedCopyA4Black";s:0:"";s:16:"speedCopyA3Color";s:0:"";s:16:"speedCopyA3Black";s:0:"";s:14:"firstCopyColor";s:0:"";s:14:"firstCopyBlack";s:0:"";s:11:"heatingTime";s:0:"";s:14:"copyResolution";s:0:"";s:9:"gradation";s:0:"";s:12:"mutliplyCopy";s:0:"";s:10:"formatOrig";s:0:"";s:4:"zoom";s:0:"";s:12:"copyFunction";s:0:"";s:17:"printSpeedA4Color";s:0:"";s:17:"printSpeedA4Black";s:0:"";s:17:"printSpeedA3Color";s:0:"";s:17:"printSpeedA3Black";s:0:"";s:19:"firstCopyPrintColor";s:0:"";s:19:"firstCopyPrintBlack";s:0:"";s:8:"printRes";s:0:"";s:9:"processor";s:0:"";s:6:"driver";s:0:"";s:2:"os";s:0:"";s:5:"fonts";s:0:"";s:14:"printFunctions";s:0:"";s:14:"scanSpeedColor";s:0:"";s:14:"scanSpeedBlack";s:0:"";s:7:"scanRes";s:0:"";s:11:"scanMethods";s:0:"";s:11:"scanFormats";s:0:"";s:15:"scanDestination";s:0:"";s:11:"faxStandard";s:0:"";s:11:"faxTransmit";s:0:"";s:6:"faxRes";s:0:"";s:14:"faxCompression";s:0:"";s:8:"faxModem";s:0:"";s:14:"faxDestination";s:0:"";s:12:"faxFunctions";s:0:"";s:6:"hddMax";s:0:"";s:6:"memory";s:0:"";s:12:"documentsMax";s:0:"";s:11:"storageType";s:0:"";s:17:"storageSystemType";s:0:"";s:12:"hddFunctions";s:0:"";s:3:"hdd";s:0:"";s:14:"compConnection";s:0:"";s:16:"networkProtocols";s:0:"";s:11:"paperFormat";s:0:"";s:11:"paperWeight";s:0:"";s:13:"inputCapacity";s:0:"";s:14:"outputCapacity";s:0:"";s:7:"monthly";s:0:"";s:12:"initialToner";s:0:"";s:16:"tonerDeclaration";s:0:"";s:17:"energyConsumption";s:0:"";s:10:"dimensions";s:0:"";s:6:"weight";s:0:"";s:8:"security";s:0:"";s:8:"software";s:0:"";s:5:"users";s:0:"";s:12:"attributeSet";s:1:"0";s:11:"3__sadfasdf";s:0:"";s:8:"3__Aloha";s:0:"";s:9:"3__alloah";s:0:"";s:8:"3__test4";s:0:"";s:8:"3__test3";s:0:"";s:8:"3__TEst2";s:0:"";s:13:"3__1234567890";s:0:"";s:15:"3__Boja_otiska2";s:0:"";s:11:"1__asdfasdf";s:0:"";s:8:"1__Aloha";s:0:"";s:8:"1__TEST1";s:0:"";s:9:"1__alloah";s:0:"";s:8:"1__test3";s:0:"";s:14:"1__Boja_otiska";s:0:"";}', 'test'), (4, 'a:0:{}', 'samo'), (5, 'a:93:{s:12:"manufacturer";s:1:"0";s:8:"category";s:1:"0";s:7:"country";s:1:"0";s:3:"plD";s:0:"";s:9:"mainCatID";s:0:"";s:9:"secondIDs";s:0:"";s:9:"distribID";s:0:"";s:3:"pdv";s:0:"";s:8:"lowPrice";s:0:"";s:9:"highPrice";s:0:"";s:14:"percentFrom_28";s:0:"";s:12:"percentTo_28";s:0:"";s:12:"priceFrom_28";s:0:"";s:10:"priceTo_28";s:0:"";s:14:"percentFrom_29";s:0:"";s:12:"percentTo_29";s:0:"";s:12:"priceFrom_29";s:0:"";s:10:"priceTo_29";s:0:"";s:16:"speedCopyA4Color";s:0:"";s:16:"speedCopyA4Black";s:0:"";s:16:"speedCopyA3Color";s:0:"";s:16:"speedCopyA3Black";s:0:"";s:14:"firstCopyColor";s:0:"";s:14:"firstCopyBlack";s:0:"";s:11:"heatingTime";s:0:"";s:14:"copyResolution";s:0:"";s:9:"gradation";s:0:"";s:12:"mutliplyCopy";s:0:"";s:10:"formatOrig";s:0:"";s:4:"zoom";s:0:"";s:12:"copyFunction";s:0:"";s:17:"printSpeedA4Color";s:0:"";s:17:"printSpeedA4Black";s:0:"";s:17:"printSpeedA3Color";s:0:"";s:17:"printSpeedA3Black";s:0:"";s:19:"firstCopyPrintColor";s:0:"";s:19:"firstCopyPrintBlack";s:0:"";s:8:"printRes";s:0:"";s:9:"processor";s:0:"";s:6:"driver";s:0:"";s:2:"os";s:0:"";s:5:"fonts";s:0:"";s:14:"printFunctions";s:0:"";s:14:"scanSpeedColor";s:0:"";s:14:"scanSpeedBlack";s:0:"";s:7:"scanRes";s:0:"";s:11:"scanMethods";s:0:"";s:11:"scanFormats";s:0:"";s:15:"scanDestination";s:0:"";s:11:"faxStandard";s:0:"";s:11:"faxTransmit";s:0:"";s:6:"faxRes";s:0:"";s:14:"faxCompression";s:0:"";s:8:"faxModem";s:0:"";s:14:"faxDestination";s:0:"";s:12:"faxFunctions";s:0:"";s:6:"hddMax";s:0:"";s:6:"memory";s:0:"";s:12:"documentsMax";s:0:"";s:11:"storageType";s:0:"";s:17:"storageSystemType";s:0:"";s:12:"hddFunctions";s:0:"";s:3:"hdd";s:0:"";s:14:"compConnection";s:0:"";s:16:"networkProtocols";s:0:"";s:11:"paperFormat";s:0:"";s:11:"paperWeight";s:0:"";s:13:"inputCapacity";s:0:"";s:14:"outputCapacity";s:0:"";s:7:"monthly";s:0:"";s:12:"initialToner";s:0:"";s:16:"tonerDeclaration";s:0:"";s:17:"energyConsumption";s:0:"";s:10:"dimensions";s:0:"";s:6:"weight";s:0:"";s:8:"security";s:0:"";s:8:"software";s:0:"";s:5:"users";s:0:"";s:12:"attributeSet";s:1:"0";s:11:"3__sadfasdf";s:0:"";s:8:"3__Aloha";s:0:"";s:9:"3__alloah";s:0:"";s:8:"3__test4";s:0:"";s:8:"3__test3";s:0:"";s:8:"3__TEst2";s:0:"";s:13:"3__1234567890";s:0:"";s:15:"3__Boja_otiska2";s:0:"";s:11:"1__asdfasdf";s:0:"";s:8:"1__Aloha";s:0:"";s:8:"1__TEST1";s:0:"";s:9:"1__alloah";s:0:"";s:8:"1__test3";s:0:"";s:14:"1__Boja_otiska";s:0:"";}', 'all products'), (6, 'a:93:{s:12:"manufacturer";s:1:"0";s:8:"category";s:1:"0";s:7:"country";s:1:"0";s:3:"plD";s:0:"";s:9:"mainCatID";s:0:"";s:9:"secondIDs";s:0:"";s:9:"distribID";s:0:"";s:3:"pdv";s:0:"";s:8:"lowPrice";s:0:"";s:9:"highPrice";s:0:"";s:14:"percentFrom_28";s:0:"";s:12:"percentTo_28";s:0:"";s:12:"priceFrom_28";s:0:"";s:10:"priceTo_28";s:0:"";s:14:"percentFrom_29";s:0:"";s:12:"percentTo_29";s:0:"";s:12:"priceFrom_29";s:0:"";s:10:"priceTo_29";s:0:"";s:16:"speedCopyA4Color";s:0:"";s:16:"speedCopyA4Black";s:0:"";s:16:"speedCopyA3Color";s:0:"";s:16:"speedCopyA3Black";s:0:"";s:14:"firstCopyColor";s:0:"";s:14:"firstCopyBlack";s:0:"";s:11:"heatingTime";s:0:"";s:14:"copyResolution";s:0:"";s:9:"gradation";s:0:"";s:12:"mutliplyCopy";s:0:"";s:10:"formatOrig";s:0:"";s:4:"zoom";s:0:"";s:12:"copyFunction";s:0:"";s:17:"printSpeedA4Color";s:0:"";s:17:"printSpeedA4Black";s:0:"";s:17:"printSpeedA3Color";s:0:"";s:17:"printSpeedA3Black";s:0:"";s:19:"firstCopyPrintColor";s:0:"";s:19:"firstCopyPrintBlack";s:0:"";s:8:"printRes";s:0:"";s:9:"processor";s:0:"";s:6:"driver";s:0:"";s:2:"os";s:0:"";s:5:"fonts";s:0:"";s:14:"printFunctions";s:0:"";s:14:"scanSpeedColor";s:0:"";s:14:"scanSpeedBlack";s:0:"";s:7:"scanRes";s:0:"";s:11:"scanMethods";s:0:"";s:11:"scanFormats";s:0:"";s:15:"scanDestination";s:0:"";s:11:"faxStandard";s:0:"";s:11:"faxTransmit";s:0:"";s:6:"faxRes";s:0:"";s:14:"faxCompression";s:0:"";s:8:"faxModem";s:0:"";s:14:"faxDestination";s:0:"";s:12:"faxFunctions";s:0:"";s:6:"hddMax";s:0:"";s:6:"memory";s:0:"";s:12:"documentsMax";s:0:"";s:11:"storageType";s:0:"";s:17:"storageSystemType";s:0:"";s:12:"hddFunctions";s:0:"";s:3:"hdd";s:0:"";s:14:"compConnection";s:0:"";s:16:"networkProtocols";s:0:"";s:11:"paperFormat";s:0:"";s:11:"paperWeight";s:0:"";s:13:"inputCapacity";s:0:"";s:14:"outputCapacity";s:0:"";s:7:"monthly";s:0:"";s:12:"initialToner";s:0:"";s:16:"tonerDeclaration";s:0:"";s:17:"energyConsumption";s:0:"";s:10:"dimensions";s:0:"";s:6:"weight";s:0:"";s:8:"security";s:0:"";s:8:"software";s:0:"";s:5:"users";s:0:"";s:12:"attributeSet";s:1:"0";s:11:"3__sadfasdf";s:0:"";s:8:"3__Aloha";s:0:"";s:9:"3__alloah";s:0:"";s:8:"3__test4";s:0:"";s:8:"3__test3";s:0:"";s:8:"3__TEst2";s:0:"";s:13:"3__1234567890";s:0:"";s:15:"3__Boja_otiska2";s:0:"";s:11:"1__asdfasdf";s:0:"";s:8:"1__Aloha";s:0:"";s:8:"1__TEST1";s:0:"";s:9:"1__alloah";s:0:"";s:8:"1__test3";s:0:"";s:14:"1__Boja_otiska";s:0:"";}', 'test again');


INSERT INTO `settingsInfo` (`id`, `settingsName`, `settingsValue`) VALUES (1, 'skype', 'test'), (2, 'pdv', '20');


INSERT INTO `submenu` (`id`, `pageID`, `name_sr`, `name_en`, `linkSr`, `linkEng`, `ordering`, `active`, `permissions`, `pageType`) VALUES (1, 2, 'asdf', 'asdfasdf', 'asdf12312', NULL, 1, 0, NULL, 'general_page'), (2, 2, 'fasdf', 'sadfas', '12312', NULL, 0, 0, NULL, 'general_page'), (3, 2, 'testÂ§a', 'fasdf', '123123asdf', NULL, 3, 0, NULL, 'general_page'), (4, 2, 'asdfasdf', 'asdfasfd', '123asdf', NULL, 2, 0, NULL, 'general_page'), (5, 2, 'adfasdf', 'asdfasdf', 'asdfasdf', NULL, 0, 0, NULL, 'product'), (6, 2, 'afdsf', 'asdfasdf', 'asdfasdf', NULL, 0, 0, NULL, 'product'), (7, 1, 'testsadfsdf', 'tesasdfasdf', 'sadfasdf', NULL, 0, 0, NULL, 'partner'), (8, 15, 'Printeri', 'Printers', 'iplus/prednosti', NULL, 0, 0, NULL, 'general_page');


INSERT INTO `tempProduct` (`id`, `name_sr`, `name_en`, `attributeSet`, `category`, `country`, `plD`, `model`, `ordering`, `manufacturer`, `manufacturerName`, `active`, `status`, `type`, `desc_sr`, `desc_en`, `size_sr`, `size_en`, `power_sr`, `power_en`, `power_type_sr`, `power_type_en`, `characteristics_sr`, `characteristics_en`, `seo_title_sr`, `seo_title_en`, `seo_desc_sr`, `seo_desc_en`, `seo_keywords_sr`, `seo_keywords_en`, `updated`, `created`, `promo`, `price`, `template`, `calcType`, `secondIDs`, `lang`, `obligatory`, `similar`, `optional`, `url`, `pdv`, `mainCatID`, `distribID`) VALUES (15, 'as23423431', '', 0, 2, 0, 'as23423432', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423434', 0, NULL, NULL, NULL, NULL, 0, 'as23423433', NULL), (17, 'as23423433', '', 0, 2, 0, 'as23423434', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423436', 0, NULL, NULL, NULL, NULL, 0, 'as23423435', NULL), (18, 'as23423434', '', 0, 2, 0, 'as23423435', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423437', 0, NULL, NULL, NULL, NULL, 0, 'as23423436', NULL), (19, 'as23423435', '', 0, 2, 0, 'as23423436', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423438', 0, NULL, NULL, NULL, NULL, 0, 'as23423437', NULL), (20, 'as23423436', '', 0, 2, 0, 'as23423437', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423439', 0, NULL, NULL, NULL, NULL, 0, 'as23423438', NULL), (21, 'as23423437', '', 0, 2, 0, 'as23423438', NULL, 0, 1, NULL, 0, 0, 0, '', 'test', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423440', 0, NULL, NULL, NULL, NULL, 0, 'as23423439', NULL), (22, 'as23423438', '', 0, 2, 0, 'as23423439', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423441', 0, NULL, NULL, NULL, NULL, 0, 'as23423440', NULL), (23, 'as23423439', '', 0, 2, 0, 'as23423440', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423442', 0, NULL, NULL, NULL, NULL, 0, 'as23423441', NULL), (24, 'as23423440', '', 0, 2, 0, 'as23423441', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423443', 0, NULL, NULL, NULL, NULL, 0, 'as23423442', NULL), (25, 'as23423441', '', 0, 2, 0, 'as23423442', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423444', 0, NULL, NULL, NULL, NULL, 0, 'as23423443', NULL), (26, 'as23423442', '', 0, 2, 0, 'as23423443', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423445', 0, NULL, NULL, NULL, NULL, 0, 'as23423444', NULL), (27, 'as23423443', '', 0, 2, 0, 'as23423444', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423446', 0, NULL, NULL, NULL, NULL, 0, 'as23423445', NULL), (28, 'as23423444', '', 0, 2, 0, 'as23423445', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423447', 0, NULL, NULL, NULL, NULL, 0, 'as23423446', NULL), (29, 'as23423445', '', 0, 2, 0, 'as23423446', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423448', 0, NULL, NULL, NULL, NULL, 0, 'as23423447', NULL), (30, 'as23423446', '', 0, 2, 0, 'as23423447', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423449', 0, NULL, NULL, NULL, NULL, 0, 'as23423448', NULL), (31, 'as23423447', '', 0, 2, 0, 'as23423448', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423450', 0, NULL, NULL, NULL, NULL, 0, 'as23423449', NULL), (32, 'as23423448', '', 0, 2, 0, 'as23423449', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423451', 0, NULL, NULL, NULL, NULL, 0, 'as23423450', NULL), (33, 'as23423449', '', 0, 2, 0, 'as23423450', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423452', 0, NULL, NULL, NULL, NULL, 0, 'as23423451', NULL), (34, 'as23423450', '', 0, 2, 0, 'as23423451', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423453', 0, NULL, NULL, NULL, NULL, 0, 'as23423452', NULL), (35, 'as23423451', '', 0, 2, 0, 'as23423452', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423454', 0, NULL, NULL, NULL, NULL, 0, 'as23423453', NULL), (37, 'as23423453', '', 0, 2, 0, 'as23423454', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423456', 0, NULL, NULL, NULL, NULL, 0, 'as23423455', NULL), (38, 'as23423454', '', 0, 2, 0, 'as23423455', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423457', 0, NULL, NULL, NULL, NULL, 0, 'as23423456', NULL), (39, 'as23423455', '', 0, 2, 0, 'as23423456', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423458', 0, NULL, NULL, NULL, NULL, 0, 'as23423457', NULL), (40, 'as23423456', '', 0, 2, 0, 'as23423457', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423459', 0, NULL, NULL, NULL, NULL, 0, 'as23423458', NULL), (41, 'as23423457', '', 0, 2, 0, 'as23423458', NULL, 0, 1, NULL, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2012-11-01 09:11:31', '2012-11-01 09:11:31', 0, 0, 0, 0, 'as23423460', 0, NULL, NULL, NULL, NULL, 0, 'as23423459', NULL);


INSERT INTO `ticketsCategory` (`id`, `name_sr`, `name_en`, `active`, `ordering`) VALUES (1, 'test', NULL, 1, 22);


INSERT INTO `visitLog` (`id`, `page`, `user`, `time`, `ipAddress`, `productID`) VALUES (1, '404', 0, 1351926149, NULL, 0), (2, 'home', 0, 1351926151, NULL, 0), (3, '404', 0, 1351926152, NULL, 0), (4, '404', 0, 1351926153, NULL, 0), (5, 'home', 0, 1351926174, '127.0.0.1', 0), (6, 'product', 0, 1351927379, '127.0.0.1', 1), (7, 'product', 0, 1351927379, '127.0.0.1', 1), (8, '404', 0, 1351927380, '127.0.0.1', 0), (9, '404', 0, 1351927380, '127.0.0.1', 0), (10, 'product', 0, 1351927430, '127.0.0.1', 0), (11, 'product', 0, 1351927431, '127.0.0.1', 0), (12, '404', 0, 1351927431, '127.0.0.1', 0), (13, '404', 0, 1351927431, '127.0.0.1', 0), (14, '404', 0, 1351927431, '127.0.0.1', 0), (15, '404', 0, 1351927431, '127.0.0.1', 0), (16, '404', 0, 1351927431, '127.0.0.1', 0), (17, '404', 0, 1351927432, '127.0.0.1', 0), (18, 'product', 0, 1351927441, '127.0.0.1', 1), (19, 'product', 0, 1351927441, '127.0.0.1', 1), (20, '404', 0, 1351927442, '127.0.0.1', 0), (21, '404', 0, 1351927442, '127.0.0.1', 0), (22, '404', 0, 1351927442, '127.0.0.1', 0), (23, '404', 0, 1351927442, '127.0.0.1', 0), (24, '404', 0, 1351927442, '127.0.0.1', 0), (25, '404', 0, 1351927442, '127.0.0.1', 0), (26, 'product', 0, 1351927464, '127.0.0.1', 1), (27, 'product', 0, 1351927465, '127.0.0.1', 1), (28, '404', 0, 1351927465, '127.0.0.1', 0), (29, '404', 0, 1351927465, '127.0.0.1', 0), (30, '404', 0, 1351927465, '127.0.0.1', 0), (31, '404', 0, 1351927465, '127.0.0.1', 0), (32, '404', 0, 1351927465, '127.0.0.1', 0), (33, '404', 0, 1351927466, '127.0.0.1', 0), (34, 'product', 0, 1351927487, '127.0.0.1', 1), (35, 'product', 0, 1351927487, '127.0.0.1', 1), (36, '404', 0, 1351927488, '127.0.0.1', 0), (37, '404', 0, 1351927488, '127.0.0.1', 0), (38, 'home', 0, 1351931120, '127.0.0.1', 0), (39, 'home', 0, 1351937342, '127.0.0.1', 0), (40, 'login', 0, 1351937345, '127.0.0.1', 0), (41, 'myAccount', 1, 1351937351, '127.0.0.1', 0), (42, 'orderInfo', 1, 1351937356, '127.0.0.1', 0), (43, '404', 1, 1351937558, '127.0.0.1', 0), (44, '404', 0, 1352058746, '127.0.0.1', 0), (45, 'home', 0, 1352068344, '127.0.0.1', 0), (46, '404', 0, 1352077172, '127.0.0.1', 0), (47, '404', 0, 1352445954, '127.0.0.1', 0), (48, '404', 0, 1352456196, '127.0.0.1', 0), (49, '404', 0, 1352459307, '127.0.0.1', 0), (50, 'home', 0, 1352489987, '127.0.0.1', 0), (51, '404', 0, 1352490003, '127.0.0.1', 0), (52, '404', 0, 1352549693, '127.0.0.1', 0), (53, '404', 0, 1352551838, '127.0.0.1', 0), (54, '404', 0, 1352685528, '127.0.0.1', 0), (55, 'home', 0, 1352899843, '127.0.0.1', 0), (56, '404', 0, 1352899854, '127.0.0.1', 0), (57, '404', 0, 1352904016, '127.0.0.1', 0), (58, 'home', 0, 1352992783, '127.0.0.1', 0), (59, 'products', 0, 1352992791, '127.0.0.1', 0), (60, '404', 0, 1352992791, '127.0.0.1', 0), (61, 'product', 0, 1352992801, '127.0.0.1', 2), (62, 'home', 0, 1352994718, '127.0.0.1', 0), (63, 'products', 0, 1352994721, '127.0.0.1', 0), (64, '404', 0, 1352994728, '127.0.0.1', 0), (65, 'product', 0, 1352994980, '127.0.0.1', 5), (66, 'products', 0, 1352999958, '127.0.0.1', 0), (67, '404', 0, 1352999959, '127.0.0.1', 0), (68, '404', 0, 1353056868, '127.0.0.1', 0), (69, '404', 0, 1353061640, '127.0.0.1', 0), (70, '404', 0, 1353064914, '127.0.0.1', 0), (71, '404', 0, 1353066717, '127.0.0.1', 0), (72, '404', 0, 1353068549, '127.0.0.1', 0), (73, 'home', 0, 1353147120, '127.0.0.1', 0);




SET FOREIGN_KEY_CHECKS = 1;


