-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 17, 2012 at 01:58 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `original`
--

-- --------------------------------------------------------

--
-- Table structure for table `characteristics`
--

CREATE TABLE IF NOT EXISTS `characteristics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `characteristics_name_sr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `characteristics_name_en` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `category_id_2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `characteristics_category`
--

CREATE TABLE IF NOT EXISTS `characteristics_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name_sr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_name_en` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `characteristics_value`
--

CREATE TABLE IF NOT EXISTS `characteristics_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value_name_sr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_name_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `characteristics_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `characteristics_id` (`characteristics_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
