<?php
if(!isset($unsubscribe)){ $unsubscribe = '';}
    if(!isset($reason)){ $reason = '';}
$template = '<html lang="en">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title>
      '.$title.'
    </title>
	<style type="text/css">
	a:hover { text-decoration: none !important; }
	h1 {color: #3f89c6; font: 300 32px Helvetica, Arial, sans-serif; letter-spacing:-1px; margin: 0; padding: 0; line-height: 40px;}
	.header p {color: #c6c6c6; font: normal 12px Helvetica, Arial, sans-serif; margin: 0; padding: 0; line-height: 18px;}

	.content h2 {color:#646464; font-weight: bold; margin: 0; padding: 0; line-height: 26px; font-size: 18px; font-family: Helvetica, Arial, sans-serif;  }
	.content p {color:#767676; font-weight: normal; margin: 0; padding: 0; line-height: 20px; font-size: 12px;font-family: Helvetica, Arial, sans-serif;}
	.content a {color: #7ab115; text-decoration: none;}
	.footer p {font-size: 11px; color:#7d7a7a; margin: 0; padding: 0; font-family: Helvetica, Arial, sans-serif;}
	.footer a {color: #7ab115; text-decoration: none;}
	</style>
  </head>
  <body style="margin: 0; padding: 0; background:#ccc url(\'imagesq/bg_email.png\');" bgcolor="#434343">
  		<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="padding: 35px 0; background: #ccc url(\'images/bg_emasil.png\');">
		  <tr>
		  	<td align="center" style="margin: 0; padding: 0; background:#ccc url(\'imageas/bg_email.png\') ;" >
			    <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: Helvetica, Arial, sans-serif; background:#fefefe;border-bottom:solid 1px #ddd" class="header">
			      	<tr>
						<td width="600" align="left" style="padding:0px; font-size: 0; line-height: 0; height: 7px;" height="7" colspan="2"></td>
				      </tr>
					<tr>
					<td width="20"style="font-size: 0px;">&nbsp;</td>
			        <td width="580" align="left" style="padding: 18px 0 10px;"> 
                                    <h1 style=" line-height: 40px;"><img src="'.SITE_ROOT.'assets/img/omnipromet-logo.jpg" alt="alt"/></h1>

			        </td>
			      </tr>
				</table><!-- header-->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: Helvetica, Arial, sans-serif; background: #fff;" bgcolor="#fff">

					<tr>
			        <td width="600" valign="top" align="left" style="font-family: Helvetica, Arial, sans-serif; padding: 20px 0 0;" class="content">
						<table cellpadding="0" cellspacing="0" border="0"  style="color: #717171; font: normal 11px Helvetica, Arial, sans-serif; margin: 0; padding: 0;" width="600">
						<tr>
							<td width="21" style="font-size: 1px; line-height: 1px;"><img src="'.E_TEMP_ROOT.'spacer.gif" alt="space" width="20"></td>
							<td style="padding: 0;  font-family: Helvetica, Arial, sans-serif; background: url(\'images/bg_date_wide.png\') no-repeat left top; height:20px; line-height: 20px;"  align="center" width="558" height="20">
								<h3 style="color:#666; font-weight: bold; text-transform: uppercase; margin: 0; padding: 0; line-height: 10px; font-size: 10px;">'.date("l, d F Y.", time()).'</h3>
							</td>
							<td width="21" style="font-size: 1px; line-height: 1px;"><img src="'.E_TEMP_ROOT.'spacer.gif" alt="space" width="20"></td>
						</tr>
						<tr>
							<td width="21" style="font-size: 1px; line-height: 1px;"><img src="'.E_TEMP_ROOT.'spacer.gif" alt="space" width="20"></td>
							<td style="padding: 20px 10px ; font-size:14px" align="left">
                                                               ';
$template .= '<h1>'.$title.'</h1>';
$template .=$content;
    $template .='
							</td>

						</tr>


						</table>
					</td>

			      </tr>
				  	<tr>
						<td width="600" align="left" style="padding: font-size:300; line-height: 0; height: 3px;" height="3" colspan="2"><img src="'.E_TEMP_ROOT.'bg_bottom.png" alt="header bg"></td>
				      </tr>
				</table><!-- body -->
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="font-family: Helvetica, Arial, sans-serif; line-height: 10px;" class="footer">
				<tr>
			        <td align="center" style="padding: 5px 0 10px; font-size: 11px; color:#7d7a7a; margin: 0; line-height: 1.2;font-family: Helvetica, Arial, sans-serif;" valign="top">
						<br><p style="font-size: 11px; color:#7d7a7a; margin: 0; padding: 0; font-family: Helvetica, Arial, sans-serif;">';
    $template.= $reason;
    $template .='</p>
						<p style="font-size: 11px; color:#7d7a7a; margin: 0; padding: 0; font-family: Helvetica, Arial, sans-serif;">Having trouble reading this? <webversion style="color: #326a9b; text-decoration: none;">View it in your browser</webversion>.';
                           if($unsubscribe!=''){
                               $template .= 'Not interested? <a href="'.$unsubscribeLink.'" style="color: #326a9b; text-decoration: none;">Unsubscribe</a> instantly.';

                               }
                               $template .='</p>
					</td>
			      </tr>
				</table><!-- footer-->
		  	</td>
		  	</td>
		</tr>
    </table>
  </body>
</html>';