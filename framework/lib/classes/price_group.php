<?php

// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(CLASSES . 'database.php');

class PriceGroup extends DatabaseObject
{

    protected static $table_name = "price_group";
    protected static $db_fields = array('id', 'percentage', 'name', 'templateID', 'isMain', 'parent', 'productID', 'conversionType', 'calcType', 'updated');
    public $id;
    public $name;
    public $percentage;
    public $isMain;
    public $templateID;
    public $parent;
    public $productID;
    public $conversionType;
    public $calcType;

    public $updated;

    // Common Database Methods
    public static function find_all()
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name);
    }

    public function conversionType()
    {
        switch ($this->conversionType) {
            case 1:
                return "Fiksna cena";
                break;
            case 2:
                return "Oduzimanje";
                break;
            case 3:
                return "Dodavanje";
                break;
        }
    }

    public static function find_main()
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE isMain = 1");
    }

    public static function find_main_id($id)
    {
        return array_shift(self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE isMain = 1 AND id = '{$id}'"));
    }

    public static function find_for_value($groupID, $templateID)
    {
        $result = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE parent = '{$groupID}' AND templateID = '{$templateID}'");
        if ($result && count($result) > 0) {
            return array_shift($result);
        } else {
            return false;
        }

    }

    public static function find_is_exists($groupID, $productID)
    {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE parent = '{$groupID}' AND productID = '{$productID}' order by id DESC LIMIT 1 ";
        $result = self::find_by_sql($sql);
        if ($result && count($result) > 0) {
            return array_shift($result);
        } else {
            return false;
        }

    }
    public static function get_unique_id(){
        $sql = 'SELECT FLOOR(100 + RAND() * 899999) AS random_number FROM ' . self::$table_name . ' WHERE "random_number" NOT IN (SELECT id FROM price) LIMIT 1';
        global $database;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        if(is_array($row)){
            return array_shift($row);
        }else{
            return $row;
        }
    }

    public static function isExists($group = '')
    {
        $result = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE name='$group'");
        if ($result && count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function find_by_product($group, $product)
    {
//        $result_array =  self::find_by_sql("SELECT * FROM " . self::$table_name." WHERE select * from price_group where parent = '$group' AND groupID in (select groupID from price where )");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_parent($group, $template)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE parent = '{$group}' AND templateID = '{$template}' LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_id($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_sql($sql = "")
    {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_all()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    public static function getParentName($id)
    {
        global $database;
        $sql = "SELECT name FROM " . self::$table_name." where id = '{$id}' LIMIT 1";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        if(is_array($row)){
            return array_shift($row);
        }else{
            return '';
        }
    }

    public static function count_active()
    {
        global $database;
        $sql = "SELECT COUNT(id) FROM " . self::$table_name . " where active = '1'";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    private static function instantiate($record)
    {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute)
    {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes()
    {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes()
    {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save()
    {
        // A new record won't have an id yet.
        return (isset($this->id) && $this->id != '') ? $this->update() : $this->create();
    }

    public function create()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return $this->id; //menjano prethodno - true
        } else {
            return 0; //menjano prethodno - false
        }
    }

    public function importCsvData()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>