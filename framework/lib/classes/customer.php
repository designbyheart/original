<?php
require_once(CLASSES . 'database.php');

class Customer extends DatabaseObject
{

    protected static $table_name = "customer";
    protected static $db_fields = array('id', 'name', 'company', 'username', 'password', 'type', 'status',
        'registered_date', 'auth_code', 'updated', 'address', 'phones', 'email', 'company_PIB',
        'company_regNumber', 'city', 'state', 'notes', 'logo', 'website', 'contact_person', 'ip_address', 'client_category', 'partner');

    public $id;
    public $name;
    public $company;
    public $username;
    public $password;
    public $type;
    public $status;
    public $registered_date;
    public $auth_code;
    public $updated;
    public $address;
    public $phones;
    public $email;
    public $company_PIB;
    public $company_regNumber;
    public $city;
    public $state;
    public $notes;
    public $logo;
    public $website;
    public $contact_person;
    public $ip_address;
    public $client_category;
    public $partner;


    public static function authenticate($username = "", $password = "")
    {
        global $database;
        $username = $database->escape_value($username);
        $password = $database->escape_value($password);

        $sql = "SELECT * FROM customer ";
        $sql .= "WHERE username = '{$username}' ";
        $sql .= "AND password = '{$password}' ";
        $sql .= "LIMIT 1";
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function isEmailFree($email)
    {
        global $database;
        $password = $database->escape_value($email);

        $sql = "SELECT * FROM customer ";
        $sql .= "WHERE email = '{$email}' ";
        $sql .= "LIMIT 1";
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? false : true;
    }

    public static function activate($verification)
    {
        $result = self::find_by_sql("SELECT * from customer where auth_code = '" . $verification . "'  limit 1");
        return !empty($result) ? false : array_shift($result);
    }

    // Common Database Methods
    public static function find_all()
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name);
    }

    public function full_name()
    {
        if (isset($this->name)) {
            return $this->name;
        } else {
            return "";
        }
    }

    public function type()
    {
        //1 - pending
        //2 - approved
        //3 - canceled
        //4 - declined
        //5 - email verifikotion done
        switch ($this->type) {
            case 1:
                return trans('Čeka odobrenje', 'Pending');
                break;
            case 2:
                return trans('Odobren', 'Approved');
                break;
            case 3:
                return trans('Otkazan', 'Canceled');
                break;
            case 4:
                return trans('Odbijen', 'Declined');
                break;
            case 5:
                return trans('Verifikovan', 'Verified');
                break;
        }

    }

    public static function find_by_id($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_sql($sql = "")
    {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_all()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public static function count_active()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " where type = 2 OR type = 5";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    private static function instantiate($record)
    {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute)
    {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes()
    {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes()
    {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save()
    {
        // A new record won't have an id yet.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>