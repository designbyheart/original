<?php

// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(CLASSES . 'database.php');

class Promo extends DatabaseObject {

    protected static $table_name = "promo";
    protected static $db_fields = array('id', 'name_sr', 'name_en', 'dateStart', 'dateEnd', 'desc_en', 'desc_sr','products','discount', 'active', 'ordering', 'subTitle_sr', 'subTitle_en', 'price','seo_title_sr', 'seo_title_en', 'seo_desc_sr', 'seo_desc_en', 'seo_keywords_sr', 'seo_keywords_en','fullWidth');
    public $id;
    public $name_sr;
    public $name_en;
    public $dateStart;
    public $dateEnd;
    public $desc_en;
    public $desc_sr;
    public $products;
    public $discount;
    public $active;
    public $price;
    public $ordering;
    public $subTitle_sr;
    public $subTitle_en;
    public $seo_title_sr;
    public $seo_title_en;
    public $seo_desc_sr;
    public $seo_desc_en;
    public $seo_keywords_sr;
    public $seo_keywords_en;
    public $fullWidth;

    // Common Database Methods
    public static function find_all() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name);
    }
    public static function products($self, $arrayOnly = TRUE){
        if($arrayOnly==false){
            $prIDs = array_filter(explode(',', $self->products));
            $products = array();
            foreach($prIDs as $pr){
               $p  = Product::find_by_id($pr);
                echo $p->name_sr;
                if(isset($p)){
                    $products[] = trans($p->name_sr, $p->name_en);
                }
            }
            return implode(',', $products);
        }
        return explode(',', $self->products);
    }
    public static function find_all_active() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name. " where active = 1");
    }
    public static function find_expired() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name. " where dateEnd < '".time()."'");
    }
    public static function find_act_children($id){
        return self::find_by_sql("SELECT * FROM " . self::$table_name." where active =1 and parent_cat= ".$id." order by ordering ASC");
    }
    public static function find_random($limit = NULL){
        if($limit > 0){ $limit  = ' LIMIT '.$limit; }
    return self::find_by_sql("SELECT * FROM " . self::$table_name." where active =1 order by RAND() ". $limit);
    }
    public  static function search($term){
        return self::find_by_sql("SELECT distinct(id) FROM " . self::$table_name." where name_sr like '%{$term}%' OR  name_en like '%{$term}%' ");
    }
    public static function find_active() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name." where active = 1 order by ordering ASC");
    }
    public static function find_by_name($name = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE name_sr='{$name}' OR  name_sr like '".str_replace('-', '%', $name)."' LIMIT 1 ";
        $result_array = self::find_by_sql($sql);
        return!empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return!empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_by_sql($sql = "") {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
        public static function count_active() {
        global $database;
        $sql = "SELECT COUNT(id) FROM " . self::$table_name." where active = '1'" ;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return (isset($this->id) && $this->id!='') ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return $this->id;						//menjano prethodno - true
        } else {
            return 0;								//menjano prethodno - false
        }
    }
    
public function importCsvData() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() < 2 ) ? true : false;
    }

    public function delete() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1 ) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>