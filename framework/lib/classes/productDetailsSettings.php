<?php
// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(CLASSES . 'database.php');

class ProductDetailsSettings extends DatabaseObject
{

    protected static $table_name = "productDetailsSettings";
    protected static $db_fields = array('id', 'name_sr', 'name_en', 'parent');

    public $id;
    public $name_en;
    public $name_sr;
    public $parent;


    // Common Database Methods
    public static function find_all()
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " order by id DESC");
    }


    public static function find_main()
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " where  parent = '0' order by id DESC");
    }

    public static function find_child($parent)
    {
        $result = self::find_by_sql("SELECT * FROM " . self::$table_name . " where  INSTR(productDetailsSettings.parent, '{$parent}') > 0  order by id DESC");
        if (count($result) < 1) {
            return false;
        } else {
            return $result;
        }
    }

    public static function find_by_attributeSet($attributeSet)
    {
        $sql = "SELECT * from " . self::$table_name . " where id in (" . $attributeSet . ")";
        return self::find_by_sql($sql);
    }

    public static function isExisting($name)
    {
        $sql = "SELECT * from " . self::$table_name . " where name_sr = '{$name}' ";
        $result_array = self::find_by_sql($sql);
        if (empty($result_array)) {
            return false;
        } else {
            return true;
        }
    }

    public static function find_by_page($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type = '" . $id . "' ORDER BY id DESC LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function parent($id)
    {
        global $database;
        $sql = "SELECT name_sr FROM " . self::$table_name . " where parent = '" . $id . "' ";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public static function find_parent($id)
    {
        $items = explode(',', $id);
        global $database;
        $itemsList = array();
        foreach ($items as $i) {
            $sql = "SELECT name_sr FROM " . self::$table_name . " where parent = '" . $i . "' ";
            $result_set = $database->query($sql);
            $row = $database->fetch_array($result_set);
            if ($row) {
                $itemsList[] = array_shift($row);
            }
        }
        if (count($itemsList) < 1) {
            return '--nema roditelja--';
        }
        return ucfirst(implode(', ', $itemsList));
    }

    public static function find_by_reference($refID, $type = 0, $limit = NULL)
    {
        $lim = '';
        if ($limit) {
            $lim = 'LIMIT ' . $limit;
        }
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type = '" . $type . "' AND refID = '$refID' ORDER BY id DESC " . $lim);
        if ($limit > 1) {
            return $result_array;
        } else {
            return !empty($result_array) ? array_shift($result_array) : false;
        }
    }

    public static function getSettingName($id)
    {
        global $database;
        $sql = "SELECT name_sr FROM " . self::$table_name . " where id = '{$id}' LIMIT 1";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        if ($row) {
            return array_shift($row);
        }

    }

    public static function find($id, $type)
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " where refID = '{$id}' AND type = '{$type}' order by id DESC");
    }

    public static function find_by_id($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id='{$id}' LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_productID($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE refID='{$id}' and type='product'  LIMIT 1");
        return !empty($result_array) ? $result_array : false;
    }

    public static function find_by_sql($sql = "")
    {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_all()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public static function latest()
    {
        global $database;
        $sql = "SELECT amount FROM " . self::$table_name . " ORDER BY id DESC LIMIT 1";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        if ($row) {
            return array_shift($row);
        }
    }

    public static function previous($id)
    {
        global $database;
        $id -= 1;
        $sql = "SELECT amount FROM " . self::$table_name . " where id='{$id}' LIMIT 1 ";
        $result_set = $database->query($sql);

        $row = $database->fetch_array($result_set);
        if ($row) {
            return array_shift($row);
        }
    }

    private static function instantiate($record)
    {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];

        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute)
    {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes()
    {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes()
    {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save()
    {
        // A new record won't have an id yet.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>