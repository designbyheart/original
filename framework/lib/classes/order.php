<?php

// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(CLASSES . 'database.php');

class Order extends DatabaseObject
{

    protected static $table_name = "productOrder";
    protected static $db_fields = array('id', 'customerID', 'orderDate', 'products', 'address', 'ipAddress', 'status', 'totalPrice', 'orderID', 'modified');

    public $id;
    public $orderDate;
    public $customerID;
    public $orderID;
    public $products;
    public $address;
    public $ipAddress;
    public $status;
    public $totalPrice;
    public $modified;

    // Common Database Methods
    public static function find_all()
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " order by id DESC");
    }

    public static function find_by_client($clientID, $limit = NULL)
    {
        $l = '';
        if (intVal($limit > 0)) {
            $l = ' LIMIT ' . $limit;
        }
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " where customerID = '" . intVal($clientID) . "' order by id DESC " . $l);
    }

    public function ordersPerDay($status)
    {
        $charts = array();
        $today = date("d.M.Y", time());
        $timeToday = strtotime($today . " 00:00:00");
        $day = $timeToday;
        for ($i = 1; $i < 11; $i++) {
            $day = $timeToday - (60 * 60 * 24);
            $sql = "select count(id) from productOrder where orderDate < {$timeToday} AND orderDate > {$day} AND status = " . $status;
            $charts[] = array(date("d.m", $timeToday), (int)self::countOrders($sql));
            $timeToday = $day;
        }
        return $charts;
    }

    public function countOrders($sql)
    {
        global $database;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public static function find_by_client_active($clientID, $limit = NULL)
    {
        $l = '';
        if (intVal($limit > 0)) {
            $l = ' LIMIT ' . $limit;
        }
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " where customerID = '" . intVal($clientID) . "' AND status != '3' AND status != '4' AND status != '5'   order by id DESC " . $l);
    }

    //notCompleteOrder 7
    //pending =1
    //confirmed =2
    //payed = 3
    //sentProducts = 4
    //closed = 5
    //delivered = 8

    public function status()
    {
        switch ($this->status) {
            case 1:
                return trans("Nova narudžbina", "Pending");
                break;
            case 2:
                return trans("Potvrđena (email)", "Confirmed");
                break;
            case 3:
                return trans("Uplaćena", "Payed");
                break;
            case 4:
                return trans("Proizvodi poslati", "Products sent");
                break;
            case 5:
                return trans("Uspešno Zaključena", "Closed Successfully");
                break;
            case 6:
                return trans("Zaključena Neuspešno", "Closed unsuccessfully");
                break;
            case 7:
                return trans("Nije završena", "Not completed");
                break;
            case 8:
                return trans("Klijent potvrdio preuzimanje isporuke", "Client confirmed delivery");
                break;
        }
    }

    public function setStatus($newStatus)
    {
        $newSt = 0;
        switch ($newStatus) {
            case "Pending":
            case"pending":
                $newSt = 1;
                break;
            case "Confirmed":
            case "confirmed":
                $newSt = 2;
                break;
            case "Payed":
            case "payed":
                $newSt = 3;
                break;
            case $newSt = 'Sent':
            case "sent":
                $newSt = 4;
                break;
            case "Closed Successfully":
            case "closed":
                $newSt = 5;
                break;
            case  "Closed unsuccessfully":
                $newSt = 6;
                break;
            case "Not completed":
            case "not completed":
                $newSt = 7;
                break;
            case "Client confirmed delivery":
                $newSt = 8;
                break;
        }

        return $newSt;
    }

    public static function find_by_id($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_orderID($id = 0)
    {
        $orderID = alphaID($id, true);
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE orderID='{$orderID}' LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_sql($sql = "")
    {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function pending_orders()
    {
        global $database;
        $today = date("d.m.y", time());
        $previousTime = strtotime($today) - 60 * 60 * 24 * 7;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        echo $sql . '<br><br>';

        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        print_r($row);
        return array_shift($row);
    }

    public static function count_active()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . ' where status != 0 AND status != 5 AND status !=6 ';
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public static function count_all()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    private static function instantiate($record)
    {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute)
    {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes()
    {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes()
    {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save()
    {
        if ($this->orderID == '') {
            $this->orderID = time() . $this->id . $_SESSION['clientID'];
        }
        // A new record won't have an id yet.
        return isset($this->id) ? $this->update() : $this->create();
    }

    public function create()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>