<?php

// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(CLASSES . 'database.php');

class Price extends DatabaseObject
{

    protected static $table_name = "price";
    protected static $db_fields = array('id', 'groupID', 'templateID', 'productID', 'amount', 'updated', 'changedBy');
    public $id;
    public $productID;
    public $groupID;
    public $amount;
    public $updated;
    public $templateID;
    public $changedBy;

    // Common Database Methods
    public static function find_all()
    {
        return self::find_by_sql("SELECT *, (select file from gallery where gallery.refID = category.id and  ) FROM " . self::$table_name);
    }

    public static function find_by_id($id = 0)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_product($id)
    {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " where productID = '{$id}'");
    }

    public static function find_by_product_and_group($id, $groupID)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " where productID = '{$id}' && groupID = '{$groupID}' ORDER BY id DESC");
        if (count($result_array) > 0) {
            return array_shift($result_array);
        } else {
            return false;
        }
    }
    public static function find_by_product_with_group($id, $groupID)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " where productID = '{$id}' and groupID in (select id from price_group where parent= '{$groupID}')");
        if (count($result_array) > 0) {
            return array_shift($result_array);
        } else {
            return false;
        }
    }
    public static function search_by_price($min, $max, $groupID)
    {
        $maxLimit = '';
        if($max>0){
            $maxLimit = " AND amount <= '".$max."'";
        }
        $sql = "SELECT * FROM " . self::$table_name . " where groupID in (select id from price_group where parent= {$groupID}) and amount >= {$min} ".$maxLimit;

        $result_array = self::find_by_sql($sql);
        if (count($result_array) > 0) {
            return $result_array;
        } else {
            return false;
        }
    }
    public static function find_by_group($groupID)
    {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " where groupID in (select id from price_group where parent= '{$groupID}')");
        if (count($result_array) > 0) {
            return array_shift($result_array);
        } else {
            return false;
        }
    }

    public static function find_by_sql($sql = "")
    {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function get_unique_id(){
        $sql = 'SELECT FLOOR(100 + RAND() * 899999) AS random_number FROM price WHERE "random_number" NOT IN (SELECT id FROM price) LIMIT 1';
        global $database;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        if(is_array($row)){
            return array_shift($row);
        }else{
            return $row;
        }
    }
    public static function count_all()
    {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public static function count_active()
    {
        global $database;
        $sql = "SELECT COUNT(id) FROM " . self::$table_name . " where active = '1'";
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    private static function instantiate($record)
    {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute)
    {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes()
    {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes()
    {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save()
    {
        // A new record won't have an id yet.
        return (isset($this->id) && $this->id != '') ? $this->update() : $this->create();
    }

    public function create()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            $this->id = $database->insert_id();
            return $this->id; //menjano prethodno - true
        } else {
            return 0; //menjano prethodno - false
        }
    }

    public function importCsvData()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($database->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function update()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete()
    {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE id=" . $database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update()
        // after calling $user->delete().
    }

}

?>