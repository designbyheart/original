<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/19/12
 * Time: 7:54 PM
 * To change this template use File | Settings | File Templates.
 */

/*
 * EnterSolutions
 * @Author : Morne Oosthuizen (http://www.entersolutions.co.za)
 *
 */

class Communications
{

    /*
    * Send Sms
    * @param         $aMsgData = array(Number,Message);
    * @return      String
    *
    */
    public function SendSms($aMsgData)
    {
        $result = file_get_contents('http://api.clickatell.com/http/sendmsg?api_id='.CLICKATELL_API.'&user='.CLICKATELL_USERNAME.'&password='.CLICKATELL_PASSWORD.'&to='.$aMsgData['to'].'&text='.substr(urlencode($aMsgData['msg']),0,160));
        return $result;
    }

    /*
    * Get available credits
    *
    * @return         String
    *
    */
    public function GetBalance()
    {
        $result = file_get_contents('http://api.clickatell.com/http/getbalance?api_id='.CLICKATELL_API.'&user='.CLICKATELL_USERNAME.'&password='.CLICKATELL_PASSWORD);
        return $result;
    }


}