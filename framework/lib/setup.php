<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/10/12
 * Time: 3:23 AM
 * To change this template use File | Settings | File Templates.
 */
switch ($_SERVER["SERVER_NAME"]) {
    case "original.net":
    case "localhost/original":
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("ROOT_DIR") ? null : define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"] . '/');
        define("IS_SERVER", 'no');
        defined("ADMIN") ? null : define("ADMIN", SITE_ROOT . 'admin/');
        break;
    case "localhost":
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/Original/");
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/Original/");
        defined("ROOT_DIR") ? null : define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"] . '/Original/');
        define("IS_SERVER", 'yes');
        defined("ADMIN") ? null : define("ADMIN", SITE_ROOT . 'admin/');
        break;
    case "127.0.0.1": // ime lokalnog servera
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/"); //putanju do foldera iz root servera
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("ROOT_DIR") ? null : define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"] . '/');
        defined("ADMIN") ? null : define("ADMIN", SITE_ROOT . 'admin/');
        break;
    case 'admin.original.net':
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("ROOT_DIR") ? null : define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"] . '/../');
        defined("ADMIN") ? null : define("ADMIN", SITE_ROOT . 'admin/');
        break;
    case '10.0.1.7':
    case '192.168.1.9':
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/original/");
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/original/");
        defined("ROOT_DIR") ? null : define("ROOT_DIR", $_SERVER["DOCUMENT_ROOT"] . '/original/');
        define("IS_SERVER", 'no');
        defined("ADMIN") ? null : define("ADMIN", SITE_ROOT . 'admin/');
        break;
    case "original.cnnproject.com":
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/");
        defined("ASSETS") ? null : define("ASSETS", SITE_ROOT . "assets/");
        define("ROOT_DIR", '/home/origb2b/public_html/');
        break;
    default;
        defined("SITE_ROOT") ? null : define("SITE_ROOT", "http://" . $_SERVER["SERVER_NAME"] . "/~origb2b/");
        defined("SITE_ROOT_CL") ? null : define("SITE_ROOT_CL", "http://" . $_SERVER["SERVER_NAME"] . "/~origb2b/");
        defined("ASSETS") ? null : define("ASSETS", SITE_ROOT . "assets/");
        define("ROOT_DIR", '/home/origb2b/public_html/');
        break;
}

date_default_timezone_set('Europe/Belgrade');
defined("LIBS_DIR") ? null : define("LIBS_DIR", ROOT_DIR . "framework/lib/");

defined("ASSETS") ? null : define("ASSETS", SITE_ROOT . "assets/");
defined("INC") ? null : define("INC", ROOT_DIR . "framework/inc/");
defined("MOD") ? null : define("MOD", ROOT_DIR . "framework/models/");
defined("VIEW") ? null : define("VIEW", ROOT_DIR . "framework/views/");
defined("ADMIN_ROOT") ? null : define("ADMIN_ROOT", ROOT_DIR . '/admin/');
defined("CLASSES") ? null : define("CLASSES", LIBS_DIR . "classes/");

defined("CSS") ? null : define("CSS", ASSETS . "css/");
defined("JS") ? null : define("JS", ASSETS . "js/");
defined("IMG") ? null : define("IMG", ASSETS . "images/");

defined("PAGE_404") ? null : define("PAGE_404", INC . "404.php");

defined("ADMIN") ? null : define("ADMIN", SITE_ROOT . 'admin/');

defined("BANNER") ? null : define("BANNER", SITE_ROOT . "images/gallery/thumbsM/");
defined("GALL") ? null : define("GALL", SITE_ROOT . "images/gallery/");
defined("GALL_DIR") ? null : define("GALL_DIR", ROOT_DIR . "images/gallery/");
defined("Th_M") ? null : define("Th_M", SITE_ROOT . "images/gallery/thumbsM/");
defined("DEF_IMG") ? null : define("DEF_IMG", "no-image.png");
defined("Th_S") ? null : define("Th_S", SITE_ROOT . "images/gallery/thumbsS/");
defined("E_TEMP") ? null : define("E_TEMP", ROOT_DIR . 'framework/lib/mailTemp/full_width.php');
defined("E_TEMP_ROOT") ? null : define("E_TEMP_ROOT", SITE_ROOT . 'framework/lib/mailTemp/images/');

defined("DRIVER_F") ? null : define("DRIVER_F", ROOT_DIR . 'driversDownload/');
defined("INSTRUCTION_F") ? null : define("INSTRUCTION_F", ROOT_DIR . 'instructionDownload/');

defined("COUNTER") ? null : define("COUNTER", INC . 'counter.php');
defined("CSV") ? null : define("CSV", ROOT_DIR . 'admin/uploadedFiles/');

//config, funcion_lib, sesion, database, database_object,
require_once(LIBS_DIR . "config.php");
require_once(LIBS_DIR . "function_lib.php");
require_once(LIBS_DIR . "session.php");
require_once(CLASSES . "database.php");
require_once(CLASSES . "database_object.php");

require_once(CLASSES . 'administrator.php');
require_once(CLASSES . "product.php");
require_once(CLASSES . "tempProduct.php");
require_once(CLASSES . "category.php");
require_once(CLASSES . "manufacturer.php");
require_once(CLASSES . "general_page.php");
require_once(CLASSES . "gallery.php");
require_once(CLASSES . "news.php");
require_once(CLASSES . "mail.php");
//require_once(CLASSES."cache.php");
require_once(CLASSES . 'reference.php');
require_once(CLASSES . 'news.php');
require_once(CLASSES . 'customer.php');
require_once(CLASSES . 'customerHistory.php');
require_once(CLASSES . 'order.php');
require_once(CLASSES . 'pricetemplate.php');
require_once(CLASSES . 'pricetemplate.php');
require_once(CLASSES . 'price.php');
require_once(CLASSES . 'price_group.php');
require_once(CLASSES . 'characteristics.php');
require_once(CLASSES . 'mainMenu.php');
require_once(CLASSES . 'promo.php');
require_once(CLASSES . 'drivers.php');
require_once(CLASSES . 'instructions.php');
require_once(CLASSES . 'submenu.php');
require_once(CLASSES . 'euro.php');
require_once(CLASSES . 'log.php');
require_once(CLASSES . 'order.php');
require_once(CLASSES . 'address.php');
require_once(CLASSES . 'settings.php');
require_once(CLASSES . 'countries.php');
require_once(CLASSES . 'country.php');
require_once(CLASSES . 'productDetailsSettings.php');
require_once(CLASSES . 'productDetails.php');
require_once(CLASSES . 'notes.php');
require_once(CLASSES . 'attributeSet.php');
require_once(CLASSES . 'ticketsCategory.php');
require_once(CLASSES . 'PHPExcel.php');
require_once(CLASSES . 'searchTemplates.php');
require_once(CLASSES . 'logVisit.php');
require_once(CLASSES . 'characteristic_category.php');
require_once(CLASSES . 'characteristic_value.php');
require_once(CLASSES . 'iplus-product.php');
require_once(CLASSES . 'iplus-promo.php');
