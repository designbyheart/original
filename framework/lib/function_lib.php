<?php
/**
Setting active item in left menu
@param page name, link name: $selPage
@returns activeClass
 */
function activeMenu($selPage)
{
    global $page;
    if ($page == $selPage) {
        return 'class="active"';
    }
    return "";
}

//getting users browser language
function get_client_language($availableLanguages, $default = 'en')
{
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

        foreach ($langs as $value) {
            $choice = substr($value, 0, 2);
            if (in_array($choice, $availableLanguages)) {
                return $choice;
            }
        }
    }
    return $default;
}

function skype($skypeName, $isChat = false)
{
    if (!$isChat) {
        return '<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
<a href="skype:' . $skypeName . '?call"><img src="http://download.skype.com/share/skypebuttons/buttons/call_blue_white_124x52.png" style="border: none;" width="124" height="52" alt="Skype Me™!" /></a>';
    } else {
        return '<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
<a href="skype:' . $skypeName . '?chat"><img src="http://download.skype.com/share/skypebuttons/buttons/chat_blue_white_164x52.png" style="border: none;" width="164" height="52" alt="Chat with me" /></a>';
    }
}

function data_uri($file, $mime)
{
    $contents = file_get_contents($file);
    $base64 = base64_encode($contents);
    echo "data:$mime;base64,$base64";
}

function userRoleSel($role, $type)
{
    if ($role == $type) {
        return ' selected ';
    }
}

function getPageUrl()
{
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}


function cleanQuery($string)
{
    if (get_magic_quotes_gpc()) // prevents duplicate backslashes
    {
        $string = stripslashes($string);
    }
    if (phpversion() >= '4.3.0') {
        $string = mysql_real_escape_string($string);
    } else {
        $string = mysql_escape_string($string);
    }
    return $string;
}

function trans($sr, $en)
{
    if (!isset($_SESSION['lang'])) {
        $_SESSION['lang'] = 'sr';
    }
    $translated = "";
    switch ($_SESSION['lang']) {
        case 'sr':
        default:
            $translated = $sr;
            break;
        case 'en':
            $translated = $en;
            break;
    }
    return $translated;
}

function redirect_to($location = null)
{
    if ($location != null) {
        header("Location: {$location}");
        exit;
    }
}

function rearrange($arr)
{
    foreach ($arr as $key => $all) {
        foreach ($all as $i => $val) {
            $new[$i][$key] = $val;
        }
    }
    return $new;
}

function uploadPhoto($file, $oldFile, $W, $tSW, $tSH, $tMW, $tMH, $fName)
{
    if ($oldFile != '' && file_exists(ROOT_DIR . 'images/gallery/' . $oldFile)) {
        unlink(ROOT_DIR . 'images/gallery/' . $oldFile);
    }
    if ($oldFile != '' && file_exists(ROOT_DIR . 'images/gallery/thumbsS/' . $oldFile)) {
        unlink(ROOT_DIR . 'images/gallery/thumbsS/' . $oldFile);
    }
    if ($oldFile != '' && file_exists(ROOT_DIR . 'images/gallery/thumbsM/' . $oldFile)) {
        unlink(ROOT_DIR . 'images/gallery/thumbsM/' . $oldFile);
    }
    move_uploaded_file($file["tmp_name"], ROOT_DIR . "images/gallery/" . $fName);

    $targetPath = ROOT_DIR . 'images/gallery/';
    //  echo "<img src=".$targetPath.$fName;
    $targetFile = str_replace('//', '/', $targetPath) . $fName;
    $targetThumbM = str_replace('//', '/', $targetPath . "thumbsM/") . $fName;
    $targetThumbS = str_replace('//', '/', $targetPath . "thumbsS/") . $fName;

    $filename = $targetFile;
    // if(!file_exists(ROOT_DIR.'images/gallery/'.$targetFile)){
    //    return true;
    // }

    list($width, $height, $type, $attr) = getimagesize($targetFile);
    $t_filenameS = $targetThumbS;
    $t_filenameM = $targetThumbM;
    // Get new dimensions
    list($width, $height) = getimagesize($targetFile);

    $new_width = $W;
    //230, 245
    $thumbM_width = $tMW;
    $thumbS_width = $tSW;
    $ratio = $width / $new_width;
    $thumbS_ratio = $width / $thumbS_width;
    $thumbM_ratio = $width / $thumbM_width;
    $new_height = $height / $ratio;
    $thumbM_height = $height / $thumbM_ratio;
    $thumbS_height = $height / $thumbS_ratio;
    if ($thumbS_height > $tSH) {
        $thumbS_height = $tSH;
        $thS_ratio = $height / $tSH;
        $thumbS_width = $width / $thS_ratio;
    }
    if ($thumbM_height > $tMH) {
        $thumbM_height = $tMH;
        $thM_ratio = $height / $tMH;
        $thumbM_width = $width / $thM_ratio;
    }


    if ($file['type'] == 'image/jpeg') {
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // Output
        imagejpeg($image_p, $filename, 100);

        #kreiranje thumba
        $image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
        $image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
        $thumbS = $image;
        $thumbM = $image;
        imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);
        imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);

        // Output
        imagejpeg($image_tS, $t_filenameS, 100);
        imagejpeg($image_tM, $t_filenameM, 100);
        //   return true;
    }
    if ($file['type'] == 'image/gif') {
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $trnprt_indx = imagecolortransparent($image_p);
        if ($trnprt_indx >= 0) {

            // Get the original image's transparent color's RGB values
            $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

            // Allocate the same color in the new image resource
            $trnprt_indx = imagecolorallocate($image_p, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

            // Completely fill the background of the new image with allocated color.
            imagefill($image_p, 0, 0, $trnprt_indx);

            // Set the background color for new image to transparent
            imagecolortransparent($image_p, $trnprt_indx);


        }

        //$image_p = imagecolortransparent($image_p,  0,0,0);
        $image = imagecreatefromgif($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // Output
        imagegif($image_p, $filename, 100);

        #kreiranje thumba
        $image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
        $trnprt_indx = imagecolortransparent($image);
        if ($trnprt_indx >= 0) {

            // Get the original image's transparent color's RGB values
            $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

            // Allocate the same color in the new image resource
            $trnprt_indx = imagecolorallocate($image_tS, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
            $trnprt_indx = imagecolorallocate($image_tM, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

            // Completely fill the background of the new image with allocated color.
            imagefill($image_tS, 0, 0, $trnprt_indx);
            imagefill($image_tM, 0, 0, $trnprt_indx);

            // Set the background color for new image to transparent
            imagecolortransparent($image_tS, $trnprt_indx);
            imagecolortransparent($image_tM, $trnprt_indx);


        }

        //		$image_t = imagecolortransparent($thumb_width, $thumb_height,  0,0,0);
        $thumbS = $image;
        $thumbM = $image;
        imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
        imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

        // Output
        imagegif($image_tS, $t_filenameM, 100);
        imagegif($image_tM, $t_filenameS, 100);
        //   return true;

    }
    if ($file['type'] == 'image/png') {
        $image = @imagecreatefrompng($filename);
        $image_p = imagecreatetruecolor($new_width, $new_height);
        // Turn off transparency blending (temporarily)
        imagealphablending($image_p, false);

        // Create a new transparent color for image
        $color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);

        // Completely fill the background of the new image with allocated color.
        imagefill($image_p, 0, 0, $color);

        // Restore transparency blending

        imageSaveAlpha($image_p, true);


        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // Output
        imagepng($image_p, $filename, 9);

        #kreiranje thumba
        $image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
        $image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
        $thumbM = $image;
        $thumbS = $image;
        // Turn off transparency blending (temporarily)
        imagealphablending($image_tM, false);
        imagealphablending($image_tS, false);

        // Create a new transparent color for image
        $color = imagecolorallocatealpha($image_tS, 0, 0, 0, 127);
        $color = imagecolorallocatealpha($image_tM, 0, 0, 0, 127);

        // Completely fill the background of the new image with allocated color.
        imagefill($image_tS, 0, 0, $color);
        imagefill($image_tM, 0, 0, $color);

        // Restore transparency blending

        imageSaveAlpha($image_tS, true);
        imageSaveAlpha($image_tM, true);
        @imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
        @imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

        // Output
        imagepng($image_tS, $t_filenameS, 9);
        imagepng($image_tM, $t_filenameM, 9);


    }

}


function uploadPhotoCat($file, $oldFile, $W, $fName)
{
    // global $name;


    if ($oldFile != '' && file_exists(ROOT_DIR . 'images/gallery/' . $oldFile)) {
        unlink(ROOT_DIR . 'images/gallery/' . $oldFile);
    }
    move_uploaded_file($file["tmp_name"], ROOT_DIR . "images/gallery/" . $fName);

    $targetPath = ROOT_DIR . 'images/gallery/';
    //  echo "<img src=".$targetPath.$fName;
    $targetFile = str_replace('//', '/', $targetPath) . $fName;

    $filename = $targetFile;
    // if(!file_exists(ROOT_DIR.'images/gallery/'.$targetFile)){
    //    return true;
    // }

    list($width, $height, $type, $attr) = getimagesize($targetFile);
    // Get new dimensions
    list($width, $height) = getimagesize($targetFile);

    $new_width = $W;
    //230, 245
    $ratio = $width / $new_width;
    $new_height = $height / $ratio;


    if ($file['type'] == 'image/jpeg') {
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // Output
        imagejpeg($image_p, $filename, 100);
    }

    if ($file['type'] == 'image/gif') {
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $trnprt_indx = imagecolortransparent($image_p);
        if ($trnprt_indx >= 0) {

            // Get the original image's transparent color's RGB values
            $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

            // Allocate the same color in the new image resource
            $trnprt_indx = imagecolorallocate($image_p, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

            // Completely fill the background of the new image with allocated color.
            imagefill($image_p, 0, 0, $trnprt_indx);

            // Set the background color for new image to transparent
            imagecolortransparent($image_p, $trnprt_indx);


        }

        //$image_p = imagecolortransparent($image_p,  0,0,0);
        $image = imagecreatefromgif($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // Output
        imagegif($image_p, $filename, 100);
    }

    if ($file['type'] == 'image/png') {
        $image = @imagecreatefrompng($filename);
        $image_p = imagecreatetruecolor($new_width, $new_height);
        // Turn off transparency blending (temporarily)
        imagealphablending($image_p, false);

        // Create a new transparent color for image
        $color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);

        // Completely fill the background of the new image with allocated color.
        imagefill($image_p, 0, 0, $color);

        // Restore transparency blending
        imageSaveAlpha($image_p, true);


        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        // Output
        imagepng($image_p, $filename, 9);

    }

}

function cleanFileName($file)
{
    $file = preg_replace("/[^a-zA-Z0-9.\/_|+ -]/", '', $file);
    $file = strtolower(trim($file, '-'));
    return $file;
}

function imageResize($fileName, $NewWith, $NewHeight)
{
    $W = $NewWith;
    $tSW = 100;
    $tSH = 60;
    $tMW = 180;
    $tMH = 100;
    $targetFile = ROOT_DIR . 'images/gallery/' . $fileName;

    list($width, $height, $type, $attr) = getimagesize($targetFile);
    // echo imgType($type);
    //echo $width;
    if ($width != 600) {

        $targetPath = ROOT_DIR . 'images/gallery/';
        $targetFile = str_replace('//', '/', $targetPath) . $fileName;
        $targetThumbM = str_replace('//', '/', $targetPath . "thumbsM/") . $fileName;
        $targetThumbS = str_replace('//', '/', $targetPath . "thumbsS/") . $fileName;

        $new_width = $W;
        //230, 245
        $thumbM_width = $tMW;
        $thumbS_width = $tSW;
        $ratio = $width / $new_width;
        $thumbS_ratio = $width / $thumbS_width;
        $thumbM_ratio = $width / $thumbM_width;
        $new_height = $height / $ratio;
        $thumbM_height = $height / $thumbM_ratio;
        $thumbS_height = $height / $thumbS_ratio;
        if ($thumbS_height > $tSH) {
            $thumbS_height = $tSH;
            $thS_ratio = $height / $tSH;
            $thumbS_width = $width / $thS_ratio;
        }
        if ($thumbM_height > $tMH) {
            $thumbM_height = $tMH;
            $thM_ratio = $height / $tMH;
            $thumbM_width = $width / $thM_ratio;
        }
        if (imgType($type) == 'jpg') {
            $image_p = imagecreatetruecolor($new_width, $new_height);
            $image = imagecreatefromjpeg($targetFile);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            // Output
            imagejpeg($image_p, $targetFile, 100);

            #kreiranje thumba
            $image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
            $image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
            $thumbS = $image;
            $thumbM = $image;
            imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);
            imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);

            // Output
            imagejpeg($image_tS, $targetThumbS, 100);
            imagejpeg($image_tM, $targetThumbM, 100);
        }
    }

    if ($type == 'gif') {
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $trnprt_indx = imagecolortransparent($image_p);
        if ($trnprt_indx >= 0) {

            // Get the original image's transparent color's RGB values
            $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

            // Allocate the same color in the new image resource
            $trnprt_indx = imagecolorallocate($image_p, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

            // Completely fill the background of the new image with allocated color.
            imagefill($image_p, 0, 0, $trnprt_indx);

            // Set the background color for new image to transparent
            imagecolortransparent($image_p, $trnprt_indx);


            //$image_p = imagecolortransparent($image_p,  0,0,0);
            $image = imagecreatefromgif($targetFile);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            // Output
            imagegif($image_p, $targetFile, 100);

            #kreiranje thumba
            $image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
            $trnprt_indx = imagecolortransparent($image);

            if ($trnprt_indx >= 0) {

                // Get the original image's transparent color's RGB values
                $trnprt_color = imagecolorsforindex($image, $trnprt_indx);

                // Allocate the same color in the new image resource
                $trnprt_indx = imagecolorallocate($image_tS, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                $trnprt_indx = imagecolorallocate($image_tM, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

                // Completely fill the background of the new image with allocated color.
                imagefill($image_tS, 0, 0, $trnprt_indx);
                imagefill($image_tM, 0, 0, $trnprt_indx);

                // Set the background color for new image to transparent
                imagecolortransparent($image_tS, $trnprt_indx);
                imagecolortransparent($image_tM, $trnprt_indx);


            }

            //		$image_t = imagecolortransparent($thumb_width, $thumb_height,  0,0,0);
            $thumbS = $image;
            $thumbM = $image;
            imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
            imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

            // Output
            imagegif($image_tS, $targetThumbS, 100);
            imagegif($image_tM, $targetThumbM, 100);
            //   return true;

        }
        if ($type == 'png') {
            $image = @imagecreatefrompng($targetFile);
            $image_p = imagecreatetruecolor($new_width, $new_height);
            // Turn off transparency blending (temporarily)
            imagealphablending($image_p, false);

            // Create a new transparent color for image
            $color = imagecolorallocatealpha($image_p, 0, 0, 0, 127);

            // Completely fill the background of the new image with allocated color.
            imagefill($image_p, 0, 0, $color);

            // Restore transparency blending

            imageSaveAlpha($image_p, true);


            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            // Output
            imagepng($image_p, $targetFile, 9);

            #kreiranje thumba
            $image_tS = imagecreatetruecolor($thumbS_width, $thumbS_height);
            $image_tM = imagecreatetruecolor($thumbM_width, $thumbM_height);
            $thumbM = $image;
            $thumbS = $image;
            // Turn off transparency blending (temporarily)
            imagealphablending($image_tM, false);
            imagealphablending($image_tS, false);

            // Create a new transparent color for image
            $color = imagecolorallocatealpha($image_tS, 0, 0, 0, 127);
            $color = imagecolorallocatealpha($image_tM, 0, 0, 0, 127);

            // Completely fill the background of the new image with allocated color.
            imagefill($image_tS, 0, 0, $color);
            imagefill($image_tM, 0, 0, $color);

            // Restore transparency blending

            imageSaveAlpha($image_tS, true);
            imageSaveAlpha($image_tM, true);
            @imagecopyresampled($image_tS, $thumbS, 0, 0, 0, 0, $thumbS_width, $thumbS_height, $width, $height);
            @imagecopyresampled($image_tM, $thumbM, 0, 0, 0, 0, $thumbM_width, $thumbM_height, $width, $height);

            // Output
            imagepng($image_tS, $targetThumbS, 9);
            imagepng($image_tM, $targetThumbM, 9);

        }
    }
}

function imgType($id)
{
    switch ($id) {
        case 1:
            return "gif";
            break;
        case 2:
            return "jpg";
            break;
        case 3:
            return "png";
            break;
    }
}

function trunc($string, $length)
{
    settype($string, 'string');
    settype($length, 'integer');
    $output = '';
    for ($a = 0; $a < $length AND $a < strlen($string); $a++) {
        $output .= $string[$a];
    }
    if (strlen($string) > $length) {
        $output .= '...';
    }
    return ($output);
}

function formatDate($date)
{
    $month = date('M ', strtotime($date));
    if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'sr') {
        $months = array('Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar');
        $month = $months[date("n", strtotime($date)) - 1];
    }
    $date = date('d. ', strtotime($date)) . $month . date(' Y. ', strtotime($date));
    return $date;
}

function timeStampToDate($tS, $time = NULL)
{
    if ($tS == 0) {
        return '';
    }
    $month = date('M ', ($tS));
    if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'sr') {
        $months = array('Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar');
        $month = $months[date("n", ($tS)) - 1];
    }
    if ($time) {
        return date('d. ', $tS) . $month . date(' Y. ', ($tS)) . date("H:m:s", $tS);
    }
    $tS = date('d. ', $tS) . $month . date(' Y. ', ($tS));
    return $tS;
}

function getImage($refID, $ref, $count)
{
    $img = Gallery::find_by_reference($ref, $refID, $count);
    if (!$img) {
        $img = "defaultCat.png";
    } else {
        if ($count < 2) {
            if (count($img) > 1) {
                $img = array_shift($img);
            }
            $img = $img->file;
        } else {
            $images = array();
            foreach ($img as $i) {
                $images[] = $i->file;
            }
            $img = $images;
        }
    }
    return $img;
}

function getGallery($id, $type)
{
    $gallery = array_filter(Gallery::find_by_sql("SELECT id, file from gallery where refID = '" . $id . "' and type = '{$type}'"));
    if (count($gallery) > 1) {
        $gallery = array_shift($gallery);
    }
//    $gallery = Gallery::find_by_sql("SELECT id, file from gallery where refID = '" . $id . "' and type = '{$type}'");
//    $gallery = array_shift($gallery);

    if (isset($gallery) && $gallery) {
        if (is_array($gallery)) {
            $g = $gallery[0];
            return $g->file;
        } else {
            return $gallery->file;
        }
    } else {
        return NULL;
    }
}

function getGalleryList($id, $type)
{
//    $gallery = array_shift(Gallery::find_by_sql("SELECT id, file from gallery where refID = '" . $id . "' and type = '{$type}'"));
    $gallery = Gallery::find_by_sql("SELECT id, file from gallery where refID = '" . $id . "' and type = '{$type}'");
    $galleryList = array();
    foreach ($gallery as $g) {
        $galleryList[] = $g->file;
    }
    if (count($galleryList)) {
        return $galleryList;
    } else {
        return NULL;
    }
}

function breadCrumb($path)
{
    global $page;
    for ($i = count($path) - 1; $i > -1; $i--) {
        $c = $path[$i];
        if (count($path) > 1 || $page == 'proizvod') {
            echo '<a href="' . SITE_ROOT . 'kategorija/' . $c->id . '/' . urlSafe(trans($c->name_sr, $c->name_en)) . '">' . trans($c->name_sr, $c->name_en) . '</a>';
        } else {
            echo '<a class="active" href="#">' . trans($c->name_sr, $c->name_en) . '</a>';
        }
    }
}

function urlSafe($str, $replace = array(), $delimiter = '-')
{
    setlocale(LC_ALL, 'en_US.UTF8');
    if (!empty($replace)) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}

function findSubCatsOption($category = NULL)
{
    ?>
<option value="0">Izaberi kategoriju</option>
<?php
    $categories = Category::find_by_sql("SELECT id, name_sr, parent_cat FROM category where parent_cat = 0 order by name_sr ASC");
    foreach ($categories as $cat) : ?>
    <option value="<?=$cat->id?>"
        <?php if ($category && $category->id > 0) {
        if (isset($category->id) && $cat->id == $category->parent_cat) {
            echo 'selected';
        }
    }?>
            ><?=$cat->name_sr;?> [glavna]
    </option>
    <?php
        $sCats = Category::find_children($cat->id);
        if ($sCats) {
            foreach ($sCats as $sC) {
                if ($cat->parent_id > 0) {
                    $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                } else {
                    $inset = '&nbsp;&nbsp;&rarr;';
                }
                $sel = '';
                if ($sC->id == $_GET['id']) {
                    $sel = ' selected';
                }
                ?>
            <option value="<?=$sC->id?>"<?=$sel?>><?=$inset . $sC->name_sr?></option>
            <?php
                $sCs = Category::find_children($sC->id);
                if ($sCs) {
                    foreach ($sCs as $ssC) {
                        if ($sC->parent_id > 0) {
                            $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                        } else {
                            $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                        }
                        $sel = '';
                        if ($ssC->id == $_GET['id']) {
                            $sel = ' selected';
                        }
                        ?>
                    <option value="<?=$ssC->id?>"<?=$sel?> style="color:red"><?=$inset . $ssC->name_sr?></option>
                    <?php
                        //showing sub sub categories
                        $ssCs = Category::find_children($ssC->id);
                        if ($ssCs) {
                            foreach ($ssCs as $ssCs) {
                                $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                $sel = '';
                                if ($ssCs->id == $_GET['id']) {
                                    $sel = ' selected';
                                }
                                ?>
                            <option value="<?=$ssCs->id?>"<?=$sel?>
                                    style="color:red"><?=$inset . $ssCs->name_sr?></option>
                            <?php
                            }
                            $sssCs = Category::find_children($ssCs->id);
                            if ($sssCs) {
                                foreach ($sssCs as $sssCs) {
                                    $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                    $sel = '';
                                    if ($sssCs->id == $_GET['id']) {
                                        $sel = ' selected';
                                    }
                                    ?>
                                <option value="<?=$sssCs->id?>"<?=$sel?>
                                        style="color:red"><?=$inset . $sssCs->name_sr?></option>
                                <?php
                                }

                            }
                        }
                    }
                }
            }
        }
    endforeach;
}

function similarProducts($product, $limit)
{
// <<<<<<< HEAD
    $sql = "SELECT * from product where product.category = (select id from category where name_sr = '" . $product->category . "') and id !=" . $product->id . " LIMIT " . $limit;
    $prods = Product::find_by_sql($sql);
// =======
//     $prods = Product::find_by_sql("SELECT * from product where product.name_sr !='{$product->name_sr}' AND product.category in (select id from category where category.name_sr = '{$product->category}'");
// >>>>>>> 568b584f21a2a444fe7a05bbd33ee55b6901c069
    if ($prods) {
        echo '<h2>' . trans("Ostali proizvodi iz ove kategorije", 'Other products in this category') . '</h2>';
        echo '<div class="proizvodi">';
        foreach ($prods as $p):
            $img = SITE_ROOT . 'images/gallery/thumbsM/defaultCat.png';
            $gallery = getGallery($p->id, 'product');
            if ($gallery) {
                $img = SITE_ROOT . 'images/gallery/' . $gallery;
            } ?>
        <div class="box">
                        <span class="title">
                            <a href="<?=SITE_ROOT?>proizvod/<?=$p->id?>/<?=cleanFileName(trans($p->name_sr, $p->name_en))?>">
                                <?=trans($p->name_sr, $p->name_en);?>
                            </a>
                        </span>
            <a href="<?=SITE_ROOT?>proizvod/<?=$p->id?>/<?=cleanFileName(trans($p->name_sr, $p->name_en))?>"
               class="img-link">
                            <span>
                                <img src="<?=$img?>" alt="<?=trans($p->name_sr, $p->name_en)?>"/>
                            </span>
            </a>
            <?php //TODO:: dodati kasnije poruci link ?>
            <span class="korpa" style="display:none"><a href="#">poruči <span
                    class="icon-shopping-cart"></span></a></span>
        </div>
        <?php endforeach;
        echo '</div>';
    }
}

function isCLientLogin()
{
    global $session;
    if (isset($session->client_id) && $session->client_id != '' || isset($_SESSION['clientID'])) {
        return $_SESSION['clientID'];
    }
    return false;
}

function checkEmail($email)
{
    //Perform a basic syntax-Check
    //If this check fails, there's no need to continue
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return false;
    }
    //extract host
    list($email, $host) = explode("@", $email);
    //check, if host is accessible
    if (!checkdnsrr($host, "MX") && !checkdnsrr($host, "A")) {
        return false;
    }
    return true;
}

function sendMail($to, $mail_subject, $template, $header, $redirectSucc, $redirectErr)
{
    $send = 0;
    $result = mail($to, $mail_subject, $template, $header);
    if ($result) {
        if ($redirectSucc != '') {
            $_SESSION['message'] = trans("Poruka je uspešno poslata.", 'Message is sent successfully.');
            $_SESSION['mType'] = 2;
            redirect_to($redirectSucc);
        }
    } else {
        if ($redirectErr != '') {
            $_SESSION['message'] = trans("Poruka nije poslata. Pokusajte ponovo", 'Message has NOT been sent. Please try again');
            $_SESSION['mType'] = 4;
            redirect_to($redirectErr);
        }
    }
}

function clearFileName($string, $force_lowercase = true, $anal = false)
{
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€”", "â€“", ",", "<", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}

function makeSearch($term)
{
    $results = array();
    $searchProducts = Product::search($term);
    if ($searchProducts) {
        $results[] = array("products" => $searchProducts);
    }
    return $results;
}

function encode($val, $base = 62, $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    // can't handle numbers larger than 2^31-1 = 2147483647
    $str = '';
    do {
        $i = $val % $base;
        $str = $chars[$i] . $str;
        $val = ($val - $i) / $base;
    } while ($val > 0);
    return $str;
}

function decode($str, $base = 62, $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $len = strlen($str);
    $val = 0;
    $arr = array_flip(str_split($chars));
    for ($i = 0; $i < $len; ++$i) {
        $val += $arr[$str[$i]] * pow($base, $len - $i - 1);
    }
    return $val;
}

function cleanInput($input)
{

    $search = array(
        '@<script[^>]*?>.*?</script>@si', // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@' // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    return $output;
}

function galleryImg($g, $type, $thumbs = TRUE, $limit = NULL)
{
    if (!$limit) {
        $limit = 1;
    }
    $gallery = Gallery::find_by_reference($g, $type, $limit);
    if (!$gallery) {
        return '<img src="' . SITE_ROOT . 'assets/img/defaultCat.jpg"  >';
    }
    if (count($gallery) == 1) {
        return '<img src="' . SITE_ROOT . 'images/gallery/thumbsM/' . $gallery->file . '"  >';
    }
    foreach ($gallery as $gl) {
        if ($thumbs == TRUE) {
            return '<img src="' . SITE_ROOT . 'images/gallery/thumbsM/' . $gl->file . '"  >';
        } else {
            return '<img src="' . SITE_ROOT . 'images/gallery/' . $gl->file . '"  >';
        }
    }
}

function printClean($ar)
{
    echo "<pre>";
    print_r($ar);
    echo "</pre>";
}

function normalize($name)
{
    return ucfirst(strtolower(strip_tags(trim($name))));
}

function isLogged()
{
    global $session;
    if ($session->user_id == '') {
        return false;
    } else {
        $user = User::find_by_id($session->user_id);
        return $user;
    }
}

function getCategoryName($id)
{
    $c = Category::find_by_id($id);
    if ($c) {
        return trans($c->name_sr, $c->name_en);
    } else {
        return '';
    }
}

function translateField($field)
{
    switch ($field) {
        case 'speedCopyA4Color':
            return trans('Brzina kopiranja A4 kolor', '');
            break;
        case 'speedCopyA4Black':
            return trans('Brzina kopiranja A4 crno belo', '');
            break;
        case 'speedCopyA3Color':
            return trans('Brzina kopiranja A3 kolor', '');
            break;
        case 'speedCopyA3Black':
            return trans('Brzina kopiranja A3 crno belo', '');
            break;
        case 'firstCopyColor':
            return trans('Vreme prve kopije kolor', '');
            break;
        case 'firstCopyBlack':
            return trans('Vreme prve kopije crno belo', '');
            break;
        case 'heatingTime':
            return trans('Vreme zagrevanja', '');
            break;
        case 'copyResolution':
            return trans('Rezolucija kopiranja', '');
            break;
        case 'gradation':
            return trans('Gradacija', '');
            break;
        case 'mutliplyCopy':
            return trans('Multipliciranje kopije', '');
            break;
        case 'formatOrig':
            return trans('Format originala', '');
            break;
        case 'zoom':
            return trans('Uvećanje', '');
            break;
        case 'copyFunction':
            return trans('Funkcije kopiranja', '');
            break;
        case 'printSpeedA4Color':
            return trans('Brzina štampanja A4 kolor', '');
            break;
        case 'printSpeedA4Black':
            return trans('Brzina štampanja A4 crno belo', '');
            break;
        case 'printSpeedA3Color':
            return trans('Brzina štampanja A3 kolor', '');
            break;
        case 'printSpeedA3Black':
            return trans('Brzina štampanja A3 crno belo', '');
            break;
        case 'firstCopyPrintColor':
            return trans('Vreme prvog otiska kolor', '');
            break;
        case 'firstCopyPrintBlack':
            return trans('Vreme prvog otiska crno belo', '');
            break;
        case 'printRes':
            return trans('Rezolucija štampanja', '');
            break;
        case 'processor':
            return trans('Procesor', '');
            break;
        case 'driver':
            return trans('Drajver', '');
            break;
        case 'os':
            return trans('Operativni sistemi', '');
            break;
        case 'fonts':
            return trans('Fontovi štampača', '');
            break;
        case 'printFunctions':
            return trans('Funkcije štampe', '');
            break;
        case 'scanSpeedColor':
            return trans('Brzina skeniranja kolor', '');
            break;
        case 'scanSpeedBlack':
            return trans('Brzina skeniranja crno belo', '');
            break;
        case 'scanRes':
            return trans('Rezolucija skeniranja', '');
            break;
        case 'scanMethods':
            return trans('Načini skeniranja', '');
            break;
        case 'scanFormats':
            return trans('Formati skeniranog fajla', '');
            break;
        case 'scanDestination':
            return trans('Destinacije skeniranja', '');
            break;
        case 'faxStandard':
            return trans('Faks standard', '');
            break;
        case 'faxTransmit':
            return trans('Prenos faksa', '');
            break;
        case 'faxRes':
            return trans('Rezolucija faksa (maksimalna)', '');
            break;
        case 'faxCompression':
            return trans('Kompresija faksa', '');
            break;
        case 'faxModem':
            return trans('Faks modem', '');
            break;
        case 'faxDestination':
            return trans('Destinacije faksiranja', '');
            break;
        case 'faxFunctions':
            return trans('Funkcije faksa', '');
            break;
        case 'hddMax':
            return trans('Maksimalan broj lokacija za skladištenje na HDD', '');
            break;
        case 'memory':
            return trans('Memorija', '');
            break;
        case 'documentsMax':
            return trans('Maksimalan broj dokumenata koje je moguće sačuvati', '');
            break;
        case 'storageType':
            return trans('Tipovi sistemskih skladišta', '');
            break;
        case 'storageSystemType':
            return trans('Tipovi sistemskih skladišta', '');
            break;
        case 'hddFunctions':
            return trans('Funkcionalnost hard diska', '');
            break;
        case 'hdd':
            return trans('Hard disk', '');
            break;
        case 'compConnection':
            return trans('Povezivanje sa računarom', '');
            break;
        case 'networkProtocols':
            return trans('Mrežni protokoli', '');
            break;
        case 'paperFormat':
            return trans('Format papira', '');
            break;
        case 'paperWeight':
            return trans('Težina papira', '');
            break;
        case 'inputCapacity':
            return trans('Kapacitet ulaznog papira', '');
            break;
        case 'outputCapacity':
            return trans('Kapacitet izlaznog papira', '');
            break;
        case 'monthly':
            return trans('Maksimalni mesečni obim', '');
            break;
        case 'initialToner':
            return trans('Inicijalni toner', '');
            break;
        case 'tonerDeclaration':
            return trans('Deklaracija tonera (5%)', '');
            break;
        case 'energyConsumption':
            return trans('Potrošnja električne energije', '');
            break;
        case 'dimensions':
            return trans('Dimenzije uređaja (Š x D x V) mm', '');
            break;
        case 'weight':
            return trans('Težina uređaja', '');
            break;
        case 'security':
            return trans('Bezbednost', '');
            break;
        case 'software':
            return trans('Prateći softver', '');
            break;
        case 'users':
            return trans('Upravljanje korisnicima', '');
            break;
        case 'optional':
            return trans('Opcioni uređaji', '');
            break;
    }
}

function findImageForProduct($id)
{
    $galls = Gallery::find_by_productID($id, 'product');
    if (count($galls) > 0) {
        if ($galls) {
            $g = array_shift($galls);
            return $g->file;
        }
    }
    return false;
}

function formatMoney($number, $fractional = false)
{
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1.$2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}

function intToDate($timeInt)
{
    return date('d.m.Y', $timeInt);
}

function rand_uniqid($in, $to_num = false, $pad_up = false, $passKey = null)
{
    $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if ($passKey !== null) {
        // Although this function's purpose is to just make the
        // ID short - and not so much secure,
        // you can optionally supply a password to make it harder
        // to calculate the corresponding numeric ID

        for ($n = 0; $n < strlen($index); $n++) {
            $i[] = substr($index, $n, 1);
        }

        $passhash = hash('sha256', $passKey);
        $passhash = (strlen($passhash) < strlen($index))
            ? hash('sha512', $passKey)
            : $passhash;

        for ($n = 0; $n < strlen($index); $n++) {
            $p[] = substr($passhash, $n, 1);
        }

        array_multisort($p, SORT_DESC, $i);
        $index = implode($i);
    }

    $base = strlen($index);

    if ($to_num) {
        // Digital number  <<--  alphabet letter code
        $in = strrev($in);
        $out = 0;
        $len = strlen($in) - 1;
        for ($t = 0; $t <= $len; $t++) {
            $bcpow = bcpow($base, $len - $t);
            $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
        }

        if (is_numeric($pad_up)) {
            $pad_up--;
            if ($pad_up > 0) {
                $out -= pow($base, $pad_up);
            }
        }
        $out = sprintf('%F', $out);
        $out = substr($out, 0, strpos($out, '.'));
    } else {
        // Digital number  -->>  alphabet letter code
        if (is_numeric($pad_up)) {
            $pad_up--;
            if ($pad_up > 0) {
                $in += pow($base, $pad_up);
            }
        }

        $out = "";
        for ($t = floor(log($in, $base)); $t >= 0; $t--) {
            $bcp = bcpow($base, $t);
            $a = floor($in / $bcp) % $base;
            $out = $out . substr($index, $a, 1);
            $in = $in - ($a * $bcp);
        }
        $out = strrev($out); // reverse
    }

    return $out;
}

function price($product)
{
    global $client;
    if ($client) {
        $prices = Price::find_by_product_with_group($product, $client->client_category);
        if ($prices) {
            if ($_SESSION['lang'] != 'sr') {
                $pr = $prices->amount / Euro::latest();
                return round($pr, 2);
            } else {
                return $prices->amount;
            }

        }
    }
    return false;
}

function alphaID($in, $to_num = false, $pad_up = false, $passKey = null)
{
    if ($in == '') {
        return;
    }
    $index = "bcdfghijklmnpqrstvwxyz0123456789BCDFGHIJKLMNPQRSTVWXYZ";
    $base = strlen($index);

    if ($to_num) {
        // Digital number  <<--  alphabet letter code
        $in = strrev($in);
        $out = 0;
        $len = strlen($in) - 1;
        for ($t = 0; $t <= $len; $t++) {
            $bcpow = bcpow($base, $len - $t);
            $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
        }

        if (is_numeric($pad_up)) {
            $pad_up--;
            if ($pad_up > 0) {
                $out -= pow($base, $pad_up);
            }
        }
    } else {
        // Digital number  -->>  alphabet letter code
        if (is_numeric($pad_up)) {
            $pad_up--;
            if ($pad_up > 0) {
                $in += pow($base, $pad_up);
            }
        }

        $out = "";
        for ($t = floor(log10($in) / log10($base)); $t >= 0; $t--) {
            $a = floor($in / bcpow($base, $t));
            $out = $out . substr($index, $a, 1);
            $in = $in - ($a * bcpow($base, $t));
        }
        $out = strrev($out); // reverse
    }

    return $out;
}


function cleanHTML($text)
{
    $text = preg_replace(
        array(
            // Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<object[^>]*?.*?</object>@siu',
            '@<embed[^>]*?.*?</embed>@siu',
            '@<applet[^>]*?.*?</applet>@siu',
            '@<noframes[^>]*?.*?</noframes>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            '@<noembed[^>]*?.*?</noembed>@siu',
            // Add line breaks before and after blocks
            '@</?((address)|(blockquote)|(center)|(del))@iu',
            '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
            '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
            '@</?((table)|(th)|(td)|(caption))@iu',
            '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
            '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
            '@</?((frameset)|(frame)|(iframe))@iu',
        ),
        array(
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', "$0", "$0", "$0", "$0", "$0", "$0", "$0", "$0",), $text);

    // you can exclude some html tags here, in this case B and A tags
    return strip_tags($text, '<b><br><p><h1><h2><h3><h4><h5><ul><ol><li><strong><a>');
}

function readExcell($file)
{


    /** Error reporting */
    error_reporting(E_ALL);
    // set_time_limit(0);
    date_default_timezone_set('Europe/London');


    if (file_exists(CSV . $file)) {
        $rows = array();

        // $objPHPExcel = PHPExcel_IOFactory::createReaderForFile(CSV . $file);
        // $objPHPExcel->setReadDataOnly(true);

        $objPHPExcel = PHPExcel_IOFactory::load(CSV . $file);
        //$objWorksheet = $objPHPExcel->load(CSV);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        //  print_r($objWorksheet);
        $rowIndex = 0;

        /* echo "<pre>";
         print_r($objWorksheet->getRowIterator());
         echo "</pre>";*/
        foreach ($objWorksheet->getRowIterator() as $row) {

            $cells = array();
            if ($rowIndex > -1) {

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,
                // even if it is not set.
                // By default, only cells
                // that are set will be
                // iterated.


                $i = 1;
                foreach ($cellIterator as $cell) {

                    $cells[$i] = $cell->getFormattedValue();

                    $i++;
                }
            }
            $rows[$rowIndex] = $cells;
            $rowIndex++;
        }
        return $rows;
    } else {
        echo "<p>Traženi fajl: <strong>$file</strong> ne postoji.</p>";
        return false;
    }
}

function fieldValue($fieldName)
{
    $field = '';
    switch ($fieldName) {
        case 'Ime':
            $field = "name_sr";
            break;
        case 'Interni kat. broj':
            $field = "plD";
            break;
        case 'Osnovni kat. broj':
            $field = "mainCatID";
            break;
        case 'Dodatni ser. brojevi':
            $field = "secondIDs";
            break;
        case 'Atribut set':
            $field = "attributeSet";
            break;
        case 'Osnovna cena':
            $field = "price";
            break;
        case 'PDV':
            $field = "pdv";
            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;
//        case 'PDV':
//            $field = "pdv";
//            break;

//        case '':
//
//            break;
    }


    return $field;
}

function datetime()
{
    return date('Y-m-d h:m:s');
}

function checked($current, $target)
{
    if ($current == $target) {
        return 'checked';
    }
}

function selected($current, $target)
{
    if ($current == $target) {
        return ' selected ';
    }
}

function groupBreadcrumb($grID)
{
    if ($grID < 1) {
        return;
    }
    $groupList = array();
    $g = Category::find_by_id($grID);
    if ($g) {
        $groupList[] = $g->name_sr;
        if ($g->parent_cat < 1) {
            return implode(' > ', array_filter(array_reverse($groupList)));
        }
        $gP = Category::find_by_id($g->parent_cat);
        $g = false;
        if ($gP) {
            $groupList[] = $gP->name_sr;
            if ($gP->parent_cat < 1) {
                return implode(' > ', array_filter(array_reverse($groupList)));
            }
            $g = Category::find_by_id($gP->parent_cat);
            $gP = false;
            if ($g) {
                $groupList[] = $g->name_sr;
                if ($g->parent_cat < 1) {
                    return implode(' > ', array_filter(array_reverse($groupList)));

                    $gP = Category::find_by_id($g->parent_cat);
                    $g = false;
                    if ($gP) {
                        $groupList[] = $gP->name_sr;
                        if ($g->parent_cat < 1) {
                            return implode(' > ', array_filter(array_reverse($groupList)));
                        }
                    }
                }
            }
        }
    }


    return implode(' > ', array_filter(array_reverse($groupList)));

}

function get_parent($parent)
{
    $detailListROOT = array_filter(explode(',', $parent));
    $detailList = array();
    foreach ($detailListROOT as $d) {
        $parent = ProductDetailsSettings::find_by_id($d);
        if ($parent) {
            $detailList[] = $parent->name_sr;
        }
    }
    if (count($detailList) < 1) {
        return '- nema roditelja -';
    }
    return implode(',', $detailList);
}

function activateTab($tab, $current, $content = '')
{
    if ($content == 'tab') {
        if ($tab == $current) {
            return ' class="activeTab" ';
        }
    } else {
        if ($tab == $current) {
            return 'display:none;';
        }
    }
    return '';
}
function checkiPlus($id){
    if(Product::isIPlus($id)){
        return '<img src="'.IMG.'.iplus_icon.png" />';
    }
    return '';
}

