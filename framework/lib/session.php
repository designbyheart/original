<?php
// A class to help work with Sessions
// In our case, primarily to manage logging users in and out

// Keep in mind when working with sessions that it is generally 
// inadvisable to store DB-related objects in sessions

class Session {

    private $logged_in=false;
    public $user_id;
    public $message;
    public $type;
    public $client_id;
    public $id;
    public $page;

    function __construct() {
        session_start();
        $this->check_message();
        $this->check_id();
        $this->check_client_login();
        $this->check_login();
        if($this->logged_in) {
            // actions to take right away if user is logged in
        } else {
            // actions to take right away if user is not logged in
        }
        if($this->client_logged_in){
            // actions to take right away if client is logged in
        }
    }

    public function is_logged_in() {
        return $this->logged_in;
    }
    public function is_client_logged_in() {
        return $this->client_logged_in;
    }

    public function login($user) {
        // database should find user based on username/password
        if($user){
            $this->user_id = $_SESSION['user_id'] = $user->id;
            $this->logged_in = true;
        }
    }
    public function client_login($client) {
        // database should find user based on username/password
        if($client){
            $this->client_id = $_SESSION['client_id'] = $client->id;
            $this->client_logged_in = true;
        }
    }
    public function client_logout(){
        unset($_SESSION['client_id']);
        unset($this->client_id);
        $this->client_logged_in = false;
    }
    public function logout() {
        unset($_SESSION['user_id']);
        unset($this->user_id);
        $this->logged_in = false;
    }

    public function message($msg="") {
        if(!empty($msg)) {
            // then this is "set message"
            // make sure you understand why $this->message=$msg wouldn't work
            $_SESSION['message'] = $msg;
        } else {
            // then this is "get message"
            return $this->message;
        }
    }
    public function id($id=""){
        if(!empty($id)){
            $_SESSION['id']=$id;
        }else{return $this->id;
        }
    }

    private function check_client_login() {
        if(isset($_SESSION['client_id'])) {
            $this->client_id = $_SESSION['client_id'];
            $this->client_logged_in = true;
        } else {
            unset($this->client_id);
            $this->client_logged_in = false;
        }
    }
    private function check_login() {
        if(isset($_SESSION['user_id'])) {
            $this->user_id = $_SESSION['user_id'];
            $this->logged_in = true;
        } else {
            unset($this->user_id);
            $this->logged_in = false;
        }
    }

    private function check_message() {
        // Is there a message stored in the session?
        if(isset($_SESSION['message'])) {
            // Add it as an attribute and erase the stored version
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }
    private function check_client_id(){
        if(isset($_SESSION['client_id'])){
            $this->id="";
        }
    }

    private function check_id(){
        if(isset($_SESSION['id'])){
            $this->id="";
        }
    }
}

$session = new Session();
$message = $session->message();

?>