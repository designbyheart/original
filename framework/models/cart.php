<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 4:05 AM
 * To change this template use File | Settings | File Templates.
 */

$selectedProducts = array_filter(explode(',', Log::usersProducts($_SESSION['clientID'])));

$news = News::find_random(2);
$promo = Promo::find_random(2);
$galleryImages = array();
$iG = 0;
foreach($news as $nS){
    $g = Gallery::find_by_reference($nS->id,'news');
    if($g){
        $galleryImages[$iG]=$g->file;
    }else{
        $galleryImages[$iG]='';
    }
    $iG++;
}