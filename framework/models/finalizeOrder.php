<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 10:36 AM
 * To change this template use File | Settings | File Templates.
 */
$order = Order::find_by_id($_POST['orderID']);

if(isset($_POST['addressID'])){
    $order->address =$_POST['addressID'];
}else{
    $address = new Address();
    foreach($address as $key=>$value){
        if($key!='id' && isset($_POST[$key])){
            $address->$key = $_POST[$key];
        }
    }
    $address->customerID = $_SESSION['clientID'];
    if(isset($_POST['addressType'])){
        $address->name = $_POST['addressType'];
    }

    $address->save();
}
$order->status = 1;
$order->address = $address->id;
$order->modified = time();
$order->save();
    $log = new  Log();
    $log->eventTime = time();
    $log->userID = $_SESSION['clientID'];
    $log->note = 'Finalized order';
    $log->refID  = $order->id;
    $log->selectedProducts = "";
    $log->save();


