<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 4:19 PM
 * To change this template use File | Settings | File Templates.
 */

$id = str_replace('/', '', $_GET['id']);
$partner = Manufacturer::find_by_id($id);
$products = Product::find_by_partner($id);
$subMenu = SubMenu::find_for_page($partner->id, 'partner');
