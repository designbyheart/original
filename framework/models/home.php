<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/10/12
 * Time: 3:42 AM
 * To change this template use File | Settings | File Templates.
 */
$news = News::find_latest(3);
$galleryImages = array();
$iG = 0;
foreach($news as $nS){
    $g = Gallery::find_by_reference($nS->id,'news');
    if($g){
        $galleryImages[$iG]=$g->file;
    }else{
        $galleryImages[$iG]='';
    }
    $iG++;
}
$promo = Promo::find_active();