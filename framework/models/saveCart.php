<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 8:44 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../lib/setup.php');

if (isset($_POST['cancelOrder']) || !isset($_POST['submitOrder'])) {
    $log = new Log();
    $log->note = "Narudžbina je otkazana";
    $log->userID = $_SESSION['clientID'];
    $log->eventTime = time();
    $log->selectedProducts = "";
    $log->save();
    $session->message("Porudžbina je otkazana");
    redirect_to(SITE_ROOT."my-account");

} else {
    $order = new Order();
    $order->customerID = $_SESSION['clientID'];
    $order->orderDate = time();
    $address = Address::find_by_customer($_SESSION['clientID']);
    $order->status = 7;
    //notCompleteOrder 7
    //pending =1
    //confirmed =2
    //payed = 3
    //sentProducts = 4
    //closed = 5
    $products = array();
    $order->totalPrice =(float)$_POST['totalPrice'];
    $order->ipAddress = $_SERVER['REMOTE_ADDR'];
    foreach($_POST['numberOfItems'] as $key=>$value){
        for($i = 0; $i<$value; $i++){
            $products[] = $key;
        }
    }
    $order->products = str_replace('\n', '', str_replace('\r', '', $_POST['products']));
    $order->customerID = $_SESSION['clientID'];

//    print_r($order);
      $order->save();

    if (isset($order->id)) {
        $_SESSION['orderID'] = $order->id;
    }
   redirect_to(SITE_ROOT.'sendOrder');
}
