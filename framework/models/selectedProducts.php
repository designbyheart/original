<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
require_once '../lib/setup.php';
$sql = "select * from product where ";
$group = '';
$error = 0;
if (isset($_GET['group'])) {
    $group = trim($_GET['group']);
    if ($group != '' && $group != '0') {
        $sql .= " category = '{$group}' ";
        $error++;
    }
}
if (isset($_GET['manufacturer'])) {
    $manufacturer = $_GET['manufacturer'];

    if ($manufacturer != '' && $manufacturer != '0') {
        if ($group != '' && $group != '0') {
            $sql .= ' AND ';
        }
        $sql .= " manufacturer = '{$manufacturer}' ";
        $error++;
    }
}
if ($error < 1) {
    echo '<option>Izaberite kategoriju ili proizvođača</option>';
} else {
    $sql .= "AND active = 1 order by name_sr, name_en ASC";

    $products = Product::find_by_sql($sql);
    if ($products) {
        foreach ($products as $p) {
            ?>
        <option value="<?=$p->id?>"><?=trans($p->name_sr, $p->name_en)?></option>
        <?php
        }
    }else{ ?>
        <option value="0"><?=trans('Nema rezultata', 'No products for selected scope')?></option>
    <?php }
}
