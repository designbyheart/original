<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/15/12
 * Time: 2:11 AM
 * To change this template use File | Settings | File Templates.
 */
if (file_exists('../lib/setup.php')) {
    require_once('../lib/setup.php');
}
$selectedProducts = 0;

if (!isset($_GET['productID']) && isset($_SESSION['clientID'])) {
    $selectedProducts = count(array_filter(explode(',', Log::usersProducts($_SESSION['clientID']))));
}

if (isset($_GET['cancelOrder'])) {
    $productsLog = Log::removeAllProducts($_SESSION['clientID']);
    $selectedProducts = 0;
}

///1. save log for current user
if (isset($_GET['productID']) && isset($_SESSION['clientID'])) {
    $productsLog = Log::addProductToBasket($_GET['productID'], $_SESSION['clientID']);
//4. show selected products
    $selectedProducts = count($productsLog) - 1;

    //  $products = Log::usersProducts($_SESSION['clientID']);
}
//if ($selectedProducts > 0) {
echo  trans('Moja korpa', 'My Basket') . ' (' . $selectedProducts . ')';
//}
if (isset($_GET['removeItem']) && isset($_GET['id'])) {
    $productsLog = Log::removeItemFromCart($_GET['id'], $_SESSION['clientID']);
}
;