<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/14/12
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */

require_once('../lib/setup.php');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, ".date(time()+60*60*24*30)."05:00:00 GMT");


$arr=array();
if(!isset($con)){
    $con = "";
}
$product = Product::search($_GET['chars']);
$partners = Manufacturer::search($_GET['chars']);
//print_r($product);
if(count($product)>0){
    foreach($product as $productID){
        $p =Product::find_by_id($productID->id);
        $arr[]=array(
            "id" => trans($p->name_sr, $p->name_en),
            "data" => trans($p->name_sr, $p->name_en),
            "thumbnail" => "",
            "description" => trunc(trans($p->desc_sr, $p->desc_en), 100)
        );
    }
}
if(count($partners)>0){
    foreach($partners as $partnerID){
        $p =Manufacturer::find_by_id($partnerID->id);
        $arr[]=array(
            "id" => trans($p->name_sr, $p->name_en),
            "data" => trans($p->name_sr, $p->name_en),
            "thumbnail" => "",
            "description" => trunc(trans($p->desc_sr, $p->desc_en), 100)
        );
    }
}

// Encode it with JSON format
echo json_encode($arr);
?>