<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
$ids = explode('/', $_GET['id']);
$id = $ids[0];
$product = Product::find_by_id($id);
$man = Manufacturer::find_by_id($product->manufacturer);
$catList = Category::find_all();
//$ch = Characteristics::find_by_product($_GET['id'], trans('sr', 'en'));
$path = Category::getPath($product->category);

$drivers = Driver::find_by_product($id);

$instructions = Instruction::find_by_product($id);
$gallery = Gallery::find_by_reference($id, "product", 9);

$optional = Product::find_optional($product->optional);
$simProducts = Product::find_man_similar($product->similar);
if (!$simProducts || count($simProducts) < 1) {
    $simProducts = Product::find_similar($product->id, 10);
}
$sql = "select * from characteristics_category where id  in  (select charCat from category where id = {$product->category}) ";
//echo $sql;
//in (select distinct category_id from characteristics where characteristics.id in
//(select distinct characteristics_id from characteristics_value where value_name_" . trans('sr', 'en') . " !='' AND productID = '" . mysql_real_escape_string($id) . "' AND characteristics_id!='0'))";
$chacCategories = CharacteristicsCategory::find_by_sql($sql);

$promo = Promo::find_random(2);
$i = 0;
$height = 1;
foreach ($gallery as $gI):
    $i++;
    if ($i == 3) {
        $height++;
        $i = 0;
    }
endforeach;
$news = News::find_random(2);

//drivers and instructions for tab listing
