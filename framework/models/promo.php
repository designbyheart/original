<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/11/12
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
$_GET['id'] = str_replace("/", "", $_GET['id']);
$promo = Promo::find_by_id($_GET['id']);
$promosArr = array_filter(explode(',', $promo->products));
$products = array();
$manufactures = array();
foreach ($promosArr as $p) {
    $product = Product::find_by_id($p);
    $products[] = $product;
    if ($product->manufacturer != '') {
        $manufactures[] = Manufacturer::find_by_id($product->manufacturer);
    }
}
//$manufacturesArray = array_filter($manufactures);


//$man = Manufacturer::find_by_id($promo->manufacturer);
//$catList = Category::find_all();
//$ch = Characteristics::find_by_product($_GET['id'], trans('sr', 'en'));
//$path = Category::getPath($promo->category);

//$drivers = Driver::find_by_product($id);

//$instructions = Instruction::find_by_product($id);
$gallery = Gallery::find_by_reference($promo->id, "promotion", 15);

//$optional = Product::find_optional($promo->optional);
//$simProducts = Product::find_man_similar($promo->similar);
//if(!$simProducts || count($simProducts)<1){
//    $simProducts = Product::find_similar($promo->id);
//}

$i = 0;
$height = 1;
foreach ($gallery as $gI):
    $i++;
    if ($i == 3) {
        $height++;
        $i = 0;
    }
endforeach;