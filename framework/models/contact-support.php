<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 8:30 PM
 * To change this template use File | Settings | File Templates.
 */
$order = '-1';
if (isset($_GET['orderID'])) {
    $order = Order::find_by_orderID($_GET['orderID']);
}

$categories = TicketsCategory::find_active();