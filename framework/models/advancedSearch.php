<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 1:02 PM
 * To change this template use File | Settings | File Templates.
 */
//print_r($_POST);

$sql = "SELECT * from product where ";
$error = 0;
if (isset($_POST['category'])) {
    $sql .= 'category in (' . implode(',', $_POST['category']) . ')';
    $error = 1;
}
if (isset($_POST['partner'])) {
    if ($error == 1) {
        $sql .= " AND ";
    }
    if (count($_POST['partner']) > 1) {
        $sql .= 'manufacturer in (' . implode(',', $_POST['partner']) . ')';
    } else if (count($_POST['partner']) == 1) {
        $sql .= "manufacturer ='" . implode(',', $_POST['partner']) . "'";
    }
    $error = 1;
}
if (isset($_POST['from']) || isset($_POST['to'])) {
    $from = floatval($_POST['from']);
    $to = floatval($_POST['to']);
    $prices = Price::search_by_price($from, $to, $client->client_category);
    if ($prices) {
        if ($error == 1) {
            $sql .= " AND ";
        }
        $error = 1;
        $priceRes = array();
        foreach ($prices as $prZ) {
            $priceRes[] = $prZ->productID;
        }

        $sql .= " id in (" . implode(',', $priceRes) . ")";
    }
}
if (isset($_POST['characteristic'])) {
    if ($error == 1) {
        $sql .= ' AND ';
    }
    $chSql = "SELECT productID from characteristics where ";
    // echo implode('!=0  AND ',$_POST['characteristic']);
    $chSql .= ' productID !=0';

    $chs = Characteristics::find_by_sql($chSql);
    if ($chs) {
        $productIDs = array();
        foreach ($chs as $c) {
            $productIDs[] = $c->productID;
        }
        $error =1;
        $sql .= " id in (" . implode(',',array_unique(array_filter($productIDs))) . ") ";
    }

}
//echo $sql;
if ($error == 1) {
    $products = Product::find_by_sql($sql);
}
