<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 6/16/12
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */
require_once '../lib/setup.php';

if(!isset($_POST['email']) || !checkEmail($_POST['email'])){
    $session->message(trans("Uneta email adresa nije ispravna.<br>Molimo pokušajte ponovo", "Email address is not valid.<br>Please try again"));
    redirect_to(SITE_ROOT.'registracija');
}else{
    $_POST = cleanInput($_POST);
    $email = mysql_real_escape_string($_POST['email']);
    $client =  Customer::find_by_sql("select * from customer where email = '{$email}'");
    if(count($client)>0){
        $session->message(trans("Već postoji registrovani korisnik sa ovom email adresom<br>Molimo pokušajte ponovo. <br><a href='".SITE_ROOT."reset-pass'>Zaboravili ste lozinku?</a>", "There is already a registered user with this email address. <br> Please try again. <a href='".SITE_ROOT."reset-pass'> Did you forgot your password? </ a>"));
        redirect_to(SITE_ROOT.'registracija');
    }
    $session->message(trans('Registracija uspešna! <br> Kliknite na link iz email-a koji smo vam poslali <br>da bi aktivirali vaš nalog.', 'Registration successfull!<br> Please activate your account by clicking link in email we send you.'));
    redirect_to(SITE_ROOT.'login');
}