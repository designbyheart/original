<?php require_once('../framework/lib/setup.php');
if (isset($_POST['submit'])) { // Form has been submitted.
    require_once("../framework/lib/setup.php");
    $username = trim($_POST['login']);
    $password = md5(trim($_POST['password']));
    $found_user = Administrator::authenticate($username, $password);
    if ($found_user) {
        $session->login($found_user);
        redirect_to(ADMIN . "index.php");
    } else {
        // username/password combo was not found in the database
        $session->message("Pogrešno korisničko ime ili lozinka. Pokušajte ponovo.");
        redirect_to(ADMIN . 'login');
    }
} else { // Form has not been submitted.
    $username = "";
    $password = "";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <title>Login</title>

    <link href="<?=ADMIN?>/doc/css/login-main.css" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Cuprum" rel="stylesheet" type="text/css"/>

</head>

<body>

<!-- Top navigation bar -->
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">
            <div class="backTo"><a href="<?=SITE_ROOT?>" title=""><img
                    src="<?=ADMIN?>/doc/images/icons/topnav/mainWebsite.png" alt=""/><span>Sajt</span></a></div>
            <div class="userNav">
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>

<!-- Login form area -->
<div class="loginWrapper">
    <div class="loginLogo"><img src="<?=ADMIN?>doc/images/original-logo.png" alt=""
                                style="left:15px; position:relative; top:-20px"/></div>
    <div class="loginPanel">
        <div class="head"><h5 class="iUser">Login</h5></div>
        <form action="<?=ADMIN?>login.php" method="post" id="valid" class="mainForm">
            <fieldset>
                <span><?=$session->message();?></span>

                <div class="loginRow noborder">
                    <label for="req1">Username:</label>

                    <div class="loginInput"><input type="text" name="login" class="validate[required]" id="req1"/></div>
                    <div class="fix"></div>
                </div>

                <div class="loginRow">
                    <label for="req2">Password:</label>

                    <div class="loginInput"><input type="password" name="password" class="validate[required]"
                                                   id="req2"/></div>
                    <div class="fix"></div>
                </div>

                <div class="loginRow">
                    <!--                    <div class="rememberMe"><input type="checkbox" id="check2" name="chbox" /><label>Remember me</label></div>-->
                    <input type="submit" name="submit" value="Log me in" class="greyishBtn submitForm"/>

                    <div class="fix"></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<?php
$page = 'login';
require_once('doc/inc/footer.php') ?>