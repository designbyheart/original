<?php
require_once('../../framework/lib/setup.php');

//echo '<pre>';
//print_r($_POST);
//echo '</pre>';exit();

if (isset($_POST['id']) && $_POST['id'] != 0 && Product::find_by_id($_POST['id'])) {
    $product = Product::find_by_id($_POST['id']);
    $new = false;
} else {
    $product = new Product();
    $product->created = date("Y-m-d h:m", time());
    $new = true;
}
if(isset($_POST['price'])){
    $_POST['price'] = str_replace(',', '.', $_POST['price']);
}

if (isset($_POST['active']) && $_POST['active'] == "on") {
    $_POST['active'] = 1;
} else {
    $_POST['active'] = 0;
}
if (isset($_POST['promo']) && $_POST['promo'] == "on") {
    $_POST['promo'] = 1;
} else {
    $_POST['promo'] = 0;
}
if (isset($_POST['atributesDetails'])) {
    $_POST['attributes'] = implode(',', $_POST['atributesDetails']);
} else {
    $_POST['attributes'] = '';
}

if (isset($_POST['submenuID'])) {
    foreach ($_POST['submenuID'] as $menu) {
        $subM = SubMenu::find_by_id($menu);
        if ($subM) {
            $subM->name_sr = $_POST['sub_name_sr'][$menu];
            $subM->name_en = $_POST['sub_name_en'][$menu];
            $subM->ordering = $_POST['sub_ordering'][$menu];
            $subM->linkSr = $_POST['sub_linkSr'][$menu];
            $subM->save();
        }
    }
}

foreach ($product as $key => $value) {
    // echo $_POST[$key];

    if ($key != 'id' && isset($_POST[$key]) && $key != 'calcType') {
        if (is_array($_POST[$key])) {
            $product->$key = trim(implode(',', $_POST[$key]));
        } else {
            $product->$key = trim($_POST[$key]);
        }

        //echo $product->$key ."<br>";
    }
}

if (!isset($_POST['isTempProduct'])) {
    if ($product->plD == '' || $product->plD == '0') {
        $product->plD = $product->nextPID();
    }
}
if (isset($_POST['linkedProducts']) && $_POST['linkedProducts'] != '') {
    $product->optional = $_POST['linkedProducts'];
}
if (isset($_POST['similarProducts']) && $_POST['similarProducts'] != '') {
    $product->similar = $_POST['similarProducts'];
}

if (isset($_POST['submit'])) {

    if ($product) {
        //save product
        $id = 0;
        $new_id = $product->save();
        //{"id":"NATO","text":"North Atlantic Treaty Organization","extra":"country code: NATO"}
//                      echo '<pre>';
//                      print_r($_POST);
//                      echo '</pre>';

//        print_r($_POST['characteristicSr']);
        if (isset($_POST['characteristicSr'])) {

            foreach ($_POST['characteristicSr'] as $key => $value) {
                if ($key == 0) {
                    $key = $_POST['charID'];
                }
                $chValue = CharacteristicsValue::getObj($product->id, $key);
                //$key.'  '.$value.' '.$_POST['characteristicEn'][$key].'<br>';
                if (!$chValue) {
                    $chValue = new CharacteristicsValue();
                }
                $chValue->value_name_en = str_replace('***', '<br>', str_replace("\n", "<br>", $_POST['characteristicEn'][$key]));
                $chValue->value_name_sr = str_replace('***', '<br>', str_replace("\n", "<br>", $value));
                $chValue->productID = $product->id;
                $chValue->characteristics_id = $key;
//                echo '<pre>';
//                print_r($chValue);
//                echo '</pre>';
//                exit();
                $chValue->save();
            }
        }
        if (isset($_POST['sr_details'])) {
            foreach ($_POST['sr_details'] as $key => $values) {
                $detail = explode('|||', $key);
                if ($detail[1] == 0) {
                    $pD = new ProductDetails();
                } else {
                    $pD = ProductDetails::find_by_id($detail[1]);
                }
//
                $pD->value_sr = $values;
                $pD->productID = $product->id;
                $pD->value_en = $_POST['en_details'][$key];
                $pD->detailID = $detail[0];
                $pD->save();
            }
        }


        foreach ($_POST['note'] as $key => $value) {
            $note = Note::find_by_product($product->id, $key);
            if (!$note) {
                $note = new Note();
            }
            $note->productID = $product->id;
            $note->refID = $key;
            $note->textSrb = $value;
            $note->save();
        }
        $product->price = $_POST['price'];

        if (isset($_POST['group'])) {
            foreach ($_POST['group'] as $key => $value) {

                $g = PriceGroup::find_is_exists($key, $product->id);
                if (!$g) {
                    $g = new PriceGroup();
                    $g->productID = $product->id;
                }
                $g->parent = $_POST['parrent'][$key];
                $g->name = PriceGroup::getParentName($g->parent);
                if ($g->name == '') {
                    $g->name = 'Proizvod';
                }
                $g->calcType = $_POST['calcType'][$key];
                $g->percentage = $value;
                $g->save();
                if (isset($_POST['prices'][$key])) {
                    //$p = Price::find_by_product_and_group($product->id, $g->id);
                    //if (!$p) {
                    $p = new Price();
                    $p->productID = $product->id;
                    $P->updated = time();
                    $p->changedBy = $session->user_id;
                    $p->groupID = $g->id;

                    //}
                    $p->amount = $_POST['prices'][$key];
                    if ($p->amount > 0) {
                        $p->save();
                    }
                }

            }
        }

//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
        $ch_srID = $_POST['characteristicsID']['sr'];
        $ch_sr = '';
        if ($ch_srID == '' || !isset($ch_srID)) {
            $ch_sr = new Characteristics();
        } else {
            $ch_sr = Characteristics::find_by_id($ch_srID);
        }
        foreach ($ch_sr as $key => $value) {
            if (isset($_POST[$key]['sr']) && $key != 'id') {
                $ch_sr->$key = $_POST[$key]['sr'];
            }
        }
        $ch_sr->productID = $_POST['id'];
        $ch_sr->lang = 1;
        $ch_sr->save();

        $ch_enID = $_POST['characteristicsID']['en'];
        $ch_en = '';
        if ($ch_enID == '' || !isset($ch_enID)) {
            $ch_en = new Characteristics();
        } else {
            $ch_en = Characteristics::find_by_id($ch_enID);
        }
        foreach ($ch_en as $key => $value) {
            if (isset($_POST[$key]['sr']) && $key != 'id') {
                $ch_en->$key = $_POST[$key]['en'];
            }
        }
        $ch_en->productID = $_POST['id'];
        $ch_en->lang = 2;
        $ch_en->save();


        // getting id for gallery refID
        if ($_POST['id'] == 0) {
            $id = $new_id;
        } else {
            $id = $_POST['id'];
        }

//        print_r($_FILES );
        if ($id != 0) {
            if (isset($_FILES)) {
                $files = rearrange($_FILES['fileUpload']);
                foreach ($files as $file) {
                    if ($file['name'] != '') {
                        $gall = new Gallery();
                        $gall->refID = $product->id;
                        $gall->type = 'product';
                        $gall->file = cleanFileName($file['name']);
                        uploadPhoto($file, '', 550, 100, 80, 150, 100, cleanFileName($file['name']));
                        $gall->save();
                    }
                }
//                print_r($files);
//                $file = $_FILES['fileUpload'];
//                print_r($_FILES);
//                foreach($file['name'] as $key=>$value){
//                    if($file['name'][$key]!=''){
//                    $gall = new Gallery();
//                        $gall->refID = $product->id;
//                        $gall->type = 'product';
//                        $gall->file = cleanFileName($file['name'][$key]);
////                        echo $file['name'][$key].'<br>';
//                        uploadPhoto($file['name'][$key], '', 550, 100, 80, 150, 100, $file['name'][$key]);
//                        $gall->save();
//                    }
//                    echo $key." ".$value;
//                }
//                for ($i = 0; $i < (int)$_POST['current']; $i++) {
//                    print_r($_POST['fileUpload[1]');
//                    if ($_FILES['name'][$i] != '') {
//                        $gall = new Gallery();
//                        $gall->refID = $product->id;
//                        $gall->type = 'product';
//                        $gall->file= cleanFileName($_FILES['name'][$i]);
//                        uploadPhoto($_FILES['name'][$i], '', 550, 100, 80, 150, 100);
//                        $gall->save();
//                    }

//                }
            }

//            foreach ($_FILES as $file) {
//                if ($file['name'] != '') {
//                    $gal = new Gallery();
//                    $gal->file = cleanFileName('product-' . $file['name']);
//                    $gal->refID = $id;
//                    $gal->type = 'product';
//
//
//                }
        }
    }
}

if ($product->save()) {
    if ($new_id == 1) {
        $session->message('Promene su uspešno sačuvane');
    } elseif ($new_id > 1) {
        $session->message('Proizvod je sačuvan');
    }

    $_SESSION['mType'] = 2;
    redirect_to(ADMIN . 'proizvod/' . $product->id);
    /*         redirect_to(ADMIN . 'proizvodi'); */
} else {
    $session->message('Postoji problem. Proizvod nije sačuvan');
    $_SESSION['mType'] = 4;
    redirect_to(ADMIN . 'proizvod/' . $product->id);
}


redirect_to(ADMIN . 'proizvod/' . $product->id);

//if($product->id != ''){
//redirect_to(ADMIN.'proizvod/'.$product->id);
//}else{
// redirect_to(ADMIN.'proizvodi');
//}
