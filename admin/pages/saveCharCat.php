<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/26/12
 * Time: 10:06 AM
 * To change this template use File | Settings | File Templates.
 */

require_once('../../framework/lib/setup.php');
if (isset($_GET)) {
    $c = CharacteristicsCategory::find_by_id($_GET['id']);
    if ($c) {
        $c->category_name_sr = $_GET['category_name_sr'];
        $c->category_name_en = $_GET['category_name_en'];
        if ($c->save()) {
            echo 'OK';
        } else {
            echo 'Nije sačuvano';
        }
    }
}