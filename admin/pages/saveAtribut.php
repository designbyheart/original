<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/20/12
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if (isset($_GET['id'])) {

    $detail = ProductDetails::find_by_id($_GET['id']);
    $detailList = array();
    if ($_GET['parent'] != $detail->detailID) {

        $detailListROOT = array_filter(explode(',', $_GET['parent']));
        foreach ($detailListROOT as $d) {
            $parent = ProductDetailsSettings::find_by_id($d);
            $detailList[] = $parent->name_sr;
        }
    }
    $detail->value_sr = str_replace(',', ' ', $_GET['detailSr']);
    $detail->value_en = str_replace(',', ' ', $_GET['detailEn']);
    $detail->detailID = $_GET['parent'];

    if ($detail->save()) {
        if ($detailList) {
            echo implode(',', $detailList);
        } else {
            echo "OK";
        }
    } else {
        echo 'Nije sačuvano';
    }
}