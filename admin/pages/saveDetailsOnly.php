<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/4/12
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
require_once '../../framework/lib/setup.php';


if (isset($_POST['name_sr'])) {
    foreach ($_POST['detailID'] as $detailID):
        $detail =ProductDetailsSettings::find_by_id($detailID);
        $detail->name_sr = $_POST['name_sr'][$detailID];
        $detail->name_en = $_POST['name_en'][$detailID];
        $detail->save();
    endforeach;
    redirect_to(ADMIN.'izmena-detalja-proizvoda');
}
