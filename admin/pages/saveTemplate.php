<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */

require_once('../../framework/lib/setup.php');

if(isset($_POST['templateName']) && $_POST['templateName']!='')  {
    if(PriceTemplate::isExists($_POST['templateName'])){
        $session->message('Šablon sa ovim imenom je već unet');
        redirect_to(ADMIN.'cenovnik');
    }
    else{
        $p = new PriceTemplate();
        $p->name = trim($_POST['templateName']);
        if(isset($_POST['type'])){
            $p->type = $_POST['type'];
        }else{
            $p->type = 0;
        }
        $p->save();
        redirect_to(ADMIN.'cenovnik');
    }
}else{
    $session->message('Unesite ime novog šablona');
    redirect_to(ADMIN.'cenovnik');

}