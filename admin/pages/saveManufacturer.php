<?php 
require_once('../../framework/lib/setup.php');

if(isset($_POST['id']) && $_POST['id']!=0 && Manufacturer::find_by_id($_POST['id'])){
    $manufacturer = Manufacturer::find_by_id($_POST['id']);
    $new = false;
}else{
    $manufacturer= new Manufacturer();
    $new = true;
}
foreach($manufacturer as $key=>$value){
        // echo $_POST[$key];

        if($key!='id' && isset($_POST[$key])){
            $manufacturer->$key = trim($_POST[$key]);
            //echo $category->$key ."<br>";
        }
    	
	}
if(isset($_POST['submenuID'])){
    foreach ($_POST['submenuID'] as $menu) {
        $subM = SubMenu::find_by_id($menu);
        if($subM){
            $subM->name_sr =$_POST['sub_name_sr'][$menu];
            $subM->name_en =$_POST['sub_name_en'][$menu];
            $subM->ordering =$_POST['sub_ordering'][$menu];
            $subM->linkSr =$_POST['sub_linkSr'][$menu];
            $subM->save();
        }
    }
}

$manufacturer->active = 0;
if(isset($_POST['active'])){
    $manufacturer->active = 1;
}
$manufacturer->banner = 0;
if(isset($_POST['banner'])){
    $manufacturer->banner = 1;
}
if(isset($_POST['banner_home']) && $manufacturer->banner_home==0){
    $manufacturer->banner_home = Manufacturer::find_max_banner()+1;
}
if(!isset($_POST['banner_home']) ){
    $manufacturer->banner_home = 0;
}
//    //var_dump($_POST['partners']);
//    if(isset($_POST['partners'])){
//        $product->partners = "@". implode("@", $_POST['partners']);
//    }

if($_FILES['logo']['name']!=''){
    $manufacturer->logo = clearFileName('proizvodjac-logo-'.$_FILES['logo']['name']);
    uploadPhoto($_FILES['logo'], '', 400, 100, 80, 250, 200, $manufacturer->logo);
}
unset($_FILES['logo']);

if($manufacturer && $manufacturer->save()){
  $session->message('Proizvođač je sačuvan');
//  if(isset($_FILES)){
//       foreach($_FILES as  $file){
//           if ($file['name']!=''){
//               $gal = new Gallery();
//               $gal->file = cleanFileName('proizvod-'.$file['name']);
//               $gal->refID = $product->id;
//               $gal->type = 'proizvod';
//
//               uploadPhoto($file, '', 400, 100, 80, 250, 200,$gal->file);
//               $gal->save();
//           }
//        }
//    }
  $_SESSION['mType']= 2;

  redirect_to(ADMIN.'proizvodjaci');
}else{
  $session->message('Postoji problem. Proizvođač nije sačuvan');
  $_SESSION['mType']= 4;
  redirect_to(ADMIN.'proizvodjac/'.$manufacturer->id);
}
?>
