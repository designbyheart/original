<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
//require_once (ClASSES.'characteristic_value.php');

if ($_GET['id'] == 0) {
    $category = new Category();
} else {
    $category = Category::find_by_id($_GET['id']);
}
?>
<!-- Content -->
<div class="content">
<div class="title" style="position:relative;"><h5>
    <?php
    if ($_GET['id'] == 0) {
        ?>Nova kategorija<?
    } else {
        ?>Izmena kategorije
        <span style="position:absolute; right:20px; top:7px; color:#bbb;">Link:
            <a href="<?=SITE_ROOT?>proizvodi/<?=$category->id?>/<?=urlSafe($category->name_sr)?>" target="_blank"
               style="color:#ddd;">/proizvodi/<?=$category->id?>
                /<?=urlSafe($category->name_sr)?>
            </a>
        </span>
        <?
    }
    ?>
</h5></div>

<!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->


<form class="mainForm" action="<?=ADMIN?>saveCategory" method="post" enctype="multipart/form-data">
<!-- Tabs -->
<fieldset>
<?php if ($session->message() != '') {
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p  class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
<div class="widget">
<ul class="tabs">
    <li><a href="#tab1">Srpski</a></li>
    <li><a href="#tab2">Engleski</a></li>
    <li><a href="#tab3">Galerija</a></li>
    <li><a href="#tab4">Seo</a></li>
    <li><a href="#tab5">Kategorije</a></li>
</ul>

<div class="tab_container">

<input type="hidden" name="id" value="<?=$category->id?>"/>

<!-- Content of tab1 -->
<div id="tab1" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv kategorije:</label>

        <div class="formRight">
            <input type="text" name="name_sr" value="<?=$category->name_sr?>"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <label>Nadređena kategorija:</label>

        <div class="formRight">
            <select name="parent_cat">
                <option value="0">Izaberi kategoriju</option>
                <?php

                $cats = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = 0 order by ordering ASC ");
                foreach ($cats as $mainC) {
                    ?>
                    <option value="<?=$mainC->id?>" <?php if ($mainC->id == $category->parent_cat) {
                        echo "selected";
                    } ?>><?=$mainC->name_sr?> [glavna]
                    </option>
                    <?php
                    $subC = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = {$mainC->id} order by ordering ASC ");
                    foreach ($subC as $sC) {
                        ?>
                        <option value="<?=$sC->id?>" <?php if ($sC->id == $category->parent_cat) {
                            echo "selected";
                        } ?>>&nbsp;&rarr;<?=$sC->name_sr?></option>
                        <?php
                        $subSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = {$sC->id} order by ordering ASC ");
                        foreach ($subSubC as $ssC) {
                            ?>
                            <option value="<?=$ssC->id?>" <?php if ($ssC->id == $category->parent_cat) {
                                echo "selected";
                            } ?>>&nbsp;&nbsp;&nbsp;&nbsp;&rarr;<?=$ssC->name_sr?></option>
                            <?php
                            $subSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = {$ssC->id} order by ordering ASC ");
                            foreach ($subSubSubC as $sssC) {
                                ?>
                                <option value="<?=$sssC->id?>" <?php if ($sssC->id == $category->parent_cat) {
                                    echo "selected";
                                } ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;<?=$sssC->name_sr?></option>
                                <?php
                            }
                        }
                    }
                }



                /* ?> <option value="0">Izaberi kategoriju</option>
                                           <?php
                                                  $categories = Category::find_by_sql("SELECT id, name_sr, parent_cat FROM category where parent_cat = 0 AND id != {$_GET['id']} order by name_sr ASC");
                                                 foreach ($categories as $cat) : ?>
                                                 <option value="<?=$cat->id?>"
                                                 <?php if($category->id >0){
                                                       if(isset($category->id) && $cat->id == $category->parent_cat){ echo 'selected';}
                                                       }?>
                                                 ><?=$cat->name_sr;?> [glavna]</option>
                                                     <?php
                                                     $sCats = Category::find_children($cat->id);
                                                     if($sCats){
                                                         foreach($sCats as $sC){
                                                             if($cat->parent_id>0){
                                                                 $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                                             }else{
                                                                 $inset = '&nbsp;&nbsp;&rarr;';
                                                             }
                                                             $sel = '';
                                                             if($sC->id == $_GET['id']){
                                                                 $sel = ' selected';
                                                             }
                                                     ?>
                                                     <option value="<?=$sC->id?>"<?=$sel?>><?=$inset.$sC->name_sr?></option>
                                                         <?php
                                                         $sCs = Category::find_children($sC->id);
                                                         if($sCs){
                                                             foreach($sCs as $ssC){
                                                                 if($sC->parent_id>0){
                                                                     $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                                                 }else{
                                                                     $inset = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr;';
                                                                 }
                                                                 $sel = '';
                                                                 if($ssC->id == $_GET['id']){
                                                                     $sel = ' selected';
                                                                 }
                                                                 ?>
                                                                 <option value="<?=$ssC->id?>"<?=$sel?> style="color:red"><?=$inset.$ssC->name_sr?></option>
                                                                 <?php } ?>
                                                   <?php } } } ?>
                                           <?php endforeach; */ ?>
            </select>
        </div>
        <div class="fix"></div>
    </div>


    <div class="rowElem">
        <label>Aktivan:</label>

        <div class="formRight">
            <input type="checkbox" name="active"
                <?php if ($category->active == 1) {
                echo 'checked';
            } ?>  />
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Redosled:</label>

        <div class="formRight">
            <input type="text" name="ordering" value="<?=$category->ordering?>"
                   style="width:30px; text-align:center; font-size:16px"/>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Statistika:</label>

        <div class="formRight">
            Ukupno proizvoda:
            <b><?php echo Product::count_by_cat($_GET['id']); ?></b>
            <br>
            Aktivno:
            <b><?php echo Product::count_by_cat_act($_GET['id']); ?></b>
        </div>
        <div class="fix"></div>
    </div>

</div>
<!-- End - tab1 -->


<!-- Content of tab2 -->
<div id="tab2" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv kategorije:</label>

        <div class="formRight">
            <input type="text" name="name_en" value="<?=$category->name_en?>"/>
        </div>
        <div class="fix"></div>
    </div>
</div>
<!-- End - tab2 -->

<!-- Content of tab3 -->
<div id="tab3" class="tab_content">
    <fieldset>
        <legend>Upload fotografije za meni</legend>
        <input type="hidden" name="" value="1" class="current"/>
        <input id="file_upload" class="fileInput" type="file" name="image0"/>

        <div class="fields"></div>
        <br/>
    </fieldset>
    <fieldset id="galleryImages">
        <?php
        $img_type = 'cat-menu';
        require(ROOT_DIR . "admin/pages/galleryList.php");
        ?>
    </fieldset>

    <fieldset>
        <legend>Upload fotografije za prikaz na strani</legend>
        <input type="hidden" name="" value="1" class="current"/>
        <input id="file_upload2" class="fileInput" type="file" name="image1"/>

        <div class="fields"></div>
        <br/>
    </fieldset>
    <fieldset id="galleryImages2">
        <?php
        $img_type = 'cat';
        require(ROOT_DIR . 'admin/pages/galleryList.php');
        ?>
    </fieldset>
</div>
<!-- End - tab3 -->

<!-- Content of tab4 -->
<div id="tab4" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv kategorije(srpski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_sr" value="<?=$category->seo_title_sr?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem noborder">
        <label>Naziv kategorije(engleski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_en" value="<?=$category->seo_title_en?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(srpski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_sr"><?=$category->seo_desc_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(engleski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_en"><?=$category->seo_desc_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(srpski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_sr"><?=$category->seo_keywords_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(engleski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_en"><?=$category->seo_keywords_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>

</div>
<!-- End - tab4 -->
<!-- Content of tab5 -->
<div id="tab5" class="tab_content">
    <?php $chVal = CharacteristicsCategory::find_all();
    if (count($chVal) > 0) {
        foreach ($chVal as $chValRecord) {
            ?>
            <label>
                <input type="checkbox" name="charValues[]" value="<?=$chValRecord->id?>" <?php
                    if (CharacteristicsCategory::is_in_category($_GET['id'], $chValRecord->id)) {
                        echo ' checked ="checked"';
                    } ?> />
                <span style="position: relative;margin-left:8px;top:3px;"><?=$chValRecord->category_name_sr?></span>
            </label>
            <?php
        }
    }?>

</div>
<!-- End - tab5 ->

 </div>
 <div class="fix"></div>
</div>
<!-- End - Tabs -->
</fieldset>
<a href="<?= ADMIN ?>kategorije"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
<input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
</form>

</div>
<div class="fix"></div>
</div>
