<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>
<?php
if ($_GET['id'] == 0) {
    $page = new GeneralPage();
} else {
    $page = GeneralPage::find_by_id($_GET['id']);
}
$otherPages = GeneralPage::find_other($_GET['id']);
//var_dump($page);
?>
<div class="newLink" style="position:absolute">
    <h5>Novi link</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveSubmenu" method="post">
        <label>
            Srpski naziv
            <input type="text" name="name_sr">
        </label>
        <label>
            Engleski naziv
            <input type="text" name="name_en">
        </label>
        <label>Link
            <input type="text" name="linkSr">
        </label>

        <input type="hidden" name="pageType" value="general_page">
        <input type="hidden" name="pageID" value="<?=$_GET['id']?>">
        <button type="submit" class="greyishBtn">Sačuvaj</button>
    </form>
</div>
<!-- Content -->
<div class="content">
<div class="title" style="position:relative;"><h5>
    <?php
    if ($_GET['id'] == 0) {
        ?>Nova strana<?
    } else {
        ?>Izmena strane
        <span style="position:absolute; right:20px; top:7px; color:#bbb;">Link:
            <a href="<?=SITE_ROOT?>info/<?=$page->id?>/<?=urlSafe($page->name_sr)?>" target="_blank"
               style="color:#ddd;">/info/<?=$page->id?>/<?=urlSafe($page->name_sr)?>
            </a>
        </span>
        <?
    }
    echo $page->name_sr?></h5></div>

<!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

<form class="mainForm" action="<?=ADMIN?>savePage" method="post" enctype="multipart/form-data">
<!-- Tabs -->
<fieldset>
<div class="widget">
<ul class="tabs">
    <li><a href="#tab1">Srpski</a></li>
    <li><a href="#tab2">Engleski</a></li>
    <li><a href="#tab3">Galerija</a></li>
    <li><a href="#tab5">Sub-menu</a></li>
    <li><a href="#tab4">Seo</a></li>
    <li><a href="#tab6">Povezane strane</a></li>
</ul>
</ul>

<div class="tab_container">

<input type="hidden" name="id" value="<?=$page->id?>"/>

<!-- Content of tab1 -->
<div id="tab1" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv strane:</label>

        <div class="formRight">
            <input type="text" name="name_sr" value="<?=$page->name_sr?>"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem noborder">
        <label>Link:</label>

        <div class="formRight">
            <input type="text" name="pg_name" value="<?=$page->pg_name?>"/>
            <span class="note">link mora biti malim slovima i bez razmaka(umesto "O nama", koristite "o-nama")</span>
            <br>
            <span class="note">Pogledaj stranu: <a href="<?=SITE_ROOT?>info/<?=$page->pg_name?>"><?=$page->pg_name?></a></span>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Aktiviran:</label>

        <div class="formRight">
            <input type="checkbox" name="active"
                <?php if ($page->active == 1) {
                echo 'checked';
            }
                ?>
                    />
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Šablon:</label>

        <div class="formRight">
            <select name="template" id="templateChooser">
                <?php for ($i = 1; $i < 4; $i++) { ?>
                <option value="<?=$i?>" <?php if ($page->template == $i) {
                    echo 'selected';
                } ?>><?=$i?> kolon<?php if ($i == 1) {
                    echo 'a';
                } else {
                    echo 'e';
                } ?></option>
                <?php } ?>
            </select>
            <a href="#" class="templateUpdate" style="position:relative; left:10px; top:3px;">Osveži šablon</a>

            <div class="templateGallery" style="margin-left:2.5em; float:left;display: none">
                <img src="<?=ADMIN?>doc/images/template1.png" alt=""
                     style="border:solid .1em #ddd; margin-right: .5em; float:left;">
                <img src="<?=ADMIN?>docs/images/temp2.png" alt="">
                <img src="<?=ADMIN?>docs/images/temp3.png" alt="">
                <img src="<?=ADMIN?>docs/images/temp4.png" alt="">
            </div>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head">
                    <h5 class="iPencil">Sadržaj strane - I kolona</h5>
                    <label style="float:right; padding-right: 2em;margin-top:.3em"><input type="checkbox"
                                                                                          name="column1Menu"><span
                            style="position:relative;left:1em;top:.4em">Podesi meni</span></label>
                </div>
                <textarea class="wysiwyg" name="desc_sr1" rows="5" cols="">
                    <?=$page->desc_sr1?>
                </textarea
                        ></div>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem" id="sr2" style="display:none">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Sadržaj strane - II kolona</h5>
                    <label style="float:right; padding-right: 2em;margin-top:.3em"><input type="checkbox"
                                                                                          name="column2Menu"><span
                            style="position:relative;left:1em;top:.4em">Podesi meni</span></label>
                </div>
                <textarea class="wysiwyg" name="desc_sr2" rows="5" cols="">
                    <?=$page->desc_sr2?>
                </textarea></div>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem" id="sr3" style="display:none">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Sadržaj strane - III kolona</h5>
                    <label style="float:right; padding-right: 2em;margin-top:.3em">
                        <input type="checkbox" name="column3Menu">
                        <span style="position:relative;left:1em;top:.4em">Podesi meni</span></label>
                </div>
                <textarea class="wysiwyg" name="desc_sr3" rows="5" cols="">
                    <?=$page->desc_sr3?>
                </textarea
                        ></div>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Glavna slika:</label>

        <div class="formRight">
            <input type="file" name="mainImg"/>


            <?php if ($page->img != '') { ?>
            <img src="<?=GALL?>thumbsM/<?=$page->img?>" alt="" class="imgPreview"
                 style="display:block;padding:1.5em 0;margin-top:1.5em;border-top:solid 1px #ddd">
            <?php } ?>
        </div>
        <span style="float:left; clear:left;position:relative;top:17px;">Pozicija slike</span>

        <div class="formRight">
            <input type="radio" name="imagePosition" value="1"
                   id="imagePosition1" <?=checked($page->imagePosition, '1')?>>
            <label for="imagePosition1">Leva kolona</label>
            <input type="radio" name="imagePosition" value="2" <?=checked($page->imagePosition, '2')?>
                   id="imagePosition2">
            <label for="imagePosition2">Centralna kolona</label>
            <input type="radio" name="imagePosition" value="3"
                   id="imagePosition3" <?=checked($page->imagePosition, '3')?>>
            <label for="imagePosition3">Desna kolona</label>
        </div>
        <div class="fix"></div>
    </div>

</div>
<!-- End - tab1 -->


<!-- Content of tab2 -->
<div id="tab2" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv strane:</label>

        <div class="formRight">
            <input type="text" name="name_en" value="<?=$page->name_en?>"/>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Sadržaj strane - I kolona</h5></div>
                <textarea class="wysiwyg" name="desc_en1" rows="5" cols="">
                    <?=$page->desc_en1?>
                </textarea
                        ></div>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem" id="en2" style="display:none">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Sadržaj strane - II kolona</h5></div>
                <textarea class="wysiwyg" name="desc_en2" rows="5" cols="">
                    <?=$page->desc_en2?>
                </textarea
                        ></div>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem" id="en3" style="display:none">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Sadržaj strane - III kolona</h5></div>
                <textarea class="wysiwyg" name="desc_en3" rows="5" cols="">
                    <?=$page->desc_en3?>
                </textarea
                        ></div>
        </div>
        <div class="fix"></div>
    </div>

</div>
<!-- End - tab2 -->

<!-- Content of tab3 -->
<div id="tab3" class="tab_content">
    <fieldset>
        <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
        <input type="hidden" name="" value="1" class="current"/>
        <input id="file_upload" class="fileInput" type="file" name="image0"/>

        <div class="fields"></div>
        <br/>
    </fieldset>
    <fieldset id="galleryImages">
        <?php
        //$_GET['page'] = $event->event_type;
        require_once(ROOT_DIR . 'admin/pages/galleryList.php');
        ?>
    </fieldset>
</div>
<!-- End - tab3 -->

<!-- Content of tab4 -->
<div id="tab4" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv strane(srpski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_sr" value="<?=$page->seo_title_sr?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem noborder">
        <label>Naziv strane(engleski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_en" value="<?=$page->seo_title_en?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(srpski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_sr"><?=$page->seo_desc_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(engleski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_en"><?=$page->seo_desc_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(srpski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_sr"><?=$page->seo_keywords_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(engleski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_en"><?=$page->seo_keywords_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>

</div>
<!-- End - tab4 -->
<!-- Content of tab5 -->
<div id="tab5" class="tab_content">

    <div class="table">
        <div class="head"><h5>Lista linkova u podmeniju - <a href="#" class="addNewLink">Dodaj novi link</a></h5></div>
        <table class="tableStatic" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td width="2.5%">Pozicija</td>
                <td width="20%">Tekst linka - spski</td>
                <td width="20%">Tekst linka - engleski</td>
                <td width="20%">Adresa - url</td>

            </tr>
            </thead>
            <tbody>
            <?php
            $subMenu = SubMenu::find_for_page($page->id, 'general_page');

            if ($subMenu && count($subMenu) > 0) {
                foreach ($subMenu as $m): ?>
                <tr>
                    <td><input type="text" name="sub_ordering[<?=$m->id?>]" value="<?=$m->ordering?>" class="centerAl"
                               style="width: 95%;left:10px">
                        <input type="hidden" name="submenuID[<?=$m->id?>]" value="<?=$m->id?>">
                    </td>
                    <td><input type="text" name="sub_name_sr[<?=$m->id?>]" value="<?=$m->name_sr?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                    <td><input type="text" name="sub_name_en[<?=$m->id?>]" value="<?=$m->name_en?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                    <td><input type="text" name="sub_linkSr[<?=$m->id?>]" value="<?=$m->linkSr?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                </tr>
                    <?php endforeach;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<!-- End - tab5 -->
<?php
$dtC = "";
$tmC = "";
if ($page->counter > 0) {
    $dtC = date("d.m.y.", $page->counter);
    $tmC = date("H.i.s.", $page->counter);
}
?>
<!-- Content of tab6 -->
<div id="tab6" class="tab_content">
    <div class="rowElem noborder">
        <label>Aktiviraj counter:</label>

        <div class="formRight">
            <label>
                datum:
                <input type="text" name="counter[date]" class="datepicker" value="<?=$dtC?>"
                       style="text-align: center;">
            </label>
            <label>
                vreme:
                <input type="text" name="counter[time]" value="<?=$tmC?>" class="timepicker"
                       style="text-align: center;">
            </label>
            <label><span style="float:left;margin:-5px 15px 0 0;"><input type="checkbox" name="noCounter"
                                                                         value="1"></span> Isključi counter</label>

            <div class="fix"></div>
        </div>
    </div>
    <div class="rowElem">
        <h4>Povezane strane</h4>

        <p>Izaberite dve ili tri druge strane </p>
    </div>
    <input type="hidden" name="leftBoxID" id="leftBoxID" value="<?=$page->leftBoxID?>">
    <input type="hidden" name="centerBoxID" id="centerBoxID" value="<?=$page->centerBoxID?>">
    <input type="hidden" name="rightBoxID" id="rightBoxID" value="<?=$page->rightBoxID?>">

    <div class="rowElem">
        <?php
        $pl = '';
        $hl = "";
        if (trim($page->leftBox) == "") {
            $hl = 'style="top:-2000px;position:absolute"';
        } else {
            $pl = 'style="display:none"';

        } ?>
        <label>Levi box
        </label>


        <div class="formRight">
            <div class="list leftList" <?=$pl?> >
                <a href="left#0" class="btn14 pageLinks leftPage ">Isključi stranu</a>
                <?php foreach ($otherPages as $p) {
                $lBox = 0;
                if ($page->leftBoxID == $p->id) {
                    $lBox = 'actPage';
                }
                ?>
                <a href="left#<?=$p->id?>" class="btn14 pageLinks leftPage <?=$lBox?>"><?=$p->name_sr?></a>
                <?php } ?>
            </div>
            <div class="centerBoxText" <?=$hl?> >
                <textarea class="wysiwyg" name="leftBoxContent"></textarea>
            </div>
        </div>
        <div class="fix"></div>
    </div>
    <?php if (trim($page->centerBox) == "") {
    $hl = 'style="top:-2000px;position:absolute"';
} else {
    $pl = 'style="display:none"';

} ?>
    <div class="rowElem">
        <label>Centralni box:<br></label>

        <div class="formRight">
            <div class="list centerList" <?=$pl?>>
                <a href="center#0" class="btn14 pageLinks centerPage ">Isključi stranu</a>
                <?php foreach ($otherPages as $p) {
                $cBox = 0;
                if ($page->centerBoxID == $p->id) {
                    $cBox = 'actPage';
                }
                ?>
                <a href="center#<?=$p->id?>" class="btn14 pageLinks centerPage <?=$cBox?>"><?=$p->name_sr?></a>
                <?php } ?>
            </div>
            <div class="centerBoxText" <?=$hl?> >
                <textarea class="wysiwyg" name="centerBoxContent"></textarea>
            </div>
        </div>
        <div class="fix"></div>
    </div>
    <?php if (trim($page->rightBox) == "") {
    $hl = 'style="top:-2000px;position:absolute"';
} else {
    $pl = 'style="display:none"';

} ?>
    <div class="rowElem">
        <label>Desni box :</label>

        <div class="formRight">
            <div class="list rightList" <?=$pl?>>
                <a href="right#0" class="btn14 pageLinks rightPage ">Isključi stranu</a>
                <?php foreach ($otherPages as $p) {
                $rBox = 0;
                if ($page->rightBoxID == $p->id) {
                    $rBox = 'actPage';
                }
                ?>
                <a href="right#<?=$p->id?>" class="btn14 pageLinks rightPage <?=$rBox?>"><?=$p->name_sr?></a>
                <?php } ?>
            </div>
            <div class="rigtBoxText" <?=$hl?>>
                <textarea class="wysiwyg" name="rightBoxContent"></textarea>
            </div>
        </div>
        <div class="fix"></div>
    </div>


</div>
<!-- End - tab6 -->

</div>
<div class="fix"></div>
</div>
<!-- End - Tabs -->
</fieldset>
<a href="<?=ADMIN?>opste_strane"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
<input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
</form>

</div>
<div class="fix"></div>
</div>