<?php
require_once('../../framework/lib/setup.php');
$_POST['desc_sr1'] = cleanHTML($_POST['desc_sr1']);
$_POST['desc_sr2'] = cleanHTML($_POST['desc_sr2']);
$_POST['desc_sr3'] = cleanHTML($_POST['desc_sr3']);
$_POST['desc_en1'] = cleanHTML($_POST['desc_en1']);
$_POST['desc_en2'] = cleanHTML($_POST['desc_en2']);
$_POST['desc_en3'] = cleanHTML($_POST['desc_en3']);


if (isset($_POST['id']) && $_POST['id'] != 0 && GeneralPage::find_by_id($_POST['id'])) {
    $page = GeneralPage::find_by_id($_POST['id']);
    $new = false;
} else {
    $page = new GeneralPage();
    $new = true;
}


if (isset($_POST['active']) && $_POST['active'] == "on") {
    $_POST['active'] = 1;
} else {
    $_POST['active'] = 0;
}
if (isset($_POST['submenuID'])) {
    foreach ($_POST['submenuID'] as $menu) {
        $subM = SubMenu::find_by_id($menu);
        if ($subM) {
            $subM->name_sr = $_POST['sub_name_sr'][$menu];
            $subM->name_en = $_POST['sub_name_en'][$menu];
            $subM->ordering = $_POST['sub_ordering'][$menu];
            $subM->linkSr = $_POST['sub_linkSr'][$menu];
            $subM->save();
        }
    }
}
foreach ($page as $key => $value) {
    // echo $_POST[$key];
    if ($key != 'id' && isset($_POST[$key]) && !is_array($_POST[$key])) {
        $page->$key = trim($_POST[$key]);
    }
}

//if (isset($_POST['name_sr'])) {
//    //replace one or more spaces with one
//    $name = preg_replace("/[[:blank:]]+/", " ", $_POST['name_sr']);
//    //replace spaces with '-', and all letters with small letters
//    //trim() - remove space from the beginning and end of the string
//    $page->pg_name = strtolower(str_replace(" ", "-", trim($name)));
//}
$page->pg_name = $_POST['pg_name'];
if (isset($_POST['template'])) {
    $page->template = $_POST['template'];
}

if (isset($_FILES)) {
    foreach ($_FILES as $file) {
        if ($file['name'] != '' && $file['name'] == 'mainImg') {
            $page->img = cleanFileName('general_page-' . basename($file['name']));
//            print_r($page);
            $page->save();
            uploadPhoto($file, '', 500, 100, 80, 250, 200, $page->img);
        }
    }
}

$page->counter = strtotime($_POST['counter']['date'] . ' ' . $_POST['counter']['time']);
if (isset($_POST['noCounter']) || $_POST['counter']['date']=='') {
    $page->counter = 0;
}
$page->rightBoxID = $_POST['rightBoxID'];
$page->centerBoxID = $_POST['centerBoxID'];
$page->leftBoxID = $_POST['leftBoxID'];

if (isset($_POST['submit'])) {

    if ($page) {
        //save page
        $id = 0;
        $new_id = $page->save();

        // getting id for gallery refID
        if ($_POST['id'] == 0) {
            $id = $new_id;
        } else {
            $id = $_POST['id'];
        }
        if ($id != 0) {
            if (isset($_FILES)) {
                foreach ($_FILES as $file) {
                    if ($file['name'] != '' && $file['name'] != 'mainImg') {
                        $gal = new Gallery();
                        $gal->file = cleanFileName('general_page-' . basename($file['name']));
                        if ($page->img == '') {
                            $page->img = $gal->file;
                            $page->save();
                        }
                        $gal->refID = $id;
                        $gal->type = 'general_page';

                        uploadPhoto($file, '', 400, 100, 80, 250, 200, $gal->file);
                        if ($gal->save()) {
                            $session->message('Slika je uneta');
                            $_SESSION['mType'] = 2;
                        } else {
                            $session->message('Postoji problem. Slika nije uneta');
                            $_SESSION['mType'] = 4;
                        }
                    }
                }
            }
        }

        if ($new_id == 1) {
            $session->message('Promene su uspešno sačuvane ');
        } elseif ($new_id > 1) {
            $session->message('Strana je sačuvana ');
        }

        $_SESSION['mType'] = 2;
        redirect_to(ADMIN . 'opste_strane');
    } else {
        $session->message('Postoji problem. Strana nije sačuvana');
        $_SESSION['mType'] = 4;
        redirect_to(ADMIN . 'opsta/' . $page->pg_name);
    }
}
?>
