<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/24/12
 * Time: 10:46 PM
 * To change this template use File | Settings | File Templates.
 */
require_once "../../framework/lib/setup.php";
if (isset($_POST['newSettingsName']) && $_POST['newSettingsName'] != '') {
    $settings = new Settings();
    $settings->settingsName = $_POST['newSettingsName'];
    $settings->settingsValue = $_POST['newSettingsValue'];
    $settings->save();
    redirect_to(ADMIN . 'podesavanja');
}
if (isset($_POST['saveAllSettings']) && $_POST['saveAllSettings'] != '') {
    foreach ($_POST['settingsName'] as $key => $value) {
        $s = Settings::find_by_id($key);
        $s->settingsValue = $_POST['settingsName'][$key];
        $s->save();
    }
}
redirect_to(ADMIN . 'podesavanja');