<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */

require_once('../../framework/lib/setup.php');
//echo '<pre>';
//print_r($_POST);
//echo '</pre>';
// [templateID] => 2 [type] => 1 [OMG] => Array ( [10] => 10 ) )
/*
 * Array
(
    [templateID] => 4
    [country] => 0
    [templateTax] => 0
    [type] => Array
        (
            [28] => 0
            [29] => 0
        )

    [parent] => Array
        (
            [28] => 0
            [29] => 0
        )

    [groupID] => 29
    [groupPercentage] => Array
        (
            [28] => 0
            [29] => 0
        )

)

*/

$template = PriceTemplate::find_by_id($_POST['templateID']);
if (!$template) {
    $template = new PriceTemplate();
}
$template->country = $_POST['country'];
$template->countryTax = $_POST['templateTax'];
$template->tax =$_POST['templateTax'];
$template->save();



//    echo '<pre>';
//    print_r($gr);
//    echo '</pre>';

//            $gr = PriceGroup::find_for_value($key, $template->id);
//            if(!$gr){
//                $gr = new PriceGroup();
//                $gr->parent = $key;
//                $gr->templateID = $template->id;
//                $parentGroup = PriceGroup::find_main_id($key);
//                $gr->name = $parentGroup->name;
//            }
//            $gr->percentage = $value;

//        }
        if($template->save()){
            foreach ($_POST['groupPercentage'] as $key => $value) {
                $gr = PriceGroup::find_for_value($key, $template->id);
                if (!$gr) {
                    $gr = new PriceGroup();
                }
                $gr->templateID = $template->id;
                $gr->parent = $_POST['parent'][$key];
                $gr->percentage = $_POST['groupPercentage'][$key];
                $gr->calcType = $_POST['type'][$key];
                $gr->name = PriceGroup::getParentName($gr->parent);
                if ($gr->name == '') {
                    $gr->name = 'Proizvod';
                }
                $gr->save();
            }
            $session->message('Šablon je sačuvan');
            redirect_to(ADMIN.'cenovnik');
        }else{
            $session->message('Postoji greška. Šablon nije sačuvan');
            redirect_to(ADMIN.'cenovnik');
        }