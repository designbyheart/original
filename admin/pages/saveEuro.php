<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/14/12
 * Time: 8:09 PM
 * To change this template use File | Settings | File Templates.
 */

if (isset($_POST)) {
    date_default_timezone_set('Europe/Belgrade');
    require_once('../../framework/lib/setup.php');
    $euro = new Euro();
    $euro->amount = $_POST['amount'];
    $euro->userID = $session->user_id;
    $euro->date = time() - (7 * 60);

    if ($euro->save()) {
        redirect_to(ADMIN . 'euro');
    }
}