<?php
require_once('../../framework/lib/setup.php');

if (isset($_GET['id']) && $_GET['id'] != '') {
    $news = GeneralPage::find_by_id($_GET['id']);

    if ($news->delete()) {
        $session->message('Strana je izbrisana');
        $_SESSION['mType'] = 2;
        redirect_to(ADMIN . 'opste_strane');

    } else {
        $session->message('Postoji problem. Strana nije izbrisana');
        $_SESSION['mType'] = 4;
        redirect_to(ADMIN . 'opste_strane');
    }
}
if (isset($_GET['generalPage'])) {
    $pIDs = array_filter(explode(',', $_GET['generalPage']));
    foreach ($pIDs as $pID) {
        $page = GeneralPage::find_by_id($pID);
        $page->delete();
    }
    $session->message('Strane su izbrisane');
    $_SESSION['mType'] = 2;
    redirect_to(ADMIN . 'opste_strane');
}

?>