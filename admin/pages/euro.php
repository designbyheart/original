<?php
if ($user->role != 4) {
    require_once(ADMIN_ROOT . 'doc/inc/side.php');
}?>

<!-- Content -->
<div class="content">
    <div class="title"><h5>Istorija izmena kursa Dinar - Euro</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $euros = Euro::find_all();
//            echo date('h:m:s', time();
    ?>
    <!-- Dynamic table -->
    <div class="table">
        <div class="head"><h5 class="iFrames"> Promena valute</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="staticTable display">
            <thead>
            <tr>
                <th width="20%" class="">Datum</th>
                <th>Vrednost</th>
                <th width="20%">Prethodna vrednost</th>
                <th width="30%">Izmenio</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($euros as $e) { ?>
            <tr class="gradeA" <?=$e->id?>>
                <td class="center">
                    <strong style="font-size: 1.2em;"><?=date("d.M.", $e->date)?></strong>
                    <?=date("H:m:s", $e->date)?></td>
                <td class="center">
                    <strong style="font-size:1.5em"><?=$e->amount?></strong>
                </td>
                <td class="center"> <?=Euro::previous($e->id)?> </td>
                <td class="center"> <?php
                    $ch = Administrator::find_by_id($e->userID);
                    if ($ch) echo $ch->full_name();
                    ?> </td>


            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>

</div>
<div class="fix"></div>
</div>