<?php
require_once('../../framework/lib/setup.php');
if (!isset($_POST['pageID']) || $_POST['pageID'] == '') {
    $_POST['pageID'] = 'novi-proizvod';
}

if (isset($_POST['charType']) && $_POST['charType'] == 'category') {
    if (!isset($_POST['id'])) {
        $ch = new Characteristics();
        //Array ( [charCategory] => 1 [name_sr] => asdfasdf [name_en] => adfasdf [pageType] => product [pageID] => 5 )
        $ch->characteristics_name_en = $_POST['name_en'];
        $ch->characteristics_name_sr = $_POST['name_sr'];
        $ch->category_id = $_POST['charCategory'];
        if ($ch->save()) {
            $_SESSION['mType'] = 2;
            $session->message("Karakteristika je sačuvana");
            if ($_POST['pageID'] == 'settings') {
                redirect_to(ADMIN . 'podesavanja');
            }
            redirect_to(ADMIN . 'proizvod/' . $_POST['pageID']);
        } else {
            $_SESSION['mType'] = 4;
            $session->message("Postoji problem. Karakteristika nije sačuvana");
            if ($_POST['pageID'] == 'settings') {
                redirect_to(ADMIN . 'podesavanja');
            }
            redirect_to(ADMIN . 'proizvod/' . $_POST['pageID']);
        }
    }
}
if (isset($_POST['pageType']) && $_POST['pageType'] == 'charCategory') {
    $char = new CharacteristicsCategory();

    $char->category_name_en = $_POST['name_en'];
    $char->category_name_sr = $_POST['name_sr'];
    if ($char->save()) {
        $_SESSION['mType'] = 2;
        $session->message("Kategorija je sačuvana");
        if ($_POST['pageID'] == 'settings') {
            redirect_to(ADMIN . 'podesavanja');
        }
        redirect_to(ADMIN . 'proizvod/' . $_POST['pageID']);
    } else {
        $_SESSION['mType'] = 4;
        $session->message("Postoji problem. Kategorija nije sačuvana");
        if ($_POST['pageID'] == 'settings') {
            redirect_to(ADMIN . 'podesavanja');
        }
        redirect_to(ADMIN . 'proizvod/' . $_POST['pageID']);
    }
}
