<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $admin = new Administrator();
}else{
    $admin = Administrator::find_by_id($_GET['id']);
}
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Novi administrator<?
            }else{
                ?>Izmena administratora<?
            }
          ?>
    	</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<form class="mainForm" action="<?=ADMIN?>saveAdministrator" method="post" enctype="multipart/form-data">
        <fieldset style="margin-top: 10px">
        <legend> Administrator <?=ucfirst($admin->first_name)?> <?=ucfirst($admin->last_name)?> </legend>
        <br>
        <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
        <div class="widget" style="margin-top: 0;">       
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$admin->id?>"/>
              		
                    	<div class="rowElem noborder">
                    		<label>Ime:</label>
                    		<div class="formRight">
                    			<input type="text" name="first_name" value="<?=$admin->first_name?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>    
                    	<div class="rowElem noborder">
                    		<label>Prezime:</label>
                    		<div class="formRight">
                    			<input type="text" name="last_name" value="<?=$admin->last_name?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>   
                    	<div class="rowElem noborder">
                    		<label>E-mail:</label>
                    		<div class="formRight">
                    			<input type="text" name="e_mail" value="<?=$admin->e_mail?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>   
                    	<div class="rowElem noborder">
                    		<label>Username:</label>
                    		<div class="formRight">
                    			<input type="text" name="username" value="<?=$admin->username?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>   
                    	<div class="rowElem noborder">
                    		<label>Password:</label>
                    		<div class="formRight">
                    			<input type="password" name="password" />
                    		</div>
                    		<div class="fix"></div>
                    	</div>                       	                    	                
	                    <div class="rowElem noborder">
                    		<label>Telefon:</label>
                    		<div class="formRight">
                    			<input type="text" name="tel" value="<?=$admin->tel?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>        
  						
                    
               </div>	
               <div class="fix"></div>		 
         </div>

		 </fieldset>
		 <a href="<?=ADMIN?>administratori"><input type="button" value="Otkazi" class="greyishBtn submitForm" /></a>
		 <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm" />
		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


