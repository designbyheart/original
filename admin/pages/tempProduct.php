<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');

if ($_GET['id'] == 0) {
    $product = new TempProduct();
} else {
    $product = TempProduct::find_by_id($_GET['id']);
}
if ($product->plD == "") {
    $product->plD = rand_uniqid(time());
}
$ch_sr = Characteristics::find_by_product($product->id, 'sr');
$ch_en = Characteristics::find_by_product($product->id, 'en');
if (!isset($ch_sr)) {
    $ch_sr = new Characteristics();
}
if (!isset($ch_en)) {
    $ch_en = new Characteristics();
}
?>


<div class="newLink" style="position:absolute">
    <h5>Novi link</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveSubmenu" method="post">
        <label>
            Srpski naziv
            <input type="text" name="name_sr">
        </label>
        <label>
            Engleski naziv
            <input type="text" name="name_en">
        </label>
        <label>Link
            <input type="text" name="linkSr">
        </label>

        <input type="hidden" name="pageType" value="product">
        <input type="hidden" name="pageID" value="<?=$_GET['id']?>">
        <button type="submit" class="greyishBtn">Sačuvaj</button>
    </form>
</div>


<!-- Content -->
<div class="content" xmlns="http://www.w3.org/1999/html">
<div class="title">
    <h5><?php if ($_GET['id'] == 0) { ?>Novi proizvod<?
    } else {
        ?>Izmena proizvoda<? } ?>
    </h5>
</div>

<!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

<form class="mainForm" action="<?=ADMIN?>saveProduct" method="post" enctype="multipart/form-data">
<!-- Tabs -->
<fieldset>
<?php if ($session->message() != '') {
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
<div class="widget" style="overflow: visible;">
<ul class="tabs">
    <li><a href="#tab1">Srpski</a></li>
    <li><a href="#tab2">Engleski</a></li>
    <li><a href="#tab5">Cenovnik</a></li>
    <li><a href="#tab6">Karakteristike</a></li>
    <li><a href="#tab7">Povezani proizvodi</a></li>
    <li><a href="#tab8">Slični proizvodi</a></li>
    <li><a href="#tab3">Galerija</a></li>
    <li><a href="#tab9">Podmeni</a></li>
    <li><a href="#tab4">Seo</a></li>
</ul>

<div class="tab_container"
     style="background:#f9f9f9; border:solid 1px #cfcfcf; border-top: none;position: relative;left:-1px; width: 99.975%; overflow: visible;">
<input type="hidden" name="id" class="productID" value="<?=$product->id?>"/>

<div id="tab1" class="tab_content">

<div class="rowElem noborder">
    <input type="text" name="isTempProduct" value="true">
    <label>Naziv proizvoda:</label>

    <div class="formRight">
        <input type="text" name="name_sr" value="<?=$product->name_sr?>"/>
    </div>
    <div class="fix"></div>
</div>

<div class="rowElem" style="float: left; clear: none; width:100%;">
    <label>Kategorija proizvoda:</label>

    <div class="formRight">
        <select name="category">
            <option value="0">Izaberi kategoriju</option>
            <?php
            $cats = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = 0 order by ordering ASC ");
            foreach ($cats as $c) {
                ?>
                <option value="<?=$c->id?>" <?php if ($c->id == $product->category) {
                    echo "selected";
                } ?>>&nbsp;&nbsp;<?=$c->name_sr?>[Glavna]
                </option><?
                $subC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$c->id} order by ordering ASC");
                foreach ($subC as $subC) {
                    ?>
                    <option value="<?=$subC->id?>" <?php if ($subC->id == $product->category) {
                        echo "selected";
                    } ?>>&nbsp;&nbsp;&nbsp;&nbsp;<?=$subC->name_sr?></option><?

                    $subSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subC->id} order by ordering ASC");
                    foreach ($subSubC as $subSubC) {
                        ?>
                        <option value="<?=$subSubC->id?>" <?php if ($subSubC->id == $product->category) {
                            echo "selected";
                        } ?>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubC->name_sr?></option><?

                        $subSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subSubC->id} order by ordering ASC");
                        foreach ($subSubSubC as $subSubSubC) {
                            ?>
                            <option value="<?=$subSubSubC->id?>" <?php if ($subSubSubC->id == $product->category) {
                                echo "selected";
                            } ?>>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubSubC->name_sr?></option><?

                            $subSubSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subSubSubC->id} order by ordering ASC");
                            foreach ($subSubSubSubC as $subSubSubSubC) {
                                ?>
                                <option
                                        value="<?=$subSubSubSubC->id?>" <?php if ($subSubSubSubC->id == $product->category) {
                                    echo "selected";
                                } ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubSubSubC->name_sr?></option><?
                                $subSubSubSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subSubSubSubC->id} order by ordering ASC");
                                foreach ($subSubSubSubSubC as $subSubSubSubSubC) {
                                    ?>
                                    <option
                                            value="<?=$subSubSubSubSubC->id?>" <?php if ($subSubSubSubSubC->id == $product->category) {
                                        echo "selected";
                                    } ?>>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubSubSubSubC->name_sr?></option><?
                                }
                            }
                        }
                    }
                }
            }?>
        </select>

    </div>
    <div class="fix"></div>
</div>
<div class="rowElem">
    <label>Izaberite državu:</label>

    <div class="formRight" style="float:right;width:75%;">
        <?php $countries = Country::find_active();
        ?>
        <select name="country">
            <option value="0">Izaberi...</option>
            <?php
            foreach ($countries as $cn) {
                ?>
                <option value="<?=$cn->id?>" <?php if ($cn->id == $product->country) {
                    echo "selected";
                } ?>><?=$cn->name?></option>
                <?php } ?>

            }
            ?>

        </select>
    </div>
    <div class="fix"></div>
</div>
<div class="rowElem " style="float: left; clear: none;width:100%;">
    <div class="formRight" style="float:left;width:45%;">
        <label style="padding-top:15px">Interni kataloški broj:</label>
        <input type="text" name="plD" value="<?=$product->plD?>"
               style="text-align: center; height:40px; font-size:1.8em;"/>
    </div>
    <div class="formRight" style="float:left;width:45%;">
        <label style="padding-top:15px">Osnovni kataloški broj proizvođača:</label>
        <input type="text" name="mainCatID" value="<?=$product->mainCatID?>"
               style="text-align: center; height:40px; font-size:1.8em;"/>
    </div>
    <div class="fix"></div>
</div>
<div class="rowElem " style="float: left; clear: left;width:100%;position:relative;left:-22px;">
    <div class="formRight" style="float:left;width:45%;">
        <label style="padding-top:15px">Zamenski kataloški brojevi:</label>
        <textarea name="secondIDs"
                  style="height:40px; text-align: center;font-size:1.2em;"><?=$product->secondIDs?></textarea>
        <span style="padding:.5em; font-size:.95em;color:#666; display:block;">Za unos više brojeva koristite , <br>(primer: serijskiBroj1, serijskiBroj2,serijskiBroj3)</span>
    </div>
    <div class="formRight" style="float:left;width:45%;">
        <label style="padding-top:15px">Šifra dobavljača:</label>
        <textarea name="distribID"
                  style="height:40px; text-align: center;font-size:1.2em;"><?=$product->distribID?></textarea>
        <span style="padding:.5em; font-size:.95em;color:#666; display:block;">Za unos više brojeva koristite , <br>(primer: serijskiBroj1, serijskiBroj2,serijskiBroj3)</span>
    </div>
    <div class="fix"></div>
</div>

<div class="rowElem">
    <label>Proizvodjač:</label>

    <div class="formRight">
        <select name="manufacturer">
            <option value="0">Izaberi proizvođača</option>
            <?php
            $manufacturers = Manufacturer::find_all();
            foreach ($manufacturers as $manufacturer) : ?>
                <option value="<?=$manufacturer->id?>"
                    <?php if ($product->id > 0) {
                    // manufacturer_p je proizvodjac datog proizvoda
                    $manufacturer_p = Manufacturer::find_by_id($product->manufacturer);
                    if (isset($manufacturer_p->id) && $manufacturer->id == $manufacturer_p->id) {
                        echo 'selected';
                    }
                }?>
                        > <?=$manufacturer->name_sr?></option>
                <?php endforeach; ?>
        </select>
    </div>
    <div class="fix"></div>
</div>

<div class="rowElem">
    <label>Aktivan:</label>

    <div class="formRight">
        <input type="checkbox" name="active"
            <?php if ($product->active == 1) {
            echo 'checked';
        } ?>
                />
    </div>
    <div class="fix"></div>
</div>

<div class="rowElem">
    <label>Status:</label>

    <div class="formRight">
        <input type="radio" name="status" value="0" <?php if ($product->status == 0) {
            echo 'checked';
        }?>/><label>Na stanju</label>
        <input type="radio" name="status" value="1" <?php if ($product->status == 1) {
            echo 'checked';
        }?>/><label>Nema ga na stanju</label>
        <input type="radio" name="status" value="2" <?php if ($product->status == 2) {
            echo 'checked';
        }?>/><label>Nije u prodaji</label>
    </div>
    <div class="fix"></div>
</div>
<div class="rowElem">
    <div class="formRight" style="width: 100%">
        <div class="widget" style="margin-top: 5px">
            <div class="head"><h5 class="iPencil">Opis proizvoda: </h5></div>
            <textarea class="wysiwyg" name="desc_sr" rows="5"><?=$product->desc_sr?></textarea>
        </div>
    </div>
</div>
<div class="rowElem noteLinks">
    <a href="#admin" class="btnIconLeft mr10 padding10">Admin</a>
    <a href="#comercial" class="btnIconLeft mr10 padding10">Komercijalista</a>
    <?php $prGroups = PriceGroup::find_main();
    foreach ($prGroups as $pG) {
        ?>
        <a href="#<?=$pG->id?>" class="btnIconLeft mr10 padding10"><?=$pG->name?></a>
        <?php
    }
    ?>

</div>
<div class="rowElem">
    <div class="formRight notesItems" style="width: 100%">
        <div class="widget" style="margin-top: 5px" id="admin">
            <div class="head"><h5 class="iPencil">Napomene - Admin: </h5></div>
            <textarea name="note[admin]" rows="5"
                      style="width:98.5%;height:150px;"><?=Note::getNoteValue($_GET['id'], 'admin')?></textarea>
        </div>
        <div class="widget" id="comercial" style="margin-top: 5px; display:none">
            <div class="head"><h5 class="iPencil">Napomene - Komercijalista: </h5></div>
            <textarea name="note[comercial]" rows="5"
                      style="width:98.5%;height:150px;"><?=Note::getNoteValue($_GET['id'], 'comercial')?></textarea>
        </div>
        <?php  foreach ($prGroups as $pG) { ?>
        <div class="widget" id="<?=$pG->id?>" style="margin-top: 5px;display:none">
            <div class="head"><h5 class="iPencil">Napomene - <?=$pG->name?>: </h5></div>
            <textarea name="note[<?=$pG->name?>]" rows="5"
                      style="width:98.5%;height:150px;"><?=Note::getNoteValue($_GET['id'], $pG->name)?></textarea>
        </div>
        <?php } ?>

    </div>
</div>

</div>
<div id="tab2" class="tab_content">

    <div class="rowElem noborder">
        <label>Naziv proizvoda:</label>

        <div class="formRight">
            <input type="text" name="name_en" value="<?=$product->name_en?>"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Opis proizvoda: </h5></div>
                <textarea class="wysiwyg" name="desc_en" rows="5" cols="">
                    <?=$product->desc_en?>
                </textarea></div>
        </div>
        <div class="fix"></div>
    </div>
</div>
<div id="tab3" class="tab_content">
    <fieldset>
        <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
        <input type="hidden" name="current" value="1" class="current"/>
        <input id="file_upload" class="fileInput" type="file" name="fileUpload[]"/>

        <div class="fields"></div>
        <div style="float:left; margin:1.5em;width:300px;clear:left;">
            <label for="url">Link fotografije</label>
            <input type="text" class="textField" name="url" id="url" style="padding:.5em; text-align: left;"
                   value="<?=$product->url?>">
        </div>
        <br/>
    </fieldset>
    <fieldset id="galleryImages">
        <?php
        //$_GET['page'] = $event->event_type;
        require_once(ROOT_DIR . 'admin/pages/galleryList.php');
        ?>
    </fieldset>
</div>
<div id="tab4" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv proizvoda(srpski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_sr" value="<?=$product->seo_title_sr?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem noborder">
        <label>Naziv proizvoda(engleski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_en" value="<?=$product->seo_title_en?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(srpski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_sr"><?=$product->seo_desc_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(engleski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_en"><?=$product->seo_desc_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(srpski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_sr"><?=$product->seo_keywords_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(engleski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_en"><?=$product->seo_keywords_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
</div>

<!-- Content of tab9 -->
<div id="tab9" class="tab_content">
    <p>Napomena: Nije neophodno dodavati linkove za drivere, brošure, tiketing ili dodatnu opremu...</p>

    <div class="table" style="margin-top:1em">

        <div class="head"><h5>Lista linkova u podmeniju - <a href="#" class="addNewLink">Dodaj novi link</a></h5></div>
        <table class="tableStatic" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td width="2.5%">Pozicija</td>
                <td width="20%">Tekst linka - spski</td>
                <td width="20%">Tekst linka - engleski</td>
                <td width="20%">Adresa - url</td>

            </tr>
            </thead>
            <tbody>
            <?php
            $subMenu = SubMenu::find_for_page($product->id, 'product');
            if ($subMenu && count($subMenu) > 0) {
                foreach ($subMenu as $m): ?>
                <tr>
                    <td><input type="text" name="sub_ordering[<?=$m->id?>]" value="<?=$m->ordering?>" class="centerAl"
                               style="width: 95%;left:10px">
                        <input type="hidden" name="submenuID[<?=$m->id?>]" value="<?=$m->id?>">
                    </td>
                    <td><input type="text" name="sub_name_sr[<?=$m->id?>]" value="<?=$m->name_sr?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                    <td><input type="text" name="sub_name_en[<?=$m->id?>]" value="<?=$m->name_en?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                    <td><input type="text" name="sub_linkSr[<?=$m->id?>]" value="<?=$m->linkSr?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                </tr>
                    <?php endforeach;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<!-- End - tab9 -->
<div id="tab5" class="tab_content">

    <div class="rowElem noborder">
        <label>Stopa PDV-a:</label>

        <div class="formRight">
            <input type="text" name="pdv" value="<?=$product->pdv?>"/>
        </div>
        <div class="fix"></div>
    </div>
    <div class="table">
        <div class="head"><h5>Cene i popusti/provizije</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
            <thead>
            <tr>
                <td>Tip popusta/provizije</td>
                <td>Tip izmene</td>
                <td width="15%">Računati od</td>
                <td width="18%">Procenat</td>
                <td width="21%">Iznos</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Osnovna cena proizvoda</td>
                <td></td>
                <td>&nbsp;</td>
                <td align="center" class="calcTypeLbls">
                </td>
                <td align="center"><input type="text" name="price" class="webStatsLink mainPrice"
                                          style="width:93%; font-size: 15px;text-align:center"
                                          value="<?=$product->price?>"></td>
            </tr>
            <?php $mGroups = PriceGroup::find_main();
//                $prGroups = PriceGroup::find_by_product($product->id);
            foreach ($mGroups as $mGroup):
//                $group = PriceGroup::find_by_product($group, $product->id);
                $g = PriceGroup::find_is_exists($mGroup->id, $product->id);
                ?>
            <tr>
                <td><?=$mGroup->name?></td>
                <td><p>Izaberite vrstu kalkulacije</p>
                    <span class="0" style="float:left">
                        <input class="calcType" type="radio" id="type<?=$mGroup->id?>"  name="calcType[<?=$mGroup->id?>]" checked
                               value="0">
                        <span style="position: relative;margin-left:2px;margin-right:3px;top:3px">Fiksno</span>
                    </span>
                    <span class="1" style="float:left">
                        <input class="calcType" id="type<?=$mGroup->id?>"  type="radio" name="calcType[<?=$mGroup->id?>]" <?php if ($g->calcType == 2) {
                            echo 'checked';
                        } ?> value="2">
                        <span style="position: relative;margin-left:2px;margin-right:3px;top:3px">Dodaj</span>
                    </span>
                    <span class="1" style="float:left">
                        <input class="calcType" id="type<?=$mGroup->id?>" type="radio"  name="calcType[<?=$mGroup->id?>]" <?php if ($g->calcType == 1) {
                            echo 'checked';
                        } ?> value="1">
                        <span style="position: relative;margin-left:2px;margin-right:3px; top:3px">Oduzmi</span>
                    </span></td>
                <?php

                $p = new Price();
                if ($g) {
                    $p = Price::find_by_product_and_group($product->id, $g->id);
                }
                ?>
                <td>
                    <select name="" id="g" class="parrentPrice<?=$mGroup->id?>">
                        <option value="0">Proizvod</option>
                        <?php foreach ($mGroups as $m):
                            if ($m->name != $mGroup->name) {  ?>
                                <option value="<?=$m->id?>" <?php if ($m->parent == $mGroup->id) ?>><?= $m->name?></option>
                        <?php } endforeach; ?>
                    </select>
                </td>
                <td align="center"><input type="text" name="group[<?=$mGroup->id?>]" value="<?php if ($g) {
                    echo $g->percentage;
                }?>" class="groupPercent" id="group<?=$mGroup->id?>" style="text-align: center; width:93px;"></td>

                <td><input type="text" name="prices[<?=$mGroup->id?>]" class="webStatsLink productGroupPrice"
                           value="<?php if ($p) {
                               echo $p->amount;
                           }?>" id="price<?=$mGroup->id?>" style="width:93%; text-align: center; font-size:13px;"></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="table">
        <div class="head"><h5>Provizija za državu</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic dynamicTableData">
            <thead>
            <tr>
                <td>Država</td>
                <td width="15%">Procenat</td>

                <td width="21%">Iznos</td>
            </tr>
            </thead>
            <tbody>
            <?php  foreach ($countries as $cN): /*
                $cTax = CountryPrice::find_byProduct_and_country($product->id, $cN->id);
                ?>
            <tr>
                <td><?=$cN->name?></td>
                <td><input type="text" name="" value=""
                           style="width:95%; text-align:right; position:relative; font-size:1.3em; left:.7em;"></td>
                <td><input type="" name="" value=""
                           style="width:95%;text-align:center; font-size:1.3em"></td>
            </tr>
                <?php */
            endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
<div id="tab6" class="tab_content">
    <p><span style="float:left; margin-right:1em;">Atribut set:</span>
        <select name="attributeSet" class="attrSet" id="attrSet">
            <option value="0">Izaberite attribute set</option>
            <?php $attrS = AttributeSet::find_all();
            if ($attrS && count($attrS) > 0) {
                foreach ($attrS as $aS):?>
                    <option value="<?=$aS->id?>" <?php if ($product->attributeSet == $aS->id) {
                        echo "selected";
                    } ?>><?=$aS->name?></option>
                    <?php endforeach;
            } ?>
        </select>
        <a href="#" class="attrSet" style="margin-left:1em; padding-top: .2em">update atributa</a>
    </p>
    <a href="<?=ADMIN?>atribute-set" class="btnIconLeft mr10" style="float:right;padding:.2em 1.5em;">Edituj grupe
        atributa</a>

    <div class="table" style="border:none;">
        <h3>Osobine proizvoda</h3>
    </div>
    <div class="table productDetailsTable">
        <?php require_once(ROOT_DIR . 'admin/pages/productAttributes.php'); ?>
    </div>
    <div class="table">
        <div class="head"><h5>Karakteristike proizvoda</h5></div>
        <input type="hidden" name="characteristicsID[sr]" value="<?=$ch_sr->id?>">
        <input type="hidden" name="characteristicsID[en]" value="<?=$ch_en->id?>">

        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic mainMenu">
            <thead>
            <tr>
                <td style="width:24%">Tip karakteristike</td>
                <td width="38%">Srpski</td>
                <td width="38%">Engleski</td>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($ch_sr as $key => $value):
                //listing all characteristics
                if ($key != 'id' && $key != 'lang' && $key != 'productID' && $key != 'obligatory' && $key != 'similar') {
                    ?>
                <tr>
                    <td align="left"><?=translateField($key)?></td>
                    <td><input type="text" name="<?=$key?>[sr]" value="<?=$value?>"
                               style="width:95%;text-align:center; font-size:1.3em"></td>
                    <td><input type="text" name="<?=$key?>[en]" value="<?=$ch_en->$key?>"
                               style="width:95%;text-align:center; font-size:1.3em"></td>
                </tr>
                    <?php
                }
            endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<div id="tab7" class="tab_content">
    <?php $prS = Product::find_similar($product->id); ?>

    <div class="table" style="width:100%;margin:0">
        <div class="head"><h5 class="iFrames">Izaberi proizvod</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedProducts" id="example"
               style="width:100%!important">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="20%">Ime proizvoda</td>
                <td width="20%">Kategorija</td>
                <td width="20%">Serijski broj</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($prS as $ps): ?>
            <tr>
                <td align="center">
                    <a href="<?=$ps->id?>" title="" class="btnIconLeft mr10 chooseProduct"
                       style="margin:0 0 -5px 0;padding:0 15px;"><span style="padding:2px 8px;">Izaberi</span></a>
                </td>
                <td align="center" id="productName<?=$ps->id?>"><h4><?=$ps->name_sr?></h4></td>
                <td align="center" id="categoryName<?=$ps->id?>"><?=$ps->category?></td>
                <td align="center"><?=$ps->plD?></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="widget first">
        <div class="head"><h5 class="iFrames">Izabrani proizvodi</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="selectedList">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="30%">Ime proizvoda</td>
                <td width="30%">Kategorija</td>
            </tr>
            </thead>
            <tbody>
            <?php if ($product->optional != '') {
                $connectedProducts = array_filter(explode(",", $product->optional));

                foreach ($connectedProducts as $cProductID) {
                    $cProduct = Product::find_by_id($cProductID);
                    $category = Category::find_by_id($cProduct->category);
                    ?>
                <tr class="<?=$cProduct->id?>">
                    <td align="center">
                        <a href="<?=$cProduct->id?>" class="btnIconLeft mr10 removeProduct"
                           style="margin:0 0 -5px 0; padding:0 15px;">
                            <span style="padding:2px 8px;">Ukloni</span>
                        </a>
                    </td>
                    <td align="center"><h4><?=$cProduct->name_sr?></h4>
                    </td>
                    <td align="center"><strong><?php if ($category) {
                        echo $category->name_sr;
                    }?></strong></td>
                </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="fix"></div>
    <input type='hidden' name="optional" id="linkedProducts">
</div>
<div id="tab8" class="tab_content">
    <?php $prS = Product::find_similar($product->id); ?>

    <div class="table" style="width:100%;margin:0">
        <div class="head"><h5 class="iFrames">Izaberi proizvod</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedSimilarProducts" id="example2"
               style="width:100%!important">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="20%">Ime proizvoda</td>
                <td width="20%">Kategorija</td>
                <td width="20%">Serijski broj</td>
            </tr>
            </thead>
            <tbody>
            <?php
            $connectedProducts = array_filter(explode(",", $product->similar));
            foreach ($prS as $ps):
                if (!in_array($ps->id, $connectedProducts)) {
                    ?>
                <tr>
                    <td align="center">
                        <a href="<?=$ps->id?>" title="" class="btnIconLeft mr10 chooseSimilarProduct"
                           style="margin:0 0 -5px 0;padding:0 15px;"><span style="padding:2px 8px;">Izaberi</span></a>
                    </td>
                    <td align="center" id="similarProductName<?=$ps->id?>"><h4><?=$ps->name_sr?></h4></td>
                    <td align="center" id="similarCategoryName<?=$ps->id?>"><?=$ps->category?></td>
                    <td align="center"><?=$ps->plD?></td>
                </tr>
                    <?php } endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="widget first">
        <div class="head"><h5 class="iFrames">Izabrani proizvodi</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="selectedSimilarList">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="30%">Ime proizvoda</td>
                <td width="30%">Kategorija</td>
            </tr>
            </thead>
            <tbody>
            <?php if ($product->similar != '') {

                foreach ($connectedProducts as $cProductID) {
                    $cProduct = Product::find_by_id($cProductID);
                    if ($cProduct) {
                        $category = Category::find_by_id($cProduct->category);
                        ?>
                    <tr class="<?=$cProduct->id?>">
                        <td align="center">
                            <a href="<?=$cProduct->id?>" class="btnIconLeft mr10 removeSimilarProduct"
                               style="margin:0 0 -5px 0; padding:0 15px;">
                                <span style="padding:2px 8px;">Ukloni</span>
                            </a>
                        </td>
                        <td align="center"><h4><?=$cProduct->name_sr?></h4>
                        </td>
                        <td align="center"><strong><?php if ($category) {
                            echo $category->name_sr;
                        }?></strong></td>
                    </tr>
                        <?php
                    }
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="fix"></div>
    <input type='hidden' name="similarProducts" id="linkedSimilarProducts">
</div>
</div>
</div>
</fieldset>
<a href="<?=ADMIN?>proizvodi"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
<input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
</form>
</div>