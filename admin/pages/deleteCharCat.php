<?php
require_once('../../framework/lib/setup.php');

$characteristics = new CharacteristicsCategory();
if (isset($_GET['id']) && $_GET['id'] != '') {
    $characteristics->id = $_GET['id'];


    if ($characteristics->delete()) {
        $session->message('Kategorija je izbrisana');
        $_SESSION['mType'] = 2;
        redirect_to(ADMIN . 'podesavanja');

    } else {
        $session->message('Postoji problem. Kategorija nije izbrisana');
        $_SESSION['mType'] = 4;
        redirect_to(ADMIN . 'podesavanja');
    }
}
?>