<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Proizvođači</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<?php 
			$manufacturers = Manufacturer::find_by_sql("SELECT * FROM manufacturer order by active ASC, name_sr ASC");
		?>
		
        <?php if($session->message()!=''){
        	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
		         
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>novi-proizvodjac">Dodaj proizvođača</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Naziv</th>
                        <th>Tip</th>
                        <th>Url</th>
                        <th>Active</th>
                        <th>Banner</th>
                        <th width="110px">banner - naslovna</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($manufacturers as $manufacturer) {?>
                    <tr class="gradeA" >
                        <td class="center"> <a href="<?=ADMIN?>proizvodjac/<?=$manufacturer->id?>"> 
                        	<strong><?=$manufacturer->name_sr?><strong></a></td>
                    	<td class="center"> 
						<?php
							switch($manufacturer->type){
								case 0: echo "proizvodjac"; 
								break;
								case 1: echo "partner";		
								break;
								case 2: echo "referenca";	
								break;
							}
						?>
						</td>
                    	<td class="center"> <?=$manufacturer->url ?></td>
                    	<td class="center"> 
                    	      <input type="checkbox" name="active" 
					         	  <?php if($manufacturer->active == 1){
					               		 echo 'checked';
					              }
					              ?>
				         	  />	
                    	</td>
                        <td class="center">
                            <input type="checkbox" name="active"
                                <?php if($manufacturer->banner == 1){ echo 'checked';  } ?>/>
                        </td>
                        <td class="center">
                            <input type="checkbox" name="active"
                                <?php if($manufacturer->banner_home > 0){ echo 'checked';  } ?>/>
                        </td>
                    	<td class="center"> 
                    		<a href="<?=ADMIN?>proizvodjac/<?=$manufacturer->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteManufacturer.php?id=<?=$manufacturer->id?>" class="delItem"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
    </div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
     	<span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


