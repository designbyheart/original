<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $order = Order::find_by_id($_GET['id']);
    
if($order->delete()){
  $session->message('Porudžbina je izbrisana');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'porudzbine');
  
}else{
  $session->message('Postoji problem. Porudžbina nije izbrisana');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'porudzbine');
}
}

?>