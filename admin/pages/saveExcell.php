<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/8/12
 * Time: 2:58 AM
 * To change this template use File | Settings | File Templates.
 */
ob_start('ob_gzhandler');
require_once "../../framework/lib/setup.php";
$groups = '';
/* print_r($_POST); */
if (isset($_GET['price'])) {
    $filename = 'cenovnik';
    $fullFileType = explode('__', time());
    $productIDs = false;
    if (isset($_GET['products'])) {
        $productIDs = array_filter(explode(',', $_GET['products']));
    }
    $class = "Product";
    $data = array();
    $data[] = "Ime";
    $data[] = "Interni kat. broj";
    $data[] = "Osnovni kat. broj";
    if(isset($_POST['secondIDs'])){
        $data[] = "Dodatni ser. brojevi";        
    }
    $data[] = "PDV";
    if(isset($_POST['characteristics'])){
        
    }
    if(isset($_POST['price'])){
    $data[] = "Osnovna cena";
    //look for created price groups
    $groups = PriceGroup::find_main();
    $groupNames = array();
    foreach ($groups as $g):
        $data[] = "$g->name - Roditelj";
        $data[] = "$g->name - Procenat";
        $data[] = "$g->name - Cena";
        $data[] = "$g->name - Tip";
        $groupNames[] = $g->name;
    endforeach;
    }
    if(isset($_POST['attributeSet']) && isset($_POST['products']) && $_POST['products']!=""){
        $prIDs =implode(',', array_filter(explode(',', $_POST['products'])));
        $products = Product::find_by_sql("select * from product where id in (".$prIDs.")");
        $attributeSetsIDs = array();
        foreach($products as $p){
            if($p->attributeSet != '' && $p->attributeSet != '0'){
                $attributeSetsIDs[] = $p->attributeSet;
            }
        }

        foreach(array_unique($attributeSetsIDs) as $attrS){
            $aS = AttributeSet::find_by_id($attrS);
            $detailsIDs = array_filter(explode(',', $aS->details));
            $data[] = 'Atribut set';
            foreach($detailsIDs as $dID){
                $data[] =ProductDetailsSettings::getSettingName($dID);
            }
        }
    }
    $valuesArray = array();

    if ($productIDs) {
        foreach ($productIDs as $pID) {
            $p = Product::find_by_id($pID);
            $values = array();
            $values[] = $p->name_sr;
            $values[] = $p->plD;
            $values[] = $p->secondIDs;
            $values[] = $p->price;
            $values[] = $p->pdv;

            foreach ($groups as $g):
                $grNames = $groupNames;
                $toDelete = $g->name;
                $filteredArray = array_diff($groupNames, array($toDelete));
                $values[] = implode(" | ", $filteredArray);
                $values[] = "Unesite procenat";
                $values[] = "Unesite cenu";
                $values[] = "Fiksno | Oduzimanje | Sabiranje";
            endforeach;
            $valuesArray[] = $values;
        }
    }
}

function cleanData(&$str)
{
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if (strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}

$header = '';
foreach ($data as $key => $value) {
    $header .= $value . "\t";
}

print "$header\n";
if(isset($values)){
    print "$values";
}
# filename for download
$filename .= ".xls";

header("Content-Disposition: attachment; filename=" . $filename);
header("Content-Type: application/vnd.ms-excel");
//  header("Content-Disposition: attachment;
//filename=".$filename);
//print "$header\n";
foreach ($valuesArray as $v) {
    $val = '';
    foreach ($v as $key => $value) {
        $val .= $value . "\t";
    }
    print "$val\n";
}

//array_walk($row, 'cleanData');
//echo implode("\t", array_values($row)) . "\n";
// }
exit;

?>