<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
                      //[id] => 1 [name_sr] => aloha.. test1 [type] => 1 [version] => 1.2.3. [active] => on [desc_sr] => [name_en] => [desc_en] => [similarProducts] => [submit] => SaÄuvaj
if(isset($_POST)){
    //echo($_POST['id']);
    if(isset($_POST['id']) && $_POST['id']){
        $driver = Driver::find_by_id($_POST['id']);
    }
    if(!isset($driver)){
        $driver = new Driver();
    }
    foreach($driver as $key=>$value){
        if($key !='id' && isset($_POST[$key])){
            $driver->$key = $_POST[$key];
        }
    }
    $driver->products = $_POST['similarProducts'];

    $file = $_FILES['uploadFile'];
    $target_path = DRIVER_F;


    if(isset($_POST['active'])){
        $driver->active = 1;
    }else{
        $driver->active = 0;
    }

    $target_path = $target_path . cleanFileName(basename( $file['name']));
    if(move_uploaded_file($file['tmp_name'], $target_path)) {
        $driver->file = cleanFileName(basename( $file['name']));
    }
    if($driver && $driver->save()){
        $_SESSION['mType'] = 2;
        $session->message('Driver je sačuvan');
        redirect_to(ADMIN.'driveri');
    }else{
        $_SESSION['mType'] = 4;
        $session->message('Postoji problem. Driver nije sačuvan');
        redirect_to(ADMIN.'driver/'.$driver->id);
    }
}