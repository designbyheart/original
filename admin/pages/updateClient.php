<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/25/12
 * Time: 9:42 AM
 * To change this template use File | Settings | File Templates.
 */
require_once "../../framework/lib/setup.php";


if (isset($_GET['type'])) {
    $t = $_GET['type'];
    $id = $_GET['id'];
    if ($t == 'rejectClient') {
        $c = Customer::find_by_id($id);
        if ($c) {
            $c->type = 4;
            $c->save();
        }
    }
    if ($t == 'approve') {
        $c = Customer::find_by_id($id);
        if ($c) {
            $c->type = 2;
            $c->save();
        }

    }
}
redirect_to(SITE_ROOT . 'admin/klijenti');