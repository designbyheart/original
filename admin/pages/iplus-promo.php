<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>

<!-- Content -->
<div class="content">
    <div class="title"><h5>iPlus Promocije</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $promos = iPlusPromo::find_all();

    ?>

    <?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
    <a href="<?=ADMIN?>istekle-iplus-promocije" title="" class="btnIconLeft mr10 mt10"
       style="position:relative;top:1em;"><img src="<?=ADMIN?>doc/images/icons/dark/presentation.png" alt=""
                                               class="icon"><span>Istekle iPlus promocije</span></a>
    <!-- Dynamic table -->
    <div class="table">
        <div class="head"><h5 class="iFrames"><a href="<?=ADMIN?>nova-promocija">Dodaj novu iPlus promociju</a></h5>
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Naziv</th>
                <th>Proizvodi</th>
                <th>Početak</th>

                <th>Kraj</th>
                <th>Aktivan</th>
                <th>Min. cena</th>
                <th> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($promos as $promo) { ?>
            <tr class="gradeA" <?=$promo->id?>>
                <td class="center"><a href="<?=ADMIN?>promocija/<?=$promo->id?>">
                    <strong><?=$promo->name_sr?><strong></a></td>
                <td class="center"> <?=$promo->products($promo, false)?></td>
                <td class="center"><?=intToDate($promo->dateStart)?></td>

                <td class="center"> <?=intToDate($promo->dateEnd)?></td>
                <td class="center">
                    <input type="checkbox" name="active" <?php if ($promo->active == 1) {
                        echo 'checked';
                    }?>/>
                </td>
                <td class="center">
                    <span class="webStatsLink"><?=$promo->price?></span>
                </td>
                <td class="center">
                    <a href="<?=ADMIN?>promocija/<?=$promo->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                    <a href="pages/deletePromo.php?id=<?=$promo->id?>" class="delItem"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>

