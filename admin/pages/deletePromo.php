<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $product = Promo::find_by_id($_GET['id']);
    
if($product->delete()){
  $session->message('Promocija je izbrisana');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'promocije');
  
}else{
  $session->message('Postoji problem. Promocija nije izbrisana');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'promocije');
}
}

?>