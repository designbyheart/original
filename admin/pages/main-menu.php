<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
$mainMenu = MainMenu::find_by_type('main');

$css = MainMenu::find_css('main');
?>
<!-- Content -->
<div class="content">
    <div class="title">
        <h5>Editovanje glavnog menija</h5>
    </div>
    <form class="mainForm" action="<?=ADMIN?>saveMainMenu" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
            <div class="widget">
                <ul class="tabs">
                    <li><a href="#tab1">Srpski</a></li>
                    <li><a href="#tab2">Engleski</a></li>
                    <li><a href="#tab3">CSS edit</a></li>
                </ul>

                <div class="tab_container">
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                        <div class="table">
                            <div class="head"><h5>Lista linkova u glavnom meniju</h5></div>
                            <table class="tableStatic mainMenu" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td width="7%">Pozicija</td>
                                    <td width="25%">Tekst linka</td>
                                    <td width="25%">Url</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($mainMenu) {
                                    foreach ($mainMenu as $m): ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="ids[<?=$m->id?>]" value="<?=$m->id?>">
                                            <input type="text" name="ordering[<?=$m->id?>]" value="<?=$m->ordering?>"
                                                   class="centerAl" style="width:75%!important;"></td>
                                        <td><input type="text" name="name_sr[<?=$m->id?>]" value="<?=$m->name_sr?>"
                                                   class="centerAl"></td>
                                        <td><span style="float:left;margin-right:15px;"><?=SITE_ROOT?></span>
                                            <input type="text" name="url[<?=$m->id?>]" value="<?=$m->url?>"
                                                   class="centerAl" style="width:62%!important;">
                                        </td>
                                    </tr>
                                        <?php endforeach;
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End - tab1 -->


                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">
                        <div class="table">
                            <div class="head"><h5>Lista linkova u glavnom meniju</h5></div>
                            <table class="tableStatic" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td width="2.5%">Pozicija</td>
                                    <td width="20%">Tekst linka - engleski</td>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($mainMenu) {
                                    foreach ($mainMenu as $m): ?>
                                    <tr>
                                        <td><input type="text" value="<?=$m->ordering?>" class="centerAl"
                                                   style="width: 95%;"></td>
                                        <td><input type="text" name="name_en[<?=$m->id?>]" value="<?=$m->name_en?>"
                                                   class="centerAl" style="width: 98%;"></td>

                                    </tr>
                                        <?php endforeach;
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End - tab2 -->
                    <!--tab 3-->
                    <div id="tab3" class="tab_content">
                        <div class="table">
                            <div class="head"><h5>CSS edit</h5></div>
                            <textarea name="css" id="css" cols="30" rows="10"
                                      style="width:98.5%;"><?=$css->css?></textarea>
                            <input type="hidden" name="type" id="type" value="mainMenu">
                            <input type="hidden" name="cssID" id="cssID" value="<?=$css->css?>">
                        </div>
                    </div>
                    <!--End - tab3-->
                </div>
                <div class="fix"></div>
            </div>
            <!-- End - Tabs -->
        </fieldset>
        <a href="<?=ADMIN?>opste_strane"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
        <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
    </form>

</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>
