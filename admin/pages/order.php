<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

    $order = Order::find_by_id($_GET['id']);


?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	Detalji porudžbine</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

        <!-- Tabs -->
        <fieldset>

        <p><b>Kupac</b></p><br>

        <?php
            $customer = Customer::find_by_id($order->customerID);
            ?>

        <table style="width: 300px">
            <tr>    <td>Ime:</td>       <td><?php if($customer->type == 0){echo $customer->name;}else {echo $customer->company;}?></td></tr>
            <tr>    <td>PIB:</td>       <td><?=$customer->company_PIB; ?></td></tr>
            <tr>    <td>E-mail:</td>    <td><?=$customer->email; ?></td></tr>
            <tr>    <td>Adresa:</td>    <td><?=$customer->address. " ". $customer->city. " ". $customer->state; ?></td></tr>

        </table>

        <br> <a href="<?=ADMIN?>kupac/<?=$customer->id?>"> Detaljnije </a>

            <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
            <div class="table">
                <div class="head"><h5 class="iFrames"> Naručeni proizvodi </h5></div>

            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                <tr>
                    <th>Naziv proizvoda</th>
                    <th>Serijski broj</th>
                    <th>Model</th>
                    <th>Kolicina</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $products =array_filter(explode(',', $order->products));

                foreach($products as $pID) {
                    $product = Product::find_by_id($pID);
                ?>
                <tr class="gradeA" >
                    <td class="center"><a href="<?=ADMIN?>proizvod/<?=$product->id?>"><?=$product->name_sr ?></a></td>
                    <td class="center"><?=$product->plD ?> </td>
                    <td class="center"><?=$product->price?></td>
                    <td class="center"></td>
                </tr>

                <?php
                } ?>

                </tbody>
            </table>
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <a href="<?=ADMIN?>porudzbine"><input type="button" value="Otkazi" class="greyishBtn submitForm" /></a>
		 <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm" />

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


