<?php
require_once('../../framework/lib/setup.php');
$characteristics = new Characteristics();
if (isset($_GET['id']) && $_GET['id'] != '')
    $characteristics->id;
if (isset($_GET['characteristics_name_sr']) && $_GET['characteristics_name_sr'] != '')
    $characteristics->characteristics_name_sr;
if (isset($_GET['characteristics_name_en']) && $_GET['characteristics_name_en'] != '')
    $characteristics->characteristics_name_en;

if ($characteristics->update()) {
    $session->message('Kategorija je azurirana');
    $_SESSION['mType'] = 2;
    redirect_to(ADMIN . 'podesavanja');

} else {
    $session->message('Postoji problem. Kategorija nije azurirana');
    $_SESSION['mType'] = 4;
    redirect_to(ADMIN . 'podesavanja');

}
?>