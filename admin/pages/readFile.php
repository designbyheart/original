<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/27/12
 * Time: 9:43 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
$result = array_filter(readExcell($_GET['file']));
$unresolvedItems = array();

if (isset($result[0]) && count($result) > 0) {
    foreach ($result[0] as $key) {
        $fields[] = fieldValue($key);
    }
}
$itemList = array();
$itemIndex = 1;
foreach ($fields as $key) {
    $itemList[$itemIndex] = $key;
    $itemIndex++;
}
$manufacturer = $_GET['manufacturer'];
$category = $_GET['category'];
$_SESSION['category'] = $category;
$_SESSION['manufacturer'] = $manufacturer;

$i = 0;
//echo '<pre>';
$updateTime = datetime();

foreach (array_filter($result) as $item):
    if ($i > 0) {
        //main settings and setting conditions
        $isNewProduct  = false;
        $product = false;

        //check is item contains important internal ID
        if(in_array('plD', $itemList)){
            $plD = $item[array_search('plD',$itemList)];
            $product = Product::find_by_pld($plD);

        }else if(in_array('mainCatID', $itemList)){
            $mainCatID = $item[array_search('mainCatID',$itemList)];
            $product = Product::find_by_pld($mainCatID, true);
        }
        if(!$product){
            $product = TempProduct::find_by_pld($plD);
            if(!$product){
                $product = TempProduct::find_by_pld($mainCatID, true);
            }
            if(!$product){
               $product = new TempProduct();
           }

            $product->created = $updateTime;
            $isNewProduct  = true;
        }
        foreach($itemList as $key=>$value){
            if($value !='' && $item[$key]!=''){
//                echo $value.' = '.$item[$key].'<br>';
                $product->$value = $item[$key];
            }
        }
        $product->updated = $updateTime;
        $man  = false;
        if(isset($_GET['manufacturer']) && $_GET['manufacturer'] !=''){
            $product->manufacturer = $_GET['manufacturer'];
        }
        if(isset($_GET['category']) && $_GET['category']!=''){
            $product->category = $_GET['category'];
        }

//        print_r($product);
        if($isNewProduct){
            $product->save();
        }
    }
    $i++;
endforeach;
$file = ROOT_DIR.'admin/uploadedFiles'.$_GET['file'];
if(file_exists($file)){
    unlink($file);
}
$_SESSION['category'] = $_GET['category'];
$_SESSION['updateTime'] = $updateTime;
$_SESSION['manufacturer'] = $_GET['manufacturer'];

redirect_to(ADMIN.'rezultati-importa');
//echo '</pre>';