<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $driver = new Driver();
}else{
    $driver = Driver::find_by_id($_GET['id']);
}

?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Nova driver/uputsvo<?
            }else{
                ?>Izmena drivera/uputsva<?
            }
          ?>
    	</h5></div>
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
		<form class="mainForm" action="<?=ADMIN?>saveDriver" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
        <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab8">Proizvodi</a></li>
                   <li><a href="#tab3">Upload dokumenata</a></li>

              </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$driver->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naziv drivera/usluge:</label>
                            <div class="formRight">
                                <input type="text" name="name_sr" value="<?=$driver->name_sr?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem noborder">
                            <label>Verzija:</label>
                            <div class="formRight">
                                <input type="text" name="version" value="<?=$driver->version?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
	                        <label>Aktivan:</label> 
	                        <div class="formRight">
	                               <input type="checkbox" name="active" 
            						         	  <?php if($driver->active == 1){ 	 echo 'checked';   } ?>  />
	                        </div>
	                        <div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                            <div class="formRight" style="width: 100%">
                                <div class="widget" style="margin-top: 5px">
                                    <div class="head"><h5 class="iPencil">Opis drivera/uputstva: </h5></div>
                                    <textarea class="wysiwyg" name="desc_sr" rows="5"><?=$driver->desc_sr?></textarea>
                                </div>
                            </div>
                        </div>
                                    	                            
                    </div>  
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem noborder">
                    		<label>Naziv drivera/usluge:</label>
                    		<div class="formRight">
                    			<input type="text" name="name_en" value="<?=$driver->name_en?>"/>
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                            <div class="formRight" style="width: 100%">
                                <div class="widget" style="margin-top: 5px">
                                    <div class="head"><h5 class="iPencil">Opis drivera/uputstva: </h5></div>
                                    <textarea class="wysiwyg" name="desc_en" rows="5"><?=$driver->desc_en?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- End - tab2 --> 
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">
			          <fieldset>
			            <legend>Upload fotografije </legend>
			            <input type="hidden" name="" value="1" class="current" />
			              <input id="file_upload" class="fileInput" type="file" name="image0" />
			              <div class="fields"></div><br />	         
			           </fieldset>
			           <fieldset id="galleryImages">
			              <?php 
			                 $img_type = 'cat-menu';
			                 require(ROOT_DIR."admin/pages/galleryList.php");
			              ?>
			           </fieldset>
			           
			           	<fieldset>
			            <legend>Upload drivera</legend>
			            <input type="hidden" name="" value="1" class="current" />
			              <input id="file_upload" class="fileInput" type="file" name="uploadFile" />
			              <div class="fields"></div><br />
			            </fieldset>
			            <fieldset id="galleryImages">
                            <?php if($driver->file!=''){?>
                            <b>Uploadovan dokument: </b> <a target="_blank" href="<?=SITE_ROOT?>driversDownload/<?=$driver->file?>" ><?=$driver->file?></a>
                            <?php } ?>
			            </fieldset>
                    </div>
                    <!-- End - tab3 -->
                  <?php /*; ?>
                    <!-- Content of tab4 -->
                    <div id="tab4" class="tab_content">                     
                    	<div class="rowElem noborder">
                    		<label>Naziv kategorije(srpski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_sr" value="<?=$driver->seo_title_sr?>" >
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem noborder">
                    		<label>Naziv kategorije(engleski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_en" value="<?=$driver->seo_title_en?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                    		<label>Opis strane za pretraživače(srpski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_sr" ><?=$driver->seo_desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Opis strane za pretraživače(engleski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_en" ><?=$driver->seo_desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(srpski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_sr" ><?=$driver->seo_keywords_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(engleski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_en" ><?=$driver->seo_keywords_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    
                    </div>
                    <!-- End - tab4 -->  */ ?>
                    
               </div>
             <div id="tab8" class="tab_content">
                 <?php $prS = Product::find_all(); ?>

                 <div class="table" style="width:100%;margin:0">
                     <div class="head"><h5 class="iFrames">Izaberi proizvod</h5></div>
                     <table cellpadding="0" cellspacing="0" border="0"
                            class="tableStatic linkedSimilarProducts dynamicTableData" id="example2"
                            style="width:100%!important">
                         <thead>
                         <tr>
                             <td width="5%"></td>
                             <td width="20%">Ime proizvoda</td>
                             <td width="20%">Kategorija</td>
                             <td width="20%">Serijski broj</td>
                         </tr>
                         </thead>
                         <tbody>
                         <?php
                         $connectedProducts = array_filter(explode(",", $driver->products));
                         foreach ($prS as $ps):
                             if (!in_array($ps->id, $connectedProducts)) {
                                 ?>
                             <tr>
                                 <td align="center">
                                     <a href="<?=$ps->id?>" title="" class="btnIconLeft mr10 chooseSimilarProduct"
                                        style="margin:0 0 -5px 0;padding:0 15px;"><span
                                         style="padding:2px 8px;">Izaberi</span></a>
                                 </td>
                                 <td align="center" id="similarProductName<?=$ps->id?>"><h4><?=$ps->name_sr?></h4></td>
                                 <td align="center" id="similarCategoryName<?=$ps->id?>"><?=$ps->category?></td>
                                 <td align="center"><?=$ps->plD?></td>
                             </tr>
                                 <?php } endforeach; ?>
                         </tbody>
                     </table>
                 </div>
                 <div class="widget first">
                     <div class="head"><h5 class="iFrames">Izabrani proizvodi</h5></div>
                     <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic " id="selectedList">
                         <thead>
                         <tr>
                             <td width="5%"></td>
                             <td width="30%">Ime proizvoda</td>
                             <td width="30%">Kategorija</td>
                         </tr>
                         </thead>
                         <tbody>
                         <?php  if ($driver->products != '') {
                             $products = array_filter(explode(',', $driver->products));
                             foreach ($products as $cProductID) {

                                 $cProduct = Product::find_by_id($cProductID);
                                 if ($cProduct) {
                                     $category = Category::find_by_id($cProduct->category);
                                     ?>
                                 <tr class="<?=$cProduct->id?>">
                                     <td align="center">
                                         <a href="<?=$cProduct->id?>" class="btnIconLeft mr10 removeSimilarProduct"
                                            style="margin:0 0 -5px 0; padding:0 15px;">
                                             <span style="padding:2px 8px;">Ukloni</span>
                                         </a>
                                     </td>
                                     <td align="center"><h4><?=$cProduct->name_sr?></h4>
                                     </td>
                                     <td align="center"><strong><?php if ($category) {
                                         echo $category->name_sr;
                                     }?></strong></td>
                                 </tr>
                                     <?php
                                 }
                             }
                         }
                         ?>
                         </tbody>
                     </table>
                 </div>
                 <div class="fix"></div>
                 <input type='hidden' name="similarProducts" id="linkedSimilarProducts">
             </div>
             <div class="fix"></div>
        </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <a href="<?=ADMIN?>kategorije"><input type="button" value="Otkazi" class="greyishBtn submitForm" /></a>
		 <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm" />
		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


