<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
$mainMenu = MainMenu::find_all();

?>
<!-- Content -->
<div class="content">
<div class="title">
    <h5>Napredna pretraga</h5>
</div>

<?php
$products = Product::find_by_sql("SELECT *, (select category.name_sr from category where category.id = product.category) as category, (select name_sr from manufacturer where manufacturer.id = product.manufacturer) as manufacturer FROM product order by category ASC, active ASC, name_sr ASC");
?>
<div class="widget" style="margin-top:0px">
<ul class="tabs">
    <li><a href="#tab2">Proizvod osnovno</a></li>
    <li><a href="#tab3">Cene</a></li>
    <li><a href="#tab4">Karakteristike</a></li>
    <li><a href="#tab5">Osobine proizvoda</a></li>

</ul>
<div class="saveSearchTemplateWrapper">
    <h5>Sačuvaj šablon za pretragu</h5>
    <label>Unesite ime šablona</label>
    <input type="text" id="searchTempName">
    <input type="submit" class="button greyishBtn cancelTemplate" value="Otkaži">
    <input type="submit" class="button greyishBtn saveSearchTemplate" value="Sačuvaj">
</div>
<div class="saveSearchTemplateWrapper priceChooser">
    <h5>Primenite šablon cena</h5>
    <?php $priceT = PriceTemplate::find_all();?>
    <select name="" id="priceTemplates">
        <option value="0">Izaberite šablon</option>
        <?php foreach ($priceT as $pT): ?>
        <option value="<?=$pT->id?>"><?=$pT->name?></option>
        <?php endforeach; ?>

    </select>
    <input type="submit" class="button greyishBtn applyPriceTemplate" rel="<?=$_SERVER['REQUEST_URI']?>"
           value="Primeni">
    <input type="submit" class="button greyishBtn cancelTemplate" value="Otkaži">
</div>
<form action="#" class="form table" id="detailedSearch" style="margin-top:10px;border:none;; ">
<div class="tab_container" style="padding-bottom:1.5em;overflow: visible;">

<div id="tab2" class="tab_content">

    <table class="staticTable searchTable">
        <tr>
            <td>
                <label>Proizvođač:</label>
                <select name="manufacturer">
                    <option value="0">Izaberi proizvođača</option>
                    <?php
                    $manufacturers = Manufacturer::find_all();
                    foreach ($manufacturers as $manufacturer) : ?>
                        <option value="<?=$manufacturer->id?>"> <?=$manufacturer->name_sr?></option>
                        <?php endforeach; ?>
                </select>
            </td>
            <td>
                Kategorija
                <select name="category">
                    <option value="0">Izaberi kategoriju</option>
                    <?php
                    $cats = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = 0 order by ordering ASC ");
                    foreach ($cats as $c) {
                        ?>
                        <option value="<?=$c->id?>">&nbsp;&nbsp;<?=$c->name_sr?>[Glavna]
                        </option><?
                        $subC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$c->id} order by ordering ASC");
                        foreach ($subC as $subC) {
                            ?>
                            <option value="<?=$subC->id?>">
                                &nbsp;&nbsp;&nbsp;&nbsp;<?=$subC->name_sr?></option><?

                            $subSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subC->id} order by ordering ASC");
                            foreach ($subSubC as $subSubC) {
                                ?>
                                <option value="<?=$subSubC->id?>">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubC->name_sr?></option><?

                                $subSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subSubC->id} order by ordering ASC");
                                foreach ($subSubSubC as $subSubSubC) {
                                    ?>
                                    <option value="<?=$subSubSubC->id?>">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubSubC->name_sr?></option><?

                                    $subSubSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subSubSubC->id} order by ordering ASC");
                                    foreach ($subSubSubSubC as $subSubSubSubC) {
                                        ?>
                                        <option
                                                value="<?=$subSubSubSubC->id?>">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubSubSubC->name_sr?></option><?
                                        $subSubSubSubSubC = Category::find_by_sql("select id, name_sr, parent_cat, ordering from category where parent_cat = {$subSubSubSubC->id} order by ordering ASC");
                                        foreach ($subSubSubSubSubC as $subSubSubSubSubC) {
                                            ?>
                                            <option
                                                    value="<?=$subSubSubSubSubC->id?>" <?php if ($subSubSubSubSubC->id == $product->category) {
                                                echo "selected";
                                            } ?>>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$subSubSubSubSubC->name_sr?></option><?
                                        }
                                    }
                                }
                            }
                        }
                    }?>
                </select>
            </td>

            <td>Država <?php $countries = Country::find_active();
                ?>
                <select name="country">

                    <option value="0">Izaberi...</option>
                    <?php
                    foreach ($countries as $cn) {
                        ?>
                        <option value="<?=$cn->id?>"><?=$cn->name?></option>
                        <?php } ?>

                    }
                    ?>

                </select></td>
        </tr>
        <tr>
            <td><label style="float:left">Interni kat. broj</label>
                <input type="text" name="plD" style="clear:left; float: left; margin: .2em 0 0 -.3em;"></td>
            <td><label style="float:left">Osnovni kat. broj</label>
                <input type="text" value="" name="mainCatID"
                       style="clear:left; float: left; margin: .2em 0 0 -.3em;">
            </td>
            <td><label style="float:left">Osnovni kat. broj</label>
                <input type="text" name="secondIDs"
                       style="clear:left; float: left; margin: .2em 0 0 -.3em;">
            </td>
            <td><label style="float:left">Šifra dobavljača </label>
                <input type="text" name="distribID"
                       style="clear:left; float: left; margin: .2em 0 0 -.3em;">
            </td>
        </tr>
        <tr>
            <td style="position:relative;">
                <label style="width:80px; postition:relative;top: -15px; float:left;">
                    <span class="activeLbl">Aktivni </span> <input type="checkbox" name="active" value='1'>
                </label>
                <label style="width:60px;position:relative;">
                    <span class="activeLbl">Neaktivni</span>
                    <input type="checkbox" name="active" value='<0'>
                </label>
            </td>

            <td>
                <label style="width:170px;float:left">PDV <input type="text" name="pdv" id="pdv"
                                                                 style="width:40px;">
                </label>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
    <input class="selectedProducts" name="products" type="hidden">
</div>
<div id="tab3" class="tab_content">
    <label for="lowPrice" class="searchPrice">Od <input type="text" name="lowPrice" id="lowPrice"></label>
    <label for="highPrice" class="searchPrice">Do <input type="text" name="highPrice" id="highPrice"></label>

    <?php $priceGroups = PriceGroup::find_main(); ?>
    <div class="table">

        <div class="table_wrapper">
            <div class="head"><h5 class="iFrames">Filter po cenovnim grupama</h5></div>
            <table class="staticTable priceFilter" style="width: auto; margin-top:10px">

                <?php foreach ($priceGroups as $pG) { ?>
                <tr>
                    <td width="20%;" style="padding-left:1em;"><strong
                            style="position: relative; top:-12px;"><?=$pG->name?></strong></td>
                    <td style="padding:.2em"><span class="from">Procenat od </span><input type="text"
                                                                                          name="percentFrom_<?=$pG->id?>"
                                                                                          class="centerAl searchPriceInp">
                    </td>
                    <td style="padding:.2em"><span class="from">Procenat do</span><input type="text"
                                                                                         name="percentTo_<?=$pG->id?>"
                                                                                         class="centerAl searchPriceInp">
                    </td>
                    <td style="padding:.2em"><span class="from">Cena od </span><input type="text"
                                                                                      name="priceFrom_<?=$pG->id?>"
                                                                                      class="centerAl searchPriceInp">
                    </td>
                    <td style="padding:.2em"><span class="from">Cena do</span><input type="text"
                                                                                     name="priceTo_<?=$pG->id?>"
                                                                                     class="centerAl searchPriceInp">
                    </td>
                </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="tab4" class="tab_content">
    <ul class="srchCharacteristics">
        <?php $ch = new Characteristics();
        foreach ($ch as $key => $value) {
            if ($key != '0' && translateField($key) != '') {
                ?>
                <li style=" width:40%;margin-left:5%; float:left;margin-top:.5em;">
                    <label for="<?=$key?>"><span
                            style="float:left; width:15em;"> <?=translateField($key)?></span>
                        <input type="text" class="inputtext" name="<?=$key?>" id="<?=$key?>"
                               style="width:8em; float:right">
                    </label>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>
<div id="tab5" class="tab_content">
    <h2>Osobine proizvoda</h2>
    <label style="float:left;padding-top: 1em;">Izaberite set atributa &nbsp;&nbsp;&nbsp;

        <div class="searchAttributes" id="attributeList">
            <?php
            $attS = ProductDetailsSettings::find_main();

            foreach ($attS as $a): ?>

                <label style="float:left;min-width: 30%;font-weight:700; text-transform: uppercase;"
                       class="mainAttribute">
                    <a href="#" class="showAttribute" id="<?=$a->id?>"> [ + ] </a> <input type="hidden"
                                                                                          name="<?=$a->id?>"/>
                    <span style="position:relative;left:1em; top:4px;"><?=$a->name_sr?></span>

                </label>
                <div class="attributeWrapper" id="attributeSet<?=$a->id?>">
                    <?php
                    $vals = ProductDetails::find_by_settingID($a->id);
                    if ($vals) {
                        foreach ($vals as $v):?>
                            <label style="float:left;clear:left; width: 100%;position:relative; left:1em;">
                                <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                                       value="<?=$v->id?>" <?php if (is_array($atttributes) && in_array($v->id, $atttributes)) {
                                    echo 'checked';
                                }?>/>
                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                            </label>
                            <?php endforeach;
                    }
                    $subA = ProductDetailsSettings::find_child($a->id);
                    if ($subA) {
                        foreach ($subA as $sA) {
                            ?>
                            <label style="float:left;clear:left;margin-left:20px;padding-left:10px;font-weight: 700">
                                [2] <input type="hidden" name="<?=$sA->id?>"/>
                                <span style="position:relative;left:1em; top:4px;"><?=$sA->name_sr?></span>
                            </label>
                            <?php
                            $vals = ProductDetails::find_by_settingID($sA->id);
                            if ($vals) {
                                foreach ($vals as $v):?>
                                    <label style="float:left;clear:left; width: 100%;position:relative; left:2em;">
                                        <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                                               value="<?=$v->id?>"/>
                                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                    </label>
                                    <?php endforeach;
                            }
                            $subAA = ProductDetailsSettings::find_child($sA->id);
                            if ($subAA) {
                                foreach ($subAA as $sAA):?>
                                    <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:20px;">[3]
                                        <input
                                                type="hidden" name="<?=$sAA->id?>"/> <span
                                                style="position:relative;left:1em; top:4px;"><?=$sAA->name_sr?></span></label>
                                    <?php

                                    $vals = ProductDetails::find_by_settingID($sAA->id);
                                    if ($vals) {
                                        foreach ($vals as $v):?>
                                            <label style="float:left;clear:left; width: 100%;position:relative; left:3em;">
                                                <input type="checkbox" name="values[<?=$v->id?>]"/>
                                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                            </label>
                                            <?php endforeach;
                                    }
                                    $subAAA = ProductDetailsSettings::find_child($sAA->id);
                                    if ($subAAA) {
                                        foreach ($subAAA as $sAAA):?>
                                            <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:30px;">[4]
                                                <input
                                                        type="hidden" name="<?=$sAAA->id?>"/> <span
                                                        style="position:relative;left:1em; top:4px;"><?=$sAAA->name_sr?></span></label>
                                            <?php
                                            $vals = ProductDetails::find_by_settingID($sAAA->id);
                                            if ($vals) {
                                                foreach ($vals as $v):?>
                                                    <label style="float:left;clear:left; width: 100%;position:relative; left:4em;">
                                                        <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                                                               value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                            echo 'checked';
                                                        }?>/>
                                                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                    </label>
                                                    <?php endforeach;
                                            }

                                            $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                            if ($subAAAA) {
                                                foreach ($subAAAA as $sAAAA):?>
                                                    <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:40px;">
                                                        [5] <input
                                                            type="hidden" name="<?=$sAAAA->id?>"/> <span
                                                            style="position:relative;left:1em; top:4px;"><?=$sAAAA->name_sr?></span></label>
                                                    <?php
                                                    $vals = ProductDetails::find_by_settingID($sAAAA->id);
                                                    if ($vals) {
                                                        foreach ($vals as $v):?>
                                                            <label style="float:left;clear:left; width: 100%;position:relative; left:5em;">
                                                                <input type="checkbox"
                                                                       name="atributesDetails[<?=$v->id?>]"
                                                                       value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                                    echo 'checked';
                                                                }?>/>
                                                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                            </label>
                                                            <?php endforeach;
                                                    }
                                                    $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                                    if ($subAAAAA) {
                                                        foreach ($subAAAAA as $sAAAAA):?>
                                                            <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:50px;">
                                                                [6] <input
                                                                    type="hidden" name="<?=$sAAAA->id?>"/> <span
                                                                    style="position:relative;left:1em; top:4px;"><?=$sAAAA->name_sr?></span></label>
                                                            <?php
                                                            $vals = ProductDetails::find_by_settingID($sAAAAA->id);
                                                            if ($vals) {
                                                                foreach ($vals as $v):?>
                                                                    <label style="float:left;clear:left; width: 100%;position:relative; left:6em;">
                                                                        <input type="checkbox"
                                                                               name="atributesDetails[<?=$v->id?>]"
                                                                               value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                                            echo 'checked';
                                                                        }?>/>
                                                                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                                    </label>
                                                                    <?php endforeach;
                                                            }
                                                            $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                            if ($subAAAAAA) {
                                                                foreach ($subAAAAAA as $sAAAAAA):?>
                                                                    <label style="float:left; margin-left:20px;font-weight:700;padding-left:70px;clear:left">[7]
                                                                        <input
                                                                                type="hidden" name="<?=$sAAAAAA->id?>"/> <span
                                                                                style="position:relative;left:1em; top:4px;"><?=$sAAAAAA->name_sr?></span></label>
                                                                    <?php
                                                                    $vals = ProductDetails::find_by_settingID($sAAAAA->id);
                                                                    if ($vals) {
                                                                        foreach ($vals as $v):?>
                                                                            <label style="float:left;clear:left; width: 100%;position:relative; left:7em;">
                                                                                <input type="checkbox"
                                                                                       name="atributesDetails[<?=$v->id?>]"
                                                                                       value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                                                    echo 'checked';
                                                                                }?>/>
                                                                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                                            </label>
                                                                            <?php endforeach;
                                                                    }
                                                                endforeach;
                                                            }
                                                        endforeach;
                                                    }
                                                endforeach;
                                            }
                                        endforeach;
                                    }
                                endforeach;
                            }
                        }
                    }
                    ?></div>
                <?php
            endforeach; ?>
        </div>
        <?php /*; ?>
    <div class="attributeList" style="float: left; clear: left;margin-top: 20px;">
        <?php
        foreach ($attr as $a) {
            ?>
            <table id="attributes<?=$a->id?>" border="0" cellspacing="0" cellpadding="0"
                   style="position: relative;float: left;display:none; clear: left;left: 250px; top: -50px;">
                <?php
                $details = ProductDetailsSettings::find_by_attributeSet($a->details);
                foreach ($details as $dS) { ?>
                    <tr>
                        <td>
                            <label style="position: relative;top: -13px;"><?=$dS->name_sr?></label>
                        </td>
                        <td><input type="text" name="<?=$a->id?>__<?=$dS->name_sr?>" style="text-align: center"/></td>
                    </tr>
                    <?php
                } ?>
            </table>
            <?php } ?>
        </table>

    </div>     */ ?>
</div>
</div>
<div class="fix"></div>
</div>
<div class='searchTemplates'>
    <span class="lblInfo">Šabloni za pretragu:</span>
    <select name="" class="searchTemplateSelect">
        <option value="0">Izaberi šablon..</option>
        <?php $searchT = SearchTemplate::find_all();
        foreach ($searchT as $sT) {
            ?>
            <option value="<?=$sT->id?>"><?=$sT->name?></option>
            <? } ?>
    </select>
    <a href="#" class="implementTemplate">Primeni šablon</a>
    <!--</div>-->
</div>
<input type="submit" class="button greyishBtn submitForm" id="detailSearchBttn" value="Pretraži"
       style="margin:1em 1em 0 0">
</form>
<!-- Dynamic table -->
<div class="widget">
    <div class="head"><h5 style="width: 90%">Rezultati pretrage <a href="#" style="float:right;display:none"
                                                                   class="saveTemplate">Sačuvaj u šablonima</a></h5>
    </div>
    <div id="searchRes">
    </div>
</div>
<a href="#" class="addPriceTemplate btn14" style="margin-top:10px">Primeni cenovni šablon</a>

</div>
<div class="fix"></div>
</div>

<input type="hidden" class="siteURL" value="<?=ADMIN?>">