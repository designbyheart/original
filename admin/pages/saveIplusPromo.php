<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/1/12
 * Time: 2:02 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');

if (isset($_POST['id']) && $_POST['id'] > 0) {
    $promo = iPlusPromo::find_by_id($_POST['id']);
} else {
    $promo = new Promo();
}
foreach ($promo as $key => $value) {
    if ($key != 'id' && isset($_POST[$key])) {
        $promo->$key = $_POST[$key];
    }
}
if (isset($_POST['dateStart'])) {
    $promo->dateStart = strtotime($_POST['dateStart']);
}
if (isset($_POST['dateEnd'])) {
    $promo->dateEnd = strtotime($_POST['dateEnd']);
}
if (isset($_POST['active'])) {
    $promo->active = 1;
} else {
    $promo->active = 2;
}
if (isset($_POST['fullWidth'])) {
    $promo->fullWidth = 1;
} else {
    $promo->fullWidth = 0;
}
$promo->products = $_POST['products'];


if ($promo && $promo->save()) {
    if (isset($_FILES)) {
        foreach ($_FILES as $file) {
            if ($file['name'] != '') {
                $gal = new Gallery();
                $gal->file = cleanFileName('promo-' . $file['name']);
                $gal->refID = $promo->id;
                $gal->type = 'promotion';
                if ($gal->save()) {
                    uploadPhoto($file, '', 800, 100, 80, 250, 200, $gal->file);
                    $session->message('Slika je uneta');
                    $_SESSION['mType'] = 2;
                } else {
                    $session->message('Postoji problem. Slika nije uneta');
                    $_SESSION['mType'] = 4;
                }
            }
        }
    }
    $promo->price = $_POST['price'];
    if (isset($_POST['group'])) {
        foreach ($_POST['group'] as $key => $value) {
            if ($value > 0) {
                $g = PriceGroup::find_is_exists($key, $promo->id);
                if (!$g) {
                    $g = new PriceGroup();
                    $g->parent = $key;
                    $g->productID = $promo->id;
                }
                $g->percentage = $value;
                $g->save();
                if (isset($_POST['prices'][$g->parent])) {
                    $p = Price::find_by_product_and_group($promo->id, $g->id);
                    if (!$p) {
                        $p = new Price();
                        $p->productID = $promo->id;
                        $p->groupID = $g->id;
                    }
                    $p->amount = $_POST['prices'][$key];
                    if ($p->amount > 0) {
                        $p->save();
                    }
                }
            }
        }
    }
    $session->message('Promocija je sačuvana');
    $_SESSION['mType'] = 2;
    redirect_to(ADMIN . 'promocije');
    $session->mType = '1';
} else {
    $session->message('Promocija nije sačuvana');
    $_SESSION['mType'] = 4;
    redirect_to(ADMIN . 'promocija/' . $promo->id);
}