<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
$mainMenu = MainMenu::find_by_type('footer');

?>
<div class="newLink" style="position:absolute">
    <h5>Novi link</h5>
    <a href="#" class="close">Close</a>
    <form action="<?=ADMIN?>saveMainMenu" method="post">
        <label>
            Srpski naziv
            <input type="text" name="name_sr">
        </label>
        <label>
            Engleski naziv
            <input type="text" name="name_en">
        </label>
        <label>Link
            <input type="text" name="url">
        </label>
        <label style="width:30%">
            Kolona
        </label>
        <input type="hidden" name="newLink" value="yes">
        <input type="hidden" name="type" value="footer">
            <select name="columnT" >
                <option value="0">Leva kolona</option>
                <option value="1">Desna kolona</option>
            </select>

        <button type="submit" class="greyishBtn">Sačuvaj</button>
    </form>
</div>
<!-- Content -->
<div class="content">
    <div class="title">
        <h5>Editovanje linkova u footeru</h5>
    </div>
    <form class="mainForm" action="<?=ADMIN?>saveMainMenu" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
            <input type="hidden" name="type" value="footer">
            <div class="widget">
                <ul class="tabs">
                    <li><a href="#tab1">Srpski</a></li>
                    <li><a href="#tab2">Engleski</a></li>
                </ul>

                <div class="tab_container" style="overflow: visible!important;">
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                        <div class="table">
                            <div class="head"><h5><a href="#" class="addNewLink">Novi link</a></h5>

                            </div>
                            <table class="tableStatic mainMenu" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <td width="7%">Pozicija</td>
                                        <td width="20%">Tekst linka</td>
                                        <td width="10%">Kolona</td>
                                        <td width="25%">Url</td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($mainMenu && count($mainMenu)>0){
                                foreach($mainMenu as $m): ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="ids[<?=$m->id?>]" value="<?=$m->id?>">
                                            <input type="text" name="ordering[<?=$m->id?>]" value="<?=$m->ordering?>" class="centerAl" style="width:75%!important">
                                        </td>
                                        <td><input type="text" name="name_sr[<?=$m->id?>]" value="<?=$m->name_sr?>" class="centerAl"></td>
                                        <td><select name="columnType[<?=$m->id?>]" >
                                            <option value="0">leva</option>
                                            <option value="1" <?php  if($m->columnT==1)echo 'selected'; ?>>desna</option>
                                        </select></td>
                                        <td><span style="float:left;margin-right:15px;"><?=SITE_ROOT?></span><input type="text" name="url[<?=$m->id?>]" value="<?=$m->url?>" class="centerAl" style="width:62%!important;"></td>
                                    </tr>
                                <?php endforeach;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End - tab1 -->


                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content" style="overflow: visible!important;">
                        <div class="table">
                            <div class="head"><h5>Lista eng. linkova footer </h5></div>
                            <table class="tableStatic" cellpadding="0" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <td width="2.5%">Pozicija</td>
                                    <td width="20%">Tekst linka - engleski</td>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($mainMenu && count($mainMenu)>0){
                                foreach($mainMenu as $m): ?>
                                <tr>
                                    <td><input type="text"  value="<?=$m->ordering?>" class="centerAl" style="width: 95%;"></td>
                                    <td><input type="text" name="name_en[<?=$m->id?>]" value="<?=$m->name_en?>" class="centerAl" style="width: 98%;"></td>

                                </tr>
                                    <?php endforeach; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End - tab2 -->
                </div>
                <div class="fix"></div>
            </div>
            <!-- End - Tabs -->
        </fieldset>
        <a href="<?=ADMIN?>opste_strane"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
        <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
    </form>

</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>
