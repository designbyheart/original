<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>
<!-- Content -->
<div class="content">
<div class="newLink newCharCategory" style="position:absolute; height:160px">
    <h5>Nova kategorija karakteristika</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveCharacteristic" method="post">
        <label>
            Srpski naziv
            <input type="text" name="name_sr">
        </label>
        <label>
            Engleski naziv
            <input type="text" name="name_en">
        </label>

        <input type="hidden" name="pageType" value="charCategory">
        <input type="hidden" name="pageID" value="settings">
        <button type="submit" class="greyishBtn">Sačuvaj</button>
    </form>
</div>
<div class="newLink chooseCharCategory" style="position:absolute; height:160px">
    <h5>Kategorije karakteristika</h5>
    <input type="hidden" value="" class="charID">
    <a href="#" class="close">Close</a>

    <form action="#" method="post">

        <div class="parentList" style="height:60px; width: 263px">
            <?php
            $chars = CharacteristicsCategory::find_all();
            foreach ($chars as $ch) {
                ?>
                <label id="<?=$ch->id?>"> <input type="checkbox" name="<?=$ch->category_name_sr?>" value="<?=$ch->id?>"
                                                 style="display:none">
                    <?=$ch->category_name_sr?>
                </label>
                <?php } ?>
        </div>
        <button type="submit" class="greyishBtn saveChar"
                style="left: 100px;position: relative;padding: 5px 10px!important;width: 80px;">Ok
        </button>
    </form>
</div>
<div class="newLink addAttributeWrapper"
     style="position:absolute; padding-top:20px; margin-left:0px;height:420px">
    <h5>Dodaj atribut</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveAttribut" method="POST">
        <input type="hidden" name="product" value="<?=$product->id?>">
        <input type="hidden" name="settings" value="1">
        <label for="attributName11" style="margin-top:30px;">Ime atributa: <br>
            <input type="text" name="attributName" id="attributName11"
                   style="float:left;clear:left;width:250px; margin-left:0;">
        </label>
        <label for="attributNameEng12">Ime atributa (engleski): <br>
            <input type="text" name="attributNameEng" id="attributNameEng12"
                   style="float:left;width:250px;clear:left; margin-left:0;">
        </label>
        <input type="hidden" name="product" value="settings">

        <div class="parentList">
            <label>Izaberi roditelje</label>
            <?php $attS = ProductDetailsSettings::find_main(); ?>
            <?php foreach ($attS as $a): ?>
            <label id="<?=$a->id?>"> <input type="checkbox" name="parent[]" value="<?=$a->id?>" style="display:none">
                <strong><?=$a->name_sr?></strong></label>
            <?php $subA = ProductDetailsSettings::find_child($a->id);
            if ($subA) {
                foreach ($subA as $sA) {
                    ?>
                    <label id="<?=$sA->id?>">&nbsp;&nbsp;↳&nbsp;&nbsp; <input type="checkbox" name="parent[]"
                                                                              value="<?=$sA->id?>"
                                                                              style="display:none"><?=$sA->name_sr?>
                    </label>
                    <?
                    $subAA = ProductDetailsSettings::find_child($sA->id);
                    if ($subAA) {
                        foreach ($subAA as $sAA):?>
                            <label id="<?=$sAA->id?>">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp; <input type="checkbox" name="parent[]"
                                                                                   value="<?=$sAA->id?>"
                                                                                   style="display:none">  <?=$sAA->name_sr?>
                            </label>
                            <?php

                            $subAAA = ProductDetailsSettings::find_child($sAA->id);
                            if ($subAAA) {
                                foreach ($subAAA as $sAAA):?>
                                    <label id="<?=$sAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                            type="checkbox" name="parent[]" value="<?=$sAAA->id?>"
                                            style="display:none"> <?=$sAAA->name_sr?> </label>
                                    <?php
                                    $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                    if ($subAAAA) {
                                        foreach ($subAAAA as $sAAAA):?>
                                            <label id="<?=$sAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                    type="checkbox" name="parent[]" value="<?=$sAAAA->id?>"
                                                    style="display:none"> <?=$sAAAA->name_sr?> </label>
                                            <?php
                                            $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                            if ($subAAAAA) {
                                                foreach ($subAAAAA as $sAAAAA):?>
                                                    <label id="<?=$sAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                            type="checkbox" name="parent[]" value="<?=$sAAAAA->id?>"
                                                            style="display:none"> <?=$sAAAAA->name_sr?> </label>
                                                    <?php
                                                    $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                    if ($subAAAAAA) {
                                                        foreach ($subAAAAAA as $sAAAAAA):?>
                                                            <label id="<?=$sAAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                                    type="checkbox" name="parent[]"
                                                                    value="<?=$sAAAAAA->id?>"
                                                                    style="display:none"> <?=$sAAAAAA->name_sr?>
                                                            </label>
                                                            <?php endforeach;
                                                    }
                                                endforeach;
                                            }
                                        endforeach;
                                    }
                                endforeach;
                            }
                        endforeach;
                    }
                }
            }
        endforeach; ?>

        </div>
        <input type="submit" value="Sačuvaj" class="greyishBtn submitForm"
               style="float:left;margin-left:5px;margin-top:30px;">
    </form>
</div>
<div class="newLink newCharacteristic" style="position:absolute; height:310px">
    <h5>Nova karakteristika</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveCharacteristic" method="post">
        <label>Kategorija: <br>
            <select name="charCategory">
                <option value="0">Izaberi...</option>
                <?php $chars = CharacteristicsCategory::find_all();

                if (count($chars) > 0) {
                    foreach ($chars as $chC):?>
                        <option value="<?=$chC->id?>"><?=$chC->category_name_sr?></option>
                        <?php endforeach;
                }
                ?>
            </select>
        </label>
        <label>
            Ime/Opis karakteristike
            <input type="text" name="name_sr" style="float:left;clear:left;width:250px;margin-top:5px; margin-left:0;">
        </label>
        <label>
            Ime/Opis karakteristike (eng)
            <input type="text" name="name_en" style="float:left;clear:left;width:250px;margin-top:5px; margin-left:0;">
        </label>

        <input type="hidden" name="charType" value="category">
        <input type="hidden" name="pageID" value="settings">
        <label>
            <input type="submit" class="greyishBtn" style="float:left;width:100px; margin-top:20px;" value="Sačuvaj">
        </label>
    </form>
</div>
<div class="newLink addAttributeWrapper"
     style="position:absolute; padding-top:20px; margin-left:0px;height:420px">
    <h5>Dodaj atribut</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveAttribut" method="POST">
        <input type="hidden" name="product" value="<?=$product->id?>">
        <input type="hidden" name="settings" value="1">
        <label for="attributName" style="margin-top:30px;">Ime atributa: <br>
            <input type="text" name="attributName" id="attributName"
                   style="float:left;clear:left;width:250px; margin-left:0;">
        </label>
        <label for="attributNameEng">Ime atributa (engleski): <br>
            <input type="text" name="attributNameEng" id="attributNameEng"
                   style="float:left;width:250px;clear:left; margin-left:0;">
        </label>

        <div class="parentList">
            <label>Izaberi roditelje</label>
            <?php $attS = ProductDetailsSettings::find_main(); ?>
            <?php foreach ($attS as $a): ?>
            <label id="<?=$a->id?>"> <input type="checkbox" name="parent[]" value="<?=$a->id?>" style="display:none">
                <strong><?=$a->name_sr?></strong></label>
            <?php $subA = ProductDetailsSettings::find_child($a->id);
            if ($subA) {
                foreach ($subA as $sA) {
                    ?>
                    <label id="<?=$sA->id?>">&nbsp;&nbsp;↳&nbsp;&nbsp; <input type="checkbox" name="parent[]"
                                                                              value="<?=$sA->id?>"
                                                                              style="display:none"><?=$sA->name_sr?>
                    </label>
                    <?
                    $subAA = ProductDetailsSettings::find_child($sA->id);
                    if ($subAA) {
                        foreach ($subAA as $sAA):?>
                            <label id="<?=$sAA->id?>"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
                                    type="checkbox" name="parent[]" value="<?=$sAA->id?>"
                                    style="display:none">  <?=$sAA->name_sr?>
                            </label>
                            <?php

                            $subAAA = ProductDetailsSettings::find_child($sAA->id);
                            if ($subAAA) {
                                foreach ($subAAA as $sAAA):?>
                                    <label id="<?=$sAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                            type="checkbox" name="parent[]" value="<?=$sAAA->id?>"
                                            style="display:none"> <?=$sAAA->name_sr?> </label>
                                    <?php
                                    $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                    if ($subAAAA) {
                                        foreach ($subAAAA as $sAAAA):?>
                                            <label id="<?=$sAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                    type="checkbox" name="parent[]" value="<?=$sAAAA->id?>"
                                                    style="display:none"> <?=$sAAAA->name_sr?> </label>
                                            <?php
                                            $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                            if ($subAAAAA) {
                                                foreach ($subAAAAA as $sAAAAA):?>
                                                    <label id="<?=$sAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                            type="checkbox" name="parent[]" value="<?=$sAAAAA->id?>"
                                                            style="display:none"> <?=$sAAAAA->name_sr?> </label>
                                                    <?php
                                                    $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                    if ($subAAAAAA) {
                                                        foreach ($subAAAAAA as $sAAAAAA):?>
                                                            <label id="<?=$sAAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                                    type="checkbox" name="parent[]"
                                                                    value="<?=$sAAAAAA->id?>"
                                                                    style="display:none"> <?=$sAAAAAA->name_sr?>
                                                            </label>
                                                            <?php endforeach;
                                                    }
                                                endforeach;
                                            }
                                        endforeach;
                                    }
                                endforeach;
                            }
                        endforeach;
                    }
                }
            }
        endforeach; ?>

        </div>
        <input type="submit" value="Sačuvaj" class="greyishBtn submitForm"
               style="float:left;margin-left:5px;margin-top:30px;">
    </form>
</div>
<div class="newLink" style="position:absolute; height:280px" id="chooseParent">
    <a href="#" class="close">Close</a>
    <input class="id" type="hidden">
    <input class="attributeParent" type="hidden">


    <form action="#">
        <h5>Izaberi nadređene grupe</h5>
        <label>Izaberi roditelje</label>

        <div class="parentList">

            <?php $attS = ProductDetailsSettings::find_main(); ?>
            <?php foreach ($attS as $a): ?>
            <label id="<?=$a->id?>"> <input type="checkbox" name="parent[]" value="<?=$a->id?>" style="display:none">
                <strong><?=$a->name_sr?></strong></label>
            <?php $subA = ProductDetailsSettings::find_child($a->id);
            if ($subA) {
                foreach ($subA as $sA) {
                    ?>
                    <label id="<?=$sA->id?>">&nbsp;&nbsp;↳&nbsp;&nbsp; <input type="checkbox" name="parent[]"
                                                                              value="<?=$sA->id?>"
                                                                              style="display:none"><?=$sA->name_sr?>
                    </label>
                    <?
                    $subAA = ProductDetailsSettings::find_child($sA->id);
                    if ($subAA) {
                        foreach ($subAA as $sAA):?>
                            <label id="<?=$sAA->id?>"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
                                    type="checkbox" name="parent[]" value="<?=$sAA->id?>"
                                    style="display:none">  <?=$sAA->name_sr?>
                            </label>
                            <?php

                            $subAAA = ProductDetailsSettings::find_child($sAA->id);
                            if ($subAAA) {
                                foreach ($subAAA as $sAAA):?>
                                    <label id="<?=$sAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                            type="checkbox" name="parent[]" value="<?=$sAAA->id?>"
                                            style="display:none"> <?=$sAAA->name_sr?> </label>
                                    <?php
                                    $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                    if ($subAAAA) {
                                        foreach ($subAAAA as $sAAAA):?>
                                            <label id="<?=$sAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                    type="checkbox" name="parent[]" value="<?=$sAAAA->id?>"
                                                    style="display:none"> <?=$sAAAA->name_sr?> </label>
                                            <?php
                                            $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                            if ($subAAAAA) {
                                                foreach ($subAAAAA as $sAAAAA):?>
                                                    <label id="<?=$sAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                            type="checkbox" name="parent[]" value="<?=$sAAAAA->id?>"
                                                            style="display:none"> <?=$sAAAAA->name_sr?> </label>
                                                    <?php
                                                    $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                    if ($subAAAAAA) {
                                                        foreach ($subAAAAAA as $sAAAAAA):?>
                                                            <label id="<?=$sAAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                                    type="checkbox" name="parent[]"
                                                                    value="<?=$sAAAAAA->id?>"
                                                                    style="display:none"> <?=$sAAAAAA->name_sr?>
                                                            </label>
                                                            <?php endforeach;
                                                    }
                                                endforeach;
                                            }
                                        endforeach;
                                    }
                                endforeach;
                            }
                        endforeach;
                    }
                }
            }
        endforeach; ?>

        </div>
        <input type="submit" value="Sačuvaj" class="greyishBtn submitForm"
               style="float:left;clear:left; margin-top:10px;">
    </form>
    <input class="chooseParentList" type="hidden" value="">
</div>
<div class="newLink addAttributeDetailWrapper"
     style="position:absolute; padding-top:20px; margin-left:20px;height:260px">
    <h5>Dodaj detalj</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveAttribut" method="POST">
        <input type="hidden" name="product" value="<?=$product->id?>">
        <input type="hidden" name="attribut" value="1">
        <label for="selectParentAttr" style="margin-bottom:1em;">
            <?php $attS = ProductDetailsSettings::find_main(); ?>
            <select name="parent" id="selectParentAttr" style="width:265px!important;border-color:#ccc!important;">
                <option value="0">Izaberi atribut</option>
                <?php foreach ($attS as $a): ?>
                <option value="<?=$a->id?>"><?=$a->name_sr?> </option>
                <?php $subA = ProductDetailsSettings::find_child($a->id);
                if ($subA) {
                    foreach ($subA as $sA) {
                        ?>
                        <option value="<?=$sA->id?>">&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sA->name_sr?> </option>
                        <?
                        $subAA = ProductDetailsSettings::find_child($sA->id);
                        if ($subAA) {
                            foreach ($subAA as $sAA):?>
                                <option value="<?=$sAA->id?>">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAA->name_sr?> </option>
                                <?php

                                $subAAA = ProductDetailsSettings::find_child($sAA->id);
                                if ($subAAA) {
                                    foreach ($subAAA as $sAAA):?>
                                        <option value="<?=$sAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAA->name_sr?> </option>
                                        <?php
                                        $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                        if ($subAAAA) {
                                            foreach ($subAAAA as $sAAAA):?>
                                                <option value="<?=$sAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAAA->name_sr?> </option>
                                                <?php
                                                $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                                if ($subAAAAA) {
                                                    foreach ($subAAAAA as $sAAAAA):?>
                                                        <option value="<?=$sAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAAAA->name_sr?> </option>
                                                        <?php
                                                        $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                        if ($subAAAAAA) {
                                                            foreach ($subAAAAAA as $sAAAAAA):?>
                                                                <option value="<?=$sAAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAAAAA->name_sr?> </option>
                                                                <?php endforeach;
                                                        }
                                                    endforeach;
                                                }
                                            endforeach;
                                        }
                                    endforeach;
                                }
                            endforeach;
                        }
                    }
                }
            endforeach; ?>
            </select>

        </label>
        <label for="attributValue">Vrednost atributa: <br>
            <input type="text" name="attributValue" id="attributValue"
                   style="float:left;clear:left;width:250px; margin-left:0;">
        </label>
        <label for="attributValueEng">Vrednost atributa (engleski): <br>
            <input type="text" name="attributValueEng" id="attributValueEng"
                   style="float:left;width:250px;clear:left; margin-left:0;">
        </label>

        <input type="submit" value="Sačuvaj" class="greyishBtn submitForm"
               style="float:left;margin-left:5px;margin-top:30px;">
    </form>
</div>


<div class="title"><h5>Podešavanja</h5></div>
<?php
$sett = Settings::find_all();
?>
<?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
<div class="widget">
<ul class="tabs">
    <li <?=activateTab($tab, 1, 'tab')?>><a href="#tab1">Osnovna podešavanja</a></li>
    <li <?=activateTab($tab, 2, 'tab')?>><a href="#tab2">Atributi</a></li>
    <li <?=activateTab($tab, 3, 'tab')?>><a href="#tab3">Karakteristike</a></li>

</ul>
<div class="tab_container" style="width:100%;">
<!-- Content of tab1 -->
<div id="tab1" class="tab_content" style="padding: 0;width: 100%;<?=activateTab($tab, 1)?>">
    <form action="<?=ADMIN?>saveSettings" style="margin-top:1em; display:none" class="newSettings" method="post">
        <h5>Novo podešavanje</h5>
        <fieldset>
            <label style="float:left; clear:left;width:100%;padding-bottom: 2em;;">Naziv podešavanja
                <input type="text" name="newSettingsName" style="position:relative; top:2em">
            </label>
            <label style="float:left; clear:left;width:100%;padding:1em 0">
                Vrednost
                <input type="text" name="newSettingsValue">
            </label>
            <input type="submit" value="Sačuvaj" class='greyishBtn submitForm'>
        </fieldset>
    </form>
    <div class="table">
        <div class="head"><h5><a href="#" class="addSettings">Dodaj novo podešavanje</a></h5></div>

        <form action="<?=ADMIN?>saveSettings" method="post">
            <table cellpadding="0" cellspacing="0" border="0" class="display dynamicTable">
                <thead>
                <tr>
                    <th>Podešavanje</th>
                    <th>Vrednost</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($sett as $s): ?>
                <tr class="gradeA" <?=$s->id?>>

                    <td align="center">
                        <?=$s->settingsName?>
                    </td>
                    <td class="center">
                        <input type="text" name="settingsName[<?=$s->id?>]" value="<?=$s->settingsValue?>"
                               style="width: 95%;margin-bottom:0!important; text-align:center; 3px;; ;">
                    </td>
                </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
    </div>
    <input type="submit" value="Sačuvaj" class="greyishBtn submitForm " name="saveAllSettings"
           style="margin-top: 1em;">
    </form>

    <!-- Dynamic table -->
    <div class="table" style="margin-top:3.2em">
        <div class="head"><h5>Aktivacija država</h5></div>

        <form action="<?=ADMIN?>saveCountries" method="post">
            <table cellpadding="0" cellspacing="0" border="0" class="display dynamicTableData">
                <thead>
                <tr>
                    <th>Država</th>
                    <th width="15%">Aktivna</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $countries = Country::find_all();
                foreach ($countries as $c): ?>
                <tr class="gradeA" <?=$c->id?>>

                    <td width="85%">
                        <?=$c->name?>
                    </td>
                    <td class="center" style="text-align:center">
                        <span style="display:none"><?=$c->active?></span>
                    <span style="float:left;position:relative;left:3.5em!important">
                        <input class="country" id="<?=$c->id?>" type="checkbox" <? /*name="<?=$c->id?>"*/ ?>
                               value="<?=$c->id?>" <?php if ($c->active == 1) {
                            echo 'checked';
                        }?>/>
                    </span>
                        <span class="result<?=$c->id?>" style="position: relative;left:20px;top:5px;"></span>
                    </td>
                </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
    </div>
</div>
<div id="tab2" class="tab_content" style="padding: 0;width: 100%; <?=activateTab($tab, 2)?>">
    <div class="table">
        <?php $attributeList = ProductDetails::find_all(); ?>
        <div class="head">
            <h5 class="iFrames">Lista atributa
                <a href="#" id="addAtribute" style="margin-right:1em; margin-left:1em; margin-top:1em">Dodaj tip
                    osobine</a>
                <a href="#" id="addAtributeDetail" style="margin-top:1em">Dodaj osobinu</a>
            </h5>
        </div>
        <input class="selectedAttributelist" type="hidden" name="">
        <table cellpadding="0" cellspacing="0" border="0" class="display inputOnly noInputMargin attributList"
               id="example">
            <thead>
            <tr>
                <th width='5%' align="center"><a href="#" class="selectAllAtributes">Select All</a></th>
                <th>Naziv atributa (sr)</th>
                <th>Naziv atributa (en)</th>
                <th width="30%">Roditelj</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($attributeList as $atItem): ?>
            <tr class="gradeA">
                <td align="center"><input type="checkbox" name="attributeCheck[]" class="attributes"
                                          value="<?=$atItem->id?>"></td>
                <td><input type="text" class="attributeSrDetail<?=$atItem->id?>" value="<?=$atItem->value_sr?>">
                </td>
                <td><input type="text" class="attributeEnDetail<?=$atItem->id?>" value="<?=$atItem->value_en?>">
                </td>
                <td align="center">
                    <a href="#<?=$atItem->id?>" id="attributeParent<?=$atItem->id?>"
                       class="btnIconLeft mr10 attributeParent"
                       style="padding:2px 7px;min-width: 100px"><?=get_parent($atItem->detailID)?></a>
                    <input type="hidden" class="parentDetail<?=$atItem->id?>" value="<?=$atItem->detailID?>">
                    <span class="resDetail<?=$atItem->id?>"></span>
                </td>
                <td width="11%" align="center">
                    <a href="<?=ADMIN?>pages/saveAtribut.php?id=<?=$atItem->id?>" id="<?=$atItem->id?>"
                       class="saveProductDetails">
                        <img src="<?=ADMIN?>doc/images/icons/color/tick.png" alt="" class="mr10"></a>
                    <a href="<?=ADMIN?>pages/deleteAttribute.php?id=<?=$atItem->id?>&type=attributeDetail"
                       class="deleteAttribute">
                        <img src="<?=ADMIN?>doc/images/icons/color/cross.png" alt="" class="mr10">
                    </a>
                </td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <a href="<?=ADMIN?>pages/deleteSelAttributes.php?selected=" class="btnIconLeft mr10 multipleDeleteAttr"
       style="padding:.5em;margin:1em 0 0 0">Delete selected</a>

    <div class="table">
        <?php $attributeList = ProductDetailsSettings::find_all(); ?>
        <div class="head"><h5 class="iFrames">Lista grupa atributa</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display inputOnly noInputMargin dynamicTableData">
            <thead>
            <tr>
                <th width="5%"><a class="selectAllCatAtributes" href="#">Select all</a></th>
                <th>Naziv grupe (sr)</th>
                <th>Naziv grupe (en)</th>
                <th>Roditelj</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($attributeList as $atItem): ?>
            <tr class="gradeA">
                <td><input type="checkbox" class="attrGroupsDel selectAllCatAtributes" name="attributeSettingsGroups[]"
                           value="<?=$atItem->id?>"></td>
                <td><input type="text" class="parentSettingsSr<?=$atItem->id?>" value="<?=$atItem->name_sr?>"></td>
                <td><input type="text" class="parentSettingsEn<?=$atItem->id?>" value="<?=$atItem->name_en?>"></td>
                <td width="30%" align="center"><a href="#<?=$atItem->id?>"
                                                  class="btnIconLeft mr10 attributeGroupParent attributeParrentSettings<?=$atItem->id?>"
                                                  style="padding:2px 7px;min-width: 100px"><?=get_parent($atItem->parent)?></a>

                    <div class="restSettings<?=$atItem->id?>"></div>
                    <input class="parentGroupSettings<?=$atItem->id?>" type="hidden">
                    <span class="resDetail<?=$atItem->id?>"></span>
                </td>
                <td width="11%" align="center">
                    <a href="<?=ADMIN?>pages/saveAttributeSettings.php?id=<?=$atItem->id?>"
                       id="attributeSettings<?=$atItem->id?>" class="attributeSettings"><img
                            src="<?=ADMIN?>doc/images/icons/color/tick.png" alt="" class="mr10"></a>
                    <a href="<?=ADMIN?>pages/deleteAttribute.php?id=<?=$atItem->id?>&type=attributeSettings"
                       class="deleteAttributeGroup"><img
                            src="<?=ADMIN?>doc/images/icons/color/cross.png" alt="" class="mr10"></a>

                    <div class="resGroupDetail<?=$atItem->id?>"></div>
                </td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <input type="hidden" class="multipleDeleteAttGroups">
    </div>
    <a href="<?=ADMIN?>pages/deleteSelAttrGroups.php?selected=" class="btnIconLeft mr10 multipleDeleteAttrGroups"
       style="padding:.5em;margin:1em 0 0 0">Delete selected</a>
    <?php //end attrubute settings    ; ?>
</div>
<div id="tab3" class="tab_content" style="padding: 0;width: 100%; <?=activateTab($tab, 3)?>">
    <div class="table">
        <?php $chList = Characteristics::find_all(); ?>
        <div class="head"><h5 class="iFrames">Lista karakteristika
            <a href="#" class="addChar" style="margin:0 1em">Dodaj karakteristiku</a>
        </h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display inputOnly dynamicTableData">
            <thead>
            <tr>
                <th></th>
                <th>Redosled</th>
                <th>Naziv atributa (sr)</th>
                <th>Naziv atributa (en)</th>
                <th width="30%">Roditelj</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($chList as $atItem): ?>
            <tr class="gradeA">
                <td><input type="checkbox" class="multipleCharDel" name="chararteristicsCheck[]"
                           value="<?=$atItem->id?>"></td>
                <td><input type="text" name="ordering" value="<?=$atItem->ordering?>" class="ordering<?=$atItem->id?>"
                           style="width:30px;float: none;margin: 0;display: inline;"></td>
                <td><?php if (strpos($atItem->characteristics_name_sr, '<br>') !== false) { ?>
                    <textarea class="charSr<?=$atItem->id?>"
                              style="padding:3px;border-color:#ddd;width: 180px;height: 50px;"><?=str_replace('<br>', '***\n', $atItem->characteristics_name_sr)?></textarea>
                    <?php } else { ?>
                    <input type="text" class="charSr<?=$atItem->id?>"
                           value="<?=$atItem->characteristics_name_sr?>">
                    <?php } ?>
                </td>
                <td>
                    <?php if (strpos($atItem->characteristics_name_en, '<br>') !== false) { ?>
                    <textarea class="charEn<?=$atItem->id?>"
                              style="padding:3px;border-color:#ddd;width: 180px;height: 50px;"><?=str_replace("<br>", "***\n", $atItem->characteristics_name_en)?></textarea>
                    <?php } else { ?>
                    <input type="text" class="charEn<?=$atItem->id?>"
                           value="<?=$atItem->characteristics_name_en?>">
                    <?php } ?>
                </td>
                <td align="center"><a href="#<?=$atItem->id?>"
                                      class="charParent<?=$atItem->id?> characterParent btnIconLeft mr10"
                                      style="padding:2px 7px;min-width: 100px; margin-top:0!important;"><?=Characteristics::find_parent($atItem->category_id)?></a>

                    <div class="charRes<?=$atItem->id?>"></div>
                    <input type="hidden" class="charCategory<?=$atItem->id?>" value="<?=$atItem->category_id?>">
                </td>
                <td width="11%" align="center">
                    <a href="<?=ADMIN?>pages/saveChar.php?id=<?=$atItem->id?>" id="char<?=$atItem->id?>"
                       class="saveCharacteristics"><img src="<?=ADMIN?>doc/images/icons/color/tick.png" alt=""
                                                        class="mr10"></a>
                    <a class="deleteCharacteristics"
                       href="<?=ADMIN?>pages/deleteCharacteristic.php?id=<?=$atItem->id?>&type=charDetail"><img
                            src="<?=ADMIN?>doc/images/icons/color/cross.png" alt="" class="mr10"></a>

                </td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <input type="hidden" class="multipleDeleteChars">

    </div>
    <p class="note">Napomena: za novi red u opisu karakteristika, koristite ***</p>
    <a href="<?=ADMIN?>pages/deleteSelChars.php?selected=" class="btnIconLeft mr10 multipleDeleteChars"
       style="padding:.5em;margin:1em 0 0 0">Delete selected</a>

    <div class="table">
        <?php $chList = CharacteristicsCategory::find_all() ?>
        <div class="head"><h5 class="iFrames">Lista kategorija karakteristika
            <a href="#" class="addCharCategory">Dodaj kategoriju karakteristika</a>
        </h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display inputOnly noInputMargin dynamicTableData">
            <thead>
            <tr>
                <th></th>
                <th>Naziv kategorije (sr)</th>
                <th>Naziv kategorija (en)</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($chList as $atItem): ?>
            <tr class="gradeA">
                <td><input type="checkbox" class="multipleCharCatDel" name="chararteristicsCatCheck[]"
                           value="<?=$atItem->id?>"></td>
                <td><input type="text" class="charCatSr<?=$atItem->id?>"
                           value="<?=$atItem->category_name_sr?>"></td>
                <td><input type="text" class="charCatEn<?=$atItem->id?>"
                           value="<?=$atItem->category_name_en?>">


                    <input type="hidden" class="charCategory<?=$atItem->id?>" value="<?=$atItem->category_id?>">
                </td>
                <td width="11%" align="center">
                    <div class="charCatRes<?=$atItem->id?>">

                    </div>
                    <a href="<?=ADMIN?>pages/saveCharCat.php?id=<?=$atItem->id?>" id="charCat<?=$atItem->id?>"
                       class="saveCharacteristicsCat"><img src="<?=ADMIN?>doc/images/icons/color/tick.png" alt=""
                                                           class="mr10"></a>
                    <a class="deleteCharacteristics"
                       href="<?=ADMIN?>pages/deleteCharCat.php?id=<?=$atItem->id?>&type=charDetail"><img
                            src="<?=ADMIN?>doc/images/icons/color/cross.png" alt="" class="mr10"></a>

                </td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <input type="hidden" class="multipleDeleteChars">

    </div>
    <a href="<?=ADMIN?>pages/deleteSelChars.php?selected=" class="btnIconLeft mr10 multipleDeleteChars"
       style="padding:.5em;margin:1em 0 0 0">Delete selected</a>

</div>
</div>
</div>

<?php /*

<!-- Dynamic table -->




<div class="title" style="margin-top: 40px">
    <h5>Karakteristike</h5>
</div>
<div class="table" style="margin-top: 1em">
    <ul class="tabs" style="margin-top: 1em">
        <li><a href="#tab1">Opsta podesavanja</a></li>
        <li><a href="#tab2">Karakteristike</a></li>
        <li><a href="#tab3">Atributi</a></li>
    </ul>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            tab1
        </div>
        <div id="tab2" class="tab_content">
            <div class="table">
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="karakteristike">
                    <thead>
                    <tr>
                        <th>Ime karakteristika (SR)</th>
                        <th>Ime karakteristika (EN)</th>
                        <th>&nbsp; &nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $characteristics = Characteristics::find_all();

                    if (count($characteristics) > 0) {
                        foreach ($characteristics as $characteristicsRecord) {

                            ?>
                        <tr class="gradeA" id="<?php $characteristicsRecord->id ?>">
                            <td align="center"><input type="text" name="charTextSr[]"
                                                      value="<?php echo $characteristicsRecord->characteristics_name_sr;?>">
                            </td>
                            <td align="center"><input type="text" name="charTextEn[]"
                                                      value="<?php echo $characteristicsRecord->characteristics_name_en;?>">
                            </td>
                            <td align="center">
                                <a href="<?=ADMIN?>pages/updateCharacteristic.php?id=<?=$characteristicsRecord->id?>?characteristics_name_sr=<?=$characteristicsRecord->characteristics_name_sr; ?>?characteristics_name_en=<?= $characteristicsRecord->characteristics_name_en;?>"
                                   class="delItem">Save</a>&nbsp;&nbsp;&nbsp;
                                <a href="<?=ADMIN?>pages/deleteCharacteristic.php?id=<?=$characteristicsRecord->id?> class="
                                   delItem">Delete</a></td>
                        </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="kategorije">
                    <thead>
                    <tr>
                        <th>Ime kategorija (SR)</th>
                        <th>Ime kategorija (EN)</th>
                        <th>&nbsp; &nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $category = CharacteristicsCategory::find_all();

                    if (count($category) > 0) {
                        foreach ($category as $categoryRecord) {

                            ?>
                        <tr class="gradeA" id="<?php $categoryRecord->id ?>">
                            <td align="center"><input type="text" name="categoryTextSr[]"
                                                      value="<?php echo $categoryRecord->category_name_sr;?>">
                            </td>
                            <td align="center"><input type="text" name="categoryTextEn[]"
                                                      value="<?php echo $categoryRecord->category_name_en;?>">
                            </td>
                            <td align="center">
                                <a href="<?=ADMIN?>pages/updateCharacteristic.php?id=<?=$categoryRecord->id?>?characteristics_name_sr=<?=$characteristicsRecord->characteristics_name_sr; ?>?characteristics_name_en=<?= $characteristicsRecord->characteristics_name_en;?>"
                                   class="delItem">Save</a>&nbsp;&nbsp;&nbsp;
                                <a href="<?=ADMIN?>pages/deleteCharacteristic.php?id=<?=$characteristicsRecord->id?> class="
                                   delItem"> Delete</a></td>
                        </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
        <div id="tab3" class="tab_content">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="productDetailSetting">
                <thead>
                <tr>
                    <th>Detalji proizvoda (SR)</th>
                    <th>Detalji (EN)</th>
                    <th>&nbsp; &nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $productDetailSetting = ProductDetailsSettings::find_all();

                if (count($productDetailSetting) > 0) {
                    foreach ($productDetailSetting as $productDetailSettingRecord) {

                        ?>
                    <tr class="gradeA" id="<?php $productDetailSettingRecord->id ?>">
                        <td align="center"><input type="text" name="productTextSr[]"
                                                  value="<?php echo $productDetailSettingRecord->name_sr;?>">
                        </td>
                        <td align="center"><input type="text" name="productTextEn[]"
                                                  value="<?php echo $productDetailSettingRecord->name_en;?>">
                        </td>
                        <td align="center">
                            <a href="<?=ADMIN?>pages/updateCharacteristic.php?id=<?=$categoryRecord->id?>?characteristics_name_sr=<?=$characteristicsRecord->characteristics_name_sr; ?>?characteristics_name_en=<?= $characteristicsRecord->characteristics_name_en;?>"
                               class="delItem">Save</a>&nbsp;&nbsp;&nbsp;
                            <a href="<?=ADMIN?>pages/deleteCharacteristic.php?id=<?=$characteristicsRecord->id?> class="
                               delItem"> Delete</a></td>
                    </tr>
                        <?php
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>

    <input type="submit" value="Sačuvaj" class="greyishBtn submitForm " name="saveAllSettings"
           style="margin-top: 1em;">
    </form>
 */ ?>
</div></div>
