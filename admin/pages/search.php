<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
$searchTerm = "Unesite pojam za pretragu";
if (isset($_POST['searchTerm'])) {
    $searchTerm = $_POST['searchTerm'];
}
//array structure / name / category(type) / link / excerpt
$results = array();
//search for products
$pr = Product::search($searchTerm);
//search for manufactures
$partners = Manufacturer::search($searchTerm);
//
//print_r($pr);
//print_r($partners);
//print_r($categories);

//search
?>
<div class="content">
    <div class="title"><h5>Traženi pojam: <?=$searchTerm?></h5></div>

    <?php
    if (count($partners) < 1 && count($pr) < 1) {
        ?>
        <div class="nNote nWarning hideit">
            <p>Nema rezultata za traženi pojam</p>
        </div>

        <?php } ?>
    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $products = Product::find_by_sql("SELECT *, (select category.name_sr from category where category.id = product.category) as category, (select name_sr from manufacturer where manufacturer.id = product.manufacturer) as manufacturer FROM product order by category ASC, active ASC, name_sr ASC");
    ?>

    <?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>

    <?php if (count($pr) > 0) { ?>

    <div class="widget templateAdd" style="margin-top:10px;position:absolute;top:-1000px">
        <div class="head"><h5 class="iChart8">Kalkulacija cena proizvoda za partnerske popuste/prodaje</h5></div>
        <div class="body" style="height:30px;">
            <form action="<?=ADMIN?>addTemplate" method="post">
                <?php $templates = PriceTemplate::find_all(); ?>
                <label  style="padding-top:5px;margin-right:20px;float:left">Dodelite šablon za kalkulaciju cene </label>
                    <select name="templateID" name="priceTemplate" style="margin:5px 20px 0 30px; float:left;">
                        <option value="0">Izaberite šablon</option>
                        <?php foreach ($templates as $t): ?>
                        <option value="<?=$t->id?>"><?=$t->name?></option>
                        <?php endforeach; ?>
                    </select>
                    <input class="selectedProducts" name="products" type="hidden">
                    <button type="submit" class="blackBtn saveTemplate" style="float:left; margin-left:20px;">Primeni template</button>
                </p>
            </form>
        </div>
    </div>


    <div class="table" style="margin-top:10px">
        <div class="head"><h5 class="iFrames"><a href="#">Rezultati pretrage - Proizvodi</a></h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th width="10%"><a href="#" class="selectAll" style="font-weight: 700;">Select all</a></th>
                <th>Naziv</th>
                <th>Kategorija</th>
                <th>Proizvođač</th>
                <th>Cena</th>
                <th>Link</th>
                <th> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
                <?php
                //products first
                foreach ($products as $p) {
                    $product = Product::find_by_id($p->id);
                    ?>
                <tr class="gradeA" <?=$product->id?>>
                    <td class="center"><input type="checkbox" name="" id="<?=$product->id?>" class="productCheck"></td>
                    <td class="center"><a href="<?=ADMIN?>proizvod/<?=$product->id?>">
                        <strong><?=$product->name_sr?><strong></a></td>
                    <td class="center"><?=getCategoryName($product->category)?></td>
                    <td class="center"><?=$product->manufacturerName?></td>
                    <td class="center"><span class="webStatsLink"><?=$product->price?></span></td>
                    <td class="center"><a href="<?=ADMIN?>proizvod/<?=$product->id?>">Detaljnije</a></td>
                    <td class="center">
                        <a href="<?=ADMIN?>proizvod/<?=$product->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                        <a href="pages/deleteProduct.php?id=<?=$product->id?>" class="delItem"> Delete </a>
                    </td>
                </tr>
                    <?php
                } ?>

            </tbody>
        </table>
    </div>
    <?php
}
    if (count($partners) > 0) {
        ?>
        <div class="table">
            <div class="head"><h5 class="iFrames"><a href="#">Rezultati pretrage Partneri</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="partner">
                <thead>
                <tr>
                    <th>Naziv</th>
                    <th>Ukupno proizvoda</th>
                    <th>Link</th>
                    <th> &nbsp;&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    //products first
                    foreach ($partners as $p) {
                        $partner = Manufacturer::find_by_id($p->id);
                        ?>
                    <tr class="gradeA" <?=$partner->id?>>
                        <td class="center"><a href="<?=ADMIN?>proizvod/<?=$partner->id?>">
                            <strong><?=$partner->name_sr?><strong></a></td>
                        <td class="center" width="100px"><?=$partner->totalProducts($partner->id)?></td>
                        <td class="center"><a href="<?=ADMIN?>partner/<?=$partner->id?>">Detaljnije</a></td>
                        <td class="center">
                            <a href="<?=ADMIN?>proizvod/<?=$partner->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                            <a href="pages/deleteProduct.php?id=<?=$partner->id?>" class="delItem"> Delete </a>
                        </td>
                    </tr>
                        <?php
                    } ?>

                </tbody>
            </table>
        </div>
        <?php } ?>

</div>
<div class="fix"></div>
</div>

