<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 11:32 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
$products = explode(',',$_POST['products']);

foreach($products as $key=>$value){
    $pr = Product::find_by_id($value);
    $pr->template = $_POST['templateID'];
    $pr->save();
    /// obrisi prethodno kreirane cene
    $priceList = Price::find_by_product($pr->id);
    foreach($priceList as $p){
        $p->delete();
    }
    //dodaj nove cene za izabrani proizvod

}