<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
//import setup file for configuration
require_once('../../framework/lib/setup.php');
if (!isset($_POST['product']) || $_POST['product'] == '') {
    $_POST['product'] = 'novi-proizvod';
}


//Array ( [attributName] => fasfasdfasdf [attributNameEng] => asdfasdf [attributValue] => asdfasdfas [attributValueEng] => asdfasdf [parent] => 15 )
if (isset($_POST) && isset($_POST['settings'])) {
    $attrS = new ProductDetailsSettings();
    $attrS->name_en = $_POST['attributNameEng'];
    $attrS->name_sr = $_POST['attributName'];
    if (isset($_POST['parent'])) {
        $attrS->parent = implode(',', $_POST['parent']);
    }

    if ($attrS->save()) {
        $_SESSION['mType'] = 2;
        $session->message("Novi detalj je uspešno sačuvan");
        if ($_POST['product'] == 'settings') {
            redirect_to(ADMIN . 'podesavanja');
        }
        redirect_to(ADMIN . 'proizvod/' . $_POST['product']);
    } else {
        $_SESSION['mType'] = 4;
        $session->message("Imamo problem. Detalj nije sačuvan");
        if ($_POST['product'] == 'settings') {
            redirect_to(ADMIN . 'podesavanja');
        }
        redirect_to(ADMIN . 'proizvod/' . $_POST['product']);
    }

}
if (isset($_POST) && isset($_POST['attribut'])) {
    $attr = new ProductDetails();
    $attr->value_en = $_POST['attributValueEng'];
    $attr->value_sr = $_POST['attributValue'];
    $attr->detailID = $_POST['parent'];
    if ($attr->save()) {
        $_SESSION['mType'] = 2;
        $session->message("Osobina je uspešno sačuvana");
        if ($_POST['product'] == 'settings') {
            redirect_to(ADMIN . 'podesavanja');
        }
        redirect_to(ADMIN . 'proizvod/' . $_POST['product']);
    } else {
        $_SESSION['mType'] = 4;
        $session->message("Imamo problem. Osobina nije sačuvana");
        if ($_POST['product'] == 'settings') {
            redirect_to(ADMIN . 'podesavanja');
        }
        redirect_to(ADMIN . 'proizvod/' . $_POST['product']);
    }
}