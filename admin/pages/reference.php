<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); 

if($_GET['id']==0){
    $reference = new Reference();
}else{
    $reference = Reference::find_by_id($_GET['id']);
}
?>
	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>	
    	    <?php 
            if($_GET['id']==0){
                ?>Nova referenca<?
            }else{
                ?>Izmena reference<?
            }
          ?>
    	</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<form class="mainForm" action="<?=ADMIN?>saveReference" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
           <?php if($session->message()!=''){
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p  class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>
        <div class="widget">       
             <ul class="tabs">
                   <li><a href="#tab1">Srpski</a></li>
                   <li><a href="#tab2">Engleski</a></li>
                   <li><a href="#tab3">Galerija</a></li>
                   <li><a href="#tab4">Seo</a></li>
              </ul>
                    
              <div class="tab_container">
              			
              		<input type="hidden" name="id" value="<?=$reference->id?>"/>
              		
                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                    	 <div class="rowElem" style=" width:47%;float:left;">
	                        <label>Aktivan:</label>
                            <div class="formRight">
	                               <input type="checkbox" name="active" 
	                               		<?php if($reference->active == 1){ echo 'checked'; } ?>
	                               /><br>
                            </div>
	                        <div class="fix"></div>
                    	</div>
                    	 <div class="rowElem">
                    		<label>Headiline:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="headline" ><?=$reference->headline?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Subline:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="subline" ><?=$reference->subline?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                   		<div class="rowElem noborder">
                        <label>Datum:</label>
                        <div class="formRight">
                        	<?php 
                        	if($reference->date != ''){
                        		$date = date("d-m-Y", strtotime($reference->date));
                        	}else{
                        		$date = '';
                        	}
                        	?>
                            <input type="text" name="date" class="datepicker" value="<?=$date?>"/>
                        </div>
                        <div class="fix"></div>
                    </div>                        	         	
                    	<div class="rowElem noborder">
                    		<label>Url:</label>
                    		<div class="formRight">
                    			<input type="text" name="url" value="<?=$reference->url?>"/>
                    		</div>
                    		<div class="fix"></div>
						</div>

                    </div>  
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">                   
                   		<div class="rowElem">
                    		<label>Headiline:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="headline_en" ><?=$reference->headline_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Subline:</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="subline_en" ><?=$reference->subline_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>                  	
                    	
                    </div>
                    <!-- End - tab2 --> 
                    
                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">
			          <fieldset>
			            <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
			            <input type="hidden" name="" value="1" class="current" />
			             <input id="file_upload" class="fileInput" type="file" name="image0" />
			           </fieldset>
			           <fieldset id="galleryImages">
			              <?php
			              //$_GET['page'] = $event->event_type; 
			                require_once(ROOT_DIR.'admin/pages/galleryList.php');
			              ?>
			           </fieldset>
                    </div>
				<!--  End - tab3  --> 
				                
                  <!--    Content of tab4 --> 
                    <div id="tab4" class="tab_content">                     
                    	<div class="rowElem noborder">
                    		<label>Headline(srpski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_sr" value="<?=$reference->seo_title_sr?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem noborder">
                    		<label>Headline(engleski):</label>
                    		<div class="formRight">
                    			<input type="text" name="seo_title_en" value="<?=$reference->seo_title_en?>">
                    		</div>
                    		<div class="fix"></div>
                    	</div>
                        <div class="rowElem">
                    		<label>Opis strane za pretraživače(srpski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_sr" ><?=$reference->seo_desc_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>Opis strane za pretraživače(engleski):</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_desc_en" ><?=$reference->seo_desc_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(srpski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_sr" ><?=$reference->seo_keywords_sr?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    	<div class="rowElem">
                    		<label>SEO ključne reči(engleski) :</label>
                    		<div class="formRight">
                    			<textarea rows="8" cols="" name="seo_keywords_en" ><?=$reference->seo_keywords_en?>
                    			</textarea></div>
                    		<div class="fix"></div>
                    	</div>
                    
                    </div>
                    <!-- End - tab4 -->
                    
               </div>	
               <div class="fix"></div>		 
         </div>
         <!-- End - Tabs -->
		 </fieldset>
		 <a href="<?=ADMIN?>reference"><input type="button" value="Otkazi" class="greyishBtn submitForm" /></a>
		 <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm" />		 
		 </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>
    	