<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/1/12
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
$id = $_GET['id'];
$pld  = '';
if(isset($_GET['plD'])){
	$pld = $_GET['plD'];
}
$tProduct = TempProduct::find_by_id($id);
// $tProduct->plD = $_GET['plD'];

if(!Product::authenticate($pld)){
	$p = new Product();
	foreach($p as $key=>$value){
		if($key!='id'){
			$p->$key = $tProduct->$key;
		}
	}
	if($p->save()){
		$tProduct->delete();
		echo "<div class=\"nNote nSuccess hideit\">
                <p><strong>URAĐENO: </strong>Proizvod je aktiviran</p>

            </div>";
	}
}else{
	echo "
	<div class=\"nNote nFailure hideit\">
                <p><strong>GREŠKA: </strong>Proizvod sa izabranim internim kat. brojem već postoji</p>
            </div>
            ";
}