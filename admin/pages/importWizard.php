<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/27/12
 * Time: 8:01 AM
 * To change this template use File | Settings | File Templates.
 */
require_once '../../framework/lib/setup.php';

$file = $_FILES['file'];
$error  = $_FILES["file"]["error"];
if($error == UPLOAD_ERR_OK) {
    $tmp_name = $_FILES["file"]["tmp_name"];
    $name = "cenovnik_".time().'.xls';
    $uploads_dir = ROOT_DIR.'admin/uploadedFiles';
    if(move_uploaded_file($tmp_name, "$uploads_dir/$name")){
        redirect_to(ADMIN."pages/readFile.php?file=".$name.'&manufacturer='.$_POST['manufacturer'].'&category='.$_POST['groupID']);
    }
}