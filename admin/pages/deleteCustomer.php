<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $customer = Customer::find_by_id($_GET['id']);
    
if($customer->delete()){
  $session->message('Klijent je izbrisan');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'klijenti');
  
}else{
  $session->message('Postoji problem. Klijent nije izbrisan');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'klijenti');
}
}

?>