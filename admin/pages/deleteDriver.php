<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $news = Driver::find_by_id($_GET['id']);
    
if($news->delete()){
  $session->message('Driver je izbrisan');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'driveri');
  
}else{
  $session->message('Postoji problem. Driver nije izbrisan');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'driver/'.$news->id);
}
}

?>