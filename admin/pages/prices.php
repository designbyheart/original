<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>

<!-- Content -->
<div class="content">
    <div class="title"><h5> Šabloni za cenovnik i grupe za popust </h5></div>
    <?php
    $products = PriceTemplate::find_all();

    ?>

    <?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}                      ?>

    <!-- Dynamic table -->
    <?php
    $templates = PriceTemplate::find_all();
    $groups = PriceGroup::find_all();
    $mainGroups = PriceGroup::find_main();
    ?>
    <div class="nNote nInformation hideit">
        <p><strong>NAPOMENA: </strong>Kreirajte šablone za brzo dodeljivanje popusta/provizija grupama klijenata. <br>
            Cene će se obračunavati automatski na osnovnu cenu, nakon dodeljivanja šablona proizvoda.</p>
    </div>
    <div class="table" style="margin-top:15px;">
        <div class="head"><h5 class="iFrames">Šabloni cenovnika</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display">
            <thead>
            <tr>
                <th class="center">Naziv šablona</th>
                <th class="center">Vrsta transakcije</th>
                <th>Roditelj</th>
                <th>Procenat</th>
                <th>Država</th>
                <th>Tax</th>
                <th></th>
                <th width="40px"> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($templates as $t) { ?>
            <tr class="gradeA" <?=$t->id?>>
                <form class="mainForm" action="<?=ADMIN?>saveTemplateValues" id="form<?=$t->id?>" method="POST">
                    <input type="hidden" name="templateID" value="<?=$t->id?>">
                    <td class="center"><strong style="font-size:1.2em"><?=$t->name?></strong>
                    <?php //echo '<pre>';
                    //print_r($t);
//                    echo '</pre>';?>
                    </td>
                    <td class="center resultTemplate<?=$t->id?>"></td>
                    <td class="center"></td>
                    <td>&nbsp;</td>
                    <td>
                        <select name="country" id="state[<?=$t->id?>]" style="margin:0!important;position:relative; top:-8px; ">
                            <option value="0">Izaberi državu..</option>
                            <?php
                                $states = Country::find_active();
                            foreach($states as $s){ ?>
                                <option value="<?=$s->id?>" <?=selected($t->country, $s->id);?>><?=$s->name?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td><input type="text" name="templateTax" class="textField" value="<?=$t->tax?>" style="margin:0; position:relative; top:.3em;float:left;width:50px;"></td>
                    <td class="center" style="height:30px; width:120px; overflow:hidden;">
                        <input type="submit" class="btn14 mr5 saveTemplate"  id="<?=$t->id?>"
                               style="float:left;margin-top:.3em;padding:4px 10px!important;" value="Save"/>
                        <a href="#" title="" class="btn14 mr5 delItem"
                           style="margin-top:3px;padding:0px 10px!important;float:left; color:#8b0000;">Delete</a>
                    </td>

                    </tr>
                <tr>
                    <?php $group = PriceGroup::find_main();
                    foreach ($group as $g):?>
                        <tr>
                            <td><?=$g->name;?></td>
                            <td><select name="type[<?=$g->id?>]" id="templateGroup[<?=$t->id?>][<?=$g->id?>]">
                                <option value="0">Fiksna cena</option>
                                <option value="1" <?php if ($t && $t->type == '1') {
                                    echo 'selected';
                                }?>>Oduzimanje
                                </option>
                                <option value="2" <?php if ($t && $t->type == '2') {
                                    echo 'selected';
                                }?>>Dodavanje
                                </option>
                            </select></td>
                            <td>
                                <select name="parent[<?=$g->id?>]" id="parent<?=$t->id?>">
                                    <option value="0">Proizvod</option>
                                    <?php foreach ($group as $parentGroup):
                                    if ($parentGroup->id != $g->id) {
                                        ?>
                                        <option value="<?=$parentGroup->id?>"><?=$parentGroup->name?></option>
                                        <?php
                                    }
                                endforeach; ?>
                                </select>
                                <input type="hidden" name="groupID" value="<?=$g->id?>">

                            </td>
                            <td><input type="text" name="groupPercentage[<?=$g->id?>]" class="text"
                                       value="<?=$g->percentage?>" style="margin:0!important;width:50px;position:relative; left:-.6em!important;top:0;"></td>
                            <td></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <? endforeach;
                    ?>
            </tr>
            </form>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>

    <div class="widget" style="margin-top:5px">
        <?php
        $state = 'closed';
        $h = 30;
        if ($session->message() != '' && ($session->message() == 'Unesite ime novog šablona' || $session->message() == 'Šablon sa ovim imenom je već unet')) {
            $state = 'opened';
            $h = 50;
        }
        ?>
        <div class="head <?=$state?> inactive" id="opened"><h5>Dodajte novi šablon</h5></div>
        <div class="body" style="">
            <form action="<?=ADMIN?>saveTemplate" method='post' class="mainForm" style="height:<?=$h?>px">
                <?php if ($session->message() != '' && ($session->message() == 'Unesite ime novog šablona' || $session->message() == 'Šablon sa ovim imenom je već unet')) { ?>
                <span style="color:red; width:100%;float:left;"><?=$session->message()?></span>
                <?php } ?>
                <input type="text" name="templateName" class="autoF" style="width:45%; float:left">
                <span style="float:left; width:80px;padding:0 5px 0 5px;"><strong
                        style="padding:5px;position:relative;top:4px;">Oduzmi</strong> <input type="radio" name="type[<?=$g->id?>]"
                                                                                              value="1"/></span>
                <span style="float:left; width:70px;padding:0 5px 0 5px"><strong
                        style="padding:5px;position:relative;top:4px;">Dodaj</strong> <input type="radio" name="type"
                                                                                             value="2"/></span>
                <span style="float:left; width:100px;padding:0 5px 0 5px;"><strong
                        style="padding:5px;position:relative;top:4px;">Fiksna cena</strong> <input type="radio"
                                                                                                   name="type"
                                                                                                   value="0"/></span>
                <input type="submit" value="Sačuvaj" class="greyishBtn submitForm">
            </form>
        </div>
    </div>
    <div class="nNote nInformation hideit">
        <p><strong>NAPOMENA: </strong>Kreirajte klijentske grupe za dodelu različitog procenta popusta/provizije.
        </p>
    </div>
    <div class="table" style="margin-top: 15px">
        <div class="head"><h5 class="iFrames">Cenovne grupe</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="partner">
            <thead>
            <tr>
                <th>Naziv grupe</th>
                <th> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($mainGroups as $gr) { ?>
            <tr class="gradeA" <?=$gr->id?>>
                <td class="center" style="padding-left:15px;">
                    <strong><?=$gr->name?><strong></td>
                <td class="center">
                    <a href="pages/deleteGroup.php?groupID=<?=$gr->id?>" class="btn14 mr5 delItem"
                       style="margin-top:3px;padding:0px 10px!important; color:#8b0000;"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>
    <div class="widget" style="margin-top:5px">
        <?php
        $state = 'closed';
        if ($session->message() != '' && ($session->message() == 'Unesite ime nove grupe' || $session->message() == 'Grupa sa ovim imenom je već uneta')) {
            $state = 'opened';
        }                                      ?>
        <div class="head <?=$state?> inactive" id="opened"><h5>Dodajte novu grupu</h5></div>
        <div class="body" style="">
            <form action="<?=ADMIN?>saveGroup" method='post' class="mainForm">
                <?php if ($session->message() != '' && ($session->message() == 'Unesite ime nove grupe' || $session->message() == 'Grupa sa ovim imenom je već uneta')) { ?>
                <span style="color:red"><?=$session->message()?></span>
                <?php } ?>
                <input type="text" name="groupName" class="autoF" style="width:85%">
                <input type="submit" value="Sačuvaj" class="greyishBtn submitForm">
            </form>
        </div>
    </div>
</div>
<div class="fix"></div>
</div>
