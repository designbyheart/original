<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');

if ($_GET['id'] == 0) {
    $category = new TicketsCategory();
} else {
    $category = TicketsCategory::find_by_id($_GET['id']);
}
?>
<!-- Content -->
<div class="content">
    <div class="title"><h5>
        <?php
        if ($_GET['id'] == 0) {
            ?>Nova kategorija tiketa<?
        } else {
            ?>Izmena tiket kategorije<?
        }
        ?>
    </h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->


    <form class="mainForm" action="<?=ADMIN?>saveTicketCategory" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
            <?php if ($session->message() != '') {
            if ($_SESSION['mType'] == 2) {
                $messageType = 'valid';
            } else {
                $messageType = 'invalid';
            }
            echo '<p  class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
        }?>
            <div class="widget">
                <ul class="tabs">
                    <li><a href="#tab1">Srpski</a></li>
                    <li><a href="#tab2">Engleski</a></li>
                </ul>

                <div class="tab_container">

                    <input type="hidden" name="id" value="<?=$category->id?>"/>

                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naziv kategorije:</label>

                            <div class="formRight">
                                <input type="text" name="name_sr" value="<?=$category->name_sr?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>


                        <div class="rowElem">
                            <label>Aktivan:</label>

                            <div class="formRight">
                                <input type="checkbox" name="active"
                                    <?php if ($category->active == 1) {
                                    echo 'checked';
                                } ?>  />
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>Redosled:</label>

                            <div class="formRight">
                                <input type="text" name="ordering" value="<?=$category->ordering?>"
                                       style="width:30px; text-align:center;float:left; position:relative;left:-22px;font-size:16px"/>
                            </div>
                            <div class="fix"></div>
                        </div>


                    </div>
                    <!-- End - tab1 -->


                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naziv kategorije:</label>

                            <div class="formRight">
                                <input type="text" name="name_en" value="<?=$category->name_en?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>
                    </div>
                    <!-- End - tab2 -->

                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">
                        <fieldset>
                            <legend>Upload fotografije za meni</legend>
                            <input type="hidden" name="" value="1" class="current"/>
                            <input id="file_upload" class="fileInput" type="file" name="image0"/>

                            <div class="fields"></div>
                            <br/>
                        </fieldset>
                        <fieldset id="galleryImages">
                            <?php
                            $img_type = 'cat-menu';
                            require(ROOT_DIR . "admin/pages/galleryList.php");
                            ?>
                        </fieldset>

                        <fieldset>
                            <legend>Upload fotografije za prikaz na strani</legend>
                            <input type="hidden" name="" value="1" class="current"/>
                            <input id="file_upload" class="fileInput" type="file" name="image1"/>

                            <div class="fields"></div>
                            <br/>
                        </fieldset>
                        <fieldset id="galleryImages">
                            <?php
                            $img_type = 'cat';
                            require(ROOT_DIR . 'admin/pages/galleryList.php');
                            ?>
                        </fieldset>
                    </div>
                    <!-- End - tab3 -->

                    <!-- Content of tab4 -->
                    <div id="tab4" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naziv kategorije(srpski):</label>

                            <div class="formRight">
                                <input type="text" name="seo_title_sr" value="<?=$category->seo_title_sr?>">
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem noborder">
                            <label>Naziv kategorije(engleski):</label>

                            <div class="formRight">
                                <input type="text" name="seo_title_en" value="<?=$category->seo_title_en?>">
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>Opis strane za pretraživače(srpski):</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_desc_sr"><?=$category->seo_desc_sr?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>Opis strane za pretraživače(engleski):</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_desc_en"><?=$category->seo_desc_en?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>SEO ključne reči(srpski) :</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_keywords_sr"><?=$category->seo_keywords_sr?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>SEO ključne reči(engleski) :</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_keywords_en"><?=$category->seo_keywords_en?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>

                    </div>
                    <!-- End - tab4 -->

                </div>
                <div class="fix"></div>
            </div>
            <!-- End - Tabs -->
        </fieldset>
        <a href="<?=ADMIN?>kategorije"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
        <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
    </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a
                href="http://designbyheart.info">Design by Heart</a></span>


