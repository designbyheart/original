<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/4/12
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
require_once '../../framework/lib/setup.php';
$pD = new ProductDetailsSettings();

if(ProductDetailsSettings::isExisting($_POST['newDetail_sr'])){
    $session->message('Ovaj detalj već postoji');
        redirect_to(ADMIN.'izmena-detalja-proizvoda');
}else{
    $pD->name_sr = $_POST['newDetail_sr'];
    $pD->name_en = $_POST['newDetail_en'];
    $pD->save();
        redirect_to(ADMIN.'izmena-detalja-proizvoda');
}

