<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/20/12
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if (isset($_GET['id']) && isset($_GET['type'])) {
    if ($_GET['type'] == 'charDetail') {
        $p = Characteristics::find_by_id($_GET['id']);
        $p->delete();

        $_SESSION['mType'] = 2;
        $session->message("Karakteristika je obrisana");
        redirect_to(ADMIN . 'podesavanja#tab3');
    }
    if ($_GET['type'] == 'charSettings') {
        $p = CharacteristicsCategory::find_by_id($_GET['id']);
        $p->delete();

        $_SESSION['mType'] = 2;
        $session->message("Grupa karakteristika je obrisana");
        redirect_to(ADMIN . 'podesavanja#tab3');
    }
}