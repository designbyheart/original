<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/1/12
 * Time: 2:02 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');

$countries = Country::find_all();
foreach($countries as $c){
    if(isset($_POST[$c->id])){
        $c->active = 1;
    }else{
        $c->active = 0;
    }
    $c->save();
}
redirect_to(ADMIN.'podesavanja');