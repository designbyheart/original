<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if (isset($_GET['templateID']) && $_GET['templateID'] != '') {

    $template = PriceTemplate::find_by_id($_GET['templateID']);
    if ($template && $template->delete()) {
        $session->message('Šabon je uspešno obrisan');
        redirect_to(ADMIN . 'cenovnik');
    } else {
        $session->message('Pojavila se greška. Šablon nije obrisan');
        redirect_to(ADMIN . 'cenovnik');
    }
}