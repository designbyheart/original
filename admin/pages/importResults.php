<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
if (!isset($_SESSION['manufacturer'])) {
    $_SESSION['manufacturer'] = '';
}
if (!isset($_SESSION['category'])) {
    $_SESSION['category'] = '';
}
?>

<!-- Content -->


<div class="content">
    <div class="result"></div>
    <div class="title"><h5>Rezultati unosa</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $products = TempProduct::find_by_sql("SELECT *, (select category.name_sr from category where category.id = tempProduct.category) as category,
    (select name_sr from manufacturer where manufacturer.id = tempProduct.manufacturer) as manufacturer FROM tempProduct order by category ASC, active ASC, name_sr ASC");
    ?>
    <?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>


    <div class="stats">
        <ul>
            <li><a href="<?=ADMIN.'proizvodi/updated'?>" class="count grey" title=""><?=Product::countProducts('updated')?></a><span>Importovani proizvodi</span>
            </li>

            <li><a href="#" class="count blue" title=""><?=TempProduct::countProducts('updated')?></a><span>Nekompletirani proizvodi</span>
            </li>
            <li><a href="#" class="count grey"
                   title=""><?=TempProduct::countProducts('endOfLife') + Product::countProducts('endOfLife')?></a><span>End of life <br>proizvodi</span>
            </li>
            <li class="last"><a href="#" class="count red"
                                title=""><?=Product::countProducts('notUpdated', $_SESSION['category'], $_SESSION['manufacturer'])?></a><span>Preostali proizvodi (nisu u cenovniku)</span>
            </li>
        </ul>
        <div class="fix"></div>
    </div>
    <!-- Dynamic table -->
    <div class="table" style="margin-bottom: 1em; width:100%; float:left;">
        <div class="head"><h5 class="iFrames"><a href="<?=ADMIN?>proizvodi">Svi aktivni proizvodi</a></h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th><a href="#" class="selectAll">Select all</a></th>
                <th>Naziv</th>
                <th>Kategorija</th>
                <th>Privremeni Interni kat. broj</th>
                <th>Proizvodjac</th>
                <th>Aktivan</th>
                <th>Cena</th>
                <th> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product) { ?>
            <tr class="gradeA" <?=$product->id?> id="tr<?=$product->id?>">
                <td align="center"><input type="checkbox" name="" id="<?=$product->id?>"
                                          class="productID productCheck"/></td>
                <td class="center"><a href="<?=ADMIN?>uneti-proizvod/<?=$product->id?>">
                    <strong><?=$product->name_sr?><strong></a></td>
                <td class="center"><?=$product->category ?></td>
                <td class="center" style="padding:0">
                    <input type="text" name="plD[<?=$product->id?>]" id="plD<?=$product->id?>" value="<?=$product->plD?>" style="top:7px;width:75%;left:-15px; margin:0!important;text-align:center">
                </td>

                <td class="center"> <?=$product->manufacturer ?></td>
                <td class="center">
                    <input type="checkbox" name="active"
                        <?php if ($product->active == 1) {
                        echo 'checked';
                    }
                        ?>
                            />
                </td>
                <td class="center">
                    <span class="webStatsLink"><?=$product->price?></span>
                </td>
                <td class="center">
                    <a href="<?=$product->id?>" class="saveTempProduct"> Save</a>    &nbsp;&nbsp;
                    <a href="<?=ADMIN?>proizvod/<?=$product->id?>"> Edit </a> &nbsp;&nbsp;
                    <a href="<?=ADMIN?>pages/deleteProduct.php?id=<?=$product->id?>" class="delItem"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>
    <a href="#" class="importTemplate btn14">Import cenovnika</a>
    <a href="#" class="exportTemplate btn14">Download cenovnika</a>
    <a href="#" class="deleteProducts btn14">Izbriši proizvode</a>
</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>

