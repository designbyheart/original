<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $news = News::find_by_id($_GET['id']);
    
if($news->delete()){
  $session->message('Aktuelnost je izbrisana');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'aktuelnosti');
  
}else{
  $session->message('Postoji problem. Aktuelnost nije izbrisana');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'aktuelnosti'); 
}
}

?>