<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/1/12
 * Time: 3:10 AM
 * To change this template use File | Settings | File Templates.
 */
if (isset($_GET['country'])) {
    require_once('../../framework/lib/setup.php');
    $country = Country::find_by_id($_GET['country']);
    if ($country->active == 0) {
        $country->active = 1;
    } else {
        $country->active = 0;
    }
    if ($country->save()) {
        echo 'OK';
    }
}