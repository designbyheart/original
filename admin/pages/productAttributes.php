<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 10/25/12
 * Time: 10:36 AM
 * To change this template use File | Settings | File Templates.
 */

$prodDetails = '';
if (!isset($_GET['productID']) && $product && $product->attributeSet != '0') {
    $attributeSet = AttributeSet::find_by_id($product->attributeSet);

    if ($attributeSet && $attributeSet->details != '') {
        $prodDetails = ProductDetailsSettings::find_by_attributeSet($attributeSet->details);
    }
} else if (isset($_GET['productID'])) {
    require_once('../../framework/lib/setup.php');
    if ($_GET['productID'] > 0) {
        $product = Product::find_by_id($_GET['productID']);
        $attributeSet = AttributeSet::find_by_id($_GET['attributeSet']);
        if ($attributeSet) {
            $prodDetails = ProductDetailsSettings::find_by_attributeSet($attributeSet->details);
        } else {
            $product->attributeSet = $_GET['attributeSet'];
            $product->save();
            $attributeSet = AttributeSet::find_by_id($_GET['attributeSet']);
            if ($attributeSet && $attributeSet->details != '') {
                $prodDetails = ProductDetailsSettings::find_by_attributeSet($attributeSet->details);
            }
        }
    }
}

if (isset($product) && $product) {
    ?>
<table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedProducts"
       id="example"
       style="width:100%!important">
    <thead>
    <tr>
        <td width="5%">Detalj proizvoda</td>
        <td width="20%">Srpski</td>
        <td width="20%">Engleski</td>
    </tr>
    </thead>
    <tbody>
        <?php

        if ($prodDetails && count($prodDetails) > 0) {
            foreach ($prodDetails as $pD):
                $p = ProductDetails::findValueForProduct($product->id, $pD->id);
                if (!$p) {
                    $p = new ProductDetails();
                    $p->id = 0;
                }
                ?>
            <tr>
                <td><?=$pD->name_sr?></td>
                <td><input type="text" value="<?=$p->value_sr?>" name="sr_details[<?=$pD->id?>|||<?=$p->id?>]"
                           style="width:90%;"></td>
                <td><input type="text" value="<?=$p->value_en?>" name="en_details[<?=$pD->id?>|||<?=$p->id?>]"
                           style="width:90%;"></td>
            </tr>
                <?php endforeach;
        } ?>
    </tbody>
</table>
<?php
}
if (isset($_GET['productID']) && $_GET['productID']< 1) {
    $attributeSet = AttributeSet::find_by_id($_GET['attributeSet']);

    if ($attributeSet) {
        $prodDetails = ProductDetailsSettings::find_by_attributeSet($attributeSet->details);
        ?>
    <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedProducts"
           id="example"
           style="width:100%!important">
        <thead>
        <tr>
            <td width="5%">Detalj proizvoda</td>
            <td width="20%">Srpski</td>
            <td width="20%">Engleski</td>
        </tr>
        </thead>
        <tbody>
            <?php

        if ($prodDetails && count($prodDetails) > 0) {
            foreach ($prodDetails as $pD):

                $p = new ProductDetails();
                $p->id = 0;

                ?>
            <tr>
                <td><?=$pD->name_sr?></td>
                <td><input type="text" value="<?=$p->value_sr?>" name="sr_details[<?=$pD->id?>|||<?=$p->id?>]"
                           style="width:90%;"></td>
                <td><input type="text" value="<?=$p->value_en?>" name="en_details[<?=$pD->id?>|||<?=$p->id?>]"
                           style="width:90%;"></td>
            </tr>
                <?php endforeach;
        } ?>
        </tbody>
    </table>
        <?php

    }
}
?>
