<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>

<!-- Content -->
<div class="content">
    <div class="title"><h5>Klijenti</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->



    <?php $newCustomers = Customer::find_by_sql("SELECT * FROM customer where type = 1 order by id ASC"); ?>

    <div class="widget first">
        <div class="head"><h5 class="iUsers">Klijenti na čekanju</h5></div>
        <div id="myList-nav"></div>
        <ul id="myList">

            <?php foreach ($newCustomers as $nC): ?>
        <li>
            <a href="<?=ADMIN?>klijent/<?=$nC->id?>"><strong style="font-size: 120%;;"><?=$nC->name?></strong> firma:
                <span style="font-size: 110%;;"><?=$nC->company?></span> </a>
            <ul class="listData">
                <li><a href="<?=ADMIN?>klijent/<?=$nC->id?>" title=""><?=$nC->email?></a></li>
                <li><span class="red"><?=$nC->phones?></span></li>
                <li><a href="#" id="<?=ADMIN?>pages/updateClient.php?type=approve&id=<?=$nC->id?>"
                       class="updateClient cNote">Odobri</a></li>
                <li><a href="#" id="<?=ADMIN?>pages/updateClient.php?type=rejectClient&id=<?=$nC->id?>"
                       class="updateClient cNote">Odbaci</a></li>
            </ul>
            <?php endforeach; ?>
        </li>
        </ul>
        <div class="fix"></div>
    </div>
    <?php
    $customers = Customer::find_by_sql("SELECT * FROM customer where type = '0' order by name ASC");
    ?>
    <?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
    <!-- Dynamic table -->
    <div class="table">
        <div class="head"><h5 class="iFrames"><a href="<?=ADMIN?>novi-klijent">Dodaj klijenta</a></h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Klijent</th>
                <th>E-mail</th>
                <th>Sajt</th>
                <th>Partner</th>
                <th>PIB</th>
                <th>Status</th>
                <th> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($customers as $customer) { ?>
            <tr class="gradeA" <?=$customer->id?>>
                <td class="center"><a href="<?=ADMIN?>klijent/<?=$customer->id?>">
                    <strong><?php if ($customer->type == 0) {
                        echo $customer->name;
                    } else {
                        echo $customer->company;
                    }?><strong></a>
                </td>
                <td class="center"><?=$customer->email?></td>
                <td class="center"><?=$customer->website?></td>
                <td class="center"><input type="checkbox" name="partner" <?php if ($customer->partner == 1) {
                    echo 'checked';
                }?>></td>
                <td class="center"><?=$customer->company_PIB?></td>
                <td class="center"><?=$customer->status?></td>
                <td class="center">
                    <a href="<?=ADMIN?>klijent/<?=$customer->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                    <a href="pages/deleteCustomer.php?id=<?=$customer->id?>" class="delItem"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>

</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>


