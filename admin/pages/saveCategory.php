<?php
require_once('../../framework/lib/setup.php');

if (isset($_POST['id']) && $_POST['id'] != 0 && Category::find_by_id($_POST['id'])) {
    $category = Category::find_by_id($_POST['id']);
    $new = false;
} else {
    $category = new Category();
    $new = true;
}

if (isset($_POST['active']) && $_POST['active'] == "on") {
    $_POST['active'] = 1;
} else {
    $_POST['active'] = 0;
}
if (isset($_POST['charValues'])) {
    $_POST['charCat'] = implode(',', array_filter($_POST['charValues']));
} else {
    $_POST['charCat'] = "";
}
foreach ($category as $key => $value) {
    // echo $_POST[$key];

    if ($key != 'id' && isset($_POST[$key])) {
        $category->$key = trim($_POST[$key]);
        //echo $category->$key ."<br>";
    }
}

if (isset($_POST['submit'])) {

    if ($category) {
        //save category
        $id = 0;
        $new_id = $category->save();

        // getting id for gallery refID
        if ($_POST['id'] == 0) {
            $id = $new_id;
        } else {
            $id = $_POST['id'];
        }
        if ($id != 0) {
            if (isset($_FILES['image0'])) {
                //image for menu
                $file = $_FILES['image0'];
                if ($file['name'] != '') {
                    $gal = new Gallery();
                    $gal->file = cleanFileName('cat-menu-' . $file['name']);
                    $gal->refID = $id;
                    $gal->type = 'cat-menu';

                    uploadPhoto($file, '', 245, 100, 80, 250, 200, $gal->file);
                    if ($gal->save()) {
                        $session->message('Slika je uneta');
                        $_SESSION['mType'] = 2;
                    } else {
                        $session->message('Postoji problem. Slika nije uneta');
                        $_SESSION['mType'] = 4;
                    }
                }
            }

            if (isset($_FILES['image1'])) {
                //image for category page
                $file = $_FILES['image1'];
                if ($file['name'] != '') {
                    $gal = new Gallery();
                    $gal->file = cleanFileName('cat-' . $file['name']);
                    $gal->refID = $id;
                    $gal->type = 'cat';

                    uploadPhoto($file, '', 370, 100, 80, 250, 200, $gal->file);
                    if ($gal->save()) {
                        $session->message('Slika je uneta');
                        $_SESSION['mType'] = 2;
                    } else {
                        $session->message('Postoji problem. Slika nije uneta');
                        $_SESSION['mType'] = 4;
                    }
                }
            }

        }

        if ($new_id == 1) {
            $session->message('Promene su uspešno sačuvane ');
        } elseif ($new_id > 1) {
            $session->message('Kategorija je sačuvana');
        }

        $_SESSION['mType'] = 2;
        redirect_to(ADMIN . 'kategorije');
    } else {
        $session->message('Postoji problem. Kategorija nije sačuvana');
        $_SESSION['mType'] = 4;
        redirect_to(ADMIN . 'kategorija/' . $category->id);
    }
}
redirect_to(ADMIN . 'kategorija/' . $category->id);

?>