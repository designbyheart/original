<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');

$customer = Customer::find_by_id($_GET['id']);
if (!$customer) {
    $customer = new Customer();
}
?>

<div class="content">
<div class="title"><h5> Detalji klijenta </h5></div>

<!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

<!-- Tabs -->
<fieldset>


<?php if ($session->message() != '') {
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p  class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}
if ($customer->type == 1) {
    ?>
<a href="#" title="" class="btnIconLeft mr10"><img src="<?=ADMIN?>doc/images/icons/dark/like.png" alt=""
                                                   class="icon"><span>Odobri klijenta</span></a>
    <?php } ?>
<a href="#" title="" class="btnIconLeft mr10" style="float:right"><img src="<?=ADMIN?>doc/images/icons/dark/dislike.png"
                                                                       alt="" class="icon"><span>Odbaci klijenta</span></a>

<div class="widget" style="margin-top:10px;">
<ul class="tabs">
    <li><a href="#tab3">Detalji o klijentu</a></li>
    <li><a href="#tab2">Aktivne Porudžbine</a></li>
    <li><a href="#tab1">Istorijat</a></li>
    <?php
    // $orders = CustomerHistory::find_by_client($customer->id);
    //  $orders = CustomerHistory::find_by_sql("SELECT id_order FROM customer_history WHERE id_customer={$customer->id} AND
    //status_order = 'not_completed'");
    //  if($orders){ echo '<li><a href="#tab3">U korpi</a></li>';}
    ?>

</ul>
<div class="tab_container">
<div id="tab3" class="tab_content">
    <form action="<?=ADMIN?>saveCustomer" class="form" method="post">
        <input type="hidden" name="id" value="<?=$customer->id?>">


        <table cellpadding="0" cellspacing="0" border="0" class="display clientDetails">
            <tbody>
            <tr>
                <td colspan="2">
                    <?php $groups = PriceGroup::find_main();?>
                    <label style="position:relative;">Kategorija klijenta
                        <div style="position:absolute;top:-10px; left:150px "><select name="client_category"
                                                                                      class="select"
                                                                                      style="margin-left:6em;">
                            <option value="0">Izaberite kategoriju</option>
                            <?php foreach ($groups as $g): ?>
                            <option value="<?=$g->id?>" <?php if ($g->id == $customer->client_category) {
                                echo 'selected';
                            } ?>><?=$g->name?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </label></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php $groups = PriceGroup::find_main();?>
                    <label style="position:relative;">Klijent je partner
                        <div style="position:absolute;top:-10px; left:150px "><input type="checkbox" name="partner"
                                                                                     id="partner" <?php if ($customer->partner == 1) {
                            echo "checked";
                        }?>>
                        </div>
                    </label></td>
            </tr>
            <tr>
                <td style="width: 20%">Ime:</td>
                <td><input type='text' name="name" value="<?=$customer->name?>">
                    <?php /* if ($customer->type == 0) {
                            echo $customer->name;
                        } else {
                            echo $customer->company;
                        } */?></td>
            </tr>
            <tr>
                <td>Korisničko ime:</td>
                <td><input type="text" name="username" value="<?=$customer->username?>"></td>
            </tr>
            <tr>
                <td>Lozinka:</td>
                <td><input type="password" name="password" value="<?=$customer->password?>"
                           style="margin-left:.4em; border:solid 1px #ddd; padding:.5em"></td>
            <tr>
                <td>Status:</td>
                <td><input type="text" name="status" value="<?=$customer->status?>"></td>
            </tr>

            <tr>
                <td>PIB:</td>
                <td><input type="text" name="company_PIB" value="<?=$customer->company_PIB?>"></td>
            </tr>
            <tr>
                <td>Registracioni broj:</td>
                <td><input type="text" name="company_regNumber" value="<?=$customer->company_regNumber?>"></td>
            </tr>
            <tr>
                <td>Sajt:</td>
                <td><input type="text" name="website" value="<?=$customer->website?>"></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><input type="text" name="email" value="<?=$customer->email?>"></td>
            </tr>
            <tr>
                <td>Kontakt osoba:</td>
                <td><input type="text" name="contact_person" value="<?=$customer->contact_person?>"></td>
            </tr>
            <tr>
                <td>Telefon:</td>
                <td><input type="text" name="phones" value="<?=$customer->phones?>"></td>
            </tr>
            <tr>
                <td>Adresa:</td>
                <td><input type="text" name="address" value="<?=$customer->address?>"></td>
            </tr>
            <tr>
                <td>Grad:</td>
                <td><input type="text" name="city" value="<?=$customer->city?>"></td>
            </tr>
            <tr>
                <td>Država:</td>
                <td><input type="text" name="state" value="<?=$customer->state?>"></td>
            </tr>
            <tr>
                <td>Drugi podaci:</td>
                <td><textarea rows="10" cols="100" name="notes"
                              style="border: solid 1px #ccc"><?=$customer->notes?></textarea></td>
            </tr>
            </tbody>

            <?php /*; ?>
                <p><a href="<?=ADMIN?>poruka_kupcu/<?=$customer->id?>">Pošalji poruku klijentu</a></p>
                        */?>
        </table>
        <p>
            <input type="submit" class="greyishBtn submitForm" value="Sačuvaj izmene"
                   style="float:left; margin-left:18em; margin-top:1em">
        </p>
    </form>
</div>
<div id="tab1" class="tab_content">
    <div class="table">
        <?php $prevOrders = Order::find_by_client($customer->id); ?>
        <table id="ordersHistory" cellpadding="0" cellspacing="0" border="0" class="display clientDetails">
            <tr>
                <th width="15%">Datum</th>
                <th width="15%">Broj</th>
                <th>Proizvodi</th>
                <th width="20%">Cena porudžbine</th>
            </tr>
            <?php foreach ($prevOrders as $pO):
            $producsNames = array();
            $i = 0;
            foreach (array_filter(explode(',', $pO->products)) as $pID) {
                if ($i < 5) {
                    $p = Product::find_by_id($pID);
                    $producsNames[] = trans($p->name_sr, $p->name_en);
                    $i++;
                }
            }
            ?>
            <tr>
                <td><?=date('d.m.y', $pO->orderDate)?></td>
                <td>
                    <a href="<?=SITE_ROOT?>order-info/<?=alphaID(intval($pO->orderID))?>"><?=trans('Br. ', 'No. ') . alphaID(intval($pO->orderID))?></a>
                </td>
                <td><?=implode(',', $producsNames)?> <?=count(explode(',', $pO->products))?> <?=trans('proizvoda', ' products')?></td>
                <td><?=$pO->totalPrice?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<div id="tab2" class="tab_content">
    <!-- Content of tab1 -->
    <div class="table">
        <?php $prevOrders = Order::find_by_client_active($customer->id); ?>
        <table id="ordersHistory" cellpadding="0" cellspacing="0" border="0" class="display clientDetails">
            <tr>
                <th width="15%">Datum</th>
                <th width="15%">Broj</th>
                <th>Proizvodi</th>
                <th width="20%">Cena porudžbine</th>
            </tr>
            <?php foreach ($prevOrders as $pO):
            $producsNames = array();
            $i = 0;
            foreach (array_filter(explode(',', $pO->products)) as $pID) {
                if ($i < 5) {
                    $p = Product::find_by_id($pID);
                    $producsNames[] = '<strong>' . $p->plD . '</strong> ' . trans($p->name_sr, $p->name_en) . '<br>';
                    $i++;
                }
            }
            ?>
            <tr>
                <td><?=date('d.m.y', $pO->orderDate)?></td>
                <td>
                    <a href="<?=SITE_ROOT?>order-info/<?=alphaID(intval($pO->orderID))?>"><?=trans('Br. ', 'No. ') . alphaID(intval($pO->orderID))?></a>
                </td>
                <td><?=implode('', $producsNames)?>
                    (<?=count(explode(',', $pO->products))?> <?=trans('proizvoda', ' products')?>)
                </td>
                <td><?=$pO->totalPrice?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<?php  /*
                      $orders = Order::find_by_client($customer->id);
                      foreach($orders as $order) :
                      ?>
                      <div class="table">
                          <div class="head"><h5 class="iFrames"> Datum porudžbine: <?=date("d-m-Y", strtotime($order->date));?> </h5></div>
                          <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                              <thead>
                              <tr>
                                  <th>Naziv proizvoda</th>
                                  <th>Serijski broj</th>
                                  <th>Model</th>
                                  <th>Kolicina</th>
                              </tr>
                              </thead>
                              <tbody>
                                  <?php
                              if($order && $order->products!=''){
                                  $products = explode(',', $order->products);

                                /*  foreach($products as $p) :
                                      $product_data = explode(':', $p);
                                      $product = array_shift(Product::find_by_sql(
                                          "SELECT name_sr,plD FROM product WHERE id = {$product_data[0]}"));
                                  ?>
                                  <tr class="gradeA" >
                                      <td class="center"><a href="<?=ADMIN?>proizvod/<?=$product_data[0]?>"><?=$product->name_sr ?></a></td>
                                      <td class="center"><?=$product->plD ?> </td>
                                      <td class="center"><?=$product_data[1] ?></td>
                                      <td class="center"><?=$product_data[2] ?></td>
                                  </tr>
                              </tbody>
                              <?php endforeach;
                              }
                                  ?>

                          </table>
                      </div>
                      <?php endforeach; ?>
                  </div>
                    <!-- End - tab1 --> 
                                         
                                         
                    <!-- Content of tab2 -->
                  <div id="tab2" class="tab_content">
                      <?php
                      $orders = CustomerHistory::find_by_client($customer->id);//"SELECT id_order, date_created FROM customer_history WHERE id_customer={$customer->id} AND
                                                                //status_order = 'completed'");
                      foreach($orders as $order) :
                          ?>
                          <div class="table">
                              <div class="head"><h5 class="iFrames"> Datum porudžbine: <?=date("d-m-Y", strtotime($order->date_created));?> </h5></div>
                              <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                                  <thead>
                                  <tr>
                                      <th>Naziv proizvoda</th>
                                      <th>Kategorija</th>
                                      <th>Kolicina</th>
                                  </tr>
                                  </thead>
                              <tbody>
                                  <?php
                                  $products ='';
//                                      CustomerHistory::find_by_sql("SELECT id_product as id,product_cat as category
//                                                    ,quantity FROM customer_history WHERE id_order={$order->id_order}");

                                /*  foreach($products as $p) :
                                      $product = array_shift(Product::find_by_sql(
                                          "SELECT name_sr,plD FROM product WHERE id = {$p->id}"));
                                      ?>
                                  <tr class="gradeA" >
                                      <td class="center"><a href="<?=ADMIN?>proizvod/<?=$p->id?>"><?=$product->name_sr ?></a></td>
                                      <td class="center"><?=$p->category ?> </td>
                                      <td class="center"><?=$p->quantity ?></td>
                                  </tr>
                              </tbody>
                              <?php endforeach;  ?>

                              </table>
                          </div>
                          <?php endforeach; ?>
                      </div>
                    <!-- End - tab2 --> 


                    <!-- Content of tab3 -->
                  <div id="tab3" class="tab_content">
                      <?php
                     // $orders = CustomerHistory::find_by_sql("SELECT id_order, date_created FROM customer_history WHERE id_customer={$customer->id} AND
                       /*                                         status_order = 'not_completed'");
                      foreach($orders as $order) :
                          ?>
                          <div class="table">
                              <div class="head"><h5 class="iFrames"> Datum porudžbine: <?=date("d-m-Y", strtotime($order->date_created));?> </h5></div>
                              <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                                  <thead>
                                  <tr>
                                      <th>Naziv proizvoda</th>
                                      <th>Kategorija</th>
                                      <th>Kolicina</th>
                                  </tr>
                                  </thead>
                              <tbody>
                                  <?php
                                  $products = CustomerHistory::find_by_sql("SELECT id_product as id,product_cat as category
                                                    ,quantity FROM customer_history WHERE id_order={$order->id_order}");

                                  foreach($products as $p) :
                                      $product = array_shift(Product::find_by_sql(
                                          "SELECT name_sr,plD FROM product WHERE id = {$p->id}"));
                                      ?>
                                  <tr class="gradeA" >
                                      <td class="center"><a href="<?=ADMIN?>proizvod/<?=$p->id?>"><?=$product->name_sr ?></a></td>
                                      <td class="center"><?=$p->category ?> </td>
                                      <td class="center"><?=$p->quantity ?></td>
                                  </tr>
                              </tbody>
                              <?php endforeach; ?>

                              </table>
                          </div>
                          <?php endforeach; ?> */ ?>

<!-- End - tab3 -->


<!-- Content of tab4 -->

<!-- End - tab4 -->
<div class="fix"></div>
</div>
<!-- End - Tabs -->
</fieldset>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>


