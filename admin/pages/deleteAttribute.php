<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/20/12
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if (isset($_GET['id']) && isset($_GET['type'])) {
    if ($_GET['type'] == 'attributeDetail') {
        $p = ProductDetails::find_by_id($_GET['id']);
        $p->delete();

        $_SESSION['mType'] = 2;
        $session->message("Detalj je obrisan");
        redirect_to(ADMIN . 'podesavanja#tab2');
    }
    if ($_GET['type'] == 'attributeSettings') {
        $p = ProductDetails::find_by_id($_GET['id']);
        if ($p) {
            $p->delete();
        }


        $_SESSION['mType'] = 2;
        $session->message("Detalj je obrisan");
        redirect_to(ADMIN . 'podesavanja#tab2');
    }
}