<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/14/12
 * Time: 11:21 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if(isset($_POST)){
    $link = new SubMenu();
    if(isset($_POST['linkID']) && $_POST['linkID']!=''){
        $link = SubMenu::find_by_id($_POST['linkID']);
    }
    //Array ( [name_sr] => fasdfsadf [name_en] => asdfsdf [url] => asdfasdf [pageType] => general_page [pageID] => 2 [newLink] => yes [type] => footer [columnT] => 1 )

    $link->pageID = $_POST['pageID'];
    $link->linkSr = $_POST['linkSr'];
    $link->pageType = $_POST['pageType'];
    $link->name_en = $_POST['name_en'];
    $link->name_sr = $_POST['name_sr'];

    if($link->save()){
        $session->message("Link je sačuvan.");
        $_SESSION['mType'] = 2;
    }
    switch($link->pageType){
        case 'general_page':
            redirect_to(ADMIN.'opsta/'.$link->pageID);
            break;
        case 'partner':
            redirect_to(ADMIN.'proizvodjac/'.$link->pageID);
            break;
        case 'product':
            redirect_to(ADMIN.'proizvod/'.$link->pageID);
            break;
    }
}