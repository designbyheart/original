<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if(isset($_POST['groupName']) && $_POST['groupName']!='')  {
    if(PriceGroup::isExists($_POST['groupName']) && !isset($_POST['parent'])){
        $session->message('Grupa sa ovim imenom je već uneta');
        redirect_to(ADMIN.'cenovnik');
    }
    else{
        $p = new PriceGroup();
        $p->name = trim($_POST['groupName']);
        $p->isMain = 1;
        if(isset($_POST['parent'])) {
            $p->parent = 1;
        }
        $p->save();
        redirect_to(ADMIN.'cenovnik');
    }
}else{
    $session->message('Unesite ime nove grupe');
    redirect_to(ADMIN.'cenovnik');

}