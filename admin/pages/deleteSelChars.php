<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/26/12
 * Time: 10:54 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if (isset($_GET)) {
    $ids = array_filter(explode(',', $_GET['selected']));
    foreach ($ids as $id) {
        $c = Characteristics::find_by_id($id);
        if ($c) {
            $c->delete();
        }
    }
}
$_SESSION['tab'] = 3;
redirect_to(ADMIN . 'podesavanja');
