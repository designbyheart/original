<?php 
require_once('../../framework/lib/setup.php');
$gal = Gallery::find_by_id($_GET['img_id']);

if($gal){
    if(file_exists(SITE_ROOT.'images/gallery/'.$gal->file)){
        unlink(ROOT_DIR.'images/gallery/'.$gal->file);
    }
    if(file_exists(SITE_ROOT.'images/gallery/thumbsS/'.$gal->file)){
        unlink(ROOT_DIR.'images/gallery/thumbsS/'.$gal->file);
    }
    if(file_exists(SITE_ROOT.'images/gallery/thumbsM/'.$gal->file)){
        unlink(ROOT_DIR.'images/gallery/thumbsM/'.$gal->file);
    }
    
    if($gal->delete()){
    	$session->message('Slika je izbrisana');
    	$_SESSION['mType']= 2;
    }else{
    	$session->message('Postoji problem. Slika nije obrisana');
    	$_SESSION['mType']= 4;
    }
    
}

 $page = $_GET['page'];
 
 switch ($page) {
 	case 'product': redirect_to(ADMIN.'proizvod/'.$_GET['id']);
 	break;
 	case 'category': redirect_to(ADMIN.'kategorija/'.$_GET['id']);
 	break;
 	case 'aktuelnost': redirect_to(ADMIN.'aktuelnost/'.$_GET['id']);
 	break;
 	case 'reference': redirect_to(ADMIN.'referenca/'.$_GET['id']);
 	break;
 	case 'general_page': redirect_to(ADMIN.'opste_strane');
 	break;
 	
 	default: redirect_to(ADMIN.'dashboard');
 	break;
 }

?>