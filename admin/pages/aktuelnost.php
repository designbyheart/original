<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');

if ($_GET['id'] == 0) {
    $news = new News();
} else {
    $news = News::find_by_id($_GET['id']);
}
?>
<!-- Content -->
<div class="content">
    <div class="title"><h5>
        <?php
        if ($_GET['id'] == 0) {
            ?>Nova aktuelnost<?php
        } else {
            ?>Izmena aktuelnosti<?php
        }
        ?>
    </h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <form class="mainForm" action="<?=ADMIN?>saveNews" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
            <?php if ($session->message() != '') {
            if ($_SESSION['mType'] == 2) {
                $messageType = 'valid';
            } else {
                $messageType = 'invalid';
            }
            echo '<p  class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
        }?>
            <div class="widget">
                <ul class="tabs">
                    <li><a href="#tab1">Srpski</a></li>
                    <li><a href="#tab2">Engleski</a></li>
                    <li><a href="#tab3">Galerija</a></li>
                    <li><a href="#tab4">Seo</a></li>
                </ul>

                <div class="tab_container">

                    <input type="hidden" name="id" value="<?=$news->id?>"/>

                    <!-- Content of tab1 -->
                    <div id="tab1" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naslov:</label>

                            <div class="formRight">
                                <input type="text" name="title" value="<?=$news->title?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem noborder">
                            <label>Datum:</label>

                            <div class="formRight">
                                <?php
                                if ($news->date != '') {
                                    $date = date("d-m-Y", strtotime($news->date));
                                } else {
                                    $date = '';
                                }
                                ?>
                                <input type="text" name="date" class="datepicker" value="<?=$date?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem">
                            <label>Aktiviran:</label>

                            <div class="formRight">
                                <input type="checkbox" name="active"
                                    <?php if ($news->active == 1) {
                                    echo 'checked';
                                }
                                    ?>
                                        />
                            </div>
                            <div class="fix"></div>
                        </div>

                        <div class="rowElem">
                            <div class="formRight" style="width: 100%">
                                <div class="widget" style="margin-top: 5px">
                                    <div class="head"><h5 class="iPencil">Opis aktuelnosti:</h5></div>
                                    <textarea class="wysiwyg" name="content" rows="5" cols="">
                                        <?=$news->content?>
                                    </textarea
                                            ></div>
                            </div>
                            <div class="fix"></div>
                        </div>
                    </div>
                    <!-- End - tab1 -->


                    <!-- Content of tab2 -->
                    <div id="tab2" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naslov:</label>

                            <div class="formRight">
                                <input type="text" name=title_en value="<?=$news->title_en?>"/>
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <div class="formRight" style="width: 100%">
                                <div class="widget" style="margin-top: 5px">
                                    <div class="head"><h5 class="iPencil">Opis aktuelnosti:</h5></div>
                                    <textarea class="wysiwyg" name="content_en" rows="5" cols="">
                                        <?=$news->content_en?>
                                    </textarea
                                            ></div>
                            </div>
                            <div class="fix"></div>
                        </div>
                    </div>
                    <!-- End - tab2 -->

                    <!-- Content of tab3 -->
                    <div id="tab3" class="tab_content">
                        <fieldset>
                            <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
                            <input type="hidden" name="" value="1" class="current"/>
                            <input id="file_upload" class="fileInput" type="file" name="image0"/>

                            <div class="fields"></div>
                            <br/>
                        </fieldset>
                        <fieldset id="galleryImages">
                            <?php
                            //$_GET['page'] = $event->event_type;
                            require_once(ROOT_DIR . 'admin/pages/galleryList.php');
                            ?>
                        </fieldset>
                    </div>
                    <!-- End - tab3 -->

                    <!-- Content of tab4 -->
                    <div id="tab4" class="tab_content">
                        <div class="rowElem noborder">
                            <label>Naziv aktuelnosti(srpski):</label>

                            <div class="formRight">
                                <input type="text" name="seo_title_sr" value="<?=$news->seo_title_sr?>">
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem noborder">
                            <label>Naziv aktuelnosti(engleski):</label>

                            <div class="formRight">
                                <input type="text" name="seo_title_en" value="<?=$news->seo_title_en?>">
                            </div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>Opis strane za pretraživače(srpski):</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_desc_sr"><?=$news->seo_desc_sr?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>Opis strane za pretraživače(engleski):</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_desc_en"><?=$news->seo_desc_en?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>SEO ključne reči(srpski) :</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_keywords_sr"><?=$news->seo_keywords_sr?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>
                        <div class="rowElem">
                            <label>SEO ključne reči(engleski) :</label>

                            <div class="formRight">
                                <textarea rows="8" cols="" name="seo_keywords_en"><?=$news->seo_keywords_en?>
                                </textarea></div>
                            <div class="fix"></div>
                        </div>

                    </div>
                    <!-- End - tab4 -->

                </div>
                <div class="fix"></div>
            </div>
            <!-- End - Tabs -->
        </fieldset>
        <a href="<?=ADMIN?>aktuelnosti"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
        <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
    </form>

</div>
<div class="fix"></div>
</div>

