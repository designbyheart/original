<?php
if ($user->role != 4) {
    require_once(ADMIN_ROOT . 'doc/inc/side.php');
}?>

<!-- Content -->
<div class="content">
    <div class="title"><h5>Dashboard</h5></div>

    <div class="stats">
        <?php
        if ($user->role != 4) {
            $sum_prod = Product::count_active();
            $sum_promo = Promo::count_active();
            $sum_partners = Manufacturer::count_active();
        }
        ?>
        <?php if ($user->role != 4) { ?>
        <ul>

            <li><a href="<?=ADMIN?>porudzbine" class="count grey" title=""><?=VisitLog::count_for_period(time()-24*60*60)?></a><span>Broj poseta <br>u zadnjih 24h</span>
            </li>
            <li><a href="<?=ADMIN?>porudzbine" class="count grey" title=""><?=Order::count_active()?></a><span>Aktivnih <br>Porudžbina</span>
            </li>
            <? /* <li><a href="<?=ADMIN?>proizvodi" class="count grey" title=""><?=$sum_prod?></a><span>Aktivnih <br>Proizvoda </span>
            </li> */?>
            <li><a href="<?=ADMIN?>promocije" class="count grey" title=""><?=$sum_promo?></a><span>Aktivnih<br>Promocija</span>
            </li>

            <li class="last"><a href="<?=ADMIN?>klijenti" class="count grey"
                                title=""><?=Customer::count_all()?></a><span>Registrovanih <br>korisnika</span></li>
        </ul>
        <?php } ?>
        <div class="fix"></div>
    </div>
    <!-- Lines -->
    <textarea id="orders" cols="100" rows="5" style="display: none;">
        <?=Order::pending_orders(); ?>
    </textarea>

    <div class="widget first">
        <div class="head"><h5 class="iGraph">Aktivne Porudžbine</h5></div>
        <div class="body">
            <div class="chart"></div>
        </div>
    </div>
    <!-- Lines with fill -->
    <div class="widget">
        <div class="head"><h5 class="iGraph">Broj poseta</h5></div>
        <div class="body">
            <div class="autoUpdate"></div>
        </div>
    </div>
    <?php
    if ($user->role != 4) {
        /*
        //$products = Product::find_by_sql("SELECT *, (select name_sr from category where category.id = product.category) as category, (select name_sr from manufacturer where manufacturer.id = product.manufacturer) as manufacturer FROM product order by active ASC, category ASC LIMIT 10");
        $products = Product::find_by_sql("SELECT id,name_sr,active, category,manufacturer FROM product order by active ASC, category ASC LIMIT 10 ");
        ?>

        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> Poslednji dodati proizvodi </h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                <tr>
                    <th>Naziv</th>
                    <th>Kategorija</th>
                    <th>Proizvodjac</th>
                    <th>Aktivan</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product) { ?>
                <tr class="gradeA" <?=$product->id?>>
                    <td class="center"><a href="<?=ADMIN?>proizvod/<?=$product->id?>">
                        <strong><?=$product->name_sr?><strong></a></td>
                    <td class="center"> <?php
                        if ($product->category) {
                            $category = Category::find_by_id($product->category);
                            echo $category->name_sr;
                        }
                        ?></td>
                    <td class="center"> <?php
                        if ($product->manufacturer) {
                            $manufacturer = Manufacturer::find_by_id($product->manufacturer);
                            echo $manufacturer->name_sr;
                        }
                        ?></td>
                    <td class="center">
                        <input type="checkbox" name="active"
                            <?php if ($product->active == 1) {
                            echo 'checked = "checked"';
                        }
                            ?>
                                />
                    </td>
                </tr>
                    <?php
                } ?>

                </tbody>
            </table>
        </div>
                */
        ?>

        <?php
    } else {
        $orders = Order::find_all();?>
        <?php if ($session->message() != '') {
            echo "<br>";
            if ($_SESSION['mType'] == 2) {
                $messageType = 'valid';
            } else {
                $messageType = 'invalid';
            }
            echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
        }?>
               <div class="table">
            <div class="head"><h5 class="iFrames"> Istorija porudžbina</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Datum</th>
                <th>Order ID</th>
                <th>Status</th>
                <th>Klijent</th>
                <th>Proizvodi</th>
                <th>Novi status</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($orders as $o) {
                $c = Customer::find_by_id($o->customerID);
                ?>
                   <tr>
                <td><?=date("d.m.y", $o->orderDate)?></td>
                <td align="center"><?=alphaID(intval($o->orderID))?></td>
                <td align="center"><strong><?=$o->status()?></strong></td>
                <td align="center"><?=$c->full_name()?></td>
                <td width="45%"><?php
                    foreach (explode(',', $o->products) as $pID) {
                        echo Product::product_name($pID) . ' ';
                    }?></td>
                <td align="center" width="30%">
                    <a href="<?=ADMIN?>changeStatus/<?=$o->id?>/sent" class="changeStatus">Poslato</a> |
                    <a href="<?=ADMIN?>changeStatus/<?=$o->id?>/payed" class="changeStatus">Plaćeno</a> |
                    <a href="<?=ADMIN?>changeStatus/<?=$o->id?>/close" class="changeStatus">Zaključi porudžbinu</a>
                </td>
                <?php } ?>
            </tr>
            </tbody>
        </table>

        <?php } ?>
</div>
    <div class="fix"></div>
</div>
