<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
if (isset($_GET['groupID']) && $_GET['groupID'] != '') {

    $group = PriceGroup::find_by_id($_GET['groupID']);
    if ($group && $group->delete()) {
        $session->message('Grupa je uspešno obrisana');
        redirect_to(ADMIN . 'cenovnik');
    } else {
        $session->message('Pojavila se greška. Groupa nije obrisana');
        redirect_to(ADMIN . 'cenovnik');
    }
} else {
    $session->message('Unesite ime nove grupe');
    redirect_to(ADMIN . 'cenovnik');

}