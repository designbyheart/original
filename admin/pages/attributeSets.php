<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
$prodDetails = ProductDetailsSettings::find_all();
$attrSets = AttributeSet::find_all();
?>
<!-- Content -->
<div class="content">
    <div class="title"><h5>
        Grupe atributa proizvoda</a>
    </h5></div>
    <?php if ($session->message() != '') {
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->


    <fieldset style="display: none;" id="newProductDetail">

        <form action="<?=ADMIN?>saveProductDetails" method="post">
            <div class="table" style="margin-top:0;">
                <div class="head">
                    <h5>Novi detalj</h5>
                </div>
                <div class="dataTables_wrapper" style="padding:1em;height:96px">
                    <label style="display:block;margin-bottom:.5em;float:left;width:100%;">
                        <span style="float:left;">Naziv detalja</span>
                        <input type="text" style="float:right;  margin-left:20%;" name="newDetail_sr">
                        <input type="hidden" name="productID" value="<?=$_GET['id']?>">
                    </label>

                    <label style="display:block;">
                        <span style="float:left;">Naziv detalja (engleski)  </span>
                        <input type="text" name="newDetail_en" id="" style="float:right; margin-left:20%;">
                    </label>
                    <a href="#"
                       style="float:right;width:40px; clear:both;margin-top:1em;position:relative; top:-3px;"><input
                            type="submit"
                            value="Sačuvaj"
                            class="greyishBtn submitForm"></a>


                </div>
            </div>
        </form>

    </fieldset>
    <fieldset style="display: none;" id="newAttributeSet">

        <form action="<?=ADMIN?>saveAttributeSet" method="post">
            <div class="table" style="margin-top:0;">
                <div class="head">
                    <h5>Novi atribut set</h5>
                </div>
                <div class="dataTables_wrapper" style="padding:1em;height:66px">
                    <label style="display:block;margin-bottom:.5em;float:left;width:100%;">
                        <span style="float:left;">Naziv atribut-seta</span>
                        <input type="text" style="float:right;  margin-left:20%;" name="attributSetName">
                    </label>
                    <a href="#"
                       style="float:right;width:40px; clear:both;margin-top:1em;position:relative; top:-3px;"><input
                            type="submit"
                            value="Sačuvaj"
                            class="greyishBtn submitForm"></a>


                </div>
            </div>
        </form>

    </fieldset>
    <form class="mainForm" action="<?=ADMIN?>saveDetails" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset>
            <?php if ($session->message() != '') {
            if ($_SESSION['mType'] == 2) {
                $messageType = 'valid';
            } else {
                $messageType = 'invalid';
            }
            echo '<p  class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
        }?>


            <div class="tab_container">

                <input type="hidden" name="product_id" value="<?=$_GET['id']?>"/>
                <a href="#" class="btnIconLeft mr10 newAttributeSet" style="padding:.3em 1em"> Dodaj novi attribut set</a>
                <a href="#" class="btnIconLeft mr10 newProductDetail" style="padding:.3em 1em"> Dodaj novi detalj</a>
                <a href="<?=ADMIN?>izmena-detalja-proizvoda" target="_blank" class="btnIconLeft mr10"
                   style="padding:.3em 1em"> Izmeni detalje</a>


                <div class="attributeGroup">
                    <?php foreach ($attrSets as $aS): ?>
                    <h3><input type="text" class="text attributeName" id="attr<?=$aS->id?>" name="attrName[<?=$aS->id?>]" value="<?=$aS->name?>"/>
                        <span class="attr<?=$aS->id?>"><?=$aS->name?></span>
                        <a href="#" id="<?=$aS->id?>" class="editAttrLink">edit</a>
                        <a href="<?=ADMIN?>pages/deleteAttributeSet.php?id=<?=$aS->id?>"  class="deleteAttrLink">delete</a>
                    </h3>
                        <div id="<?=$aS->id?>">
                            <?php $details = ProductDetailsSettings::find_all();
                    $selDetails = array_filter(explode(',', $aS->details));
                    foreach ($details as $d) {
                        ?>
                        <label>
                            <input type="checkbox"
                                   name="setting[<?=$aS->id?>][<?=$d->id?>]" <?php if (in_array($d->id, $selDetails)) {
                                echo "checked";
                            }; ?> >
                            <span class="nm"><?=$d->name_sr?></span>
                        </label>

                        <?php
                    }

                    ?>

                    <?php endforeach;; ?>
                </div>
                </div>

                <div class="fix"></div>
            </div>
            <!-- End - Tabs -->
        </fieldset>
        <a href="<?=ADMIN?>kategorije"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
        <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
    </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a
                href="http://designbyheart.info">Design by Heart</a></span>


