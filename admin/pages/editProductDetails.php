<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');
$prodDetails = ProductDetailsSettings::find_all();
?>
<!-- Content -->
<div class="content">
    <div class="title"><h5>
        Izmena detalja proizvoda</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->


    <fieldset style="display: none;" id="newProductDetail">

        <form action="<?=ADMIN?>saveProductDetails" method="post">
            <div class="table" style="margin-top:0;">
                <div class="head">
                    <h5>Novi detalj</h5>
                </div>
                <div class="dataTables_wrapper" style="padding:1em;height:96px">
                    <label style="display:block;margin-bottom:.5em;float:left;width:100%;">
                        <span style="float:left;">Naziv detalja</span>
                        <input type="text" style="float:right;  margin-left:20%;" name="newDetail_sr" >
                    </label>

                    <label style="display:block;">
                        <span style="float:left;">Naziv detalja (engleski)  </span>
                        <input type="text" name="newDetail_en" id="" style="float:right; margin-left:20%;">
                    </label>
                    <a href="#"  style="float:right;width:40px; clear:both;margin-top:1em;position:relative; top:-3px;"><input type="submit" value="Sačuvaj"  class="greyishBtn submitForm"></a>
                </div>
            </div>
        </form>

    </fieldset>
    <form class="mainForm" action="<?=ADMIN?>saveDetailsOnly" method="post" enctype="multipart/form-data">
        <!-- Tabs -->
        <fieldset >
            <?php if ($session->message() != '') {
            if ($_SESSION['mType'] == 2) {
                $messageType = 'valid';
            } else {
                $messageType = 'invalid';
            }
            echo '<p  class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
        }?>


            <div class="tab_container">

                <a href="#" class="btnIconLeft mr10 newProductDetail" style="padding:.3em 1em"> Dodaj novi detalj</a>
                <a href="<?=ADMIN?>atribute-set" class="btnIconLeft mr10" style="padding:.3em 1em"> Izmena grupa atributa</a>
                <div class="table">
                    <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedProducts"
                           id="example"
                           style="width:100%!important">
                        <thead>
                        <tr>
                            <td width="20%">Srpski</td>
                            <td width="20%">Engleski</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($prodDetails && count($prodDetails) > 0) {
                            foreach ($prodDetails as $pD):
                                ?>
                            <tr>
                                <td width="45%">
                                    <input type="hidden" name="detailID[]" value="<?=$pD->id?>">
                                    <input type="text" value="<?=$pD->name_sr?>" name="name_sr[<?=$pD->id?>]" style="width:90%;"></td>
                                <td width="45%"><input type="text" value="<?=$pD->name_en?>" name="name_en[<?=$pD->id?>]" style="width:90%;"></td>
                                <td><a href="<?=ADMIN?>deleteDetail">Izbriši</a></td>
                            </tr>
                                <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>


                <div class="fix"></div>
            </div>
            <!-- End - Tabs -->
        </fieldset>
        <a href="<?=ADMIN?>kategorije"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
        <input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
    </form>

</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a
                href="http://designbyheart.info">Design by Heart</a></span>


