<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Statistika o korisnicima</h5></div>

        <div class="widgets">
            <div class="left">
                <div class="widget">
                    <div class="head"><h5 class="iChart8">Popularni proizvodi</h5><div class="num"></div></div>
                    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
                        <thead>
                        <tr>
                            <td width="21%">Poseta</td>
                            <td>Ime proizvoda</td>
                            <td width="21%">Promena</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $prodID = VisitLog::countProducts();
                            foreach($prodID as $prID){
                                $p = Product::find_by_id($prID->productID);
                                if($p){
                                ?>
                            <tr>
                                <td align="center"><a href="#" title="" class="webStatsLink">980</a></td>
                                <td><?php
                                    echo $p->name_sr;
                                   ?></td>
                                <td><span class="statPlus">0.32%</span></td>
                            </tr>
                            <?php }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="widget">
                    <div class="head"><h5 class="iChart8">Najaktivniji korisnici</h5><div class="num"></div></div>
                    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
                        <thead>
                        <tr>
                            <td width="21%">Poseta</td>
                            <td width="21%">Broj narudžbi</td>
                            <td>Korisnik</td>
                            <td width="21%">Promena</td>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>

        <div class="right">

        <div class="widget">
            <div class="head"><h5 class="iChart8">Popularne promocije</h5><div class="num"><a href="#" class="blueNum">245</a></div></div>
            <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
                <thead>
                <tr>
                    <td width="21%">Broj poseta</td>
                    <td>Ime promocije</td>
                    <td width="21%">Promena</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        </div>        </div>