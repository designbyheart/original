<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');

if ($_GET['id'] == 0) {
    $product = new Product();
} else {
    $product = Product::find_by_id($_GET['id']);
}


if ($product->plD == "") {
    $product->plD = rand_uniqid(time());
}
//get char. categories

//get char.
if (!isset($ch_sr)) {
    $ch_sr = new Characteristics();
}

if (!isset($ch_en)) {
    $ch_en = new Characteristics();
}
$atttributes = array_filter(explode(',', $product->attributes));

?>


<div class="newLink newLinkk " style="position:absolute">
    <h5>Novi link</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveSubmenu" method="post">
        <label>
            Srpski naziv
            <input type="text" name="name_sr">
        </label>
        <label>
            Engleski naziv
            <input type="text" name="name_en">
        </label>
        <label>Link
            <input type="text" name="linkSr">
        </label>

        <input type="hidden" name="pageType" value="product">
        <input type="hidden" name="pageID" value="<?=$_GET['id']?>">
        <button type="submit" class="greyishBtn">Sačuvaj</button>
    </form>
</div>
<div class="newLink newCharCategory" style="position:absolute; height:160px">
    <h5>Nova kategorija karakteristika</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveCharacteristic" method="post">
        <label>
            Srpski naziv
            <input type="text" name="name_sr">
        </label>
        <label>
            Engleski naziv
            <input type="text" name="name_en">
        </label>

        <input type="hidden" name="pageType" value="charCategory">
        <input type="hidden" name="pageID" value="<?=$_GET['id']?>">
        <button type="submit" class="greyishBtn">Sačuvaj</button>
    </form>
</div>
<div class="newLink newCharacteristic" style="position:absolute; height:280px">
    <h5>Nova karakteristika</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveCharacteristic" method="post">
        <label>Kategorija: <br>
            <select name="charCategory">
                <option value="0">Izaberi...</option>
                <?php $chars = CharacteristicsCategory::find_all();
                if (count($chars) > 0) {
                    foreach ($chars as $chC):?>
                        <option value="<?=$chC->id?>"><?=$chC->category_name_sr?></option>

                        <?php endforeach;
                }
                ?>
            </select>
        </label>
        <label>
            Ime/Opis karakteristike
            <input type="text" name="name_sr" style="float:left;clear:left;width:250px;margin-top:5px; margin-left:0;">
        </label>
        <label>
            Ime/Opis karakteristike (eng)
            <input type="text" name="name_en" style="float:left;clear:left;width:250px;margin-top:5px; margin-left:0;">
        </label>

        <input type="hidden" name="charType" value="category">
        <input type="hidden" name="pageID" value="<?=$_GET['id']?>">
        <label>
            <input type="submit" class="greyishBtn" style="float:left;width:100px; margin-top:20px;" value="Sačuvaj">
        </label>
    </form>
</div>
<div class="newLink addAttributeWrapper"
     style="position:absolute; padding-top:20px; margin-left:0px;height:420px">
    <h5>Dodaj atribut</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveAttribut" method="POST">
        <input type="hidden" name="product" value="<?=$product->id?>">
        <input type="hidden" name="settings" value="1">
        <label for="attributName" style="margin-top:30px;">Ime atributa: <br>
            <input type="text" name="attributName" id="attributName"
                   style="float:left;clear:left;width:250px; margin-left:0;">
        </label>
        <label for="attributNameEng">Ime atributa (engleski): <br>
            <input type="text" name="attributNameEng" id="attributNameEng"
                   style="float:left;width:250px;clear:left; margin-left:0;">
        </label>

        <div class="parentList">
            <label>Izaberi roditelje</label>
            <?php $attS = ProductDetailsSettings::find_main(); ?>
            <?php foreach ($attS as $a): ?>
            <label id="<?=$a->id?>"> <input type="checkbox" name="parent[]" value="<?=$a->id?>" style="display:none">
                <strong><?=$a->name_sr?></strong></label>
            <?php $subA = ProductDetailsSettings::find_child($a->id);
            if ($subA) {
                foreach ($subA as $sA) {
                    ?>
                    <label id="<?=$sA->id?>">&nbsp;&nbsp;↳&nbsp;&nbsp; <input type="checkbox" name="parent[]"
                                                                              value="<?=$sA->id?>"
                                                                              style="display:none"><?=$sA->name_sr?>
                    </label>
                    <?
                    $subAA = ProductDetailsSettings::find_child($sA->id);
                    if ($subAA) {
                        foreach ($subAA as $sAA):?>
                            <label id="<?=$sAA->id?>">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp; <input type="checkbox" name="parent[]"
                                                                                   value="<?=$sAA->id?>"
                                                                                   style="display:none">  <?=$sAA->name_sr?>
                            </label>
                            <?php

                            $subAAA = ProductDetailsSettings::find_child($sAA->id);
                            if ($subAAA) {
                                foreach ($subAAA as $sAAA):?>
                                    <label id="<?=$sAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                            type="checkbox" name="parent[]" value="<?=$sAAA->id?>"
                                            style="display:none"> <?=$sAAA->name_sr?> </label>
                                    <?php
                                    $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                    if ($subAAAA) {
                                        foreach ($subAAAA as $sAAAA):?>
                                            <label id="<?=$sAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                    type="checkbox" name="parent[]" value="<?=$sAAAA->id?>"
                                                    style="display:none"> <?=$sAAAA->name_sr?> </label>
                                            <?php
                                            $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                            if ($subAAAAA) {
                                                foreach ($subAAAAA as $sAAAAA):?>
                                                    <label id="<?=$sAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                            type="checkbox" name="parent[]" value="<?=$sAAAAA->id?>"
                                                            style="display:none"> <?=$sAAAAA->name_sr?> </label>
                                                    <?php
                                                    $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                    if ($subAAAAAA) {
                                                        foreach ($subAAAAAA as $sAAAAAA):?>
                                                            <label id="<?=$sAAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<input
                                                                    type="checkbox" name="parent[]"
                                                                    value="<?=$sAAAAAA->id?>"
                                                                    style="display:none"> <?=$sAAAAAA->name_sr?>
                                                            </label>
                                                            <?php endforeach;
                                                    }
                                                endforeach;
                                            }
                                        endforeach;
                                    }
                                endforeach;
                            }
                        endforeach;
                    }
                }
            }
        endforeach; ?>

        </div>
        <input type="submit" value="Sačuvaj" class="greyishBtn submitForm"
               style="float:left;margin-left:5px;margin-top:30px;">
    </form>
</div>
<div class="newLink addAttributeDetailWrapper"
     style="position:absolute; padding-top:20px; margin-left:20px;height:260px">
    <h5>Dodaj detalj</h5>
    <a href="#" class="close">Close</a>

    <form action="<?=ADMIN?>saveAttribut" method="POST">
        <input type="hidden" name="product" value="<?=$product->id?>">
        <input type="hidden" name="attribut" value="1">
        <label for="selectParentAttr" style="margin-bottom:1em;">
            <?php $attS = ProductDetailsSettings::find_main(); ?>
            <select name="parent" id="selectParentAttr" style="width:265px;border-color:#ccc!important;">
                <option value="0">Izaberi atribut</option>
                <?php foreach ($attS as $a): ?>
                <option value="<?=$a->id?>"><?=$a->name_sr?> </option>
                <?php $subA = ProductDetailsSettings::find_child($a->id);
                if ($subA) {
                    foreach ($subA as $sA) {
                        ?>
                        <option value="<?=$sA->id?>">&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sA->name_sr?> </option>
                        <?
                        $subAA = ProductDetailsSettings::find_child($sA->id);
                        if ($subAA) {
                            foreach ($subAA as $sAA):?>
                                <option value="<?=$sAA->id?>">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAA->name_sr?> </option>
                                <?php
                                $subAAA = ProductDetailsSettings::find_child($sAA->id);
                                if ($subAAA) {
                                    foreach ($subAAA as $sAAA):?>
                                        <option value="<?=$sAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAA->name_sr?> </option>
                                        <?php
                                        $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                        if ($subAAAA) {
                                            foreach ($subAAAA as $sAAAA):?>
                                                <option value="<?=$sAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAAA->name_sr?> </option>
                                                <?php
                                                $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                                if ($subAAAAA) {
                                                    foreach ($subAAAAA as $sAAAAA):?>
                                                        <option value="<?=$sAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAAAA->name_sr?> </option>
                                                        <?php
                                                        $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                        if ($subAAAAAA) {
                                                            foreach ($subAAAAAA as $sAAAAAA):?>
                                                                <option value="<?=$sAAAAAA->id?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;↳&nbsp;&nbsp;<?=$sAAAAAA->name_sr?> </option>
                                                                <?php endforeach;
                                                        }
                                                    endforeach;
                                                }
                                            endforeach;
                                        }
                                    endforeach;
                                }
                            endforeach;
                        }
                    }
                }
            endforeach; ?>
            </select>

        </label>
        <label for="attributValue">Vrednost atributa: <br>
            <input type="text" name="attributValue" id="attributValue"
                   style="float:left;clear:left;width:250px; margin-left:0;">
        </label>
        <label for="attributValueEng">Vrednost atributa (engleski): <br>
            <input type="text" name="attributValueEng" id="attributValueEng"
                   style="float:left;width:250px;clear:left; margin-left:0;">
        </label>

        <input type="submit" value="Sačuvaj" class="greyishBtn submitForm"
               style="float:left;margin-left:5px;margin-top:30px;">
    </form>
</div>


<!-- Content -->
<div class="content">
<div class="title" style="position:relative;">
    <h5><?php if ($_GET['id'] == 0) { ?>Novi proizvod<?
    } else {
        ?>Izmena proizvoda
        <span style="position:absolute; right:20px; top:7px; color:#bbb;">Link:<a
                href="<?=SITE_ROOT?>proizvod/<?=$product->id?>/<?=urlSafe($product->name_sr)?>" target="_blank"
                style="color:#ddd;">/proizvod/<?=$product->id?>/<?=urlSafe($product->name_sr)?></a>
        </span>
        <? } ?>
    </h5>
</div>

<!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

<form class="mainForm" action="<?=ADMIN?>saveProduct" method="post" enctype="multipart/form-data">
<!-- Tabs -->
<fieldset>
<?php if ($session->message() != '') {
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
<div class="widget" style="overflow: visible;">
<ul class="tabs">
    <li><a href="#tab1">Srpski</a></li>
    <li><a href="#tab2">Engleski</a></li>
    <li><a href="#tab5">Cenovnik</a></li>
    <li><a href="#tab6">Karakteristike</a></li>
    <li><a href="#tab7">Povezani proizvodi</a></li>
    <li><a href="#tab8">Slični proizvodi</a></li>
    <li><a href="#tab3">Galerija</a></li>
    <li><a href="#tab9">Podmeni</a></li>
    <li><a href="#tab4">Seo</a></li>
</ul>

<div class="tab_container"
     style="background:#f9f9f9; border:solid 1px #cfcfcf; border-top: none;position: relative;left:-1px; width: 99.975%; overflow: visible;">
<input type="hidden" name="id" class="productID" value="<?=$product->id?>"/>

<div id="tab1" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv proizvoda:</label>

        <div class="formRight">
            <input type="text" name="name_sr" value="<?=$product->name_sr?>"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem" style="float: left; clear: none; width:100%;">
        <label>Kategorija proizvoda:</label>

        <div class="formRight">
            <?php
            if ($_GET['id'] > 0) {
                $group = Category::find_by_id($product->category);
            }
            if (isset($group) && $group) {
                ?>
                <p style="position:relative;top:-10px;">Izabrana grupa: <?=groupBreadcrumb($group->id)?></p>
                <?php }; ?>
            <select name="category" id="category1" multiple="multiple" size="5" class="groupSelector"
                    style="width: 250px;border:solid 1px #cccccc;">
                <option value="0"> -- Izaberi kategoriju --</option>
                <?php $cats = Category::find_by_sql("select id, name_sr, parent_cat, ordering From category where parent_cat = 0 order by ordering ASC ");
                foreach ($cats as $c) {
                    ?>
                    <option value="<?=$c->id?>" <?php if ($c->id == $product->category) {
                        echo "selected";
                    } ?>>&nbsp;&nbsp;<?=$c->name_sr?>
                    </option>
                    <?php }?>
            </select>

            <select name="category" class="groupSelector" id="category2" multiple="multiple" size="5"
                    style="display:none; width: 250px;margin-top:10px;border:solid 1px #cccccc">
            </select>
            <select name="category" class="groupSelector" id="category3" multiple="multiple" size="5"
                    style="display:none; width: 250px;margin-top:10px;border:solid 1px #cccccc">

            </select>
            <select name="category" class="groupSelector" id="category4" multiple="multiple" size="5"
                    style="display:none; width: 250px;margin-top:10px;border:solid 1px #cccccc">

            </select>
            <select name="category" id="category5" class="groupSelector" multiple="multiple" size="5"
                    style="display:none; width: 250px;margin-top:10px;border:solid 1px #cccccc">

            </select>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Izaberite državu:</label>

        <div class="formRight" style="float:right;width:75%;">
            <?php $countries = Country::find_active();
            ?>
            <select name="country">
                <option value="0">Izaberi...</option>
                <?php
                foreach ($countries as $cn) {
                    ?>
                    <option value="<?=$cn->id?>" <?php if ($cn->id == $product->country) {
                        echo "selected";
                    } ?>><?=$cn->name?></option>
                    <?php }  ?>

            </select>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem " style="float: left; clear: none;width:100%;">
        <div class="formRight" style="float:left;width:45%;">
            <label style="padding-top:15px">Interni kataloški broj:</label>
            <input type="text" name="plD" value="<?=$product->plD?>"
                   style="text-align: center; height:40px; font-size:1.8em;"/>
        </div>
        <div class="formRight" style="float:left;width:45%;">
            <label style="padding-top:15px">Osnovni kataloški broj proizvođača:</label>
            <input type="text" name="mainCatID" value="<?=$product->mainCatID?>"
                   style="text-align: center; height:40px; font-size:1.8em;"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem " style="float: left; clear: left;width:100%;position:relative;left:-22px;">
        <div class="formRight" style="float:left;width:45%;">
            <label style="padding-top:15px">Zamenski kataloški brojevi:</label>
            <textarea name="secondIDs"
                      style="height:40px; text-align: center;font-size:1.2em;"><?=$product->secondIDs?></textarea>
            <span style="padding:.5em; font-size:.95em;color:#666; display:block;">Za unos više brojeva koristite , <br>(primer: serijskiBroj1, serijskiBroj2,serijskiBroj3)</span>
        </div>
        <div class="formRight" style="float:left;width:45%;">
            <label style="padding-top:15px">Šifra dobavljača:</label>
            <textarea name="distribID"
                      style="height:40px; text-align: center;font-size:1.2em;"><?=$product->distribID?></textarea>
            <span style="padding:.5em; font-size:.95em;color:#666; display:block;">Za unos više brojeva koristite , <br>(primer: serijskiBroj1, serijskiBroj2,serijskiBroj3)</span>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <label>Proizvodjač:</label>

        <div class="formRight">
            <select name="manufacturer">
                <option value="0">Izaberi proizvođača</option>
                <?php
                $manufacturers = Manufacturer::find_all();
                foreach ($manufacturers as $manufacturer) : ?>
                    <option value="<?=$manufacturer->id?>"
                        <?php if ($product->id > 0) {
                        // manufacturer_p je proizvodjac datog proizvoda
                        $manufacturer_p = Manufacturer::find_by_id($product->manufacturer);
                        if (isset($manufacturer_p->id) && $manufacturer->id == $manufacturer_p->id) {
                            echo 'selected';
                        }
                    }?>
                            > <?=$manufacturer->name_sr?></option>
                    <?php endforeach; ?>
            </select>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <label>Aktivan:</label>

        <div class="formRight">
            <input type="checkbox" name="active"
                <?php if ($product->active == 1) {
                echo 'checked';
            } ?>
                    />
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <label>Status:</label>

        <div class="formRight">
            <input type="radio" name="status" value="0" <?php if ($product->status == 0) {
                echo 'checked';
            }?>/><label>Na stanju</label>
            <input type="radio" name="status" value="1" <?php if ($product->status == 1) {
                echo 'checked';
            }?>/><label>Nema ga na stanju</label>
            <input type="radio" name="status" value="2" <?php if ($product->status == 2) {
                echo 'checked';
            }?>/><label>Nije u prodaji</label>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Opis proizvoda: </h5></div>
                <textarea class="wysiwyg" name="desc_sr" rows="5"><?=$product->desc_sr?></textarea>
            </div>
        </div>
    </div>
    <div class="rowElem noteLinks">
        <a href="#admin" class="btnIconLeft mr10 padding10">Admin</a>
        <a href="#comercial" class="btnIconLeft mr10 padding10">Komercijalista</a>
        <?php $prGroups = PriceGroup::find_main();
        foreach ($prGroups as $pG) {
            ?>
            <a href="#<?=$pG->id?>" class="btnIconLeft mr10 padding10"><?=$pG->name?></a>
            <?php
        }
        ?>

    </div>
    <div class="rowElem">
        <div class="formRight notesItems" style="width: 100%">
            <div class="widget" style="margin-top: 5px" id="admin">
                <div class="head"><h5 class="iPencil">Napomene - Admin: </h5></div>
                <textarea name="note[admin]" rows="5"
                          style="width:98.5%;height:150px;"><?=Note::getNoteValue($_GET['id'], 'admin')?></textarea>
            </div>
            <div class="widget" id="comercial" style="margin-top: 5px; display:none">
                <div class="head"><h5 class="iPencil">Napomene - Komercijalista: </h5></div>
                <textarea name="note[comercial]" rows="5"
                          style="width:98.5%;height:150px;"><?=Note::getNoteValue($_GET['id'], 'comercial')?></textarea>
            </div>
            <?php  foreach ($prGroups as $pG) { ?>
            <div class="widget" id="<?=$pG->id?>" style="margin-top: 5px;display:none">
                <div class="head"><h5 class="iPencil">Napomene - <?=$pG->name?>: </h5></div>
                <textarea name="note[<?=$pG->name?>]" rows="5"
                          style="width:98.5%;height:150px;"><?=Note::getNoteValue($_GET['id'], $pG->name)?></textarea>
            </div>

            <?php } ?>
        </div>
    </div>
</div>
<div id="tab2" class="tab_content">

    <div class="rowElem noborder">
        <label>Naziv proizvoda:</label>

        <div class="formRight">
            <input type="text" name="name_en" value="<?=$product->name_en?>"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Opis proizvoda: </h5></div>
                <textarea class="wysiwyg" name="desc_en" rows="5" cols="">
                    <?=$product->desc_en?>
                </textarea></div>
        </div>
        <div class="fix"></div>
    </div>
</div>
<div id="tab4" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv proizvoda(srpski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_sr" value="<?=$product->seo_title_sr?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem noborder">
        <label>Naziv proizvoda(engleski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_en" value="<?=$product->seo_title_en?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(srpski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_sr"><?=$product->seo_desc_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(engleski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_en"><?=$product->seo_desc_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(srpski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_sr"><?=$product->seo_keywords_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(engleski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_en"><?=$product->seo_keywords_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
</div>


<div id="tab8" class="tab_content">
    <?php $prS = Product::find_similar($product->id); ?>

    <div class="table" style="width:100%;margin:0">
        <div class="head"><h5 class="iFrames">Izaberi proizvod</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedSimilarProducts" id="example2"
               style="width:100%!important">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="20%">Ime proizvoda</td>
                <td width="20%">Kategorija</td>
                <td width="20%">Serijski broj</td>
            </tr>
            </thead>
            <tbody>
            <?php
            $connectedProducts = array_filter(explode(",", $product->similar));
            foreach ($prS as $ps):
                if (!in_array($ps->id, $connectedProducts)) {
                    ?>
                <tr>
                    <td align="center">
                        <a href="<?=$ps->id?>" title="" class="btnIconLeft mr10 chooseSimilarProduct"
                           style="margin:0 0 -5px 0;padding:0 15px;"><span style="padding:2px 8px;">Izaberi</span></a>
                    </td>
                    <td align="center" id="similarProductName<?=$ps->id?>"><h4><?=$ps->name_sr?></h4></td>
                    <td align="center" id="similarCategoryName<?=$ps->id?>"><?=$ps->category?></td>
                    <td align="center"><?=$ps->plD?></td>
                </tr>
                    <?php } endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="widget first">
        <div class="head"><h5 class="iFrames">Izabrani proizvodi</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="selectedSimilarList">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="30%">Ime proizvoda</td>
                <td width="30%">Kategorija</td>
            </tr>
            </thead>
            <tbody>
            <?php if ($product->similar != '') {

                foreach ($connectedProducts as $cProductID) {
                    $cProduct = Product::find_by_id($cProductID);
                    if ($cProduct) {
                        $category = Category::find_by_id($cProduct->category);
                        ?>
                    <tr class="<?=$cProduct->id?>">
                        <td align="center">
                            <a href="<?=$cProduct->id?>" class="btnIconLeft mr10 removeSimilarProduct"
                               style="margin:0 0 -5px 0; padding:0 15px;">
                                <span style="padding:2px 8px;">Ukloni</span>
                            </a>
                        </td>
                        <td align="center"><h4><?=$cProduct->name_sr?></h4>
                        </td>
                        <td align="center"><strong><?php if ($category) {
                            echo $category->name_sr;
                        }?></strong></td>
                    </tr>
                        <?php
                    }
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="fix"></div>
    <input type='hidden' name="similarProducts" id="linkedSimilarProducts">
</div>

<div id="tab7" class="tab_content">
    <?php $prS = Product::find_similar($product->id); ?>
    <div class="table" style="width:100%;margin:0">
        <div class="head"><h5 class="iFrames">Izaberi proizvod</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedProducts"
               style="width:100%!important">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="20%">Ime proizvoda</td>
                <td width="20%">Kategorija</td>
                <td width="20%">Serijski broj</td>
            </tr>
            </thead>
            <tbody>
            <?php if (count($prS) > 0) {
                foreach ($prS as $ps): ?>
                <tr>
                    <td align="center">
                        <a href="<?=$ps->id?>" title="" class="btnIconLeft mr10 chooseProduct"
                           style="margin:0 0 -5px 0;padding:0 15px;"><span style="padding:2px 8px;">Izaberi</span></a>
                    </td>
                    <td align="center" id="productName<?=$ps->id?>"><h4><?=$ps->name_sr?></h4></td>
                    <td align="center" id="categoryName<?=$ps->id?>"><?=$ps->category?></td>
                    <td align="center"><?=$ps->plD?></td>
                </tr>
                    <?php endforeach;
            } ?>
            </tbody>
        </table>
    </div>
    <div class="widget first">
        <div class="head"><h5 class="iFrames">Izabrani proizvodi</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="selectedProductList">
            <!--id="selectedList"-->
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="30%">Ime proizvoda</td>
                <td width="30%">Kategorija</td>
            </tr>
            </thead>
            <tbody>
            <?php if ($product->optional != '') {
                $connectedProducts = array_filter(explode(",", $product->optional));
                if (count($connectedProducts) > 0) {
                    foreach ($connectedProducts as $cProductID) {
                        $cProduct = Product::find_by_id($cProductID);
                        $category = Category::find_by_id($cProduct->category);
                        ?>
                    <tr class="<?=$cProduct->id?>">
                        <td align="center">
                            <a href="<?=$cProduct->id?>" class="btnIconLeft mr10 removeProduct"
                               style="margin:0 0 -5px 0; padding:0 15px;">
                                <span style="padding:2px 8px;">Ukloni</span>
                            </a>
                        </td>
                        <td align="center"><h4><?=$cProduct->name_sr?></h4>
                        </td>
                        <td align="center"><strong><?php if ($category) {
                            echo $category->name_sr;
                        } ?></strong></td>
                    </tr>
                        <?php
                    }
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<div class="fix"></div>
<input type='hidden' name="optional" id="linkedProducts">

<div id="tab5" class="tab_content">

    <div class="rowElem noborder">
        <label>Stopa PDV-a:</label>

        <div class="formRight">
            <input type="text" name="pdv" value="<?=$product->pdv?>"/>
        </div>
        <div class="fix"></div>
    </div>
    <div class="table">
        <div class="head"><h5>Cene i popusti/provizije</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
            <thead>
            <tr>
                <td>Tip popusta/provizije</td>
                <td>Tip izmene</td>
                <td width="15%">Računati od</td>
                <td width="18%">Procenat</td>
                <td width="21%">Iznos</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Osnovna cena proizvoda</td>
                <td></td>
                <td>&nbsp;</td>
                <td align="center" class="calcTypeLbls">
                    <input type="text" name="price" class="webStatsLink countryTax"
                           style="width:93%;position: relative;left: .55em; font-size: 15px;text-align:center"
                           value="<?=$product->countryTax?>">
                    <span style="font-size:10px ;">(Porez države)</span>
                </td>
                <td align="center">
                    <input type="text" name="price" class="webStatsLink mainPrice"
                           style="width:93%;position: relative;left: .55em; font-size: 15px;text-align:center"
                           id="price0" value="<?=round($product->price, 2)?>">
                    <span style="font-size:10px ;">&nbsp;</span>
                </td>
            </tr>
            <?php $mGroups = PriceGroup::find_main();
//                $prGroups = PriceGroup::find_by_product($product->id);
            foreach ($mGroups as $mGroup):
//                $group = PriceGroup::find_by_product($group, $product->id);
                $g = PriceGroup::find_is_exists($mGroup->id, $product->id);
                if (!$g) {
                    $g = new PriceGroup();
                    $tempGroupID = PriceGroup::get_unique_id();
                } else {
                    $tempGroupID = $g->id;
                }

                ?>
            <tr>
                <td><?php //print_r($g);?><?=$mGroup->name?></td>
                <?php
                $p = new Price();
                if ($g) {
                    $p = Price::find_by_product_and_group($product->id, $tempGroupID);
                }
                $tempID = $tempGroupID;
                ?>
                <td><p>Izaberite vrstu kalkulacije</p>
                    <span class="0" style="float:left">
                        <input class="calcType" type="radio" id="type<?=$tempID?>" name="calcType[<?=$tempID?>]" checked
                               value="0">
                        <span style="position: relative;margin-left:2px;margin-right:3px;top:3px">Fiksno</span>
                    </span>
                    <span class="1" style="float:left">
                        <input class="calcType" id="type<?=$tempID?>" type="radio"
                               name="calcType[<?=$tempID?>]" <?php if ($g && $g->calcType == 2) {
                            echo 'checked';
                        } ?> value="2">
                        <span style="position: relative;margin-left:2px;margin-right:3px;top:3px">Dodaj</span>
                    </span>
                    <span class="1" style="float:left">
                        <input class="calcType" id="type<?=$tempID?>" type="radio"
                               name="calcType[<?=$tempID?>]" <?php if ($g && $g->calcType == 1) {
                            echo 'checked';
                        } ?> value="1">
                        <span style="position: relative;margin-left:2px;margin-right:3px; top:3px">Oduzmi</span>
                    </span></td>
                <td>

                    <div class="g<?=$tempID?>">
                        <select id="g<?=$tempID?>" class="parPrice parrentPrice<?=$mGroup->id?>"
                                name="parrent[<?=$tempGroupID?>]">
                            <option value="0">Proizvod</option>
                            <?php
                            if ($mGroups) {
                                foreach ($mGroups as $m):

                                    if ($m->name != $mGroup->name) {
                                        ?>
                                        <option value="<?=$m->id?>" <?php if ($g && $m && $g->parent == $m->id) {
                                            echo 'selected';
                                        } ?>>
                                            <?=$m->name?></option>
                                        <?php
                                    } endforeach;
                            } ?>
                        </select>
                    </div>
                </td>
                <td align="center"><input type="text" name="group[<?= $tempID ?>]" value="<?php if ($g) {
                    echo $g->percentage;
                } else {
                    echo '0';
                }?>" class="groupPercent" id="group<?= $tempID ?>" style="text-align: center; width:93px;"></td>

                <td><input type="text" name="prices[<?= $tempID ?>]"
                           class="webStatsLink productGroupPrice price<?= $mGroup->id ?>"
                           value="<?php if ($p) {
                               echo $p->amount;
                           } else {
                               echo '0';
                           }?>" id="price<?= $tempID ?>" style="width:93%; text-align: center; font-size:13px;"></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<div id="tab6" class="tab_content">
<?php /*
      * <p><span style="float:left; margin-right:1em;">Atribut set:</span>
            <select name="attributeSet" class="attrSet" id="attrSet">
                <option value="0">Izaberite attribute set</option>
                <?php $attrS = AttributeSet::find_all();
                if ($attrS && count($attrS) > 0) {
                    foreach ($attrS as $aS):?>
                        <option value="<?=$aS->id?>" <?php if ($product->attributeSet == $aS->id) {
                            echo "selected";
                        } ?>><?=$aS->name?></option>
                        <?php endforeach;
                } ?>
            </select>
            <a href="#" class="attrSet" style="margin-left:1em; padding-top: .2em">update atributa</a>
        </p>
        <a href="<?=ADMIN?>atribute-set" class="btnIconLeft mr10" style="float:right;padding:.2em 1.5em;">Edituj grupe
            atributa</a>
      * */
;
?>


<div class="table" id="attributeList" style="border:none;margin-top:10px">
    <h3 style="margin-bottom:30px;">Osobine proizvoda <br>
        <a href="#" id="addAtribute" style="font-size:.8em;margin-right:1em; margin-top:1em">Dodaj tip osobine</a>
        <a href="#" id="addAtributeDetail" style="font-size:.8em;margin-top:1em">Dodaj osobinu</a>
    </h3>

    <?php foreach ($attS as $a): ?>
    <div class="attributeItem">
        <label class="mainAttribute">
            <a href="#" class="showAttribute" id="<?= $a->id ?>"> [ + ] </a> <input type="hidden" name="<?= $a->id ?>"/>
            <span style="position:relative;left:1em; top:4px;"><?=$a->name_sr?></span>

        </label>

        <div class="attributeWrapper" id="attributeSet<?= $a->id ?>">
            <?php
            $vals = ProductDetails::find_by_settingID($a->id);
            if ($vals) {
                foreach ($vals as $v):?>
                    <label style="float:left;clear:left; width: 100%;position:relative; left:1em;">
                        <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                               value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                            echo 'checked';
                        }?>/>
                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                    </label>
                    <?php endforeach;
            }
            $subA = ProductDetailsSettings::find_child($a->id);
            if ($subA) {
                foreach ($subA as $sA) {
                    ?>
                    <label style="float:left;clear:left;margin-left:20px;padding-left:10px;font-weight: 700">
                        [2] <input type="hidden" name="<?=$sA->id?>"/>
                        <span style="position:relative;left:1em; top:4px;"><?=$sA->name_sr?></span>
                    </label>
                    <?php
                    $vals = ProductDetails::find_by_settingID($sA->id);
                    if ($vals) {
                        foreach ($vals as $v):?>
                            <label style="float:left;clear:left; width: 100%;position:relative; left:2em;">
                                <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                                       value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                    echo 'checked';
                                }?>/>
                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                            </label>
                            <?php endforeach;
                    }
                    $subAA = ProductDetailsSettings::find_child($sA->id);
                    if ($subAA) {
                        foreach ($subAA as $sAA):?>
                            <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:20px;">[3]
                                <input
                                        type="hidden" name="<?=$sAA->id?>"/> <span
                                        style="position:relative;left:1em; top:4px;"><?=$sAA->name_sr?></span></label>
                            <?php

                            $vals = ProductDetails::find_by_settingID($sAA->id);
                            if ($vals) {
                                foreach ($vals as $v):?>
                                    <label style="float:left;clear:left; width: 100%;position:relative; left:3em;">
                                        <input type="checkbox" name="values[<?=$v->id?>]"/>
                                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                    </label>
                                    <?php endforeach;
                            }
                            $subAAA = ProductDetailsSettings::find_child($sAA->id);
                            if ($subAAA) {
                                foreach ($subAAA as $sAAA):?>
                                    <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:30px;">[4]
                                        <input
                                                type="hidden" name="<?=$sAAA->id?>"/> <span
                                                style="position:relative;left:1em; top:4px;"><?=$sAAA->name_sr?></span></label>
                                    <?php
                                    $vals = ProductDetails::find_by_settingID($sAAA->id);
                                    if ($vals) {
                                        foreach ($vals as $v):?>
                                            <label style="float:left;clear:left; width: 100%;position:relative; left:4em;">
                                                <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                                                       value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                    echo 'checked';
                                                }?>/>
                                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                            </label>
                                            <?php endforeach;
                                    }

                                    $subAAAA = ProductDetailsSettings::find_child($sAAA->id);
                                    if ($subAAAA) {
                                        foreach ($subAAAA as $sAAAA):?>
                                            <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:40px;">
                                                [5] <input
                                                    type="hidden" name="<?=$sA->id?>"/> <span
                                                    style="position:relative;left:1em; top:4px;"><?=$sAAAA->name_sr?></span></label>
                                            <?php
                                            $vals = ProductDetails::find_by_settingID($sAAAA->id);
                                            if ($vals) {
                                                foreach ($vals as $v):?>
                                                    <label style="float:left;clear:left; width: 100%;position:relative; left:5em;">
                                                        <input type="checkbox" name="atributesDetails[<?=$v->id?>]"
                                                               value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                            echo 'checked';
                                                        }?>/>
                                                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                    </label>
                                                    <?php endforeach;
                                            }
                                            $subAAAAA = ProductDetailsSettings::find_child($sAAAA->id);
                                            if ($subAAAAA) {
                                                foreach ($subAAAAA as $sAAAAA):?>
                                                    <label style="float:left; margin-left:20px;font-weight:700;clear:left;padding-left:50px;">
                                                        [6] <input
                                                            type="hidden" name="<?=$sAAAA->id?>"/> <span
                                                            style="position:relative;left:1em; top:4px;"><?=$sAAAA->name_sr?></span></label>
                                                    <?php
                                                    $vals = ProductDetails::find_by_settingID($sAAAAA->id);
                                                    if ($vals) {
                                                        foreach ($vals as $v):?>
                                                            <label style="float:left;clear:left; width: 100%;position:relative; left:6em;">
                                                                <input type="checkbox"
                                                                       name="atributesDetails[<?=$v->id?>]"
                                                                       value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                                    echo 'checked';
                                                                }?>/>
                                                                <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                            </label>
                                                            <?php endforeach;
                                                    }
                                                    $subAAAAAA = ProductDetailsSettings::find_child($sAAAAA->id);
                                                    if ($subAAAAAA) {
                                                        foreach ($subAAAAAA as $sAAAAAA):?>
                                                            <label style="float:left; margin-left:20px;font-weight:700;padding-left:70px;clear:left">[7]
                                                                <input
                                                                        type="hidden" name="<?=$sAAAAAA->id?>"/> <span
                                                                        style="position:relative;left:1em; top:4px;"><?=$sAAAAAA->name_sr?></span></label>
                                                            <?php
                                                            $vals = ProductDetails::find_by_settingID($sAAAAA->id);
                                                            if ($vals) {
                                                                foreach ($vals as $v):?>
                                                                    <label style="float:left;clear:left; width: 100%;position:relative; left:7em;">
                                                                        <input type="checkbox"
                                                                               name="atributesDetails[<?=$v->id?>]"
                                                                               value="<?=$v->id?>" <?php if (in_array($v->id, $atttributes)) {
                                                                            echo 'checked';
                                                                        }?>/>
                                                                        <span style="position:relative;left:1em; top:4px;"><?=$v->value_sr?></span>
                                                                    </label>
                                                                    <?php endforeach;
                                                            }
                                                        endforeach;
                                                    }
                                                endforeach;
                                            }
                                        endforeach;
                                    }
                                endforeach;
                            }
                        endforeach;
                    }
                }
            }
            ?></div>
    </div>
    <?php
endforeach;
    ?>

</div>
<h3 style="padding:1em;display:block;clear:both; width: 100%;">Karakteristike proizvoda <br>
    <a href="#" class="addChar" style="font-size:.85em;">Dodaj karakteristiku</a>

    | <a href="#" class="addCharCategory" style="font-size:.85em;">Dodaj kategoriju karakteristika</a>
</h3>

<div class="table" style="float:left;clear:left;width:100%; margin-top:1em  ">
    <?php
    //list here ch categories
    //list inside ch cat. list of characteristics
    //list values wth checkboxes and connect them to product

    $chCarateristics = Characteristics::find_by_category($product->category);
    //    print_r($chCarateristics);

    if ($chCarateristics) {
        ?>
        <div class="head"><h5 class="iFrames">Lista karakteristika</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th>Karakteristika</th>
                <th>Vrednost (sr)</th>
                <th>Vrednost (en)</th>
            </tr>
            </thead>
            <tbody>
                <?php
                foreach ($chCarateristics as $chC):?>
                <tr>
                    <td align="center"><strong><?=$chC->characteristics_name_sr?>
                        <?php $value = CharacteristicsValue::find_by_product_and_char($product->id, $chC->id);
                        //                        print_r($value);
                        if (!$value) {
                            $value = new CharacteristicsValue();
                        }
                        ?>

                    </strong></td>
                    <td>
                        <input type="hidden" name="charID[]" value="<?php if ($chC->id) {
                            echo $chC->id;
                        }?>">

                        <?php if (strpos($value->value_name_sr, '<br>') !== false) { ?>
                        <textarea name="characteristicSr[<?=$chC->id?>] "
                                  style="float:left;padding:3px;border-color:#ddd;width: 160px;height: 50px;"><?=str_replace('<br>',"***\n", $value->value_name_sr)?></textarea>
                        <?php } else { ?>
                        <input type="text" name="characteristicSr[<?=$chC->id?>]" value="<?=$value->value_name_sr?>"
                               style="width:98%; position:relative; left:7px; ;text-align: center;">
                        <?php } ?>

                    </td>
                    <td>
                        <?php if (strpos($value->value_name_en, '<br>') !== false) { ?>
                        <textarea name="characteristicSr[<?=$chC->id?>]"
                                  style="float:left;padding:3px;border-color:#ddd;width: 140px;height: 50px;" ><?=str_replace('<br>', "***\n", $value->value_name_en)?></textarea>
                        <?php } else { ?>
                        <input type="text" name="characteristicEn[<?=$chC->id?>]"
                               value="<?=$value->value_name_en?>"
                               style="width:98%;text-align: center; position:relative; left:7px;">
                        <?php } ?>
                    </td>
                </tr>
                    <? endforeach; ?>

            </tbody>
        </table>
        <?php }  ?>
</div>
<p class="note">Napomena: za novi red u opisu karakteristika, koristite ***</p>
<div class="table" style="float:left;clear:left;display:none">
    <div class="head"></div>
    <input type="hidden" name="characteristicsID[sr]" value="<?= $ch_sr->id ?>">
    <input type="hidden" name="characteristicsID[en]" value="<?= $ch_en->id ?>">


    <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic mainMenu">
        <thead>
        <tr>
            <td style="width:24%">Tip karakteristike</td>
            <td width="38%">Srpski</td>
            <td width="38%">Engleski</td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($ch_sr as $key => $value):
//listing all characteristics
            if ($key != 'id' && $key != 'lang' && $key != 'productID' && $key != 'obligatory' && $key != 'similar') {
                ?>
            <tr>
                <td align="left"><?=translateField($key)?></td>
                <td><input type="text" name="<?=$key?>[sr]" value="<?=$value?>"
                           style="width:95%;text-align:center; font-size:1.3em"></td>
                <td><input type="text" name="<?=$key?>[en]" value="<?=$ch_en->$key?>"
                           style="width:95%;text-align:center; font-size:1.3em"></td>
            </tr>
                <?php
            }
        endforeach; ?>
        </tbody>
    </table>
</div>
</div>
<!-- Content of tab9 -->
<div id="tab9" class="tab_content">
    <p>Napomena: Nije neophodno dodavati linkove za drivere, brošure, tiketing ili dodatnu opremu...</p>

    <div class="table" style="margin-top:1em">

        <div class="head"><h5>Lista linkova u podmeniju - <a href="#" class="addNewLink">Dodaj novi link</a></h5></div>
        <table class="tableStatic" cellpadding="0" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td width="2.5%">Pozicija</td>
                <td width="20%">Tekst linka - spski</td>
                <td width="20%">Tekst linka - engleski</td>
                <td width="20%">Adresa - url</td>

            </tr>
            </thead>
            <tbody>
            <?php
            $subMenu = SubMenu::find_for_page($product->id, 'product');
            if ($subMenu && count($subMenu) > 0) {
                foreach ($subMenu as $m): ?>
                <tr>
                    <td><input type="text" name="sub_ordering[<?=$m->id?>]" value="<?=$m->ordering?>" class="centerAl"
                               style="width: 95%;left:10px">
                        <input type="hidden" name="submenuID[<?=$m->id?>]" value="<?=$m->id?>">
                    </td>
                    <td><input type="text" name="sub_name_sr[<?=$m->id?>]" value="<?=$m->name_sr?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                    <td><input type="text" name="sub_name_en[<?=$m->id?>]" value="<?=$m->name_en?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                    <td><input type="text" name="sub_linkSr[<?=$m->id?>]" value="<?=$m->linkSr?>" class="centerAl"
                               style="width: 98%;left:10px"></td>
                </tr>
                    <?php endforeach;
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<div id="tab3" class="tab_content">
    <fieldset>
        <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
        <input type="hidden" name="current" value="1" class="current"/>
        <input id="file_upload" class="fileInput" type="file" name="fileUpload[]"/>

        <div class="fields"></div>
        <div style="float:left; margin:1.5em;width:300px;clear:left;">
            <label for="url">Link fotografije</label>
            <input type="text" class="textField" name="url" id="url" style="padding:.5em; text-align: left;"
                   value="<?= $product->url ?>">
        </div>
        <br/>
    </fieldset>
    <fieldset id="galleryImages">
        <?php
        //$_GET['page'] = $event->event_type;
        require_once(ROOT_DIR . 'admin/pages/galleryList.php');
        ?>
    </fieldset>
</div>
</div>
</fieldset>
<a href="<?= ADMIN ?>proizvodi"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
<input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
</form>
</div>
</div>