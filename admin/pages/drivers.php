<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>

<!-- Content -->
<div class="content">

    <div class="title"><h5>Driveri</h5></div>
    <div class="widget" >
        <?php $drivers = Driver::find_all();
        if ($session->message() != '') {
        echo "<br>";
        if ($_SESSION['mType'] == 2) {
            $messageType = 'valid';
        } else {$messageType = 'invalid'; }
        echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>'; }?>

        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"><a href="<?=ADMIN?>novi-driver">Dodaj novi driver</a></h5>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                <tr>
                    <th>Naziv/Verzija</th>
                    <th>Proizvodi</th>
                    <th>Aktivan</th>
                    <th>&nbsp;&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($drivers as $driver) { ?>
                <tr class="gradeA" <?=$driver->id?>>
                    <td class="center"><a href="<?=ADMIN?>driver/<?=$driver->id?>">
                        <strong><?=$driver->name_sr?><strong></a></td>
                    <td class="center"> <?=Product::products_names($driver->products)?></td>
                    <td class="center">
                        <input type="checkbox" name="active" <?php if ($driver->active == 1) {
                            echo 'checked';
                        } ?> />
                    </td>
                    <td class="center">
                        <a href="<?=ADMIN?>driver/<?=$driver->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                        <a href="<?=ADMIN?>pages/deleteDriver.php?id=<?=$driver->id?>" class="delItem"> Delete </a>
                    </td>
                </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
        <div class="title" style="margin-top:30px"><h5>Uputstva</h5></div>
        <div class="widget">
        <?php $instructions = Instruction::find_all();
        if ($session->message() != '') {
            echo "<br>";
            if ($_SESSION['mType'] == 2) {
                $messageType = 'valid';
            } else {$messageType = 'invalid'; }
            echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>'; }?>

        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"><a href="<?=ADMIN?>novo-uputstvo">Dodaj novo uputstvo</a></h5>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                <tr>
                    <th>Naziv/Verzija</th>
                    <th>Proizvodi</th>
                    <th>Aktivan</th>
                    <th>&nbsp;&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($instructions as $instruction) { ?>
                <tr class="gradeA" <?=$instruction->id?>>
                    <td class="center"><a href="<?=ADMIN?>uputstvo/<?=$instruction->id?>">
                        <strong><?=$instruction->name_sr?><strong></a></td>
                    <td class="center"> <?=Product::products_names($instruction->products)?></td>
                    <td class="center">
                        <input type="checkbox" name="active" <?php if ($instruction->active == 1) {
                            echo 'checked';
                        } ?> />
                    </td>
                    <td class="center">
                        <a href="<?=ADMIN?>uputstvo/<?=$instruction->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                        <a href="pages/deleteInstruction.php?id=<?=$instruction->id?>" class="delItem"> Delete </a>
                    </td>
                </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
    <div class="fix"></div>
</div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>

