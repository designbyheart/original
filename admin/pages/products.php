<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>

<!-- Content -->
<div class="priceListWizard">
    <form action="<?=ADMIN?>pages/saveExcell.php?price=yes" method="post">
        <h2>Izaberite elemente za export</h2>

        <ul>
            <li style="margin-bottom:.2em"><input type="checkbox" name="all" id="all" class="allAttributes"><label
                    for="all"><strong>Izaberi sve</strong></label></li>
            <li style="margin-top:1em"><input type="checkbox" name="plD" id="plD"><label for="plD">Interni kat.
                broj</label></li>
            <li><input type="checkbox" id="secondIDs" name="secondIDs"><label for="secondIDs">Zamenski kat. broj</label>
            </li>
            <li><input type="checkbox" name="distribID" id="distribID"><label for="distribID">Šifra dobavljača</label>
            </li>
            <li><input type="checkbox" id="status" name="status"><label for="status">Status</label></li>
            <li style="margin-top:1em"><input type="checkbox" id="price" name="price"><label for="price">Cenovne
                grupe</label></li>
            <li><input type="checkbox" name="desc_sr" id="desc_sr"><label for="desc_sr">Opis proizvoda</label></li>
            <li><input type="checkbox" name="desc_en" id="desc_en"><label for="desc_en">Opis proizvoda (eng)</label>
            </li>
            <li><input type="checkbox" name="attributeSet" id="attributeSet"><label for="attributeSet">Atribut
                setovi</label></li>
            <li><input type="checkbox" name="notes" id="notes"><label for="notes">Napomene</label></li>
            <li><input type="checkbox" name="characteristics" id="characteristics"><label for="characteristics">Karakteristike</label>
            </li>
            <li><input type="checkbox" name="relatedProducts" id="relatedProducts"><label for="relatedProducts">Povezani
                proizvodi</label></li>
        </ul>
        <input class="selectedProducts" name="products" type="hidden">
        <input type="submit" name="submit" value="Eksportuj" class="greyishBtn exportTemplateBttn submitForm"
               style="position:relative; top:2em;"/>
        <a class="closeExportMenu" href="#"><img src="<?=ADMIN?>doc/images/icons/dark/close.png" alt=""></a>
    </form>
</div>
<div class="importWizard" style="display:block;position:absolute; top:-2000px">
    <form action="<?=ADMIN?>pages/importWizard.php" method="post" enctype="multipart/form-data">
        <h2>Uvoz proizvoda</h2>
        <label for="groupID" id="groupID">Izaberite kategoriju proizvoda <br>
            <?php $groups = Category::find_all();
//        print_r($groups);
            ?>
            <select name="groupID" id="groupID">
                <option value="0">Izaberite kategoriju</option>
                <?php
                foreach ($groups as $g) {
                    ?>
                    <option value="<?=$g->id?>"><?=$g->name_sr?></option>
                    <? } ?>

            </select>
        </label>
        <label for="manufacturer" style="float:left; clear:left; margin-top:1.4em">
            Izaberite proizvođača <br>
            <?php $man = Manufacturer::find_all(); ?>
            <select name="manufacturer" id="manufacturer">
                <option value="0">Izaberi..</option>
                <?php foreach ($man as $m): ?>
                <option value="<?=$m->id?>"><?=$m->name_sr?></option>
                <?php endforeach; ?>
            </select>
        </label>
        <label for="chooseFile" style="float:left; clear:left; margin-top:2em;">Izbor .excel fajla za uvoz <br>
            <input type="file" name="file" id="chooseFile">
        </label>
        <input type="submit" name="submit" value="Import" class="greyishBtn submitForm"
               style="position:relative; top:3.5em;"/>
    </form>
    <a class="closeExportMenu" href="#"><img src="<?=ADMIN?>doc/images/icons/dark/close.png" alt=""></a>
</div>
<div class="content">
    <div class="title"><h5>Proizvodi</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $products = Product::find_by_sql("SELECT *, (select category.name_sr from category where category.id = product.category) as category, (select name_sr from manufacturer where manufacturer.id = product.manufacturer) as manufacturer FROM product order by category ASC, active ASC, name_sr ASC");
    if (isset($_GET['type'])) {
        $products = Product::find_by_sql("SELECT *, (select category.name_sr from category where category.id = product.category) as category, (select name_sr from manufacturer where manufacturer.id = product.manufacturer) as manufacturer FROM product where updated > '" . date('Y-m-d H:i:s', time()) . "' order by category ASC, active ASC, name_sr ASC");
    }
    ?>

    <?php if ($session->message() != '') {
    echo "<br>";
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>

    <div class="saveSearchTemplateWrapper priceChooser">
        <h5>Primenite šablon cena</h5>
        <?php $priceT = PriceTemplate::find_all();?>
        <select name="" id="priceTemplates">
            <option value="0">Izaberite šablon</option>
            <?php foreach ($priceT as $pT): ?>
            <option value="<?=$pT->id?>"><?=$pT->name?></option>
            <?php endforeach; ?>

        </select>
        <input type="submit" class="button greyishBtn applyPriceTemplate" rel="<?=$_SERVER['REQUEST_URI']?>"
               value="Primeni">
        <input type="submit" class="button greyishBtn cancelTemplate" value="Otkaži">
    </div>
    <!-- Dynamic table -->
    <div class="table" style="margin-bottom: 1em;">
        <div class="head"><h5 class="iFrames"><a href="<?=ADMIN?>novi-proizvod">Dodaj proizvod</a> | <a
                href="<?=ADMIN?>rezultati-importa">Privremeni proizvodi (nakon importa)</a></h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
            <thead>
            <tr>
                <th><a href="#" class="selectAll">Select all</a></th>
                <th>Naziv</th>
                <th>Kategorija</th>
                <th>Model</th>
                <th>Proizvodjac</th>
                <th>Aktivan</th>
                <th>Cena</th>
                <th> &nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product) { ?>
            <tr class="gradeA" <?=$product->id?> id="tr<?=$product->id?>">
                <td align="center"><input type="checkbox" name="" id="<?=$product->id?>"
                                          class="productID productCheck"/></td>
                <td class="center"><a href="<?=ADMIN?>proizvod/<?=$product->id?>">
                    <strong><?=$product->name_sr?><strong></a></td>
                <td class="center"> <?=$product->category ?></td>
                <td class="center"><?=$product->model?></td>

                <td class="center"> <?=$product->manufacturer ?></td>
                <td class="center">
                    <input type="checkbox" name="active"
                        <?php if ($product->active == 1) {
                        echo 'checked';
                    }
                        ?>
                            />
                </td>
                <td class="center">
                    <span class="webStatsLink"><?=$product->price?></span>
                </td>
                <td class="center">
                    <a href="<?=ADMIN?>proizvod/<?=$product->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                    <a href="<?=ADMIN?>pages/deleteProduct.php?id=<?=$product->id?>" class="delItem"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>
    <a href="#" class="importTemplate btn14">Import cenovnika</a>
    <a href="#" class="exportTemplate btn14">Download cenovnika</a>
    <a href="#" class="deleteProducts btn14">Izbriši proizvode</a>
    <a href="#" class="priceTemplate btn14">Primeni šablon cena</a>
    <a href="#" class="addPriceTemplate btn14">Primeni cenovni šablon</a>
</div>
<div class="fix"></div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a href="http://designbyheart.info">Design
            by Heart</a></span>

