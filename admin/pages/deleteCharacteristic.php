<?php
require_once('../../framework/lib/setup.php');

$characteristics = new Characteristics();
if (isset($_GET['id']) && $_GET['id'] != '') {
    $characteristics->id = $_GET['id'];


    if ($characteristics->delete()) {
        $session->message('Karakteristika je izbrisana');
        $_SESSION['mType'] = 2;
        $_SESSION['tab'] = 3;
        redirect_to(ADMIN . 'podesavanja');

    } else {
        $session->message('Postoji problem. Karakteristika nije izbrisana');
        $_SESSION['mType'] = 4;
        $_SESSION['tab'] = 3;
        redirect_to(ADMIN . 'podesavanja');
    }
}
?>