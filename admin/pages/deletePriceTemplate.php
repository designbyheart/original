<?php
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $category = PriceTemplate::find_by_id($_GET['id']);

    if($category->delete()){
        $session->message('Template je izbrisan');
        $_SESSION['mType']= 2;
        redirect_to(ADMIN.'proizvod/'.$category->productID);

    }else{
        $session->message('Postoji problem. Template nije izbrisan');
        $_SESSION['mType']= 4;
        redirect_to(ADMIN.'proizvod/'.$category->productID);
    }
}

?>