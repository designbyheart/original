<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/26/12
 * Time: 10:06 AM
 * To change this template use File | Settings | File Templates.
 */

require_once('../../framework/lib/setup.php');
if (isset($_GET)) {
    $c = Characteristics::find_by_id($_GET['id']);
    if ($c) {
        $c->characteristics_name_sr = str_replace('***', '<br>', str_replace("\n", "<br>", str_replace(',', ' ', $_GET['characteristics_name_sr'])));
        $c->characteristics_name_en = str_replace('***', '<br>', str_replace("\n", "<br>", str_replace(',', ' ', $_GET['characteristics_name_en'])));
        $c->ordering = $_GET['ordering'];
        $c->category_id = $_GET['category_id'];
        if ($c->save()) {
            echo 'OK';
        } else {
            echo 'Nije sačuvano';
        }
    }
}