<?php require_once(ADMIN_ROOT . 'doc/inc/side.php'); ?>

<!-- Content -->
<div class="content">
    <div class="title"><h5 style="width:96%;">Tiketi <a href="<?=ADMIN?>kategorije-tiketa"
                                                        style="color:#ccc; float:right">Edituj kategorije tiketa</a>
    </h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $tickets = Mail::find_by_sql("SELECT * FROM mail where type ='contactSupport' order by id ASC");

    if ($session->message() != '') {
        echo "<br>";
        if ($_SESSION['mType'] == 2) {
            $messageType = 'valid';
        } else {
            $messageType = 'invalid';
        }
        echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
    }?>
    <!-- Dynamic table -->
    <div class="table">
        <div class="head"></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display dynamicTableData">
            <thead>
            <tr>
                <th>Klijent</th>
                <th>Datum</th>
                <th>Poruka</th>
                <th>Odgovoreno</th>
                <th>&nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tickets as $t) {
                $customer = Customer::find_by_id($t->customer);
                ?>
            <tr class="gradeA" <?=$t->id?>>
                <td align="center">
                    <a href="<?=ADMIN?>tiket/<?=$t->id?>">
                        <?=$customer->name?>
                    </a>
                </td>
                <td align="center" width="15%">
                    <?=date("d.M.y H:i:s", strtotime($t->date))?>
                </td>
                <td class="center">
                    <?=trunc(cleanHTML($t->messageContent), 200)?>
                </td>
                <td align="center">
                    <input type="checkbox" name="status"
                        <?php if ($t->status == 1) {
                        echo "checked";
                    }?>
                            />
                </td>
                <td class="center">
                    <a href="<?=ADMIN?>poruka/<?=$t->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                    <a href="<?=ADMIN?>pages/deleteMessage.php?id=<?=$t->id?>" class="delItem"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>


    <div class="title" style="margin-top:2em;"><h5>Kontakt support</h5></div>

    <!-- Statistics -->
    <!--        <div class="stats">-->
    <!--        	<ul>-->
    <!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
    <!--                -->
    <!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
    <!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
    <!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
    <!--            </ul>-->
    <!--            <div class="fix"></div>-->
    <!--        </div>-->

    <?php
    $tickets = Mail::find_by_sql("SELECT * FROM mail where type ='contactSupportProfile' order by id ASC");

    if ($session->message() != '') {
        echo "<br>";
        if ($_SESSION['mType'] == 2) {
            $messageType = 'valid';
        } else {
            $messageType = 'invalid';
        }
        echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
    }?>
    <!-- Dynamic table -->
    <div class="table">
        <div class="head"></div>
        <table cellpadding="0" cellspacing="0" border="0" class="display dynamicTableData">
            <thead>
            <tr>
                <th>Klijent</th>
                <th>Datum</th>
                <th>Poruka</th>
                <th>Odgovoreno</th>
                <th>&nbsp;&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tickets as $t) {
                $customer = Customer::find_by_id($t->customer);
                ?>
            <tr class="gradeA" <?=$t->id?>>
                <td align="center">
                    <a href="<?=ADMIN?>tiket/<?=$t->id?>">
                        <?=$customer->name?>
                    </a>
                </td>
                <td align="center" width="15%">
                    <?=date("d.M.y H:i:s", strtotime($t->date))?>
                </td>
                <td class="center">
                    <?=trunc(cleanHTML($t->messageContent), 200)?>
                </td>
                <td align="center">
                    <input type="checkbox" name="status"
                        <?php if ($t->status == 1) {
                        echo "checked";
                    }?>
                            />
                </td>
                <td class="center">
                    <a href="<?=ADMIN?>poruka/<?=$t->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                    <a href="<?=ADMIN?>pages/deleteMessage.php?id=<?=$t->id?>" class="delItem"> Delete </a>
                </td>
            </tr>
                <?php
            } ?>

            </tbody>
        </table>
    </div>

</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&#169; Copyright <?=date("Y", time())?> Banjica promet d.o.o. | Powered by <a
                href="http://designbyheart.info">Design by Heart</a></span>


