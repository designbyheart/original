<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Porudžbine</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<?php 
			$orders = Order::find_by_sql("SELECT * FROM productOrder
 where status != 5 AND status != 6 AND status != 7 order by orderDate DESC");
		?>
		  <?php if($session->message()!=''){
		  	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>      
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> </h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Kupac</th>
                        <th>Datum porudžbine</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($orders as $order) {?>
                    <tr class="gradeA" <?=$order->id?>>
                        <td class="center"> <a href="<?=ADMIN?>porudzbina/<?=$order->id?>">
                            <?php
                            $customer = array_shift(Customer::find_by_sql("SELECT id,name,company,type FROM customer WHERE id = {$order->customerID} LIMIT 1"));?>
                        	<strong><?php if($customer->type == 0){echo $customer->name;}else {echo $customer->company;}?><strong></a>
                        </td>
                        <td class="center"><?php 
                        if($order->orderDate > 0){
                        	echo date("d-m-Y", $order->orderDate);
                	    }?>
                        </td>
                        <td class="center">
                    		<a href="<?=ADMIN?>porudzbina/<?=$order->id?>"> Edit </a> &nbsp;&nbsp;&nbsp;
                    		<a href="pages/deleteOrder.php?id=<?=$order->id?>" class="delItem"> Delete </a>
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
	</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


