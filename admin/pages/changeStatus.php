<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 9/16/12
 * Time: 9:30 PM
 * To change this template use File | Settings | File Templates.
 */

 //[orderID] => 28 [newStatus] => close )

require_once "../../framework/lib/setup.php";
//notCompleteOrder 7
    //pending =1
    //confirmed =2
//payed = 3
//sentProducts = 4
//closed = 5
$order = Order::find_by_id($_GET['orderID']);
$order->setStatus($_GET['newStatus']);

if($order->save()){
    $_SESSION['mType']=2;
    $session->message("Status je izmenjen");
    redirect_to(ADMIN.'dashboard');
}else{
    $_SESSION['mType']=4;
    $session->message("Postoji problem. Status NIJE izmenjen");
    redirect_to(ADMIN.'dashboard');
}
