<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 11/3/12
 * Time: 1:11 AM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
$searchTemplate = new SearchTemplate();
$searchTemplate->name =str_replace('||', ' ' , $_GET['name']);
unset($_GET['name']);
$searchTemplate->query = serialize($_GET);
$searchTemplate->save();
?>
<span class="lblInfo" style="position:relative;top:20px;">Šabloni za pretragu:</span>
<select name="" class="searchTemplateSelect" style="position:relative;top:20px;">
    <option value="0">Nema sačuvanih šablona..</option>
    <?php $searchT = SearchTemplate::find_all();
    foreach($searchT as $sT){?>
        <option value="<?=$sT->id?>"><?=$sT->name?></option>
        <? } ?>
</select>