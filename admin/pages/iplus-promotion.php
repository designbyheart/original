<?php require_once(ADMIN_ROOT . 'doc/inc/side.php');

if ($_GET['id'] == 0) {
    $promo = new iPlusPromo();
} else {
    $promo = iPlusPromo::find_by_id($_GET['id']);
}
if ($promo->dateStart == 0) {
    $promo->dateStart = time();
}
?>
<!-- Content -->
<div class="content" xmlns="http://www.w3.org/1999/html">
<div class="title">
    <h5><?php if ($_GET['id'] == 0) { ?>Nova promocija<?
    } else {
        ?>Izmena promocije<? } ?>
    </h5>
</div>

<!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->

<form class="mainForm" action="<?=ADMIN?>savePromo" method="post" enctype="multipart/form-data">
<!-- Tabs -->
<fieldset>
<?php if ($session->message() != '') {
    if ($_SESSION['mType'] == 2) {
        $messageType = 'valid';
    } else {
        $messageType = 'invalid';
    }
    echo '<p class="message ' . $messageType . '">' . $session->message() . '<span class="close"> X </span></p>';
}?>
<div class="widget" style="overflow: visible;">
<ul class="tabs">
    <li><a href="#tab1">Srpski</a></li>
    <li><a href="#tab2">Engleski</a></li>
    <li><a href="#tab5">Cenovnik</a></li>
    <li><a href="#tab7">Proizvodi</a></li>
    <li><a href="#tab3">Galerija</a></li>
    <li><a href="#tab4">Seo</a></li>
</ul>

<div class="tab_container"
     style="background:#f9f9f9; border:solid 1px #cfcfcf; border-top: none;position: relative;left:-1px; width: 99.975%; overflow: visible;">
<input type="hidden" name="id" value="<?=$promo->id?>"/>

<div id="tab1" class="tab_content">

    <div class="rowElem noborder">
        <label>Naziv promocije:</label>

        <div class="formRight">
            <input type="text" name="name_sr" value="<?=$promo->name_sr?>"/>
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Aktivan:</label>

        <div class="formRight">
            <input type="checkbox" name="active"
                <?php if ($promo->active == 1) {
                echo 'checked';
            } ?>
                    />
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <label>Trajanje promocije:</label>

        <div class="formRight">
            <label for="startDate" style="width: 45%;"><span style="float:left">Početak</span>
                <input type="text" class="datepicker" name="dateStart" value="<?=date('d.m.Y', $promo->dateStart)?>"
                       id="startDate"
                       style="float:left; width: 65%;margin-left: 10px;position: relative;top: -3px ;position: relative;">
            </label>
            <label for="endDate" style="width: 45%;"><span style="float:left">Kraj</span>
                <?php  if ($promo->dateEnd > 0) {
                    $promo->dateEnd = date('d.m.Y', $promo->dateEnd);
                } else {
                    $promo->dateEnd = "";
                } ?>
                <input type="text" class="datepicker" name="dateEnd" value="<?=$promo->dateEnd?>" id="endDate"
                       style="float:left;width: 65%;margin-left: 10px;position: relative;top: -3px ;position: relative;">
            </label>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Opis promocije: </h5></div>
                <textarea class="wysiwyg" name="desc_sr" rows="5"><?=$promo->desc_sr?></textarea>
            </div>
        </div>
    </div>

</div>
<div id="tab2" class="tab_content">

    <div class="rowElem noborder">
        <label>Naziv promocije:</label>

        <div class="formRight">
            <input type="text" name="name_en" value="<?=$promo->name_en?>"/>
        </div>
        <div class="fix"></div>
    </div>

    <div class="rowElem">
        <div class="formRight" style="width: 100%">
            <div class="widget" style="margin-top: 5px">
                <div class="head"><h5 class="iPencil">Opis promocije: </h5></div>
                <textarea class="wysiwyg" name="desc_en" rows="5" cols="">
                    <?=$promo->desc_en?>
                </textarea></div>
        </div>
        <div class="fix"></div>
    </div>
</div>
<div id="tab3" class="tab_content">
    <fieldset>
        <legend>Upload fotografija <a href="#" id="addMoreFields">+</a></legend>
        <input type="hidden" name="" value="1" class="current"/>

        <input id="file_upload" class="fileInput" type="file" name="image0"/>

        <div class="fields"></div>
        <br/>
        <input type="checkbox" name="fullWidth"
            <?php if ($promo->fullWidth == 1) {
            echo 'checked';
        } ?>
                />

        <label>Prikaži sliku celom širinom strane :</label>

        <div class="fix"></div>

    </fieldset>
    <fieldset id="galleryImages">
        <?php
        //$_GET['page'] = $event->event_type;
        require_once(ROOT_DIR . 'admin/pages/galleryList.php');
        ?>
    </fieldset>
</div>
<div id="tab4" class="tab_content">
    <div class="rowElem noborder">
        <label>Naziv promocije(srpski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_sr" value="<?=$promo->seo_title_sr?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem noborder">
        <label>Naziv promocije(engleski):</label>

        <div class="formRight">
            <input type="text" name="seo_title_en" value="<?=$promo->seo_title_en?>">
        </div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(srpski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_sr"><?=$promo->seo_desc_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>Opis strane za pretraživače(engleski):</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_desc_en"><?=$promo->seo_desc_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(srpski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_sr"><?=$promo->seo_keywords_sr?>
            </textarea></div>
        <div class="fix"></div>
    </div>
    <div class="rowElem">
        <label>SEO ključne reči(engleski) :</label>

        <div class="formRight">
            <textarea rows="8" cols="" name="seo_keywords_en"><?=$promo->seo_keywords_en?>
            </textarea></div>
        <div class="fix"></div>
    </div>
</div>
<div id="tab5" class="tab_content">
    <div class="table">
        <div class="head"><h5>Cene i popusti/provizije</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic">
            <thead>
            <tr>
                <td>Tip popusta/provizije</td>
                <td width="15%">Računati od</td>
                <td width="18%">Procenat</td>
                <td width="21%">Iznos</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Osnovna cena promocije</td>
                <td>&nbsp;</td>
                <td align="center" class="calcTypeLbls">
                    <?php /*; ?> Izaberite vrstu kalkulacije
                    <span class="0" style="float:left"><input class="calcType" type="radio" name="type" checked
                                                              value="0"> <span
                        style="position: relative;left:10px;top:3px">Fiksna cena</span>     <br>                                                        </span>
                    <span class="1" style="float:left"> <input class="calcType" type="radio"
                                                               name="type" <?php if ($promo->calcType == 1) {
                        echo 'checked';
                    } ?> value="1"> <span
                        style="position: relative;left:10px;top:3px">Dodavanje</span>    <br>       </span>
                    <span class="2" style="float:left"><input class="calcType" type="radio"
                                                              name="type" <?php if ($promo->calcType == 2) {
                        echo 'checked';
                    } ?> value="2"> <span style="position: relative;left:10px;top:3px">Oduzimanje</span>   <br></span>
                                    </td>
 */ ?>
                <td align="center"><input type="text" name="price" class="webStatsLink mainPrice"
                                          style="width:93%; font-size: 15px;text-align:center"
                                          value="<?=$promo->price?>"></td>
            </tr>
            <?php $mGroups = PriceGroup::find_main();
//                $prGroups = PriceGroup::find_by_product($promo->id);
            foreach ($mGroups as $mGroup):
//                $group = PriceGroup::find_by_product($group, $promo->id);
                ?>
            <tr>
                <td><?=$mGroup->name?></td>
                <?php
                $g = PriceGroup::find_is_exists($mGroup->id, $promo->id);
                $p = new Price();
                if ($g) {
                    $p = Price::find_by_product_and_group($promo->id, $g->id);
                }
                ?>
                <td>
                    <select name="" id="$g">
                        <option value="">Proizvod</option>
                        <?php foreach ($mGroups as $m):
                        if ($m->name != $mGroup->name) {
                            ?>
                            <option value="<?=$m->id?>"><?=$m->name?></option>
                            <?php
                        }
                    endforeach; ?>
                    </select>
                </td>
                <td align="center"><input type="text" name="group[<?=$mGroup->id?>]" value="<?php if ($g) {
                    echo $g->percentage;
                }?>" class="groupPercent" id="group<?=$mGroup->id?>" style="text-align: center; width:93px;"></td>

                <td><input type="text" name="prices[<?=$mGroup->id?>]" class="webStatsLink productGroupPrice"
                           value="<?php if ($p) {
                               echo $p->amount;
                           }?>" id="price<?=$mGroup->id?>" style="width:93%; text-align: center; font-size:13px;"></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<div id="tab7" class="tab_content">
    <?php $prS = Product::find_similar($promo->id); ?>

    <div class="table" style="width:100%;margin:0">
        <div class="head"><h5 class="iFrames">Izaberi proizvod</h5></div>
        <table cellpadding="0" cellspacing="0" border="0" class="tableStatic linkedProducts" id="example"
               style="width:100%!important">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="20%">Ime promocije</td>
                <td width="20%">Kategorija</td>
                <td width="20%">Serijski broj</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($prS as $ps): ?>
            <tr>
                <td align="center">
                    <a href="<?=$ps->id?>" title="" class="btnIconLeft mr10 chooseProduct"
                       style="margin:0 0 -5px 0;padding:0 15px;"><span style="padding:2px 8px;">Izaberi</span></a>
                </td>
                <td align="center" id="productName<?=$ps->id?>"><h4><?=$ps->name_sr?></h4></td>
                <td align="center" id="categoryName<?=$ps->id?>"><?=$ps->category?></td>
                <td align="center"><?=$ps->plD?></td>
            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="widget first">
        <div class="head"><h5 class="iFrames">Izabrani proizvodi</h5></div>
        <table cellpadding="0" cellspacing="0" width="100%" class="tableStatic" id="selectedList">
            <thead>
            <tr>
                <td width="5%"></td>
                <td width="30%">Ime promocije</td>
                <td width="30%">Kategorija</td>
            </tr>
            </thead>
            <tbody>
            <?php if ($promo->products != '') {
                $connectedProducts = array_filter(explode(",", $promo->products));

                foreach ($connectedProducts as $cProductID) {
                    $cProduct = Product::find_by_id($cProductID);
                    $category = Category::find_by_id($cProduct->category);
                    ?>
                <tr class="<?=$cProduct->id?>">
                    <td align="center">
                        <a href="<?=$cProduct->id?>" class="btnIconLeft mr10 removeProduct"
                           style="margin:0 0 -5px 0; padding:0 15px;">
                            <span style="padding:2px 8px;">Ukloni</span>
                        </a>
                    </td>
                    <td align="center"><h4><?=$cProduct->name_sr?></h4>
                    </td>
                    <td align="center"><strong><?php if ($category) {
                        echo $category->name_sr;
                    }?></strong></td>
                </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="fix"></div>
    <input type='hidden' name="products" id="linkedProducts" value="<?=$promo->products?>">
</div>
<div id="tab8" class="tab_content">

</div>
</div>
</div>
</fieldset>
<a href="<?=ADMIN?>proizvodi"><input type="button" value="Otkazi" class="greyishBtn submitForm"/></a>
<input type="submit" name="submit" value="Sačuvaj" class="greyishBtn submitForm"/>
</form>
</div>
