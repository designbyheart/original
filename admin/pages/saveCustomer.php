<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/14/12
 * Time: 3:05 PM
 * To change this template use File | Settings | File Templates.
 */
require_once('../../framework/lib/setup.php');
//[id] => 1 [name_sr] => aloha.. test1 [type] => 1 [version] => 1.2.3. [active] => on [desc_sr] => [name_en] => [desc_en] => [similarProducts] => [submit] => SaÄuvaj
if (isset($_POST)) {

    //echo($_POST['id']);
    if (isset($_POST['id']) && $_POST['id']) {
        $customer = Customer::find_by_id($_POST['id']);
    }
    if (!isset($customer)) {
        $customer = new Customer();
        if (!isset($_POST['email']) && count(trim($_POST['email'])) > 0 && !isset($_POST['password'])) {
            $session->message(trans("Morate uneti e-mail adresu korisnika", "You need to enter clients email adress"));
            redirect_to(ADMIN . "novi-klijent");
        }
        if (!Customer::isEmailFree($_POST['email'])) {
            $session->message(trans("Ova email adresa je već registrovana. Unesite drugu email adresu.", "This email address is already registered. Please enter another email address"));
            redirect_to(ADMIN . "novi-klijent");
        }
        if (!checkEmail($_POST['email'])) {
            $session->message(trans("Email adresa nije ispravna", "Email address is not correct"));
            redirect_to(ADMIN . "novi-klijent");
        }
        foreach ($customer as $key => $value) {
            if ($key != 'id' && isset($_POST[$key])) {
                $customer->$key = $_POST[$key];
            }
        }
    }

    if (isset($_POST['partner'])) {
        $customer->partner = 1;
    } else {
        $customer->partner = 0;
    }
    $customer->password = md5($_POST['username'] . '--' . $_POST['password']);

    if ($customer && $customer->save()) {
        $_SESSION['mType'] = 2;
        $session->message('Klijent je sačuvan');
        redirect_to(ADMIN . 'klijenti');
    } else {
        $_SESSION['mType'] = 4;
        $session->message('Postoji problem. Klijent nije sačuvan');
        redirect_to(ADMIN . 'klijent/' . $customer->id);
    }
}