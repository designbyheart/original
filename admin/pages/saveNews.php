<?php
require_once('../../framework/lib/setup.php');


if(isset($_POST['id']) && $_POST['id']!=0 && News::find_by_id($_POST['id'])){
    $news = News::find_by_id($_POST['id']);
    $new = false;
}else{
    $news = new News();
    $new = true;
}

if(isset($_POST['active']) && $_POST['active']=="on"){
	$_POST['active']=1; 
}else{
	$_POST['active']=0;
}


    foreach($news as $key=>$value){
        // echo $_POST[$key];

        if($key!='id' && isset($_POST[$key])){
            $news->$key = trim($_POST[$key]);
           // echo $news->$key ."<br>";
        }   	
	}
	if(isset($_POST['date']) && $_POST['date'] != ''){
		$news->date = date("Y-m-d", strtotime($_POST['date']));
	}else{
		$news->date = '';
	}


if(isset($_POST['submit']))	{
	
	if($news){
		//save news
	  $id = 0;
	  $new_id = $news->save();

	  // getting id for gallery refID
	  if($id != 0){$id = $new_id;} elseif($news->id !=0){$id = $news->id;}
	  if($id !=0){
		  if(isset($_FILES)){
		       foreach($_FILES as  $file){
		           if ($file['name']!=''){
		               $gal = new Gallery();
		               $gal->file = cleanFileName('aktuelnost-'.$file['name']);
		               $gal->refID = $id;
		               $gal->type = 'news';
		
		               uploadPhoto($file, '', 400, 100, 80, 250, 200, $gal->file);
		               if($gal->save()){
			               $session->message('Slika je uneta');
			               $_SESSION['mType']= 2;
		               }else{
		               	   $session->message('Postoji problem. Slika nije uneta');
			               $_SESSION['mType']= 4;
		               }
		           }
		        }
    		}	
	  }
	  
	  if($new_id == 1){
	  	$session->message('Promene su uspešno sačuvane '. $new_id);
	  }elseif($new_id > 1){
	  	$session->message('Aktuelnost je sačuvana');
	  }
	  $_SESSION['mType']= 2;
	  redirect_to(ADMIN.'aktuelnosti');
	}else{
	  $session->message('Postoji problem. Aktuelnost nije sačuvana');
	  $_SESSION['mType']= 4;
	  redirect_to(ADMIN.'aktuelnost/'.$news->id);
	}
}
redirect_to(ADMIN.'aktuelnost/'.$news->id);
