<?php
/**
 * Created by JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/23/12
 * Time: 12:37 AM
 * To change this template use File | Settings | File Templates.
 */

require_once('../../framework/lib/setup.php');
if (isset($_POST['css'])) {
    if (isset($_POST['mainMenu']) && $_POST['mainMenu'] != '') {
    foreach($_POST['ids'] as $key=>$value){

        $css = MainMenu::find_by_id($_POST['cssID']);
    }
    if (isset($css)) {
        $css = new MainMenu();
    }
    if (isset($_POST['css'])) {
        $css->css = $_POST['css'];
    }
    $css->type = $_POST['type'];
//    $css->columnT = $_POST['columnT'];
//    $css->save();
    }
}
if (isset($_POST['newLink'])) {
    $css = new MainMenu();
    $css->type = $_POST['type'];
    if(isset($_POST['columnT'])){
        $css->columnT = $_POST['columnT'];
    }
    $css->name_en = $_POST['name_en'];
    $css->name_sr = $_POST['name_sr'];
    $css->url = $_POST['url'];
    $css->save();
    redirect_to(ADMIN . 'footerLink');
}

//echo "<pre>";
//print_r($_POST);
//             echo "</pre>";
if ($_POST['type'] != 'footer') {
    foreach($_POST['ids'] as $id){
        $m = MainMenu::find_by_id($id);
        if (isset($_POST['name_sr'])) {
            $m->name_sr = trim($_POST['name_sr'][$id]);
        }
        if (isset($_POST['name_en'])) {
            $m->name_en = trim($_POST['name_en'][$id]);
        }
        if (isset($_POST['ordering'])) {
            $m->ordering = trim($_POST['ordering'][$id]);
        }
        if (isset($_POST['url'])) {
            $m->url = trim(str_replace(SITE_ROOT, '', $_POST['url'][$id]));
        }
        $m->save();
    }
    redirect_to(ADMIN . 'glavni-meni');
} else {
    foreach ($_POST['ids'] as $key => $value) {
        $m = MainMenu::find_by_id($value);
        $m->name_sr = trim($_POST['name_sr'][$key]);
        $m->name_en = trim($_POST['name_en'][$key]);
        $m->ordering = trim($_POST['ordering'][$key]);
        $m->columnT =  $_POST['columnType'][$key];
        $m->url = trim(str_replace(SITE_ROOT, '', $_POST['url'][$key]));
        $m->save();
    }
    redirect_to(ADMIN . 'footerLink');

}

