<?php
require_once('../../framework/lib/setup.php');

if (isset($_POST['id']) && $_POST['id'] != 0 && TicketsCategory::find_by_id($_POST['id'])) {
    $category = TicketsCategory::find_by_id($_POST['id']);
    $new = false;
} else {
    $category = new TicketsCategory();
    $new = true;
}

if (isset($_POST['active']) && $_POST['active'] == "on") {
    $_POST['active'] = 1;
} else {
    $_POST['active'] = 0;
}
foreach ($category as $key => $value) {
    // echo $_POST[$key];

    if ($key != 'id' && isset($_POST[$key])) {
        $category->$key = trim($_POST[$key]);
        //echo $category->$key ."<br>";
    }
}

if (isset($_POST['submit'])) {

    if ($category) {
        //save category
        $id = 0;
        $new_id = $category->save();

        // getting id for gallery refID
        if ($_POST['id'] == 0) {
            $id = $new_id;
        } else {
            $id = $_POST['id'];
        }
        if ($new_id == 1) {
            $session->message('Promene su uspešno sačuvane ' . $new_id);
        } elseif ($new_id > 1) {
            $session->message('Kategorija je sačuvana');
        }

        $_SESSION['mType'] = 2;
        redirect_to(ADMIN . 'kategorije-tiketa');
    } else {
        $session->message('Postoji problem. Kategorija nije sačuvana');
        $_SESSION['mType'] = 4;
        redirect_to(ADMIN . 'kategorija-tiketa/' . $category->id);
    }
}
redirect_to(ADMIN . 'kategorija-tiketa/' . $category->id);

?>