<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $manufacturer = Manufacturer::find_by_id($_GET['id']);
    
if($manufacturer->delete()){
  $session->message('Proizvođač je izbrisan');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'proizvodjaci');
  
}else{
  $session->message('Postoji problem. Proizvodjac nije izbrisan');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'proizvodjac'); 
}
}

?>