<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $news = Instruction::find_by_id($_GET['id']);
    
if($news->delete()){
  $session->message('Uputstvo je izbrisano');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'drivers');
  
}else{
  $session->message('Postoji problem. Uputstvo nije izbrisan');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'uputstvo/'.$news->id);
}
}

?>