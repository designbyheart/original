<?php 
require_once('../../framework/lib/setup.php');

if(isset($_GET['id']) && $_GET['id']!=''){
    $admin = Administrator::find_by_id($_GET['id']);
    
if($admin->delete()){
  $session->message('Administrator je izbrisana');
  $_SESSION['mType']= 2;  		
  redirect_to(ADMIN.'administratori');
  
}else{
  $session->message('Postoji problem. Administrator nije izbrisan');
  $_SESSION['mType']= 4;  		 
  redirect_to(ADMIN.'administratori'); 
}
}

?>