<?php require_once(ADMIN_ROOT.'doc/inc/side.php'); ?>

	<!-- Content -->
    <div class="content">
    	<div class="title"><h5>Aktuelnosti</h5></div>
        
        <!-- Statistics -->
<!--        <div class="stats">-->
<!--        	<ul>-->
<!--            	<li><a href="#" class="count grey" title="">52</a><span>new pending tasks</span></li>-->
<!--                -->
<!--                <li><a href="#" class="count grey" title="">520</a><span>pending orders</span></li>-->
<!--                <li><a href="#" class="count grey" title="">14</a><span>new opened tickets</span></li>-->
<!--                <li class="last"><a href="#" class="count grey" title="">48</a><span>new user registrations</span></li>-->
<!--            </ul>-->
<!--            <div class="fix"></div>-->
<!--        </div>-->
        
		<?php 
			$news_array = News::find_by_sql("SELECT * FROM news order by active ASC, title ASC");
		?>
		  <?php if($session->message()!=''){
		  	  echo "<br>";
		      if($_SESSION['mType']==2){
		          $messageType = 'valid';
		      }else{
		          $messageType = 'invalid';
		      }
		     echo '<p class="message '.$messageType.'">'.$session->message().'<span class="close"> X </span></p>'; 
		  }?>      
        <!-- Dynamic table -->
        <div class="table">
            <div class="head"><h5 class="iFrames"> <a href="<?=ADMIN?>nova-aktuelnost">Dodaj aktuelnost</a></h5></div>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                <thead>
                    <tr>
                        <th>Naslov</th>
                        <th>Datum</th>
                        <th>Active</th>
                        <th> &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($news_array as $news) {?>
                    <tr class="gradeA" <?=$news->id?>>
                        <td class="center"> <a href="<?=ADMIN?>aktuelnost/<?=$news->id?>"> 
                        	<strong><?=$news->title?><strong></a>
                        </td>
                        <td class="center"><?php 
                        if($news->date != '0000-00-00'){
                        	echo date("d-m-Y", strtotime($news->date));
                	    }?></td>
                    	<td class="center"> 
                    	      <input type="checkbox" name="active" 
					         	  <?php if($news->active == 1){
					               		 echo 'checked';
					              }
					              ?>
				         	  />	
                    	</td>
                    	<td class="center"> 
                    		<a href="<?=ADMIN?>aktuelnost/<?=$news->id?>"> Edit </a> &nbsp;&nbsp;&nbsp; 
                    		<a href="pages/deleteNews.php?id=<?=$news->id?>" class="delItem"> Delete </a>                    		
                    	</td>
                    </tr>
                    <?php 
                		} ?>
                    
                </tbody>
            </table>
        </div>
        
	</div>
<div class="fix"></div>
</div>


<!-- Footer -->
<div id="footer">
	<div class="wrapper">
    	<span>&#169; Copyright <?=date("Y", time())?> Original d.o.o. | Powered by <a  href="http://designbyheart.info">Design by Heart</a></span>


