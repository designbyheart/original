<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<?php
require_once('../framework/lib/setup.php');
if (!$session->is_logged_in()) {
    redirect_to(ADMIN . 'login.php');
}
require_once('doc/pages.php');