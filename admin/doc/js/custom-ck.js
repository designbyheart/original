$(function () {
    function a() {
        var e = $.makeArray();
        $("input.productID:checked").each(function () {
            e.push($(this).attr("id"))
        });
        return e
    }

    $("#dialog-message").dialog({autoOpen:!1, modal:!0, buttons:{Ok:function () {
        $(this).dialog("close")
    }}});
    $("#opener").click(function () {
        $("#dialog-message").dialog("open");
        return!1
    });
    $("a[rel^='prettyPhoto']").prettyPhoto();
    $("input.fileInput").filestyle({imageheight:26, imagewidth:89, width:296});
    $.configureBoxes();
    $(".timepicker").timeEntry({show24Hours:!0, showSeconds:!0, spinnerImage:"images/ui/spinnerUpDown.png", spinnerSize:[17, 26, 0], spinnerIncDecOnly:!0});
    $(".wizard").smartWizard({selected:0, keyNavigation:!0, enableAllSteps:!1, transitionEffect:"slideleft", contentURL:null, contentCache:!0, cycleSteps:!1, enableFinishButton:!1, errorSteps:[], labelNext:"Next", labelPrevious:"Previous", labelFinish:"Finish", onLeaveStep:null, onShowStep:null, onFinish:null});
    $("#uploader").pluploadQueue({runtimes:"html5,html4", url:"php/upload.php", max_file_size:"2mb", unique_names:!0, filters:[
        {title:"Image files", extensions:"jpg,gif,png"},
        {title:"Zip files", extensions:"zip"}
    ]});
    $("#fileManager").elfinder({url:"php/connector.php"});
    $(".bAlert").click(function () {
        jAlert("This is a custom alert box. Title and this text can be easily editted", "Alert Dialog Sample")
    });
    $(".bConfirm").click(function () {
        jConfirm("Can you confirm this?", "Confirmation Dialog", function (e) {
            jAlert("Confirmed: " + e, "Confirmation Results")
        })
    });
    $(".bPromt").click(function () {
        jPrompt("Type something:", "Prefilled value", "Prompt Dialog", function (e) {
            e && window.alert("You entered " + e)
        })
    });
    $(".bHtml").click(function () {
        jAlert("You can use HTML, such as <strong>bold</strong>, <em>italics</em>, and <u>underline</u>!")
    });
    $("div.menu_body:eq(0)").show();
    $(".acc .head:eq(0)").show().css({color:"#2B6893"});
    $(".acc .head").click(function () {
        $(this).css({color:"#2B6893"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
        $(this).siblings().css({color:"#404040"})
    });
    $(".wysiwyg").wysiwyg({iFrameClass:"wysiwyg-input", controls:{bold:{visible:!0}, italic:{visible:!0}, underline:{visible:!0}, strikeThrough:{visible:!1}, justifyLeft:{visible:!0}, justifyCenter:{visible:!0}, justifyRight:{visible:!0}, justifyFull:{visible:!0}, indent:{visible:!0}, outdent:{visible:!0}, subscript:{visible:!1}, superscript:{visible:!1}, undo:{visible:!0}, redo:{visible:!0}, insertOrderedList:{visible:!0}, insertUnorderedList:{visible:!0}, insertHorizontalRule:{visible:!1}, h1:{visible:!0, className:"h1", command:$.browser.msie || $.browser.safari ? "formatBlock" : "heading", arguments:$.browser.msie || $.browser.safari ? "<h1>" : "h1", tags:["h1"], tooltip:"Header 1"}, h2:{visible:!0, className:"h2", command:$.browser.msie || $.browser.safari ? "formatBlock" : "heading", arguments:$.browser.msie || $.browser.safari ? "<h2>" : "h2", tags:["h2"], tooltip:"Header 2"}, h3:{visible:!0, className:"h3", command:$.browser.msie || $.browser.safari ? "formatBlock" : "heading", arguments:$.browser.msie || $.browser.safari ? "<h3>" : "h3", tags:["h3"], tooltip:"Header 3"}, h4:{visible:!0, className:"h4", command:$.browser.msie || $.browser.safari ? "formatBlock" : "heading", arguments:$.browser.msie || $.browser.safari ? "<h4>" : "h4", tags:["h4"], tooltip:"Header 4"}, h5:{visible:!0, className:"h5", command:$.browser.msie || $.browser.safari ? "formatBlock" : "heading", arguments:$.browser.msie || $.browser.safari ? "<h5>" : "h5", tags:["h5"], tooltip:"Header 5"}, h6:{visible:!0, className:"h6", command:$.browser.msie || $.browser.safari ? "formatBlock" : "heading", arguments:$.browser.msie || $.browser.safari ? "<h6>" : "h6", tags:["h6"], tooltip:"Header 6"}, cut:{visible:!0}, copy:{visible:!0}, paste:{visible:!0}, html:{visible:!0}, increaseFontSize:{visible:!1}, decreaseFontSize:{visible:!1}}, events:{click:function (e) {
        if ($("#click-inform:checked").length > 0) {
            e.preventDefault();
            window.alert("You have clicked jWysiwyg content!")
        }
    }}});
    $().UItoTop({easingType:"easeOutQuart"});
    var e = [
        {url:"http://ejohn.org", title:"John Resig"},
        {url:"http://bassistance.de/", title:"J&ouml;rn Zaefferer"},
        {url:"http://snook.ca/jonathan/", title:"Jonathan Snook"},
        {url:"http://rdworth.org/", title:"Richard Worth"},
        {url:"http://www.paulbakaus.com/", title:"Paul Bakaus"},
        {url:"http://www.yehudakatz.com/", title:"Yehuda Katz"},
        {url:"http://www.azarask.in/", title:"Aza Raskin"},
        {url:"http://www.karlswedberg.com/", title:"Karl Swedberg"},
        {url:"http://scottjehl.com/", title:"Scott Jehl"},
        {url:"http://jdsharp.us/", title:"Jonathan Sharp"},
        {url:"http://www.kevinhoyt.org/", title:"Kevin Hoyt"},
        {url:"http://www.codylindley.com/", title:"Cody Lindley"},
        {url:"http://malsup.com/jquery/", title:"Mike Alsup"}
    ], t = {s1:{decimals:2}, s2:{stepping:.25}, s3:{currency:"$"}, s4:{}, s5:{init:function (t, n) {
        for (var r = 0; r < e.length; r++)n.add('<a href="' + e[r].url + '" target="_blank">' + e[r].title + "</a>")
    }, format:'<a href="%(url)" target="_blank">%(title)</a>', items:e}};
    for (var n in t)$("#" + n).spinner(t[n]);
    $("button").click(function () {
        var e = $(this).attr("id").match(/(s\d)\-(\w+)$/);
        e !== null && $("#" + e[1]).spinner(e[2] === "create" ? t[e[1]] : e[2])
    });
    $("#myList").listnav({initLetter:"a", includeAll:!0, includeOther:!0, flagDisabled:!0, noMatchText:"Nothing matched your filter, please click another letter.", prefixes:["the", "a"]});
    $(".showCode").sourcerer("js html css php");
    $(".showCodeJS").sourcerer("js");
    $(".showCodeHTML").sourcerer("html");
    $(".showCodePHP").sourcerer("php");
    $(".showCodeCSS").sourcerer("css");
    var r = new Date, i = r.getDate(), s = r.getMonth(), o = r.getFullYear();
    $("#calendar").fullCalendar({header:{left:"prev,next", center:"title", right:"month,basicWeek,basicDay"}, editable:!0, events:[
        {title:"All day event", start:new Date(o, s, 1)},
        {title:"Long event", start:new Date(o, s, 5), end:new Date(o, s, 8)},
        {id:999, title:"Repeating event", start:new Date(o, s, 2, 16, 0), end:new Date(o, s, 3, 18, 0), allDay:!1},
        {id:999, title:"Repeating event", start:new Date(o, s, 9, 16, 0), end:new Date(o, s, 10, 18, 0), allDay:!1},
        {title:"Actually any color could be applied for background", start:new Date(o, s, 30, 10, 30), end:new Date(o, s, i + 1, 14, 0), allDay:!1, color:"#B55D5C"},
        {title:"Lunch", start:new Date(o, s, 14, 12, 0), end:new Date(o, s, 15, 14, 0), allDay:!1},
        {title:"Birthday PARTY", start:new Date(o, s, 18), end:new Date(o, s, 20), allDay:!1},
        {title:"Click for Google", start:new Date(o, s, 27), end:new Date(o, s, 29), url:"http://google.com/"}
    ]});
    oTable = $("#example").dataTable({bJQueryUI:!0, sPaginationType:"full_numbers", aLengthMenu:[
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
    ], sDom:'<""f>t<"F"lp>'});
    oTable = $(".dynamicTableData").dataTable({bJQueryUI:!0, bRetrieve:!0, sPaginationType:"full_numbers", aLengthMenu:[
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
    ], sDom:'<""f>t<"F"lp>'});
    oTable = $("#selectedList").dataTable({bJQueryUI:!0, sPaginationType:"full_numbers", aLengthMenu:[
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
    ], sDom:'<""f>t<"F"lp>'});
    $("form").jqTransform({imgPath:"../images/forms"});
    $("#valid").validationEngine();
    $(".datepicker").datepicker({defaultDate:7, autoSize:!0, appendText:"(dd-mm-yyyy)", dateFormat:"dd-mm-yy"});
    $(".datepickerInline").datepicker({defaultDate:7, autoSize:!0, appendText:"(dd-mm-yyyy)", dateFormat:"dd-mm-yy", numberOfMonths:1});
    $("#progressbar").progressbar({value:37});
    $(".leftDir").tipsy({fade:!0, gravity:"e"});
    $(".rightDir").tipsy({fade:!0, gravity:"w"});
    $(".topDir").tipsy({fade:!0, gravity:"s"});
    $(".botDir").tipsy({fade:!0, gravity:"n"});
    $(".hideit").click(function () {
        $(this).fadeOut(400)
    });
    var u = function (e) {
        var t = $(e.currentTarget).find("th"), n = "columns widths: ";
        t.each(function () {
            n += $(this).width() + "px; "
        })
    };
    $(".resize").colResizable({liveDrag:!0, gripInnerHtml:"<div class='grip'></div>", draggingClass:"dragging", onResize:u});
    $("ul.sub li a").hover(function () {
        $(this).stop().animate({color:"#3a6fa5"}, 400)
    }, function () {
        $(this).stop().animate({color:"#494949"}, 400)
    });
    $(".pics ul li").hover(function () {
        $(this).children(".actions").show("fade", 200)
    }, function () {
        $(this).children(".actions").hide("fade", 200)
    });
    $("#colorpickerField").ColorPicker({onSubmit:function (e, t, n, r) {
        $(r).val(t);
        $(r).ColorPickerHide()
    }, onBeforeShow:function () {
        $(this).ColorPickerSetColor(this.value)
    }}).bind("keyup", function () {
        $(this).ColorPickerSetColor(this.value)
    });
    $(".auto").autoGrow();
    $(".onlyNums input").autotab_magic().autotab_filter("numeric");
    $(".onlyText input").autotab_magic().autotab_filter("text");
    $(".onlyAlpha input").autotab_magic().autotab_filter("alpha");
    $(".onlyRegex input").autotab_magic().autotab_filter({format:"custom", pattern:"[^0-9.]"});
    $(".allUpper input").autotab_magic().autotab_filter({format:"alphanumeric", uppercase:!0});
    $(".uiSlider").slider();
    $(".uiSliderInc").slider({value:100, min:0, max:500, step:50, slide:function (e, t) {
        $("#amount").val("$" + t.value)
    }});
    $("#amount").val("$" + $(".uiSliderInc").slider("value"));
    $(".uiRangeSlider").slider({range:!0, min:0, max:500, values:[75, 300], slide:function (e, t) {
        $("#rangeAmount").val("$" + t.values[0] + " - $" + t.values[1])
    }});
    $("#rangeAmount").val("$" + $(".uiRangeSlider").slider("values", 0) + " - $" + $(".uiRangeSlider").slider("values", 1));
    $(".uiMinRange").slider({range:"min", value:37, min:1, max:700, slide:function (e, t) {
        $("#minRangeAmount").val("$" + t.value)
    }});
    $("#minRangeAmount").val("$" + $(".uiMinRange").slider("value"));
    $(".uiMaxRange").slider({range:"max", min:1, max:100, value:20, slide:function (e, t) {
        $("#maxRangeAmount").val(t.value)
    }});
    $("#maxRangeAmount").val($(".uiMaxRange").slider("value"));
    $("#eq > span").each(function () {
        var e = parseInt($(this).text(), 10);
        $(this).empty().slider({value:e, range:"min", animate:!0, orientation:"vertical"})
    });
    $("#breadCrumb").jBreadCrumb();
    $(".autoF").focus();
    $.fn.simpleTabs = function () {
        $(this).find(".tab_content").hide();
        var e = $("ul.tabs li.activeTab a").attr("href");
        $("ul.tabs li.activetab").show();
        $(this).find(e).show();
        $("ul.tabs li").click(function () {
            $(this).parent().parent().find("ul.tabs li").removeClass("activeTab");
            $(this).addClass("activeTab");
            $(this).parent().parent().find(".tab_content").hide();
            var e = $(this).find("a").attr("href");
            $(e).show();
            return!1
        })
    };
    $("div[class^='widget']").simpleTabs();
    $("input").each(function () {
        if ($(this).val() === "" && $(this).attr("placeholder") !== "") {
            $(this).val($(this).attr("placeholder"));
            $(this).focus(function () {
                $(this).val() === $(this).attr("placeholder") && $(this).val("")
            });
            $(this).blur(function () {
                $(this).val() === "" && $(this).val($(this).attr("placeholder"))
            })
        }
    });
    $(".dd").click(function () {
        $("ul.menu_body").slideToggle(100)
    });
    $(document).bind("click", function (e) {
        var t = $(e.target);
        t.parents().hasClass("dd") || $("ul.menu_body").hide(10)
    });
    $(".acts").click(function () {
        $("ul.actsBody").slideToggle(100)
    });
    $(".active").collapsible({defaultOpen:"current", cookieName:"nav", speed:300});
    $(".exp").collapsible({defaultOpen:"current", cookieName:"navAct", cssOpen:"active", cssClose:"inactive", speed:300});
    $(".opened").collapsible({defaultOpen:"opened,toggleOpened", cssOpen:"inactive", cssClose:"normal", speed:200});
    $(".closed").collapsible({defaultOpen:"", cssOpen:"inactive", cssClose:"normal", speed:200});
    $(".selectAll").live("click", function () {
        var e = $.makeArray();
        if ($(".selectedProducts").val() === "") {
            $(".productCheck").attr("checked", !0);
            $(".productCheck").each(function () {
                e.push($(this).attr("id"))
            });
            $(".selectedProducts").val(e.join(", "));
            $(".templateAdd").css("position", "relative").css("top", 0)
        } else {
            $(".productCheck").attr("checked", !1);
            $(".selectedProducts").val("");
            $(".templateAdd").css("position", "absolute").css("top", 1e4)
        }
        return!1
    });
    $(".productCheck").click(function () {
        $(this).is(":checked")
    });
    $(".groupPercent").live("change", function () {
        var e = $(".mainPrice").val();
        if (e === "0") {
            window.alert("Nije podešena cena proizvoda");
            return!1
        }
        var t = $(this).attr("id").replace("group", ""), n = parseFloat($(this).val()), r = $(".g" + t).val(), i = parseFloat(e);
        alert(r);
        return!1;
        var s, o
    });
    $(".calcType").bind("change", function () {
        var e = $(this).val();
        if ($(this).is(":checked")) {
            var t = $(this).attr("id").replace("type", ""), n = parseFloat($("#group" + t).val()), r = $("#g" + t).val(), i = parseFloat($(".mainPrice").val());
            r !== "0" && (i = parseFloat($(".price" + r).val()));
            var s = 0;
            n > 0 && (s = i * n / 100 || "0");
            if (e === "1") {
                s = parseFloat(i) - parseFloat(s);
                $("#price" + t).val(s.toFixed(0))
            }
            if (e === "2") {
                s = i + s;
                $("#price" + t).val(s.toFixed(0))
            }
        }
        return!1
    });
    $(".chooseProduct").live("click", function () {
        console.log("aloha");
        var e = $(this).attr("href"), t = $("#linkedProducts").val(), n = e + ",", r = $(".categoryName" + e).text(), i = $("#productName" + e).text();
        t = t.replace(n, "");
        t += n;
        $("#linkedProducts").val(t);
        var s = $("#selectedList tbody"), o = '<tr class="' + e + '"><td align="center"><a href="' + e + '" class="btnIconLeft mr10 removeProduct" style="margin:0 0 -5px 0; padding:0 15px;"><span style="padding:2px 8px;">Ukloni</span></a></td><td align="center"><h4>' + i + "</h4></td><td>" + r + "</td></tr>";
        s.append(o);
        return!1
    });
    $(".chooseSimilarProduct").live("click", function () {
        var e = $(this).attr("href"), t = $("#linkedSimilarProducts").val(), n = e + ",", r = $(".similarCategoryName" + e).text(), i = $("#similarProductName" + e).text();
        t = t.replace(n, "");
        t += n;
        $("#linkedSimilarProducts").val(t);
        var s = $("#selectedSimilarList tbody"), o = '<tr class="' + e + '"><td align="center"><a href="' + e + '" class="btnIconLeft mr10 removeSimilarProduct" style="margin:0 0 -5px 0; padding:0 15px;"><span style="padding:2px 8px;">Ukloni</span></a></td><td align="center"><h4>' + i + "</h4></td><td>" + r + "</td></tr>";
        $("tr." + e).remove();
        s.append(o);
        return!1
    });
    $(".removeProduct").live("click", function () {
        var e = $(this).attr("href"), t = $("#linkedProducts").val();
        t = t.replace(e + ",", "");
        $("#linkedProducts").val(t);
        $("tr." + e).remove();
        return!1
    });
    $(".removeSimilarProduct").live("click", function () {
        var e = $(this).attr("href"), t = $("#linkedProducts").val();
        t = t.replace(e + ",", "");
        $("#linkedProducts").val(t);
        $("tr." + e).remove();
        return!1
    });
    $("#detailSearchBttn").live("click", function () {
        var e = $(".siteRoot").val(), t = $("form#detailedSearch").serialize();
        $(".saveTemplate").show().attr("href", t);
        $("#searchRes").load(e + "pages/searchRes.php?" + t);
        return!1
    });
    $(".saveTemplate").live("click", function () {
        $(".saveSearchTemplateWrapper").show();
        return!1
    });
    $(".adminRole").change(function () {
        return!1
    });
    $(document).keyup(function (e) {
        e.keyCode === 27 && $(".newLink").css("top", "-1000px")
    });
    $(".newLink .close").live("click", function () {
        $(".newLink").css("top", "-1000px");
        return!1
    });
    $(".addNewLink").live("click", function () {
        $(".newLinkk").css("top", "240px");
        return!1
    });
    $(".changeStatus").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("href");
            window.location = e;
            window.open(e, "_self")
        }
        return!1
    });
    $(".timepicker").timeEntry({show24Hours:!0, showSeconds:!0, spinnerImage:"images/ui/spinnerUpDown.png", spinnerSize:[17, 26, 0], spinnerIncDecOnly:!0});
    $(".pageLinks").live("click", function () {
        var e = $(this).attr("href").split("#");
        $("." + e[0] + "List a").removeClass("actPage");
        var t = e[0] + "BoxID";
        $("#" + t).val(e[1]);
        $(this).addClass("actPage");
        return!1
    });
    $(".addSettings").live("click", function () {
        $(".newSettings").toggle();
        return!1
    });
    $(".updateClient").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("id");
            window.location = e;
            window.open(e, "_self")
        }
    });
    $("#addMoreFields").live("click", function () {
        var e = parseInt($(".current").val(), 10);
        e += 1;
        $(".fields").append('<input id="file_upload" class="fileInput" type="file" name="fileUpload[]" />');
        $(".current").val(e);
        return!1
    });
    $(".close").click(function () {
        $(".message").hide()
    });
    $("input.country").live("change", function () {
        var e = $(this).attr("id"), t = $(".siteRoot").val();
        $(".result" + e).load(t + "pages/activateCountry.php?country=" + e)
    });
    $(".newProductDetail").live("click", function () {
        $("#newProductDetail").toggle();
        return!1
    });
    $(".deleteProducts").live("click", function () {
        var e = a();
        if (e.length && window.confirm("Da li ste sigurni?")) {
            var t = $(".siteRoot").val() + "pages/deleteProduct.php?products=" + e;
            window.location = t;
            window.open(t, "_self")
        }
        return!1
    });
    $(".deletePages").live("click", function () {
        var e = a();
        if (e.length && window.confirm("Da li ste sigurni?")) {
            var t = $(".siteRoot").val() + "pages/deleteGeneralPage.php?generalPage=" + e;
            window.location = t;
            window.open(t, "_self")
        }
        return!1
    });
    $(".noteLinks a").live("click", function () {
        var e = $(this).attr("href");
        $(".notesItems .widget").hide();
        $(e).show()
    });
    $(".editAttrLink").live("click", function () {
        var e = $(this).attr("id");
        $("#attr" + e).toggle();
        $(".attr" + e).toggle();
        var t = $(this).text();
        t === "edit" ? $(this).text("cancel") : $(this).text("edit")
    });
    $(".deleteAttrLink").live("click", function () {
    });
    $(".attrSet").live("click", function () {
        var e = $("select#attrSet").val(), t = $(".siteRoot").val(), n = $(".productID").val();
        $(".productDetailsTable").load(t + "pages/productAttributes.php?productID=" + n + "&attributeSet=" + e);
        return!1
    });
    $(".newAttributeSet").live("click", function () {
        $("#newAttributeSet").toggle()
    });
    $(".deleteAttrLink").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("href");
            window.location = e;
            window.open(e, "_self")
        }
        return!1
    });
    $(".closeExportMenu").live("click", function () {
        $(".priceListWizard").hide();
        $(".importWizard").css("top", "-2000px");
        return!1
    });
    $(".exportTemplate").live("click", function () {
        $(".priceListWizard").show();
        return!1
    });
    $(".productID").live("change", function () {
        var e = $.makeArray();
        e = $("input.selectedProducts").val().split(",");
        var t = $(this).attr("id");
        $.inArray(t, e) > -1 ? e.splice($.inArray(t, e), 1) : e.push(t);
        $("input.selectedProducts").val(e.join(","));
        return!1
    });
    $(".allAttributes").live("change", function () {
        $(this).is(":checked") ? $(".priceListWizard form input:checkbox").each(function () {
            this.checked = !0;
            $("a.jqTransformCheckbox").addClass("jqTransformChecked")
        }) : $(".priceListWizard form input:checkbox").each(function () {
            this.checked = !1;
            $("a.jqTransformCheckbox").removeClass("jqTransformChecked")
        });
        return!1
    });
    $(".importTemplate").live("click", function () {
        $(".importWizard").css("top", "19%")
    });
    $(".exportTemplateBttn").live("click", function () {
        $(".priceListWizard").hide()
    });
    $(".templateUpdate").live("click", function () {
        var e = $("#templateChooser").val();
        if (e === "1") {
            $("#sr2").hide();
            $("#en2").hide();
            $("#sr3").hide();
            $("#en3").hide()
        }
        if (e === "2") {
            $("#sr2").show();
            $("#en2").show();
            $("#sr3").hide();
            $("#en3").hide()
        }
        if (e === "3") {
            $("#sr2").show();
            $("#en2").show();
            $("#sr3").show();
            $("#en3").show()
        }
        return!1
    });
    $(".saveTempProduct").live("click", function () {
        var e = $(this).attr("href"), t = $(".siteRoot").val(), n = $("#plD" + e).val();
        $("#tr" + e).hide();
        $(".result").load(t + "pages/saveTempProduct.php?id=" + e + "&plD=" + n);
        return!1
    });
    $(".loadAttr").live("click", function () {
        $(".attributeList table").hide();
        var e = $("#attributeSet").val();
        $("#attributes" + e).show();
        return!1
    });
    $(".saveSearchTemplate").live("click", function () {
        var e = $(".saveTemplate").attr("href"), t = $(".siteRoot").val(), n = $("#searchTempName").val().replace(" ", "||");
        $(".saveSearchTemplateWrapper").hide();
        $(".searchTemplates").load(t + "pages/saveSearchTemplate.php?name=" + n + "&" + e);
        return!1
    });
    $(".cancelTemplate").live("click", function () {
        $(".saveSearchTemplateWrapper").hide();
        return!1
    });
    $(".implementTemplate").live("click", function () {
        var e = $(".siteRoot").val(), t = $(".searchTemplateSelect").val();
        $("#searchRes").load(e + "pages/searchRes.php?isTemplate=1&templateID=" + t);
        return!1
    });
    $(".addPriceTemplate").live("click", function () {
        $(".priceChooser").show()
    });
    $(".applyPriceTemplate").live("click", function () {
        var e = $(".selectedProducts").val();
        if (e === "") {
            $(".priceChooser").hide();
            window.alert("Nije izabran nijedan proizvod")
        } else {
            var t = $(".siteRoot").val(), n = $("#priceTemplates").val(), r = $(this).attr("rel"), i = t + "pages/applyPriceTemplate.php?priceTemplate=" + n + "&products=" + e + "&page=" + r;
            window.location = i;
            window.open(i, "_self")
        }
    });
    $(".saveTemplate").live("click", function () {
        var e = $(this).attr("id");
        $("#form" + e).submit();
        return!1
    });
    $(".groupSelector").live("change", function () {
        if ($(this).val() === 0)return!1;
        var e = parseInt($(this).attr("id").replace("category", ""), 10);
        e++;
        var t = $(".siteRoot").val(), n = $(this).val();
        $("#category" + e).load(t + "pages/groupSelector.php?parentID=" + n, function (t) {
            t !== "" && $("#category" + e).show()
        });
        $(".groupSelector").each(function (t) {
            t > e && $("#category" + t).hide()
        })
    });
    $("#addAtribute").live("click", function () {
        $(".addAttributeWrapper").css("top", "100px");
        return!1
    });
    $("#addAtributeDetail").live("click", function () {
        $(".addAttributeDetailWrapper").css("top", "100px");
        return!1
    });
    $(".addChar").live("click", function () {
        var e = $(".addChar").offset(), t = e.top - 300;
        $(".newCharacteristic").css("top", t + "px");
        return!1
    });
    $(".showAttribute").live("click", function () {
        var e = $(this).attr("id"), t = $(this).text();
        if (t === " [ + ] ") {
            $("#attributeSet" + e).slideDown();
            $(this).text(" [ - ] ")
        } else {
            $("#attributeSet" + e).slideUp();
            $(this).text(" [ + ] ")
        }
        return!1
    });
    $(".attributeWrapper .jqTransformChecked").each(function () {
        $(this).parent().parent().parent().show();
        $(".showAttribute").text(" [ - ] ")
    });
    $(".addCharCategory").live("click", function () {
        var e = $(".addChar").offset(), t = e.top - 300;
        $(".newCharCategory").css("top", t + "px");
        return!1
    });
    $(".deleteAttribute").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("href");
            window.location = e;
            window.open(e, "_self")
        }
        return!1
    });
    $(".saveProductDetails").live("click", function () {
        var e = $(this).attr("href"), t = $(this).attr("id"), n = $(".parentDetail" + t).val(), r = $(".attributeSrDetail" + t).val().split(" "), i = $(".attributeEnDetail" + t).val().split(" "), s = "&parent=" + n + "&detailSr=" + r + "&detailEn=" + i;
        $(".resDetail" + t).load(e + s);
        return!1
    });
    $(".attributeParent").live("click", function () {
        var e = $(this).attr("href").replace("#", ""), t = $(this).offset(), n = t.top - 100;
        $("#chooseParent .id").val(e);
        $("#chooseParent").css("top", n + "px");
        var r = $(".parentDetail" + e).val();
        $(".chooseParentList").val(r);
        return!1
    });
    $("#chooseParent .submitForm").live("click", function () {
        var e = $("#chooseParent .id").val(), t = $("#chooseParentList").val(t);
        $(".parentDetail" + e).val(t);
        $(".newLink").css("top", "-1000px");
        var n = "";
        $.each($("#chooseParent form input"), function () {
            $(this).is(":checked") && (n = n + $(this).val() + ",")
        });
        var r = $(".siteRoot").val() + "pages/saveAtribut.php?id=" + e, i = "", s = n, o = "", u = "";
        console.log(n);
        if ($("input.attributeParent").val() === "") {
            $(".chooseParentList").val(n);
            console.log("this is atr");
            s = $(".chooseParentList").val();
            o = $(".attributeSrDetail" + e).val().split(" ");
            u = $(".attributeEnDetail" + e).val().split(" ");
            i = r + "&parent=" + n + "&detailSr=" + o + "&detailEn=" + u;
            $(".resDetail" + e).load(i, function () {
                s = $(".resDetail" + e).text();
                if (s !== "OK") {
                    $(".resDetail" + e).text("");
                    $("#attributeParent" + e).text(s)
                }
            })
        } else {
            e = $("input.attributeParent").val();
            $(".parentGroupSettings" + e).val(n);
            o = $(".parentSettingsSr" + e).val().split(" ");
            u = $(".parentSettingsEn" + e).val().split(" ");
            r = $(".siteRoot").val() + "pages/saveAtributGroup.php?id=" + e;
            i = r + "&parent=" + n + "&detailSr=" + o + "&detailEn=" + u;
            console.log(i);
            $(".resGroupDetail" + e).load(i, function () {
                s = $(".resGroupDetail" + e).text();
                if (s !== "OK") {
                    $(".resGroupDetail" + e).text("");
                    $(".attributeParrentSettings" + e).text(s)
                }
            })
        }
        return!1
    });
    $(".attributeSettings").live("click", function () {
        var e = $(this).attr("id").replace("attributeSettings", ""), t = $(".parentSettings" + e).val(), n = $(".parentSettingsSr" + e).val().split(" "), r = $(".parentSettingsEn" + e).val().split(" "), i = $(this).attr("href") + "&parent=" + t + "&detailSr=" + n + "&detailEn=" + r;
        $(".resDetail" + e).load(i, function () {
            t = $(".resDetail" + e).text();
            if (t !== "OK") {
                $(".resDetail" + e).text("");
                $(".attributeParrentSettings" + e).text(t)
            }
        });
        return!1
    });
    $(".saveCharacteristics").live("click", function () {
        var e = $(this).attr("id").replace("char", ""), t = $(this).attr("href"), n = "&characteristics_name_sr=" + $(".charSr" + e).val().split(" "), r = "&characteristics_name_en=" + $(".charEn" + e).val().split(" "), i = "&ordering=" + $(".ordering" + e).val(), s = "&category_id=" + $(".charCategory" + e).val();
        t = t + n + r + s + i;
        console.log(t);
        $(".charRes" + e).load(t);
        return!1
    });
    $(".deleteCharacteristics").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("href");
            window.location = e;
            window.open(e, "_self")
        }
        return!1
    });
    $(".multipleCharDel").live("change", function () {
        var e = $(".multipleDeleteChars").val(), t = $(this).val();
        $(this).is(":checked") ? e = e + t + "," : e = e.replace(t + ",", "");
        $(".multipleDeleteChars").val(e);
        e !== "" ? $(".multipleDeleteChars").show() : $(".multipleDeleteChars").hide();
        return!1
    });
    $(".multipleDeleteChars").live("click", function () {
        var e = $(".multipleDeleteChars").val();
        if (e !== "") {
            var t = $(this).attr("href") + e;
            window.location = t;
            window.open(t, "_self")
        } else window.alert("Niste izabrali nijednu karakteristiku");
        return!1
    });
    $(".deleteAttributeGroup").live("click", function () {
        window.confirm("Da li ste sigurni?"), function () {
            window.location = $(this).attr("href");
            window.open($(this).attr("href"), "_self")
        };
        return!1
    });
    $(".saveCharacteristicsCat").live("click", function () {
        window.confirm("Da li ste sigurni?"), function () {
            window.location = $(this).attr("href");
            window.open($(this).attr("href"), "_self")
        };
        return!1
    });
    $(".saveCharacteristicsCats").live("click", function () {
        var e = $(this).attr("id").replace("charCat", ""), t = "&";
        t = t + "category_name_sr=" + $(".charCatSr" + e).val().split(" ");
        t = t + "&category_name_en=" + $(".charCatEn" + e).val().split(" ");
        var n = $(this).attr("href") + t;
        $(".charCatRes" + e).load(n);
        return!1
    });
    $(".attrGroupsDel").live("change", function () {
    });
    $(".selectAllAtributes").live("click", function () {
        var e = $(".selectedAttributelist").val();
        if (e === "") {
            $(".attributList input[type=checkbox]").each(function () {
                e = e + $(this).val() + ",";
                $(this).attr("checked", !0)
            });
            $(".selectedAttributelist").val(e)
        } else {
            $(".selectedAttributelist").val("");
            $(".attributList input[type=checkbox]").each(function () {
                $(this).attr("checked", !1)
            })
        }
        return!1
    });
    $(".attributes").live("change", function () {
        var e = $(".selectedAttributelist").val(), t = $(this).val();
        e = e.replace(t + ",", "") + t + ",";
        $(".selectedAttributelist").val(e)
    });
    $(".selectAllCatAtributes").live("change", function () {
        var e = $(".multipleDeleteAttGroups").val(), t = $(this).val();
        e = e.replace(t + ",", "") + t + ",";
        $(".multipleDeleteAttGroups").val(e);
        return!1
    });
    $(".multipleDeleteAttr").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("href") + $(".selectedAttributelist").val();
            window.location = e;
            window.open(e, "_self")
        }
        return!1
    });
    $(".multipleDeleteAttrGroups").live("click", function () {
        if (window.confirm("Da li ste sigurni?")) {
            var e = $(this).attr("href") + $(".multipleDeleteAttGroups").val();
            window.location = e;
            window.open(e, "_self")
        }
        return!1
    });
    $("a.selectAllCatAtributes").live("click", function () {
        var e = $("input.multipleDeleteAttGroups").val();
        if (e === "") {
            $(".selectAllCatAtributes").each(function () {
                e = e + $(this).val() + ",";
                $(this).attr("checked", !0)
            });
            $("input.multipleDeleteAttGroups").val(e)
        } else {
            $(".selectAllCatAtributes").each(function () {
                $(this).attr("checked", !1)
            });
            $("input.multipleDeleteAttGroups").val("")
        }
        return!1
    });
    $(".attributeGroupParent").live("click", function () {
        var e = $(this).attr("href").replace("#", "");
        $(".attributeParent").val(e);
        var t = $(this).offset(), n = t.top - 100;
        $("#chooseParent").css("top", n + "px");
        return!1
    });
    $(".characterParent").live("click", function () {
        var e = $(this).attr("href").replace("#", ""), t = $(this).offset(), n = t.top - 100;
        $(".charID").val(e);
        $(".chooseCharCategory").css("top", n + "px");
        return!1
    });
    $(".saveChar").live("click", function () {
        var e = $(".chooseCharCategory input:checked").val(), t = $(".charID").val(), n = $(".chooseCharCategory input:checked").attr("name");
        $(".charParent" + t).text(n);
        $(".charCategory" + t).val(e);
        $(".newLink").css("top", "-10000px");
        return!1
    })
});