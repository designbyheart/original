$(function () {


    //***** UI dialog *****//
    $("#dialog-message").dialog({
        autoOpen:false,
        modal:true,
        buttons:{
            Ok:function () {
                $(this).dialog("close");
            }
        }
    });

    $("#opener").click(function () {
        $("#dialog-message").dialog("open");
        return false;
    });


    //***** PrettyPhoto lightbox plugin *****//

    $("a[rel^='prettyPhoto']").prettyPhoto();


    //***** Custom single file input *****//

    $("input.fileInput").filestyle({
        imageheight:26,
        imagewidth:89,
        width:296
    });


    //***** Dual select boxes *****//

    $.configureBoxes();


    //***** Time picker *****//

    $('.timepicker').timeEntry({
        show24Hours:true, // 24 hours format
        showSeconds:true, // Show seconds?
        spinnerImage:'images/ui/spinnerUpDown.png', // Arrows image
        spinnerSize:[17, 26, 0], // Image size
        spinnerIncDecOnly:true // Only up and down arrows
    });


    //***** Wizard *****//

    $('.wizard').smartWizard({
        selected:0, // Selected Step, 0 = first step
        keyNavigation:true, // Enable/Disable key navigation(left and right keys are used if enabled)
        enableAllSteps:false, // Enable/Disable all steps on first load
        transitionEffect:'slideleft', // Effect on navigation, none/fade/slide/slideleft
        contentURL:null, // specifying content url enables ajax content loading
        contentCache:true, // cache step contents, if false content is fetched always from ajax url
        cycleSteps:false, // cycle step navigation
        enableFinishButton:false, // makes finish button enabled always
        errorSteps:[], // array of step numbers to highlighting as error steps
        labelNext:'Next', // label for Next button
        labelPrevious:'Previous', // label for Previous button
        labelFinish:'Finish', // label for Finish button
        // Events
        onLeaveStep:null, // triggers when leaving a step
        onShowStep:null, // triggers when showing a step
        onFinish:null  // triggers when Finish button is clicked
    });


    //***** File uploader *****//

    $("#uploader").pluploadQueue({
        runtimes:'html5,html4',
        url:'php/upload.php',
        max_file_size:'2mb',
        unique_names:true,
        filters:[
            {title:"Image files", extensions:"jpg,gif,png"},
            {title:"Zip files", extensions:"zip"}
        ]
    });


    //***** File manager *****//

    $('#fileManager').elfinder({
        url:'php/connector.php'
    });


    //***** Alert windows *****//

    $(".bAlert").click(function () {
        jAlert('This is a custom alert box. Title and this text can be easily editted', 'Alert Dialog Sample');
    });

    $(".bConfirm").click(function () {
        jConfirm('Can you confirm this?', 'Confirmation Dialog', function (r) {
            jAlert('Confirmed: ' + r, 'Confirmation Results');
        });
    });

    $(".bPromt").click(function () {
        jPrompt('Type something:', 'Prefilled value', 'Prompt Dialog', function (r) {
            if (r) {
                window.alert('You entered ' + r);
            }
        });
    });

    $(".bHtml").click(function () {
        jAlert('You can use HTML, such as <strong>bold</strong>, <em>italics</em>, and <u>underline</u>!');
    });


    //***** Accordion *****//

    $('div.menu_body:eq(0)').show();
    $('.acc .head:eq(0)').show().css({color:"#2B6893"});

    $(".acc .head").click(function () {
        $(this).css({color:"#2B6893"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
        $(this).siblings().css({color:"#404040"});
    });


    //***** WYSIWYG editor *****//

    $('.wysiwyg').wysiwyg({
        iFrameClass:"wysiwyg-input",
        controls:{
            bold:{ visible:true },
            italic:{ visible:true },
            underline:{ visible:true },
            strikeThrough:{ visible:false },

            justifyLeft:{ visible:true },
            justifyCenter:{ visible:true },
            justifyRight:{ visible:true },
            justifyFull:{ visible:true },

            indent:{ visible:true },
            outdent:{ visible:true },

            subscript:{ visible:false },
            superscript:{ visible:false },

            undo:{ visible:true },
            redo:{ visible:true },

            insertOrderedList:{ visible:true },
            insertUnorderedList:{ visible:true },
            insertHorizontalRule:{ visible:false },

            h1:{
                visible:true,
                className:'h1',
                command:($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                arguments:($.browser.msie || $.browser.safari) ? '<h1>' : 'h1',
                tags:['h1'],
                tooltip:'Header 1'
            },
            h2:{
                visible:true,
                className:'h2',
                command:($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                arguments:($.browser.msie || $.browser.safari) ? '<h2>' : 'h2',
                tags:['h2'],
                tooltip:'Header 2'
            },
            h3:{
                visible:true,
                className:'h3',
                command:($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                arguments:($.browser.msie || $.browser.safari) ? '<h3>' : 'h3',
                tags:['h3'],
                tooltip:'Header 3'
            },
            h4:{
                visible:true,
                className:'h4',
                command:($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                arguments:($.browser.msie || $.browser.safari) ? '<h4>' : 'h4',
                tags:['h4'],
                tooltip:'Header 4'
            },
            h5:{
                visible:true,
                className:'h5',
                command:($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                arguments:($.browser.msie || $.browser.safari) ? '<h5>' : 'h5',
                tags:['h5'],
                tooltip:'Header 5'
            },
            h6:{
                visible:true,
                className:'h6',
                command:($.browser.msie || $.browser.safari) ? 'formatBlock' : 'heading',
                arguments:($.browser.msie || $.browser.safari) ? '<h6>' : 'h6',
                tags:['h6'],
                tooltip:'Header 6'
            },

            cut:{ visible:true },
            copy:{ visible:true },
            paste:{ visible:true },
            html:{ visible:true },
            increaseFontSize:{ visible:false },
            decreaseFontSize:{ visible:false }
        },
        events:{
            click:function (event) {
                if ($("#click-inform:checked").length > 0) {
                    event.preventDefault();
                    window.alert("You have clicked jWysiwyg content!");
                }
            }
        }
    });

    //$('.wysiwyg').wysiwyg("insertHtml", "Sample code");


    //***** ToTop *****//

    $().UItoTop({ easingType:'easeOutQuart' });


    //***** Spinner options *****//

    var itemList = [
        {url:"http://ejohn.org", title:"John Resig"},
        {url:"http://bassistance.de/", title:"J&ouml;rn Zaefferer"},
        {url:"http://snook.ca/jonathan/", title:"Jonathan Snook"},
        {url:"http://rdworth.org/", title:"Richard Worth"},
        {url:"http://www.paulbakaus.com/", title:"Paul Bakaus"},
        {url:"http://www.yehudakatz.com/", title:"Yehuda Katz"},
        {url:"http://www.azarask.in/", title:"Aza Raskin"},
        {url:"http://www.karlswedberg.com/", title:"Karl Swedberg"},
        {url:"http://scottjehl.com/", title:"Scott Jehl"},
        {url:"http://jdsharp.us/", title:"Jonathan Sharp"},
        {url:"http://www.kevinhoyt.org/", title:"Kevin Hoyt"},
        {url:"http://www.codylindley.com/", title:"Cody Lindley"},
        {url:"http://malsup.com/jquery/", title:"Mike Alsup"}
    ];

    var opts = {
        's1':{decimals:2},
        's2':{stepping:0.25},
        's3':{currency:'$'},
        's4':{},
        's5':{
            //
            // Two methods of adding external items to the spinner
            //
            // method 1: on initalisation call the add method directly and format html manually
            init:function (e, ui) {
                for (var i = 0; i < itemList.length; i++) {
                    ui.add('<a href="' + itemList[i].url + '" target="_blank">' + itemList[i].title + '</a>');
                }
            },

            // method 2: use the format and items options in combination
            format:'<a href="%(url)" target="_blank">%(title)</a>',
            items:itemList
        }
    };

    for (var n in opts) {
        $("#" + n).spinner(opts[n]);
    }

    $("button").click(function () {
        var ns = $(this).attr('id').match(/(s\d)\-(\w+)$/);
        if (ns !== null) {
            $('#' + ns[1]).spinner((ns[2] === 'create') ? opts[ns[1]] : ns[2]);
        }
    });


    //***** Contacts list *****//

    $('#myList').listnav({
        initLetter:'a',
        includeAll:true,
        includeOther:true,
        flagDisabled:true,
        noMatchText:'Nothing matched your filter, please click another letter.',
        prefixes:['the', 'a']
    });


    //***** ShowCode plugin for <pre> tag *****//

    $('.showCode').sourcerer('js html css php'); // Display all languages
    $('.showCodeJS').sourcerer('js'); // Display JS only
    $('.showCodeHTML').sourcerer('html'); // Display HTML only
    $('.showCodePHP').sourcerer('php'); // Display PHP only
    $('.showCodeCSS').sourcerer('css'); // Display CSS only


    //***** Calendar *****//

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        header:{
            left:'prev,next',
            center:'title',
            right:'month,basicWeek,basicDay'
        },
        editable:true,
        events:[
            {
                title:'All day event',
                start:new Date(y, m, 1)
            },
            {
                title:'Long event',
                start:new Date(y, m, 5),
                end:new Date(y, m, 8)
            },
            {
                id:999,
                title:'Repeating event',
                start:new Date(y, m, 2, 16, 0),
                end:new Date(y, m, 3, 18, 0),
                allDay:false
            },
            {
                id:999,
                title:'Repeating event',
                start:new Date(y, m, 9, 16, 0),
                end:new Date(y, m, 10, 18, 0),
                allDay:false
            },
            {
                title:'Actually any color could be applied for background',
                start:new Date(y, m, 30, 10, 30),
                end:new Date(y, m, d + 1, 14, 0),
                allDay:false,
                color:'#B55D5C'
            },
            {
                title:'Lunch',
                start:new Date(y, m, 14, 12, 0),
                end:new Date(y, m, 15, 14, 0),
                allDay:false
            },
            {
                title:'Birthday PARTY',
                start:new Date(y, m, 18),
                end:new Date(y, m, 20),
                allDay:false
            },
            {
                title:'Click for Google',
                start:new Date(y, m, 27),
                end:new Date(y, m, 29),
                url:'http://google.com/'
            }
        ]
    });


    //***** Dynamic data table *****//

    oTable = $('#example').dataTable({
        "bJQueryUI":true,
        "sPaginationType":"full_numbers",
        "aLengthMenu":[
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "sDom":'<""f>t<"F"lp>'
    });
    oTable = $('.dynamicTableData').dataTable({
        "bJQueryUI":true,
        "bRetrieve":true,
        "sPaginationType":"full_numbers",
        "aLengthMenu":[
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "sDom":'<""f>t<"F"lp>'
    });
    oTable = $('#selectedList').dataTable({
        "bJQueryUI":true,
        "sPaginationType":"full_numbers",
        "aLengthMenu":[
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        "sDom":'<""f>t<"F"lp>'
    });


    //***** Form elements styling *****//

    $('form').jqTransform({imgPath:'../images/forms'});


    //***** Form validation engine *****//

    $("#valid").validationEngine();


    //***** Datepickers *****//

    $(".datepicker").datepicker({
        defaultDate:+7,
        autoSize:true,
        appendText:'(dd-mm-yyyy)',
        dateFormat:'dd-mm-yy'
    });

    $(".datepickerInline").datepicker({
        defaultDate:+7,
        autoSize:true,
        appendText:'(dd-mm-yyyy)',
        dateFormat:'dd-mm-yy',
        numberOfMonths:1
    });


    //***** Progressbar (Jquery UI) *****//

    $("#progressbar").progressbar({
        value:37
    });


    //***** Tooltip *****//

    $('.leftDir').tipsy({fade:true, gravity:'e'});
    $('.rightDir').tipsy({fade:true, gravity:'w'});
    $('.topDir').tipsy({fade:true, gravity:'s'});
    $('.botDir').tipsy({fade:true, gravity:'n'});


    //***** Information boxes *****//

    $(".hideit").click(function () {
        $(this).fadeOut(400);
    });


    //*****Resizable table columns *****//

    var onSampleResized = function (e) {
        var columns = $(e.currentTarget).find("th");
        var msg = "columns widths: ";
        columns.each(function () {
            msg += $(this).width() + "px; ";
        });
    };

    $(".resize").colResizable({
        liveDrag:true,
        gripInnerHtml:"<div class='grip'></div>",
        draggingClass:"dragging",
        onResize:onSampleResized});


    //***** Left navigation submenu animation *****//

    $("ul.sub li a").hover(function () {
        $(this).stop().animate({ color:"#3a6fa5" }, 400);
    }, function () {
        $(this).stop().animate({ color:"#494949" }, 400);
    });


    //***** Image gallery control buttons *****//

    $(".pics ul li").hover(
        function () {
            $(this).children(".actions").show("fade", 200);
        },
        function () {
            $(this).children(".actions").hide("fade", 200);
        }
    );


    //***** Color picker *****//

    $('#colorpickerField').ColorPicker({
        onSubmit:function (hsb, hex, rgb, el) {
            $(el).val(hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow:function () {
            $(this).ColorPickerSetColor(this.value);
        }
    })
        .bind('keyup', function () {
            $(this).ColorPickerSetColor(this.value);
        });


    //***** Autogrowing textarea *****//

    $(".auto").autoGrow();


    //***** Autotabs. Inline data rows *****//

    $('.onlyNums input').autotab_magic().autotab_filter('numeric');
    $('.onlyText input').autotab_magic().autotab_filter('text');
    $('.onlyAlpha input').autotab_magic().autotab_filter('alpha');
    $('.onlyRegex input').autotab_magic().autotab_filter({ format:'custom', pattern:'[^0-9\.]' });
    $('.allUpper input').autotab_magic().autotab_filter({ format:'alphanumeric', uppercase:true });


    //***** jQuery UI sliders *****//

    $(".uiSlider").slider();

    $(".uiSliderInc").slider({
        value:100,
        min:0,
        max:500,
        step:50,
        slide:function (event, ui) {
            $("#amount").val("$" + ui.value);
        }
    });
    $("#amount").val("$" + $(".uiSliderInc").slider("value"));

    $(".uiRangeSlider").slider({
        range:true,
        min:0,
        max:500,
        values:[ 75, 300 ],
        slide:function (event, ui) {
            $("#rangeAmount").val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
        }
    });
    $("#rangeAmount").val("$" + $(".uiRangeSlider").slider("values", 0) + " - $" + $(".uiRangeSlider").slider("values", 1));

    $(".uiMinRange").slider({
        range:"min",
        value:37,
        min:1,
        max:700,
        slide:function (event, ui) {
            $("#minRangeAmount").val("$" + ui.value);
        }
    });
    $("#minRangeAmount").val("$" + $(".uiMinRange").slider("value"));

    $(".uiMaxRange").slider({
        range:"max",
        min:1,
        max:100,
        value:20,
        slide:function (event, ui) {
            $("#maxRangeAmount").val(ui.value);
        }
    });
    $("#maxRangeAmount").val($(".uiMaxRange").slider("value"));


    $("#eq > span").each(function () {
        // read initial values from markup and remove that
        var value = parseInt($(this).text(), 10);
        $(this).empty().slider({
            value:value,
            range:"min",
            animate:true,
            orientation:"vertical"
        });
    });


    //***** Breadcrumbs *****//

    $("#breadCrumb").jBreadCrumb();


    //***** Autofocus *****//

    $('.autoF').focus();


    //***** Tabs *****//

    $.fn.simpleTabs = function () {

        //Default Action
        $(this).find(".tab_content").hide(); //Hide all content

        var id = $("ul.tabs li.activeTab a").attr('href');

        $("ul.tabs li.activetab").show();
        $(this).find(id).show(); //Show first tab content
//        }else{
//            $(this).find("ul.tabs li:first").addClass("activeTab").show(); //Activate first tab
//            $(this).find(".tab_content:first").show(); //Show first tab content
//        }


        //On Click Event
        $("ul.tabs li").click(function () {
            $(this).parent().parent().find("ul.tabs li").removeClass("activeTab"); //Remove any "active" class
            $(this).addClass("activeTab"); //Add "active" class to selected tab
            $(this).parent().parent().find(".tab_content").hide(); //Hide all tab content
            var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
            $(activeTab).show(); //Fade in the active content
            return false;
        });

    };//end function

    $("div[class^='widget']").simpleTabs(); //Run function on any div with class name of "Simple Tabs"


    //***** Placeholder for all browsers *****//

    $("input").each(
        function () {
            if ($(this).val() === "" && $(this).attr("placeholder") !== "") {
                $(this).val($(this).attr("placeholder"));
                $(this).focus(function () {
                    if ($(this).val() === $(this).attr("placeholder")) {
                        $(this).val("");
                    }
                });
                $(this).blur(function () {
                    if ($(this).val() === "") {
                        $(this).val($(this).attr("placeholder"));
                    }
                });
            }
        });


    //***** User nav dropdown *****//

    $('.dd').click(function () {
        $('ul.menu_body').slideToggle(100);
    });

    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dd")) {
            $("ul.menu_body").hide(10);
        }
    });


    $('.acts').click(function () {
        $('ul.actsBody').slideToggle(100);
    });


    //***** Collapsible elements management *****//

    $('.active').collapsible({
        defaultOpen:'current',
        cookieName:'nav',
        speed:300
    });

    $('.exp').collapsible({
        defaultOpen:'current',
        cookieName:'navAct',
        cssOpen:'active',
        cssClose:'inactive',
        speed:300
    });

    $('.opened').collapsible({
        defaultOpen:'opened,toggleOpened',
        cssOpen:'inactive',
        cssClose:'normal',
        speed:200
    });

    $('.closed').collapsible({
        defaultOpen:'',
        cssOpen:'inactive',
        cssClose:'normal',
        speed:200
    });


    //pedja's edit
    $('.selectAll').live('click', function () {
        var indexes = $.makeArray();
        if ($('.selectedProducts').val() === '') {
            $('.productCheck').attr("checked", true);
            $('.productCheck').each(function () {
                indexes.push($(this).attr('id'));
            });
            $('.selectedProducts').val(indexes.join(", "));
            $('.templateAdd').css('position', 'relative').css('top', 0);
        } else {
            $('.productCheck').attr("checked", false);
            $('.selectedProducts').val("");
            $('.templateAdd').css('position', 'absolute').css('top', 10000);
        }
        return false;
    });
    $('.productCheck').click(function () {
//        var id = $(this).attr('id');
//        var selected = 'not';
        if ($(this).is(':checked')) {
//            window.alert('is Selected');
        }
    });
//    calculate product group price
    $('.groupPercent').live('change', function () {


        var currentPrice = $('.mainPrice').val();

        if (currentPrice === '0') {
            window.alert('Nije podešena cena proizvoda');
            return false;
        }
        var id = $(this).attr('id').replace('group', '');
        var percent = parseFloat($(this).val());
        var parrent = $('.g' + id).val();
        var price = parseFloat(currentPrice);
        alert(parrent);
        return false;
        price = parseFloat($('#price' + parrent).val());
        var amount = (price * percent) / 100 || "0";
        var type = $('#type' + id + ':checked').val();


        if (type === '1') { //oduzimanje
            amount = (price - amount).toFixed(0);
            $('#price' + id).val(amount);
        }
        if (type === '2') {
            amount = (price + amount).toFixed(0);
            $('#price' + id).val(amount);
        }
        return false;
    });
    //change price when change type of calculation
    $('.calcType').bind("change", function () {
        var type = $(this).val();
        if ($(this).is(':checked')) {

            var id = $(this).attr('id').replace('type', '');

            var percent = parseFloat($("#group" + id).val());
            var parrent = $('#g' + id).val();
            var price = parseFloat($('.mainPrice').val());
            if (parrent !== '0') {
                price = parseFloat($('.price' + parrent).val());
            }
            var amount = 0;
            if (percent > 0) {
                amount = (price * percent) / 100 || "0";
            }
            if (type === '1') { //oduzimanje
                amount = (parseFloat(price) - parseFloat(amount));
                $('#price' + id).val(amount.toFixed(0));
            }
            if (type === '2') {
                amount = (price + amount);
                $('#price' + id).val(amount.toFixed(0));
            }
        }
        return false;
    });
    $('.chooseProduct').live('click', function () {

        var id = $(this).attr('href');
        var selected = $('#linkedProducts').val();
        var converted = id + ',';
        var categoryName = $('.categoryName' + id).text();
        var productName = $('#productName' + id).text();
        console.log(productName);
        selected = selected.replace(converted, '');
        selected += converted;
        $('#linkedProducts').val(selected);

        //adding to table
        var tbody = $('#selectedProductList tbody');
        var itemTmpl = '<tr class="' + id + '"><td align="center"><a href="' + id + '" class="btnIconLeft mr10 removeProduct" style="margin:0 0 -5px 0; padding:0 15px;"><span style="padding:2px 8px;">Ukloni</span></a></td><td align="center"><h4>' + productName + '</h4></td><td>' + categoryName + '</td></tr>';

//        tbody = tbody.replace(itemTmpl,'');
        tbody.append(itemTmpl);
        return false;
    });
    $('.chooseSimilarProduct').live('click', function () {
        var id = $(this).attr('href');
        var selected = $('#linkedSimilarProducts').val();
        var converted = id + ',';
        var categoryName = $('.similarCategoryName' + id).text();
        var productName = $('#similarProductName' + id).text();
        selected = selected.replace(converted, '');
        selected += converted;
        $('#linkedSimilarProducts').val(selected);
//
//        //adding to table
        var tbody = $('#selectedSimilarList tbody');
        var itemTmpl = '<tr class="' + id + '"><td align="center"><a href="' + id + '" class="btnIconLeft mr10 removeSimilarProduct" style="margin:0 0 -5px 0; padding:0 15px;"><span style="padding:2px 8px;">Ukloni</span></a></td><td align="center"><h4>' + productName + '</h4></td><td>' + categoryName + '</td></tr>';
//        itemTmpl.remove();
//        tbody = tbody.replace(itemTmpl,'');
        $('tr.' + id).remove();
        tbody.append(itemTmpl);
        return false;
    });
    $('.removeProduct').live('click', function () {
        var id = $(this).attr('href');
//        var tbody = $('#selectedList tbody');
        var newList = $('#linkedProducts').val();
        newList = newList.replace(id + ',', '');
        $('#linkedProducts').val(newList);
        $('tr.' + id).remove();
        return false;
    });
    $('.removeSimilarProduct').live('click', function () {
        var id = $(this).attr('href');
//        var tbody = $('#selectedSimilarList tbody');
        var newList = $('#linkedProducts').val();
        newList = newList.replace(id + ',', '');
        $('#linkedProducts').val(newList);
        $('tr.' + id).remove();
        return false;
    });
    $('#detailSearchBttn').live('click', function () {
        var url = $('.siteRoot').val();
        var data = $('form#detailedSearch').serialize();
        $('.saveTemplate').show().attr('href', data);
        $('#searchRes').load(url + 'pages/searchRes.php?' + data);
        return false;
    });
    $('.saveTemplate').live('click', function () {
        $('.saveSearchTemplateWrapper').show();

        return false;
    });
    $('.adminRole').change(function () {
//        var siteUrl = $('.siteRoot').val();
//        var id = $(this).attr('id');
//        var role = $(this).val();
//        $('.roleInfo' + id).load(siteUrl + 'pages/changeRole.php?roleID=' + role + '&userID=' + id);
        return false;
    });

    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            $('.newLink').css('top', '-1000px');
        }   // esc
    });
    $('.newLink .close').live('click', function () {
        $('.newLink').css('top', '-1000px');
        return false;
    });
    $('.addNewLink').live('click', function () {
        $('.newLinkk').css('top', '240px');
        return false;
    });
    $('.changeStatus').live('click', function () {
        if (window.confirm('Da li ste sigurni?')) {
            var url = $(this).attr('href');
            window.location = url;
            window.open(url, '_self');
        }
        return false;
    });
    $('.timepicker').timeEntry({
        show24Hours:true, // 24 hours format
        showSeconds:true, // Show seconds?
        spinnerImage:'images/ui/spinnerUpDown.png', // Arrows image
        spinnerSize:[17, 26, 0], // Image size
        spinnerIncDecOnly:true // Only up and down arrows
    });
    $('.pageLinks').live('click', function () {
        var id = $(this).attr('href').split('#');
        $('.' + id[0] + 'List a').removeClass('actPage');
        var field = id[0] + 'BoxID';

        $('#' + field).val(id[1]);
        $(this).addClass('actPage');
//        $('#'+id[0]+'boxID').val(id);
        return false;
    });
    $('.addSettings').live('click', function () {
        $('.newSettings').toggle();
        return false;
    });
    $('.updateClient').live('click', function () {
        if (window.confirm('Da li ste sigurni?')) {
            var id = $(this).attr('id');
            window.location = id;
            window.open(id, '_self');
        }
    });
    $('#addMoreFields').live('click', function () {
        var id = parseInt($('.current').val(), 10);
        id = id + 1;
        $('.fields').append('<input id="file_upload" class="fileInput" type="file" name="fileUpload[]" />');
        $('.current').val(id);
        return false;
    });
    $('.close').click(function () {
        $('.message').hide();
    });
    $('input.country').live('change', function () {
        var id = $(this).attr('id');
        var siteUrl = $('.siteRoot').val();
        $('.result' + id).load(siteUrl + 'pages/activateCountry.php?country=' + id);
    });
    $('.newProductDetail').live('click', function () {
        $('#newProductDetail').toggle();
        return false;
    });
//    function countChecked() {
//        var n = $("input.productID:checked").length;
//        window.alert(n + (n === 1 ? " is" : " are") + " checked!");
//    }

    function setCheckedIDs() {
        var selectedProducts = $.makeArray();
        $("input.productID:checked").each(function () {
            selectedProducts.push($(this).attr('id'));
        });
        return selectedProducts;
    }

    $('.deleteProducts').live('click', function () {
        var selectedItems = setCheckedIDs();
        if (selectedItems.length) {
            if (window.confirm('Da li ste sigurni?')) {
                var siteUrl = $('.siteRoot').val() + 'pages/deleteProduct.php?products=' + selectedItems;
                window.location = siteUrl;
                window.open(siteUrl, '_self');
            }
        }
        return false;
    });
    $('.deletePages').live('click', function () {
        var selectedItems = setCheckedIDs();
        if (selectedItems.length) {
            if (window.confirm('Da li ste sigurni?')) {
                var siteUrl = $('.siteRoot').val() + 'pages/deleteGeneralPage.php?generalPage=' + selectedItems;
                window.location = siteUrl;
                window.open(siteUrl, '_self');
            }
        }
        return false;
    });

    $('.noteLinks a').live('click', function () {
        var id = $(this).attr('href');
        $('.notesItems .widget').hide();
        $(id).show();
    });


    $('.editAttrLink').live('click', function () {
        var id = $(this).attr('id');
        $('#attr' + id).toggle();
        $('.attr' + id).toggle();
        var linkText = $(this).text();
        if (linkText === 'edit') {
            $(this).text('cancel');
        } else {
            $(this).text('edit');
        }
    });
    $('.deleteAttrLink').live('click', function () {
//        var id = $(this).attr('id');
    });
    $('.attrSet').live('click', function () {
//        var id = $(this).val();
        var selected = $('select#attrSet').val(); //$('.jqTransformSelectWrapper a.selected').attr('index');
        var url = $('.siteRoot').val();
//        $('#searchRes').load(url + 'pages/searchRes.php?'+data);
        var productID = $('.productID').val();
        $('.productDetailsTable').load(url + 'pages/productAttributes.php?productID=' + productID + '&attributeSet=' + selected);
        return false;
    });
    $('.newAttributeSet').live('click', function () {
        $('#newAttributeSet').toggle();
    });
    $('.deleteAttrLink').live('click', function () {
        if (window.confirm('Da li ste sigurni?')) {
            var url = $(this).attr('href');
            window.location = url;
            window.open(url, '_self');
        }
        return false;
    });
    $('.closeExportMenu').live('click', function () {
        $('.priceListWizard').hide();
        $('.importWizard').css('top', '-2000px');
        return false;
    });
    $('.exportTemplate').live('click', function () {
        $('.priceListWizard').show();
        return false;
    });

    $('.productID').live('change', function () {
        var selectedProducts = $.makeArray();
        selectedProducts = $('input.selectedProducts').val().split(',');
        var id = $(this).attr('id');
        if ($.inArray(id, selectedProducts) > -1) {
            //contains id
            selectedProducts.splice($.inArray(id, selectedProducts), 1);
        } else {
            selectedProducts.push(id);
        }
        $('input.selectedProducts').val(selectedProducts.join(','));
        return false;
    });
    $('.allAttributes').live('change', function () {
        if ($(this).is(':checked')) {
            $('.priceListWizard form input:checkbox').each(function () {
                this.checked = true;
                $('a.jqTransformCheckbox').addClass('jqTransformChecked');
            });
        } else {
            $('.priceListWizard form input:checkbox').each(function () {
                this.checked = false;
                $('a.jqTransformCheckbox').removeClass('jqTransformChecked');
            });
        }
        return false;
    });
    $('.importTemplate').live('click', function () {
        $('.importWizard').css('top', '19%');
    });
    $('.exportTemplateBttn').live('click', function () {
        $('.priceListWizard').hide();
    });

    $('.templateUpdate').live('click', function () {
        var id = $("#templateChooser").val();
        if (id === '1') {
            $('#sr2').hide();
            $('#en2').hide();
            $('#sr3').hide();
            $('#en3').hide();
        }
        if (id === '2') {
            $('#sr2').show();
            $('#en2').show();
            $('#sr3').hide();
            $('#en3').hide();
        }
        if (id === '3') {
            $('#sr2').show();
            $('#en2').show();
            $('#sr3').show();
            $('#en3').show();
        }
        return false;
    });

    $('.saveTempProduct').live('click', function () {
        var id = $(this).attr('href');
        var siteUrl = $('.siteRoot').val();
        var plD = $('#plD' + id).val();
        $('#tr' + id).hide();
        $('.result').load(siteUrl + 'pages/saveTempProduct.php?id=' + id + '&plD=' + plD);
        return false;
    });
    $('.loadAttr').live('click', function () {
        $('.attributeList table').hide();
        var id = $('#attributeSet').val();
        $('#attributes' + id).show();
        return false;
    });
    $('.saveSearchTemplate').live('click', function () {
        var data = $('.saveTemplate').attr('href');
        var url = $('.siteRoot').val();
        var name = $('#searchTempName').val().replace(' ', "||");
        $('.saveSearchTemplateWrapper').hide();

//        window.alert(url+'pages/saveSearchTemplate.php?name='+name+'&'+data);
        $('.searchTemplates').load(url + 'pages/saveSearchTemplate.php?name=' + name + '&' + data);
        return false;
    });
    $('.cancelTemplate').live('click', function () {
        $('.saveSearchTemplateWrapper').hide();
        return false;
    });
    $('.implementTemplate').live('click', function () {
        var url = $('.siteRoot').val();
        var dataID = $('.searchTemplateSelect').val();
        $('#searchRes').load(url + 'pages/searchRes.php?isTemplate=1&templateID=' + dataID);
        return false;
    });
    $('.addPriceTemplate').live('click', function () {
        $('.priceChooser').show();
    });
    $('.applyPriceTemplate').live('click', function () {
        var products = $('.selectedProducts').val();
        if (products === '') {
            $('.priceChooser').hide();
            window.alert('Nije izabran nijedan proizvod');
        } else {
            var siteUrl = $('.siteRoot').val();
            var priceT = $('#priceTemplates').val();
            var page = $(this).attr('rel');
            var url = siteUrl + 'pages/applyPriceTemplate.php?priceTemplate=' + priceT + '&products=' + products + '&page=' + page;
            window.location = url;
            window.open(url, '_self');
        }
    });

    $('.saveTemplate').live('click', function () {
        var id = $(this).attr('id');
//        var data = $('#form'+id).serialize();
//        var url = $('.siteRoot').val();
//        var action = $('#form' + id).attr('action');
        $('#form' + id).submit();
        //$('.resultTemplate'+id).load(url+'pages/saveTemplateValues.php?'+data);
        return false;
    });
    $('.groupSelector').live('change', function () {
        if ($(this).val() === 0) {
            return false;
        }
        var order = parseInt($(this).attr('id').replace('category', ''), 10);
        order++;
        var url = $('.siteRoot').val();
        var id = $(this).val();

        $('#category' + order).load(url + "pages/groupSelector.php?parentID=" + id, function (data) {
            if (data !== '') {
                $('#category' + order).show();
            }
        });
        $('.groupSelector').each(function (index) {
            if (index > order) {
                $('#category' + index).hide();
            }
        });
    });
    $('#addAtribute').live('click', function () {
        $('.addAttributeWrapper').css('top', '100px');
        return false;
    });
    $('#addAtributeDetail').live('click', function () {
        $('.addAttributeDetailWrapper').css('top', '100px');
        return false;
    });
    $('.addChar').live('click', function () {
        var offset = $('.addChar').offset();
        var top = offset.top - 300;
        $('.newCharacteristic').css('top', top + 'px');
        return false;
    });
    $('.showAttribute').live('click', function () {
        var id = $(this).attr('id');
        var t = $(this).text();
        if (t === ' [ + ] ') {
            $('#attributeSet' + id).slideDown();
            $(this).text(' [ - ] ');
        } else {
            $('#attributeSet' + id).slideUp();
            $(this).text(' [ + ] ');
        }
        return false;
    });
    $('.attributeWrapper .jqTransformChecked').each(function () {
        $(this).parent().parent().parent().show();
//        var id = $(this).parent().parent().parent().attr('id').replace('attributeSet', '');
        $('.showAttribute').text(' [ - ] ');

    });
    $('.addCharCategory').live('click', function () {
        var offset = $('.addChar').offset();
        var top = offset.top - 300;
        $('.newCharCategory').css('top', top + 'px');
        return false;
    });
    $('.deleteAttribute').live('click', function () {
//        var id = $(this).attr('href');
        if (window.confirm("Da li ste sigurni?")) {
            var url = $(this).attr('href');
            window.location = url;
            window.open(url, '_self');
        }
        return false;
    });
    $('.saveProductDetails').live('click', function () {
        var url = $(this).attr('href');
        var id = $(this).attr('id');
        var parent = $('.parentDetail' + id).val();
        var sr = $('.attributeSrDetail' + id).val().split(' ');
        var en = $('.attributeEnDetail' + id).val().split(' ');
        var data = '&parent=' + parent + '&detailSr=' + sr + '&detailEn=' + en;

        $('.resDetail' + id).load(url + data);
        //alert(data);
        return false;
    });
    $('.attributeParent').live('click', function () {
        var id = $(this).attr('href').replace('#', '');
        var offset = $(this).offset();
        var top = offset.top - 100;
        $('#chooseParent .id').val(id);
        $('#chooseParent').css('top', top + 'px');
        var selectedParents = $('.parentDetail' + id).val();
        $('.chooseParentList').val(selectedParents);
        return false;
    });
    $('#chooseParent .submitForm').live('click', function () {
        var id = $('#chooseParent .id').val();
        var selectedParents = $('#chooseParentList').val(selectedParents);
        $('.parentDetail' + id).val(selectedParents);
        $('.newLink').css('top', '-1000px');
        var items = "";
        $.each($('#chooseParent form input'), function () {
            if ($(this).is(':checked')) {
                items = items + $(this).val() + ',';
            }
        });
        var siteUrl = $('.siteRoot').val() + "pages/saveAtribut.php?id=" + id;
        var data = '';
        var parent = items;
        var sr = '';
        var en = '';
        console.log(items);


        if ($('input.attributeParent').val() === '') {
            $('.chooseParentList').val(items);
            console.log('this is atr');
            parent = $('.chooseParentList').val();
            sr = $('.attributeSrDetail' + id).val().split(' ');
            en = $('.attributeEnDetail' + id).val().split(' ');
            data = siteUrl + '&parent=' + items + '&detailSr=' + sr + '&detailEn=' + en;
            $('.resDetail' + id).load(data, function () {
                parent = $('.resDetail' + id).text();
                if (parent !== 'OK') {
                    $('.resDetail' + id).text('');
                    $('#attributeParent' + id).text(parent);
                }
            });
        } else {
            id = $('input.attributeParent').val();
            $('.parentGroupSettings' + id).val(items);
            sr = $('.parentSettingsSr' + id).val().split(' ');
            en = $('.parentSettingsEn' + id).val().split(' ');

            siteUrl = $('.siteRoot').val() + "pages/saveAtributGroup.php?id=" + id;
            data = siteUrl + '&parent=' + items + '&detailSr=' + sr + '&detailEn=' + en;
            console.log(data);
            $('.resGroupDetail' + id).load(data, function () {
                parent = $('.resGroupDetail' + id).text();
                if (parent !== 'OK') {
                    $('.resGroupDetail' + id).text('');
                    $('.attributeParrentSettings' + id).text(parent);
                }
            });
        }
        return false;
    });
    $('.attributeSettings').live('click', function () {
        var id = $(this).attr('id').replace('attributeSettings', '');

        var parent = $('.parentSettings' + id).val();
        var sr = $('.parentSettingsSr' + id).val().split(' ');
        var en = $('.parentSettingsEn' + id).val().split(' ');
        var data = $(this).attr('href') + '&parent=' + parent + '&detailSr=' + sr + '&detailEn=' + en;
        $('.resDetail' + id).load(data, function () {
            parent = $('.resDetail' + id).text();
            if (parent !== 'OK') {
                $('.resDetail' + id).text('');
                $('.attributeParrentSettings' + id).text(parent);
            }
        });
        return false;
    });
    $('.saveCharacteristics').live('click', function () {
        var id = $(this).attr('id').replace('char', '');
        var data = $(this).attr('href');
        var sr = '&characteristics_name_sr=' + $('.charSr' + id).val().split(' ');
        var en = '&characteristics_name_en=' + $('.charEn' + id).val().split(' ');
        var ordering = '&ordering=' + $('.ordering' + id).val();
        var category = '&category_id=' + $('.charCategory' + id).val();
        data = data + sr + en + category + ordering;
        console.log(data);
        $('.charRes' + id).load(data);
        return false;
    });
    $('.deleteCharacteristics').live('click', function () {
        if (window.confirm('Da li ste sigurni?')) {
            var url = $(this).attr('href');
            window.location = url;
            window.open(url, '_self');
        }
        return false;
    });
    $('.multipleCharDel').live('change', function () {
        var selectedCh = $('.multipleDeleteChars').val();
        var id = $(this).val();
        if ($(this).is(':checked')) {
            selectedCh = selectedCh + id + ',';
        } else {
            selectedCh = selectedCh.replace(id + ',', '');
        }
        $('.multipleDeleteChars').val(selectedCh);
        if (selectedCh !== '') {
            $('.multipleDeleteChars').show();
        } else {
            $('.multipleDeleteChars').hide();
        }
        return false;
    });
    $('.multipleDeleteChars').live('click', function () {
        var data = $('.multipleDeleteChars').val();
        if (data !== '') {
            //lert(data);

            // var url = $(this).attr('href');

            var url = $(this).attr('href') + data;

            window.location = url;
            window.open(url, '_self');

        } else {
            window.alert('Niste izabrali nijednu karakteristiku');
        }
        return false;
    });
    $('.deleteAttributeGroup').live('click', function () {
        if (window.confirm('Da li ste sigurni?'), function () {
            window.location = $(this).attr('href');
            window.open($(this).attr('href'), '_self');
        });
        return false;
    });
    $('.saveCharacteristicsCat').live('click', function () {
        if (window.confirm('Da li ste sigurni?'), function () {
            window.location = $(this).attr('href');
            window.open($(this).attr('href'), '_self');
        });
        return false;
    })

    $('.saveCharacteristicsCats').live('click', function () {
        var id = $(this).attr("id").replace('charCat', '');
        var data = '&';
        data = data + 'category_name_sr=' + $('.charCatSr' + id).val().split(' ');
        data = data + '&category_name_en=' + $('.charCatEn' + id).val().split(' ');

        var url = $(this).attr('href') + data;
        $('.charCatRes' + id).load(url);
        return false;
    });
    $('.attrGroupsDel').live('change', function () {

    });
    $('.selectAllAtributes').live('click', function () {
        var selectedItems = $('.selectedAttributelist').val();
        if (selectedItems === '') {
            $('.attributList input[type=checkbox]').each(function () {
                selectedItems = selectedItems + $(this).val() + ',';
                $(this).attr('checked', true);
            });
            $('.selectedAttributelist').val(selectedItems);
        } else {
            $('.selectedAttributelist').val('');
            $('.attributList input[type=checkbox]').each(function () {
                $(this).attr('checked', false);
            });
        }
        return false;
    });
    $('.attributes').live('change', function () {
        var selecteditems = $('.selectedAttributelist').val();
        var id = $(this).val();
//        console.log(id);
        selecteditems = selecteditems.replace(id + ',', '') + id + ',';
        $('.selectedAttributelist').val(selecteditems);
    });
    $('.selectAllCatAtributes').live('change', function () {
        var selecteditems = $('.multipleDeleteAttGroups').val();
        var id = $(this).val();
//        console.log(id);
        selecteditems = selecteditems.replace(id + ',', '') + id + ',';
        $('.multipleDeleteAttGroups').val(selecteditems);
        return false;
    });
    $('.multipleDeleteAttr').live('click', function () {
        if (window.confirm('Da li ste sigurni?')) {
            var url = $(this).attr('href') + $('.selectedAttributelist').val();
            window.location = url;
            window.open(url, '_self');
        }
        ;
        return false;
    });
    $('.multipleDeleteAttrGroups').live('click', function () {
        if (window.confirm('Da li ste sigurni?')) {
            var href = $(this).attr('href') + $('.multipleDeleteAttGroups').val();
            window.location = href;
            window.open(href, '_self');
        }
        ;
        return false;
    });
    $('a.selectAllCatAtributes').live('click', function () {
        var selected = $('input.multipleDeleteAttGroups').val();
        if (selected === '') {
            $('.selectAllCatAtributes').each(function () {
                selected = selected + $(this).val() + ',';
                $(this).attr('checked', true);
            });
            $('input.multipleDeleteAttGroups').val(selected);
        } else {
            $('.selectAllCatAtributes').each(function () {
                $(this).attr('checked', false);
            });
            $('input.multipleDeleteAttGroups').val('');
        }
        return false;
    });
    $('.attributeGroupParent').live('click', function () {
        var id = $(this).attr('href').replace('#', '');
        $('.attributeParent').val(id);
        var offset = $(this).offset();
        var top = offset.top - 100;
        $('#chooseParent').css('top', top + 'px');
        return false;
    });
    $('.characterParent').live('click', function () {
        var id = $(this).attr('href').replace('#', '');
        var offset = $(this).offset();
        var top = offset.top - 100;
        $('.charID').val(id);
        $('.chooseCharCategory').css('top', top + 'px');
        return false;
    });
    $('.saveChar').live('click', function () {
        var selectedID = $('.chooseCharCategory input:checked').val();
        var id = $('.charID').val();
        var name = $('.chooseCharCategory input:checked').attr('name');
        $('.charParent' + id).text(name);
        $('.charCategory' + id).val(selectedID);
        $('.newLink').css('top', '-10000px');
        return false;
    });


});

