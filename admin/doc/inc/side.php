<!-- Left navigation -->
<div class="leftNav">
    <a href="<?=SITE_ROOT?>" style="width:200px;display:block; text-align: center; font-size:1.2em; padding-bottom:1em"
       target="_blank">Original website</a>
    <ul id="menu">
        <li class="dash"><a href="<?=ADMIN?>dashboard" title="" <?php echo activeMenu('dashboard')?>>
            <span>Dashboard</span></a></li>

        <li class="tables"><a href="<?=ADMIN?>proizvodi" title=""><span>Proizvodi</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>iplus-proizvodi" title=""><span>iPlus Proizvodi</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>driveri" title=""><span>Driveri i upustva</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>cenovnik" title=""><span>Cenovnik</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>kategorije" title=""><span>Kategorije</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>proizvodjaci" title=""><span>Proizvođači</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>detaljna-pretraga" title=""><span>Detaljna pretraga</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>promocije"><span>Promocije</span></a></li>
        <li class="tables" style="margin-top:1em"><a href="<?=ADMIN?>klijenti" title=""><span>Klijenti</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>loyalty" title=""><span>Loyalty</span></a></li>
        <li class="tables"><a href="<?=ADMIN?>tiketi"><span>Tiketi</span></a></li>
        <!--		<li class="tables"><a href="-->
        <?//=ADMIN?><!--reference" title=""><span>Reference</span></a></li>-->

        <?php /*
<!--        <li class="login"><a href="ui_elements.html" title=""><span>Interface elements</span></a></li>-->
<!--        <li class="typo"><a href="typo.html" title=""><span>Typography</span></a></li>-->
<!--        <li class="tables"><a href="tables.html" title=""><span>Tables</span></a></li>-->
<!--        <li class="cal"><a href="calendar.html" title=""><span>Calendar</span></a></li>-->
<!--        <li class="gallery"><a href="gallery.html" title=""><span>Gallery</span></a></li>-->
<!--        <li class="widgets"><a href="widgets.html" title=""><span>Widgets</span></a></li>-->
<!--        <li class="files"><a href="file_manager.html" title=""><span>File manager</span></a></li>-->
<!--        <li class="errors"><a href="#" title="" class="exp"><span>Error pages</span><span class="numberLeft">6</span></a>-->
<!--        	<ul class="sub">-->
<!--                <li><a href="403.html" title="">403 page</a></li>-->
<!--                <li><a href="404.html" title="">404 page</a></li>-->
<!--                <li><a href="405.html" title="">405 page</a></li>-->
<!--                <li><a href="500.html" title="">500 page</a></li>-->
<!--                <li><a href="503.html" title="">503 page</a></li>-->
<!--                <li class="last"><a href="offline.html" title="">Website is offline</a></li>-->
<!--            </ul>-->
<!--        </li>-->
<!--        <li class="pic"><a href="icons.html" title=""><span>Buttons and icons</span></a></li>-->
<!--        <li class="contacts"><a href="contacts.html" title=""><span>Contact list</span></a></li>-->
        */ ?>
    </ul>
</div>
