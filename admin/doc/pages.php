<?php
if (!isset($_GET['page'])) {
    $page = 'dashboard';
} else {
    $page = $_GET['page'];
    if ($page == '') {
        $page = 'dashboard';
    }
}
$tab = 1;
if (isset($_SESSION['tab'])) {
    $tab = $_SESSION['tab'];
    unset($_SESSION['tab']);
}
$user = Administrator::find_by_id($session->user_id);

require_once(ADMIN_ROOT . "doc/inc/header.php");

switch ($page) {
    case $page:
        require_once ADMIN_ROOT . 'pages/' . $page . ".php";
        break;
}
require_once(ADMIN_ROOT . "doc/inc/footer.php");
?>