/**
 * Created with JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/12/12
 * Time: 7:32 PM
 * To change this template use File | Settings | File Templates.
 */
    // JavaScript Document
featuredcontentslider.init({
    id: "slider",  //id of main slider DIV
    contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]
    toc: "#increment",  //Valid values: "#increment", "markup", ["label1", "label2", etc]
    nextprev: ["Previous", "Next"],  //labels for "prev" and "next" links. Set to "" to hide.
    revealtype: "click", //Behavior of pagination links to reveal the slides: "click" or "mouseover"
    enablefade: [true, 0.2],  //[true/false, fadedegree]
    autorotate: [true, 3000],  //[true/false, pausetime]
    scroll:1,
    onChange: function(previndex, curindex){  //event handler fired whenever script changes slide
        //previndex holds index of last slide viewed b4 current (1=1st slide, 2nd=2nd etc)
        //curindex holds index of currently shown slide (1=1st slide, 2nd=2nd etc)
    }
})