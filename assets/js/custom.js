/**
 * Created with JetBrains PhpStorm.
 * User: predragjevtic
 * Date: 8/27/12
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */
$(function(){
    $('.product .submenu a').live('click', function(){
        $('.product .submenu a').removeClass('active');
        var id = $(this).attr('class');
        $('.tabs .tab').hide();
        $('.tabs #'+id).fadeIn(200);
        $(this).addClass('active');
        return false;
    });
    $('.galleryListing img').live('click',function(){
        var id =  $(this).attr('id');
        $('.galleryListing img').removeClass('active');
        $('.mainImg img').hide();
        $('.mainImg img.'+id).fadeIn(300);
        $(this).addClass('active');
    });
    $("#sendMessage").live('click',function(){
        var inputs = $('form.contact input');
        var error ;
        $.each(inputs, function(index, obj){
            if(obj.name ==='name' || obj.name==='email'){
                if(obj.value===''){
                    switch(obj.name){
                        case 'name':
                             $('.reqname').show();
                             error += obj.name;
                        break;
                        case 'email':
                            $('.reqemail').show();
                            error+=obj.name;
                        break;
                    }
                }else{
                    switch(obj.name){
                        case 'name':
                            $('.reqname').hide();
                            error-=obj.name;
                            break;
                        case 'email':
                            $('.reqemail').hide();
                            error-=obj.name;
                            break;
                    }
                }
            }
        });
        if($("form.contact textarea").val()==''){
            $('.reqmessage').show();
            error+='message';
        }else{
            $('.reqmessage').hide();
            error-='message';
        }
        //alert($("form.contact textarea").val());
        if($("form textarea").val()!='' && $('.contact .name').val()!='' && $('.contact .email').val()!=''){
            $('form.contact').attr('action', 'http://https://88.198.21.170/~origb2b/sendMessage');
            $('form.contact').submit();
        }
        return false;
    });

    $('.characteristicPage').live('click', function () {
        $('.characteristics').toggle();
        return false;
    });
    $('.newLink .close').live('click', function(){
        $('.newLink').hide();

        return false;
    });
    $('.toBasket').live("click", function(){
        var siteUrl = $(this).attr('href');
        var productID = $(this).attr('id');

        $('.basketInfo').load(siteUrl +'framework/models/addProductToBasket.php?productID='+productID);
        $('.addProductInfo').html('<p>Proizvod je dodat u korpu.<p>');
        return false;
    });
    $('.cancelOrder').live('click', function(){
        if(confirm('Da li ste sigurni?')) {
            var siteUrl = $(this).attr('href');
            $('.basketInfo').load(siteUrl +'framework/models/addProductToBasket.php?cancelOrder');
        }
        return false;
    });
    $('#selectManufacturer').change(function(){
        var group = $('#selectGroup').val();
        var manufacturer = $(this).val();
        var siteUrl = $('.siteURL').val();
        var loadURL = siteUrl+"framework/models/selectedProducts.php?group="+group+"&manufacturer="+manufacturer;
        $('#selectedProduct').load(loadURL);
        return false;
    });
    $('#selectGroup').change(function(){
        var group = $(this).val();
        var manufacturer = $('#selectManufacturer').val();
        var siteUrl = $('.siteURL').val();
        var loadURL = siteUrl+"framework/models/selectedProducts.php?group="+group+"&manufacturer="+manufacturer;
        $('#selectedProduct').load(loadURL);
        return false;
    });
    $('.compare').live('click', function(){
        $('#compareContent').show();
        return false;
    });
    $('#comparePrBttn').live('click', function(){
        var rightP = $('#selectedProduct').val();
        var linkHref = $(this).attr('href');//+rightP;

        $(this).attr('href', linkHref+rightP);
    });

    $('#advancedSearch a').live('click', function(){
        var id = $(this).attr('id');
        $('.'+id).slideToggle(100);
        return false;
    });
    $('.removeItem').live('click', function(){
        var id = $(this).attr('rel');
        if(confirm('Da li ste sigurni?')) {
            $('#cartItem'+id).remove();
            var siteUrl = $(this).attr('href');
//            $('.basketInfo').load(siteUrl +'framework/models/addProductToBasket.php?removeItem=yes&id='+id);
        }
        return false;
    });
    $('.closeCompare').live('click', function(){
        $('#compareContent').hide();
    });
    $('.cancelBttn').live('click', function(){
        if(confirm('Da li ste sigurni?')){
            $('form').submit();
        }else{
        return false;
        }
    });
    //searchResuls
    setInterval("getList()", 10000) // Get users-online every 10 seconds

    function getList() {
        var siteURL = $('.siteURL').val();
        $.post(siteURL+"getList.php", function(list) {
            $("listBox").html(list);
        });
    }
    $('.confirmDelivery').live('click',function(){
       if(confirm('Da li ste sigurni?')){
          var url = $(this).attr('href');
           window.open(url, '_self');
       }else{
           return false;
       }
    });
});